package futurepack.common.block;

//import futurepack.common.wire.BlockWire;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;

import futurepack.api.Constants;
import futurepack.common.FPConfig;
import futurepack.common.FPLog;
import futurepack.common.FPMain;
import futurepack.common.block.deco.BlockDekoMetaGlass;
import futurepack.common.block.deco.BlockSlabMultiTexture;
import futurepack.common.block.deco.BlockThruster;
import futurepack.common.block.inventory.BlockAdvancedBoardcomputer;
import futurepack.common.block.inventory.BlockBoardComputer;
import futurepack.common.block.inventory.BlockDroneStation;
import futurepack.common.block.inventory.BlockFuelCell;
import futurepack.common.block.logistic.BlockLaserTransmitter;
import futurepack.common.block.logistic.monorail.BlockMonorailBasic;
import futurepack.common.block.logistic.monorail.BlockMonorailBooster;
import futurepack.common.block.logistic.monorail.BlockMonorailCharger;
import futurepack.common.block.logistic.monorail.BlockMonorailDetector;
import futurepack.common.block.logistic.monorail.BlockMonorailOneway;
import futurepack.common.block.logistic.monorail.BlockMonorailStation;
import futurepack.common.block.logistic.monorail.BlockMonorailWaypoint;
import futurepack.common.block.machines.BlockFilterAssembler;
import futurepack.common.block.misc.BlockClaime;
import futurepack.common.block.misc.BlockFish;
import futurepack.common.block.misc.BlockFpButton;
import futurepack.common.block.misc.BlockModularDoor;
import futurepack.common.block.misc.BlockRsTimer;
import futurepack.common.block.modification.BlockExternalCore;
import futurepack.common.block.modification.machines.BlockFluidPump;
import futurepack.common.block.multiblock.BlockDeepCoreMiner;
import futurepack.common.block.multiblock.BlockFTLMulti;
import futurepack.common.block.plants.BlockFpLeaves;
import futurepack.common.block.terrain.BlockFpSapling;
import futurepack.common.block.terrain.BlockMenelausMushroms;
import futurepack.common.block.terrain.BlockSpaceMushroom;
import futurepack.common.item.ItemMetaMultiTex;
import futurepack.common.item.ItemMetaSlap;
import futurepack.depend.api.interfaces.IItemMetaSubtypes;
import net.minecraft.block.Block;
import net.minecraft.block.DoorBlock;
import net.minecraft.block.LeavesBlock;
import net.minecraft.block.SlabBlock;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.statemap.IStateMapper;
import net.minecraft.client.renderer.block.statemap.StateMap;
import net.minecraft.client.renderer.block.statemap.StateMapperBase;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.item.Item;
import net.minecraft.item.AirItem;
import net.minecraft.state.IProperty;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.registries.IForgeRegistry;

public class FPBlocks 
{
//	public static List<Item> itemBlocks = new ArrayList<Item>();
//	
//	public static final HashMap<Integer,PropertyInteger> META = new HashMap<Integer,PropertyInteger>(16);
//	public static PropertyInteger META(int maxmeta)
//	{
//		if(META.get(maxmeta)==null)
//		{
//			META.put(maxmeta, PropertyInteger.create("metadata", 0, maxmeta));
//		}
//		return META.get(maxmeta);
//	}
	
/**	public static final Block colorIron = new BlockDekoMeta(Material.IRON).setHardness(5.0F).setResistance(10.0F).setTranslationKey("colorIron");///*.setBlockTextureName("colorIron");
	public static final Block colorLuftung = new BlockDekoMeta(Material.IRON).setHardness(5.0F).setResistance(10.0F).setTranslationKey("colorLuftung");///*.setBlockTextureName("luftung");
	public static final Block colorGitter = new BlockDekoMetaGlass(Material.IRON).setHardness(5.0F).setResistance(10.0F).setTranslationKey("colorGitter");///*.setBlockTextureName("gitter");
	public static final Block colorGlas = new BlockDekoMetaGlass(Material.IRON).setHardness(5.0F).setResistance(10.0F).setTranslationKey("colorGlas");///*.setBlockTextureName("glas");
	
	public static final Block colorIronStair0 = new BlockTreppe(colorIron, 0).setTranslationKey("colorIronTreppe");
	public static final Block colorIronStair1 = new BlockTreppe(colorIron, 1).setTranslationKey("colorIronTreppe");
	public static final Block colorIronStair2 = new BlockTreppe(colorIron, 2).setTranslationKey("colorIronTreppe");
	public static final Block colorIronStair3 = new BlockTreppe(colorIron, 3).setTranslationKey("colorIronTreppe");
	public static final Block colorIronStair4 = new BlockTreppe(colorIron, 4).setTranslationKey("colorIronTreppe");
	public static final Block colorIronStair5 = new BlockTreppe(colorIron, 5).setTranslationKey("colorIronTreppe");
	public static final Block colorIronStair6 = new BlockTreppe(colorIron, 6).setTranslationKey("colorIronTreppe");
	public static final Block colorIronStair7 = new BlockTreppe(colorIron, 7).setTranslationKey("colorIronTreppe");
	public static final Block colorIronStair8 = new BlockTreppe(colorIron, 8).setTranslationKey("colorIronTreppe");
	public static final Block colorIronStair9 = new BlockTreppe(colorIron, 9).setTranslationKey("colorIronTreppe");
	public static final Block colorIronStair10 = new BlockTreppe(colorIron, 10).setTranslationKey("colorIronTreppe");
	public static final Block colorIronStair11 = new BlockTreppe(colorIron, 11).setTranslationKey("colorIronTreppe");
	public static final Block colorIronStair12 = new BlockTreppe(colorIron, 12).setTranslationKey("colorIronTreppe");
	public static final Block colorIronStair13 = new BlockTreppe(colorIron, 13).setTranslationKey("colorIronTreppe");
	public static final Block colorIronStair14 = new BlockTreppe(colorIron, 14).setTranslationKey("colorIronTreppe");
	public static final Block colorIronStair15 = new BlockTreppe(colorIron, 15).setTranslationKey("colorIronTreppe");
	public static final Block[] colorIronStairs = new Block[]{colorIronStair0,colorIronStair1,colorIronStair2,colorIronStair3,colorIronStair4,colorIronStair5,colorIronStair6,colorIronStair7,colorIronStair8,colorIronStair9,colorIronStair10,colorIronStair11,colorIronStair12,colorIronStair13,colorIronStair14,colorIronStair15};
	
	
//	public static final Block eisenleiter = new BlockEisenleiter().setHardness(5.0F).setResistance(10.0F).setTranslationKey("eisenleiter");
	
	public static final Block antenne = new BlockAntenne(Material.IRON).setHardness(5.0F).setResistance(10.0F).setTranslationKey("antenne");
	
	//public static final Block funkcomputer_weiss_on = new BlockFunkComputer(true).setHardness(5.0F).setResistance(10.0F).setSoundType(SoundType.METAL).setTranslationKey("funkcomputer");
	//public static final Block funkcomputer_weiss_off = new BlockFunkComputer(false).setHardness(5.0F).setResistance(10.0F).setSoundType(SoundType.METAL).setTranslationKey("funkcomputer");
	//12 IDs frei
	
    public static final Block colorGitterStair0 = new BlockGitterTreppe(colorGitter, 0).setTranslationKey("colorGitterTreppe");
	public static final Block colorGitterStair1 = new BlockGitterTreppe(colorGitter, 1).setTranslationKey("colorGitterTreppe");
	public static final Block colorGitterStair2 = new BlockGitterTreppe(colorGitter, 2).setTranslationKey("colorGitterTreppe");
	public static final Block colorGitterStair3 = new BlockGitterTreppe(colorGitter, 3).setTranslationKey("colorGitterTreppe");
	public static final Block colorGitterStair4 = new BlockGitterTreppe(colorGitter, 4).setTranslationKey("colorGitterTreppe");
	public static final Block colorGitterStair5 = new BlockGitterTreppe(colorGitter, 5).setTranslationKey("colorGitterTreppe");
	public static final Block colorGitterStair6 = new BlockGitterTreppe(colorGitter, 6).setTranslationKey("colorGitterTreppe");
	public static final Block colorGitterStair7 = new BlockGitterTreppe(colorGitter, 7).setTranslationKey("colorGitterTreppe");
	public static final Block colorGitterStair8 = new BlockGitterTreppe(colorGitter, 8).setTranslationKey("colorGitterTreppe");
	public static final Block colorGitterStair9 = new BlockGitterTreppe(colorGitter, 9).setTranslationKey("colorGitterTreppe");
	public static final Block colorGitterStair10 = new BlockGitterTreppe(colorGitter, 10).setTranslationKey("colorGitterTreppe");
	public static final Block colorGitterStair11 = new BlockGitterTreppe(colorGitter, 11).setTranslationKey("colorGitterTreppe");
	public static final Block colorGitterStair12 = new BlockGitterTreppe(colorGitter, 12).setTranslationKey("colorGitterTreppe");
	public static final Block colorGitterStair13 = new BlockGitterTreppe(colorGitter, 13).setTranslationKey("colorGitterTreppe");
	public static final Block colorGitterStair14 = new BlockGitterTreppe(colorGitter, 14).setTranslationKey("colorGitterTreppe");
	public static final Block colorGitterStair15 = new BlockGitterTreppe(colorGitter, 15).setTranslationKey("colorGitterTreppe");
	public static final Block[] colorGitterStairs = new Block[]{colorGitterStair0,colorGitterStair1,colorGitterStair2,colorGitterStair3,colorGitterStair4,colorGitterStair5,colorGitterStair6,colorGitterStair7,colorGitterStair8,colorGitterStair9,colorGitterStair10,colorGitterStair11,colorGitterStair12,colorGitterStair13,colorGitterStair14,colorGitterStair15};
	
//	private static Block[] blocks = new Block[] {colorIron, colorIron, colorIron, colorIron, colorIron, colorIron, colorIron, colorIron};
//	
//	public static final BlockSlab colorIronStepHalf_0 = (BlockSlab) new BlockStepMultiTexture(false, blocks, false, false).setTranslationKey("colorIronStep");
//	public static final BlockSlab colorIronStepFull_0 = (BlockSlab) new BlockStepMultiTexture(true, blocks, false, false, true).setTranslationKey("colorIronStep");
//	
//	public static final BlockSlab colorIronStepHalf_1 = (BlockSlab) new BlockStepMultiTexture(false, blocks, true, false).setTranslationKey("colorIronStep");
//	public static final BlockSlab colorIronStepFull_1 = (BlockSlab) new BlockStepMultiTexture(true, blocks, true, false, true).setTranslationKey("colorIronStep");
//	
//	private static Block[] blocks2 = new Block[] {colorGitter, colorGitter, colorGitter, colorGitter, colorGitter, colorGitter, colorGitter, colorGitter};
//	
//	public static final BlockSlab colorGitterStepHalf_0 = (BlockSlab) new BlockStepMultiTexture(false, blocks2, false, false).setTranslationKey("colorGitterStep");
//	public static final BlockSlab colorGitterStepFull_0 = (BlockSlab) new BlockStepMultiTexture(true, blocks2, false, false, true).setTranslationKey("colorGitterStep");
//	
//	public static final BlockSlab colorGitterStepHalf_1 = (BlockSlab) new BlockStepMultiTexture(false, blocks2, true, false).setTranslationKey("colorGitterStep");
//	public static final BlockSlab colorGitterStepFull_1 = (BlockSlab) new BlockStepMultiTexture(true, blocks2, true, false, true).setTranslationKey("colorGitterStep");
//	
//	private static Block[] blocks3 = new Block[] {colorLuftung, colorLuftung, colorLuftung, colorLuftung, colorLuftung, colorLuftung, colorLuftung, colorLuftung};
//	
//	public static final BlockSlab colorLuftungStepHalf_0 = (BlockSlab) new BlockStepLuftung(false, blocks3, blocks, false, false).setTranslationKey("colorLuftungStep");
//	public static final BlockSlab colorLuftungStepFull_0 = (BlockSlab) new BlockStepLuftung(true, blocks3, blocks, false, true).setTranslationKey("colorLuftungStep");
//	
//	public static final BlockSlab colorLuftungStepHalf_1 = (BlockSlab) new BlockStepLuftung(false, blocks3, blocks, true, false).setTranslationKey("colorLuftungStep");
//	public static final BlockSlab colorLuftungStepFull_1 = (BlockSlab) new BlockStepLuftung(true, blocks3, blocks, true, true).setTranslationKey("colorLuftungStep");
//	
	public static final Block erzBlocke = new BlockErzeBlocke(Material.IRON).setTranslationKey("erzBlocke")/*.setBlockTextureName("iron_block").setHardness(3.0F).setResistance(5.0F);
	public static final Block erzBlocke2 = new BlockErzeBlocke2(Material.ROCK).setTranslationKey("erzBlocke").setHardness(3.0F).setResistance(5.0F);
	
	public static final Block metalSlap0 =  new BlockMetalSlapHalf(erzBlocke, 4).setHardness(3.0F).setResistance(5.0F).setTranslationKey("metalSlap");
	public static final Block metalSlap1 =  new BlockMetalSlapDouble(erzBlocke, 4).setHardness(3.0F).setResistance(5.0F).setTranslationKey("metalSlap").setCreativeTab(null);
	
	
	public static final BlockSlabMultiTextureBottom colorIronSlabBottom = (BlockSlabMultiTextureBottom) new BlockSlabMultiTextureBottom(Material.IRON, metalSlap0.getDefaultState().withProperty(FPBlocks.META(7), 0).withProperty(BlockSlab.HALF, EnumBlockHalf.BOTTOM)).setSoundType(SoundType.METAL).setHardness(5.0F).setResistance(10.0F).setTranslationKey("colorIronStep");
	public static final BlockSlabMultiTextureTop colorIronSlabTop = (BlockSlabMultiTextureTop) new BlockSlabMultiTextureTop(Material.IRON, colorIronSlabBottom,  metalSlap0.getDefaultState().withProperty(FPBlocks.META(7), 0).withProperty(BlockSlab.HALF, EnumBlockHalf.TOP)).setSoundType(SoundType.METAL).setHardness(5.0F).setResistance(10.0F).setTranslationKey("colorIronStep");
	
	public static final BlockSlabMultiTextureBottom colorGitterSlabBottom = (BlockSlabMultiTextureBottom) new BlockSlabMultiTextureBottom(Material.IRON,  metalSlap0.getDefaultState().withProperty(FPBlocks.META(7), 1).withProperty(BlockSlab.HALF, EnumBlockHalf.BOTTOM)).setSoundType(SoundType.METAL).setHardness(5.0F).setResistance(10.0F).setTranslationKey("colorGitterStep");
	public static final BlockSlabMultiTextureTop colorGitterSlabTop = (BlockSlabMultiTextureTop) new BlockSlabMultiTextureTop(Material.IRON,colorGitterSlabBottom,  metalSlap0.getDefaultState().withProperty(FPBlocks.META(7), 1).withProperty(BlockSlab.HALF, EnumBlockHalf.TOP)).setSoundType(SoundType.METAL).setHardness(5.0F).setResistance(10.0F).setTranslationKey("colorGitterStep");
	
	public static final BlockSlabMultiTextureBottom colorLuftungSlabBottom = (BlockSlabMultiTextureBottom) new BlockSlabMultiTextureBottom(Material.IRON,  metalSlap0.getDefaultState().withProperty(FPBlocks.META(7), 2).withProperty(BlockSlab.HALF, EnumBlockHalf.BOTTOM)).setSoundType(SoundType.METAL).setHardness(5.0F).setResistance(10.0F).setTranslationKey("colorLuftungStep");
	public static final BlockSlabMultiTextureTop colorLuftungSlabTop = (BlockSlabMultiTextureTop) new BlockSlabMultiTextureTop(Material.IRON,colorLuftungSlabBottom,  metalSlap0.getDefaultState().withProperty(FPBlocks.META(7), 2).withProperty(BlockSlab.HALF, EnumBlockHalf.TOP)).setSoundType(SoundType.METAL).setHardness(5.0F).setResistance(10.0F).setTranslationKey("colorLuftungStep");

	public static final Block colorIronFence = new BlockColorFence(Material.IRON).setTranslationKey("colorFence").setHardness(3.0F).setResistance(5.0F);
	public static final Block colorGitterPane = new BlockColorPane("gitter", Material.IRON).setTranslationKey("colorPane").setHardness(3.0F).setResistance(5.0F);
	
	public static final Block colorGate0 = new BlockColorGate().setTranslationKey("color_gate_0").setHardness(3.0F).setResistance(5.0F);
	public static final Block colorGate1 = new BlockColorGate().setTranslationKey("color_gate_1").setHardness(3.0F).setResistance(5.0F);
	public static final Block colorGate2 = new BlockColorGate().setTranslationKey("color_gate_2").setHardness(3.0F).setResistance(5.0F);
	public static final Block colorGate3 = new BlockColorGate().setTranslationKey("color_gate_3").setHardness(3.0F).setResistance(5.0F);
	public static final Block colorGate4 = new BlockColorGate().setTranslationKey("color_gate_4").setHardness(3.0F).setResistance(5.0F);
	public static final Block colorGate5 = new BlockColorGate().setTranslationKey("color_gate_5").setHardness(3.0F).setResistance(5.0F);
	public static final Block colorGate6 = new BlockColorGate().setTranslationKey("color_gate_6").setHardness(3.0F).setResistance(5.0F);
	public static final Block colorGate7 = new BlockColorGate().setTranslationKey("color_gate_7").setHardness(3.0F).setResistance(5.0F);
	public static final Block colorGate8 = new BlockColorGate().setTranslationKey("color_gate_8").setHardness(3.0F).setResistance(5.0F);
	public static final Block colorGate9 = new BlockColorGate().setTranslationKey("color_gate_9").setHardness(3.0F).setResistance(5.0F);
	public static final Block colorGate10 = new BlockColorGate().setTranslationKey("color_gate_10").setHardness(3.0F).setResistance(5.0F);
	public static final Block colorGate11 = new BlockColorGate().setTranslationKey("color_gate_11").setHardness(3.0F).setResistance(5.0F);
	public static final Block colorGate12 = new BlockColorGate().setTranslationKey("color_gate_12").setHardness(3.0F).setResistance(5.0F);
	public static final Block colorGate13 = new BlockColorGate().setTranslationKey("color_gate_13").setHardness(3.0F).setResistance(5.0F);
	public static final Block colorGate14 = new BlockColorGate().setTranslationKey("color_gate_14").setHardness(3.0F).setResistance(5.0F);
	public static final Block colorGate15 = new BlockColorGate().setTranslationKey("color_gate_15").setHardness(3.0F).setResistance(5.0F);
	public static final Block[] colorGates = new Block[]{colorGate0, colorGate1, colorGate2, colorGate3, colorGate4, colorGate5, colorGate6, colorGate7, colorGate8, colorGate9, colorGate10, colorGate11, colorGate12, colorGate13, colorGate14, colorGate15}; 
	
	
	public static final BlockColorNeonLamp colorNeonLamp_off =  (BlockColorNeonLamp) new BlockColorNeonLamp(Material.IRON).setTranslationKey("colorNeonLamp").setBlockTextureName("neon").setHardness(3.0F).setResistance(5.0F);
	public static final BlockColorNeonLamp colorNeonLamp_on = (BlockColorNeonLamp) new BlockColorNeonLamp(Material.IRON).setTranslationKey("colorNeonLamp").setBlockTextureName("neon").setLightLevel(1.0F).setHardness(3.0F).setResistance(5.0F);
	public static final BlockColorPlasmaLamp colorPlasmaLamp_off = (BlockColorPlasmaLamp) new BlockColorPlasmaLamp(Material.IRON).setTranslationKey("colorPlasmaLamp").setBlockTextureName("plasma").setHardness(3.0F).setResistance(5.0F);
	public static final BlockColorPlasmaLamp colorPlasmaLamp_on = (BlockColorPlasmaLamp) new BlockColorPlasmaLamp(Material.IRON).setTranslationKey("colorPlasmaLamp").setBlockTextureName("plasma").setLightLevel(0.8F).setHardness(3.0F).setResistance(5.0F);
	
	//public static final Block spaceshipmaker = new BlockSpaceshipCreater(Material.IRON).setTranslationKey("spaceship")/*.setBlockTextureName("futurepack:SchauBoxSide");	
	//public static final Block microbox = new BlockMicroBox(Material.IRON).setTranslationKey("microbox")/*.setBlockTextureName("futurepack:colorIron_0");
	public static final Block spacedoor = new BlockSpaceDoor(Material.IRON).setTranslationKey("spacedoor")/*.setBlockTextureName("futurepack:colorIron_0").setHardness(3.0F).setResistance(5.0F);
	public static final Block ionCollector = new BlockIonCollector(Material.IRON).setHardness(5.0F).setResistance(10.0F).setTranslationKey("ionCollector")/*.setBlockTextureName("ProtonenColektor");
	public static final Block wire = new BlockWire(Material.IRON).setHardness(5.0F).setResistance(10.0F).setTranslationKey("wire")/*.setBlockTextureName("futurepack:colorIron_0");
	public static final Block wandrobe = new BlockWandrobe(Material.IRON).setTranslationKey("wandrobe")/*.setBlockTextureName("futurepack:Schrank").setHardness(3.0F).setResistance(5.0F);
	public static final Block erse = new BlockErse().setTranslationKey("erse")/*.setBlockTextureName("futurepack:Erse_Pflanze");
	public static final Block mendelBerry = new BlockMendelBerry().setTranslationKey("mendel_berry");
	public static final Block topinambur = new BlockTopinambur().setTranslationKey("topinambur");*/
	
//	public static final Block monorail = new BlockMonoRailBasic().setHardness(3.0F).setResistance(5.0F).setTranslationKey("monorail")/*.setBlockTextureName("futurepack:monorail")*/;
//	public static final Block monorailStation = new BlockMonorailStation().setHardness(3.0F).setResistance(5.0F).setTranslationKey("monorail_station")/*.setBlockTextureName("futurepack:monorail")*/;
//	public static final Block monorail_waypoint = new BlockMonorailWaypoint().setHardness(3.0F).setResistance(5.0F).setTranslationKey("monorail_waypoint");
//	public static final Block monorail_booster = new BlockMonorailBooster().setHardness(3.0F).setResistance(5.0F).setTranslationKey("monorail_booster");
//	public static final Block monorail_charger = new BlockMonorailCharger().setHardness(3.0F).setResistance(5.0F).setTranslationKey("monorail_charger");
//	public static final Block monorail_detector = new BlockMonorailDetector().setHardness(3.0F).setResistance(5.0F).setTranslationKey("monorail_detector");
//	public static final Block monorail_oneway = new BlockMonorailOneway().setHardness(3.0F).setResistance(5.0F).setTranslationKey("monorail_oneway");
	
	//public static final Block erze = new BlockErze().setTranslationKey("erze").setHardness(3.0F).setResistance(5.0F);
	/**public static final Block neonsand = new BlockNeonSand().setHardness(0.5F).setTranslationKey("neonsand")/*.setBlockTextureName("futurepack:NeonGranulat").setLightLevel(0.666F);
	public static final Block cristal = new BlockCristal(Material.PLANTS).setLightLevel(0.666F).setTranslationKey("cristall").setBlockTextureName("futurepack:kristall");
	public static final Block optiBench = new BlockOptiBensch().setHardness(5.0F).setResistance(10.0F).setTranslationKey("optiBench")/*.setBlockTextureName("futurepack:OptiBenchFront_0");
	public static final Block eMagnet =  new BlockElektroMagnet().setHardness(5.0F).setResistance(10.0F).setTranslationKey("elektroMagnet")/*.setBlockTextureName("futurepack:elektro_magnet");
	public static final Block Magnet = new BlockElektroMagnet().setHardness(5.0F).setResistance(10.0F).setTranslationKey("Magnet").setBlockTextureName("futurepack:magnet_block");
	public static final Block pulsit = new BlockElektroMagnet().setHardness(5.0F).setResistance(10.0F).setTranslationKey("pulsit").setBlockTextureName("futurepack:pulsite_block");
	public static final Block pucher = new BlockPusher().setHardness(5.0F).setResistance(10.0F).setTranslationKey("pucher")/*.setBlockTextureName("futurepack:push");
	public static final Block fishBlock = new BlockFish().setHardness(1.5F).setResistance(2.5F).setTranslationKey("blockFish").setBlockTextureName("futurepack:fisch_block");
	public static final Block industrieOfen = new BlockIndFurnace().setHardness(5.0F).setResistance(10.0F).setTranslationKey("industrieFurnace")/*.setBlockTextureName("futurepack:industrieofen");
	public static final Block blockBreaker = new BlockBreaker().setHardness(5.0F).setResistance(10.0F).setTranslationKey("blockBreaker")/*.setBlockTextureName("furnace_top");
	
	public static final Block metalTreppe = new BlockTreppe(erzBlocke, 4).setHardness(3.0F).setResistance(5.0F).setTranslationKey("metalTreppe");
	public static final Block metalFence = new BlockMetalFence(Material.IRON).setHardness(3.0F).setResistance(5.0F).setTranslationKey("metalFence").setCreativeTab(FPMain.tab_deco);
	public static final Block metalGitterPane = new BlockMetalPane( Material.IRON, true).setHardness(5.0F).setResistance(10.0F).setTranslationKey("uncolorPane");	
	public static final Block metalGitterBlock = new BlockMetalGitter().setHardness(5.0F).setResistance(10.0F).setTranslationKey("uncolorGitter").setBlockTextureName("futurepack:metalBlock_gitter");	
	public static final Block metalGitterTreppe = new BlockGitterTreppe(metalGitterBlock, 0).setHardness(3.0F).setResistance(5.0F).setTranslationKey("metalTreppe");
	
	public static final Block partpress = new BlockPartPress().setTranslationKey("partpress").setHardness(5.0F).setResistance(10.0F)/*.setBlockTextureName("futurepack:stanze");
	public static final Block eFurnace = new BlockEFurnace().setTranslationKey("efurnace").setHardness(5.0F).setResistance(10.0F)/*.setBlockTextureName("futurepack:stanze");
	public static final Block solarpanel = new BlockSolarPanel().setTranslationKey("solarpanel").setHardness(5.0F).setResistance(10.0F)/*.setBlockTextureName("futurepack:solarzelle");
	public static final Block AssemblyTable = new BlockAssemblyTable().setTranslationKey("assemblytable").setHardness(5.0F).setResistance(10.0F)/*.setBlockTextureName("futurepack:assembler");
	public static final Block crusher = new BlockCrusher().setTranslationKey("crusher").setHardness(5.0F).setResistance(10.0F)/*.setBlockTextureName("futurepack:super_crusher");
	public static final Block entityEater = new BlockEntityLasers().setTranslationKey("entityeater").setHardness(5.0F).setResistance(10.0F)/*.setBlockTextureName("futurepack:TriebwerkW_4");
	public static final Block baterieBox = new BlockBaterieBox().setTranslationKey("bateriebox").setHardness(5.0F).setResistance(10.0F)/*.setBlockTextureName("futurepack:Neonzelle");
	public static final Block externCooler = new BlockExternCooler().setTranslationKey("externCooler").setHardness(5.0F).setResistance(10.0F);
	public static final Block pipe = new BlockPipe().setHardness(3.0F).setResistance(5.0F).setTranslationKey("pipe").setBlockTextureName("futurepack:colorIron_0");
	public static final Block droneStation = new BlockDroneStation().setTranslationKey("DroneStation").setHardness(5.0F).setResistance(10.0F).setBlockTextureName("futurepack:colorIron_8");
	public static final Block sorter = new BlockSorter().setTranslationKey("sorter").setHardness(5.0F).setResistance(10.0F)/*.setBlockTextureName("futurepack:sorter");*/
	
	/**public static final Block NeonEngine = new BlockNeonEngine().setTranslationKey("neonengine").setHardness(5.0F).setResistance(10.0F)/*.setBlockTextureName("futurepack:colorIron_0");	
	public static final Block claime = new BlockClaime().setHardness(5.0F).setResistance(2000.0F).setTranslationKey("claime").setLightLevel(1F);
	public static final Block plasmaGenerator = new BlockPlasmaGenerator().setHardness(5.0F).setResistance(10.0F).setTranslationKey("plasmaGenerator")/*.setBlockTextureName("futurepack:colorIron_0");
	
	public static final Block neonBricks = new BlockNeonBricks(Material.ROCK).setTranslationKey("neonbricks")/*.setBlockTextureName("bricks").setHardness(3.0F).setResistance(5.0F);
	public static final Block triebwerk = new BlockThruster().setTranslationKey("triebwerk")/*.setBlockTextureName("futurepack:Triebwerk").setHardness(3.0F).setResistance(5.0F);
	public static final Block boardComputer = new BlockBoardComputer().setTranslationKey("boardcomputer")/*.setBlockTextureName("futurepack:funkcomputer_w_1").setHardness(3.0F).setResistance(5.0F);
	public static final Block beam = new BlockTeleporter().setTranslationKey("beam")/*.setBlockTextureName("futurepack:Eingabefeld").setHardness(3.0F).setResistance(5.0F);
	
	public static final Block stone = (new BlockFpStone()).setHardness(1.5F).setResistance(10.0F).setTranslationKey("stone").setBlockTextureName("futurepack:stone").setCreativeTab(FPMain.tab_deco);
	public static final Block sand = (new BlockFpSand()).setHardness(0.5F).setTranslationKey("sand").setBlockTextureName("futurepack:sand").setCreativeTab(FPMain.tab_deco);
	public static final Block gravel = (new BlockFpGravel()).setHardness(0.6F).setTranslationKey("gravel");
	public static final Block dirt = (new BlockFpDirt()).setHardness(0.6F).setTranslationKey("dirt");
	public static final Block grass = (new BlockFpGrass()).setHardness(0.6F).setTranslationKey("grass");
	public static final Block planks = (new BlockFpPlanks()).setHardness(2.0F).setResistance(5.0F).setTranslationKey("planks");
	//public static final Block sandstone = (new BlockSandStone()).setSoundType(Block.soundTypePiston).setHardness(0.8F).setTranslationKey("sandStone")/*.setBlockTextureName("futurepack:sandstone").setCreativeTab(FPMain.fpTab_deco);
	
	public static final Block prismid = (new BlockPrismid()).setHardness(2.0F).setResistance(4.0F).setTranslationKey("prismid");
	public static final Block prismid_stone = (new BlockPrismidStone()).setHardness(3.0F).setResistance(5.0F).setTranslationKey("prismid_stone");
	
	public static final Fluid neonFluid = new Fluid("liquid.neon", new ResourceLocation(Constants.MOD_ID, "blocks/neon_still"), new ResourceLocation(Constants.MOD_ID, "blocks/neon_flow")).setTemperature(473).setLuminosity(10).setViscosity(1500).setDensity(200);	
	public static final Fluid bitripentiumFluid = new Fluid("liquid.bitripentium", new ResourceLocation(Constants.MOD_ID, "blocks/rocketfuel_still"), new ResourceLocation(Constants.MOD_ID, "blocks/rocketfuel_flow")).setTemperature(200).setViscosity(10000).setDensity(26000);
	public static final Fluid salt_water = new FluidSaltWater("saltwater", new ResourceLocation(Constants.MOD_ID, "blocks/saltwater_still"), new ResourceLocation(Constants.MOD_ID, "blocks/saltwater_flow")).setDensity(1240).setViscosity(1040);
	public static final Fluid biogasFluid = new Fluid("liquid.biogas", new ResourceLocation(Constants.MOD_ID, "blocks/biogas"), new ResourceLocation(Constants.MOD_ID, "blocks/biogas")).setDensity(-100).setViscosity(100).setGaseous(true);
	
	static
	{
		FluidRegistry.registerFluid(neonFluid);
		FluidRegistry.registerFluid(bitripentiumFluid);
		FluidRegistry.registerFluid(salt_water);
		FluidRegistry.registerFluid(biogasFluid);
	}
	public static final Block neonLiquid = new BlockNeonLiquid(neonFluid).setTranslationKey(neonFluid.getUnlocalizedName()).setHardness(100.0F);
	//public static final Block bitripentiumLiquid = new BlockBitripentiumLiquid(bitripentiumFluid).setTranslationKey(bitripentiumFluid.getUnlocalizedName()).setHardness(100.0F).setLightOpacity(10);
	public static final Block saltWater = new BlockFluidClassic(salt_water, Material.WATER).setTranslationKey(salt_water.getUnlocalizedName()).setHardness(100.0F).setLightOpacity(4);
	//public static final Block biogas = new BlockFluidClassic(biogasFluid, Material.GLASS).setTranslationKey(biogasFluid.getUnlocalizedName()).setHardness(100.0F).setLightOpacity(1);
	
	public static final Block mushroom = new BlockSpaceMushroom().setHardness(0.2F).setTranslationKey("mushroom");//.setCreativeTab(null);/*.setBlockTextureName("futurepack:pilz")*/;
	
	public static final Block neonGas = null;// new BlockNeonGas().setHardness(1.5F).setResistance(2.5F).setTranslationKey("neonGas")/*.setBlockTextureName("futurepack:kristall")*/;
	public static final Block blockWithHole = new BlockWithHole().setTranslationKey("blockWithHole");
//	public static final Block wasserTurbine = new BlockWaterTurbine().setHardness(5.0F).setResistance(10.0F).setTranslationKey("wasserTurbine");
	public static final Block waterCooler = new BlockWaterCooler().setHardness(5.0F).setResistance(10.0F).setTranslationKey("waterCooler");
//	public static final Block externalCore = new BlockExternalCore().setHardness(5.0F).setResistance(10.0F).setTranslationKey("externalCore");
	/**public static final Block techtable = new BlockTechtable().setHardness(1.0F).setResistance(3.0F).setTranslationKey("techtable");
	
	public static final Block glowmelo = new BlockGlowmelo().setTranslationKey("glowmelo");
	public static final Block wood = new BlockFpWood().setHardness(0.5F).setTranslationKey("wood");
	public static final Block sapling = new BlockFpSapling().setHardness(0.0F).setCreativeTab(FPMain.tab_items).setTranslationKey("sapling");
	public static final Block leaves = new BlockFpLeaves().setTranslationKey("leaves");
	
	//Techblocks
	public static final Block modul1 = new BlockModul(1).setHardness(5.0F).setResistance(10.0F).setTranslationKey("modul.1");
	public static final Block modul2 = new BlockModul(2).setHardness(5.0F).setResistance(10.0F).setTranslationKey("modul.2");
	public static final Block modul3 = new BlockModul(3).setHardness(5.0F).setResistance(10.0F).setTranslationKey("modul.3");*/
	
	/**public static final Block scannerBlock = new BlockScanner().setHardness(5.0F).setResistance(10.0F).setTranslationKey("scanner");
	public static final Block forscherBlock = new BlockForscher().setHardness(5.0F).setResistance(10.0F).setTranslationKey("forscher");
	public static final Block laserTransmitter = new BlockLaserTransmitter().setHardness(5.0F).setResistance(10.0F).setTranslationKey("laser_transmitter");
	
	public static final Block brennstoffGenerator = new BlockBrennstoffGenerator().setTranslationKey("brennstoff_generator").setCreativeTab(FPMain.tab_maschiens).setHardness(5.0F).setResistance(10.0F);
	public static final Block blockPlacer = new BlockPlacer().setHardness(5.0F).setResistance(10.0F).setTranslationKey("block_placer");
	public static final Block industrialNeonFurnace = new BlockIndustrialNeonFurnace().setHardness(5.0F).setResistance(10.0F).setTranslationKey("industrial_neon_furnace");
	public static final Block zentrifuge = new BlockZentrifuge().setHardness(5.0F).setResistance(10.0F).setTranslationKey("zentrifuge");
	public static final Block flashserver = new BlockFlashServer().setHardness(5.0F).setResistance(10.0F).setTranslationKey("flashserver");
	public static final Block fuelcell = new BlockFuelCell().setHardness(5.0F).setResistance(10.0F).setTranslationKey("fuelcell");
	public static final Block ftlDrive = new BlockFTLMulti().setHardness(5.0F).setResistance(10.0F).setTranslationKey("ftl_drive");
	public static final Block advancedBoardcomputer = new BlockAdvancedBoardcomputer().setTranslationKey("advanced_boardcomputer").setHardness(3.0F).setResistance(5.0F);
	public static final Block recycler = new BlockRecycler().setHardness(5.0F).setResistance(10.0F).setTranslationKey("recycler");
	
	public static final Block modularDoor = new BlockModularDoor().setHardness(5.0F).setResistance(10.0F).setTranslationKey("modular_door");
	public static final Block compositeDoor = new BlockCompositeDoor().setHardness(1.0F).setResistance(3.0F).setTranslationKey("composite_door");
	public static final Block moduleSpeedUpgrade = new BlockModulSpeedUpgrade().setHardness(5.0F).setResistance(10.0F).setTranslationKey("modul_speed_upgrade");
	
	public static final Block fireflies = new BlockFireflies().setTranslationKey("fireflies");
	public static final Block fallingTree = new BlockFallingTree().setTranslationKey("fallingtree").setBlockUnbreakable();
	public static final Block saplingHolder = new BlockSaplingHolder().setHardness(1.0F).setResistance(3.0F).setTranslationKey("sapling_holder");
	
	public static final Block metalGate = new BlockColorGate().setTranslationKey("metalFenceGate").setHardness(3.0F).setResistance(5.0F);*/
	
	public static final Block fluidTube = new BlockFluidTube().setHardness(3.0F).setResistance(5.0F).setTranslationKey("fluid_tube");
	public static final Block fluidPump = new BlockFluidPump().setHardness(5.0F).setResistance(10.0F).setTranslationKey("fp_fluid_pump");
	/**public static final Block wirelessRedstoneReceiver = new BlockWirelessRedstoneReceiver().setHardness(3.0F).setResistance(5.0F).setTranslationKey("wr_receiver");
	public static final Block wirelessRedstoneTransmitter = new BlockWirelessRedstoneTransmitter(false).setHardness(3.0F).setResistance(5.0F).setTranslationKey("wr_transmitter");
	public static final Block wirelessRedstoneTransmitterInverted = new BlockWirelessRedstoneTransmitter(true).setHardness(3.0F).setResistance(5.0F).setTranslationKey("wr_transmitter_i");
	public static final Block dungeonSpawner = new BlockDungeonSpawner().setBlockUnbreakable().setResistance(100F).setTranslationKey("dungeon_spawner");
	public static final Block quantanium = new BlockQuantanium().setHardness(3.0F).setResistance(5.0F).setTranslationKey("quantanium");
	
	public static final Block modul1calc = new BlockModul(4).setHardness(5.0F).setResistance(10.0F).setTranslationKey("modul1_calc");*/
	public static final Block fluidTank = new BlockFluidTank().setHardness(3.0F).setResistance(5.0F).setTranslationKey("fluid_tank");
	public static final Block fluidIntake = new BlockFluidIntake().setHardness(3.0F).setResistance(5.0F).setTranslationKey("fluid_intake");
//	public static final Block hugemycel = new BlockHugeMycel().setResistance(0.5F).setHardness(0.5F).setTranslationKey("huge_mycel");
	/**public static final Block menelaus_mushroom = new BlockMenelausMushroms().setTranslationKey("menelaus_mushroom");
	public static final Block force_field = new BlockForceField().setBlockUnbreakable().setResistance(1000).setTranslationKey("force_field");
	public static final Block dungeon_core = new BlockDungeonCore().setHardness(5.0F).setResistance(10.0F).setTranslationKey("dungeon_core");
	public static final Block fp_lever = new BlockFpLever().setHardness(5.0F).setTranslationKey("lever");
	public static final Block fp_button = new BlockFpButton().setHardness(5.0F).setTranslationKey("button");
	
	public static final Block bedrock_rift = new BlockBedrockRift().setTranslationKey("bedrock_rift").setCreativeTab(FPMain.tab_deco);*/
	public static final Block filter_assembler = new BlockFilterAssembler().setHardness(5.0F).setResistance(10.0F).setTranslationKey("filter_assembler");
	/**public static final Block rs_timer = new BlockRsTimer().setHardness(3.0F).setResistance(5.0F).setTranslationKey("rs_timer");
	public static final Block composite_chest = new BlockCompositeChest().setHardness(2.5F).setTranslationKey("composite_chest");
	public static final Block deep_core_miner = new BlockDeepCoreMiner().setHardness(5.0F).setResistance(10.0F).setTranslationKey("deep_core_miner");
	public static final Block syncronizer = new BlockSyncronizer().setTranslationKey("syncronizer").setHardness(5.0F).setResistance(10.0F);
	public static final Block rf2ne = new BlockRFtoNEConverted().setTranslationKey("rf_ne_converter").setHardness(5.0F).setResistance(10.0F);
	public static final Block optiAssembler = new BlockOptiAssembler().setTranslationKey("opti_assembler").setHardness(5.0F).setResistance(10.0F);
	public static final Block insertNode = new BlockInsertNode().setTranslationKey("insert_node").setHardness(3.0F).setResistance(5.0F);
	public static final Block oxades = new BlockOxades().setTranslationKey("oxades");*/
	
	public static final Block fermentationBarrel = new BlockFermentationBarrel().setTranslationKey("fermentationBarrel").setHardness(5.0F).setResistance(10.0F);
	/**public static final Block gasturbine = new BlockGasTurbine().setTranslationKey("gasturbine").setHardness(5.0F).setResistance(10.0F);*/
	
	public static final Block[] NeonProducer = new Block[]{ionCollector,solarpanel,entityEater,plasmaGenerator,wasserTurbine,brennstoffGenerator}; 
	
//	public static void register(RegistryEvent.Register<Block> event)
//	{
//		IForgeRegistry<Block> r = event.getRegistry();
//		
//		neonLiquid.setRegistryName(new ResourceLocation(Constants.MOD_ID, "neon_liquid"));
//		//bitripentiumLiquid.setRegistryName(new ResourceLocation(Constants.MOD_ID, "bitripentium_liquid"));
//		saltWater.setRegistryName(new ResourceLocation(Constants.MOD_ID, "salt_water"));
//		//biogas.setRegistryName(new ResourceLocation(Constants.MOD_ID, "biogasFluid"));
//		
//		r.registerAll(neonLiquid, saltWater);
//		
//		FluidRegistry.addBucketForFluid(neonFluid);
//		FluidRegistry.addBucketForFluid(salt_water);
//		FluidRegistry.addBucketForFluid(biogasFluid);
//		
//		colorNeonLamp_off.setOnOffBlock(colorNeonLamp_on, colorNeonLamp_off);
//		colorNeonLamp_on.setOnOffBlock(colorNeonLamp_on, colorNeonLamp_off);
//		colorPlasmaLamp_off.setOnOffBlock(colorPlasmaLamp_on, colorPlasmaLamp_off);
//		colorPlasmaLamp_on.setOnOffBlock(colorPlasmaLamp_on, colorPlasmaLamp_off);
//		
//		registerBlockWithItem(colorIron, "color_iron", r);
//		FPBlocks.registerMetaHarvestLevel(colorIron, "pickaxe", 1);
//		
//		registerBlockWithItem(colorLuftung, "color_luftung", r);
//		FPBlocks.registerMetaHarvestLevel(colorLuftung, "pickaxe", 1);
//		
//		registerBlockWithItem(colorGitter, "color_gitter", r);
//		FPBlocks.registerMetaHarvestLevel(colorGitter, "pickaxe", 1);
//		
//		registerBlockWithItem(colorGlas, "color_glas", r);
//		FPBlocks.registerMetaHarvestLevel(colorGlas, "pickaxe", 1);
//		
//		if(!FPConfig.BTM)
//		{
//			registerBlockWithItem(colorIronStair0, "color_iron_stair0", r);
//			FPBlocks.registerMetaHarvestLevel(colorIronStair0, "pickaxe", 1);
//			
//			registerBlockWithItem(colorIronStair1, "color_iron_stair1", r);
//			FPBlocks.registerMetaHarvestLevel(colorIronStair1, "pickaxe", 1);
//			
//			registerBlockWithItem(colorIronStair2, "color_iron_stair2", r);
//			FPBlocks.registerMetaHarvestLevel(colorIronStair2, "pickaxe", 1);
//			
//			registerBlockWithItem(colorIronStair3, "color_iron_stair3", r);
//			FPBlocks.registerMetaHarvestLevel(colorIronStair3, "pickaxe", 1);
//			
//			registerBlockWithItem(colorIronStair4, "color_iron_stair4", r);
//			FPBlocks.registerMetaHarvestLevel(colorIronStair4, "pickaxe", 1);
//			
//			registerBlockWithItem(colorIronStair5, "color_iron_stair5", r);
//			FPBlocks.registerMetaHarvestLevel(colorIronStair5, "pickaxe", 1);
//			
//			registerBlockWithItem(colorIronStair6, "color_iron_stair6", r);
//			FPBlocks.registerMetaHarvestLevel(colorIronStair6, "pickaxe", 1);
//			
//			registerBlockWithItem(colorIronStair7, "color_iron_stair7", r);
//			FPBlocks.registerMetaHarvestLevel(colorIronStair7, "pickaxe", 1);
//			
//			registerBlockWithItem(colorIronStair8, "color_iron_stair8", r);
//			FPBlocks.registerMetaHarvestLevel(colorIronStair8, "pickaxe", 1);
//			
//			registerBlockWithItem(colorIronStair9, "color_iron_stair9", r);
//			FPBlocks.registerMetaHarvestLevel(colorIronStair9, "pickaxe", 1);
//			
//			registerBlockWithItem(colorIronStair10, "color_iron_stair10", r);
//			FPBlocks.registerMetaHarvestLevel(colorIronStair10, "pickaxe", 1);
//			
//			registerBlockWithItem(colorIronStair11, "color_iron_stair11", r);
//			FPBlocks.registerMetaHarvestLevel(colorIronStair11, "pickaxe", 1);
//			
//			registerBlockWithItem(colorIronStair12, "color_iron_stair12", r);
//			FPBlocks.registerMetaHarvestLevel(colorIronStair12, "pickaxe", 1);
//			
//			registerBlockWithItem(colorIronStair13, "color_iron_stair13", r);
//			FPBlocks.registerMetaHarvestLevel(colorIronStair13, "pickaxe", 1);
//			
//			registerBlockWithItem(colorIronStair14, "color_iron_stair14", r);
//			FPBlocks.registerMetaHarvestLevel(colorIronStair14, "pickaxe", 1);
//			
//			registerBlockWithItem(colorIronStair15, "color_iron_stair15", r);
//			FPBlocks.registerMetaHarvestLevel(colorIronStair15, "pickaxe", 1);
//			
//			registerBlockWithItem(colorGitterStair0, "color_gitter_stair0", r);
//			FPBlocks.registerMetaHarvestLevel(colorGitterStair0, "pickaxe", 1);
//			
//			registerBlockWithItem(colorGitterStair1, "color_gitter_stair1", r);
//			FPBlocks.registerMetaHarvestLevel(colorGitterStair1, "pickaxe", 1);
//			
//			registerBlockWithItem(colorGitterStair2, "color_gitter_stair2", r);
//			FPBlocks.registerMetaHarvestLevel(colorGitterStair2, "pickaxe", 1);
//			
//			registerBlockWithItem(colorGitterStair3, "color_gitter_stair3", r);
//			FPBlocks.registerMetaHarvestLevel(colorGitterStair3, "pickaxe", 1);
//			
//			registerBlockWithItem(colorGitterStair4, "color_gitter_stair4", r);
//			FPBlocks.registerMetaHarvestLevel(colorGitterStair4, "pickaxe", 1);
//			
//			registerBlockWithItem(colorGitterStair5, "color_gitter_stair5", r);
//			FPBlocks.registerMetaHarvestLevel(colorGitterStair5, "pickaxe", 1);
//			
//			registerBlockWithItem(colorGitterStair6, "color_gitter_stair6", r);
//			FPBlocks.registerMetaHarvestLevel(colorGitterStair6, "pickaxe", 1);
//			
//			registerBlockWithItem(colorGitterStair7, "color_gitter_stair7", r);
//			FPBlocks.registerMetaHarvestLevel(colorGitterStair7, "pickaxe", 1);
//			
//			registerBlockWithItem(colorGitterStair8, "color_gitter_stair8", r);
//			FPBlocks.registerMetaHarvestLevel(colorGitterStair8, "pickaxe", 1);
//			
//			registerBlockWithItem(colorGitterStair9, "color_gitter_stair9", r);
//			FPBlocks.registerMetaHarvestLevel(colorGitterStair9, "pickaxe", 1);
//			
//			registerBlockWithItem(colorGitterStair10, "color_gitter_stair10", r);
//			FPBlocks.registerMetaHarvestLevel(colorGitterStair10, "pickaxe", 1);
//			
//			registerBlockWithItem(colorGitterStair11, "color_gitter_stair11", r);
//			FPBlocks.registerMetaHarvestLevel(colorGitterStair11, "pickaxe", 1);
//			
//			registerBlockWithItem(colorGitterStair12, "color_gitter_stair12", r);
//			FPBlocks.registerMetaHarvestLevel(colorGitterStair12, "pickaxe", 1);
//			
//			registerBlockWithItem(colorGitterStair13, "color_gitter_stair13", r);
//			FPBlocks.registerMetaHarvestLevel(colorGitterStair13, "pickaxe", 1);
//			
//			registerBlockWithItem(colorGitterStair14, "color_gitter_stair14", r);
//			FPBlocks.registerMetaHarvestLevel(colorGitterStair14, "pickaxe", 1);
//			
//			registerBlockWithItem(colorGitterStair15, "color_gitter_stair15", r);
//			FPBlocks.registerMetaHarvestLevel(colorGitterStair15, "pickaxe", 1);
//		}
//		
//		registerBlock(colorIronSlabBottom, ItemMetaSlap.class, "color_iron_step_half_0", (BlockSlab)colorIronSlabBottom, (BlockSlab)colorIron, r);
//		registerBlock(colorIronSlabTop, ItemMetaSlap.class, "color_iron_step_half_1", (BlockSlab)colorIronSlabTop, (BlockSlab)colorIron, r);
//		FPBlocks.registerMetaHarvestLevel(colorIronSlabBottom, "pickaxe", 1);
//		FPBlocks.registerMetaHarvestLevel(colorIronSlabTop, "pickaxe", 1);
//		
//		registerBlock(colorGitterSlabBottom, "color_gitter_step_half_0", colorGitterSlabBottom, (BlockDekoMetaGlass) colorGitter, r);
//		registerBlock(colorGitterSlabTop, "color_gitter_step_half_1", colorGitterSlabTop, (BlockDekoMetaGlass) colorGitter, r);
//		FPBlocks.registerMetaHarvestLevel(colorGitterSlabBottom, "pickaxe", 1);
//		FPBlocks.registerMetaHarvestLevel(colorGitterSlabTop, "pickaxe", 1);
//		
//		registerBlock(colorLuftungSlabBottom, ItemMetaSlap.class, "color_luftung_step_half_0", (BlockSlab)colorLuftungSlabBottom, (BlockSlab)colorLuftung, r);
//		registerBlock(colorLuftungSlabTop, ItemMetaSlap.class, "color_luftung_step_half_1", (BlockSlab)colorLuftungSlabTop, (BlockSlab)colorLuftung, r);
//		FPBlocks.registerMetaHarvestLevel(colorLuftungSlabBottom, "pickaxe", 1);
//		FPBlocks.registerMetaHarvestLevel(colorLuftungSlabTop, "pickaxe", 1);
//		
//		registerBlockWithItem(colorGitterPane, "color_gitter_pane", r);
//		FPBlocks.registerMetaHarvestLevel(colorGitterPane, "pickaxe", 1);
//		
//		registerBlockWithItem(metalGitterPane, "metal_gitter_pane", r);
//		FPBlocks.registerMetaHarvestLevel(metalGitterPane, "pickaxe", 1);
//		
//		registerBlockWithItem(metalGitterBlock, "metal_gitter_block", r);
//		FPBlocks.registerMetaHarvestLevel(metalGitterBlock, "pickaxe", 1);
//		
//		registerBlockWithItem(metalGitterTreppe, "metal_gitter_treppe", r);
//		FPBlocks.registerMetaHarvestLevel(metalGitterTreppe, "pickaxe", 1);
//		
//		if(!FPConfig.BTM)
//		{
//			registerBlockWithItem(colorGate0, "color_gate_0", r);
//			FPBlocks.registerMetaHarvestLevel(colorGate0, "pickaxe", 1);		
//			registerBlockWithItem(colorGate1, "color_gate_1", r);
//			FPBlocks.registerMetaHarvestLevel(colorGate1, "pickaxe", 1);	
//			registerBlockWithItem(colorGate2, "color_gate_2", r);
//			FPBlocks.registerMetaHarvestLevel(colorGate2, "pickaxe", 1);		
//			registerBlockWithItem(colorGate3, "color_gate_3", r);
//			FPBlocks.registerMetaHarvestLevel(colorGate3, "pickaxe", 1);		
//			registerBlockWithItem(colorGate4, "color_gate_4", r);
//			FPBlocks.registerMetaHarvestLevel(colorGate4, "pickaxe", 1);	
//			registerBlockWithItem(colorGate5, "color_gate_5", r);
//			FPBlocks.registerMetaHarvestLevel(colorGate5, "pickaxe", 1);	
//			registerBlockWithItem(colorGate6, "color_gate_6", r);
//			FPBlocks.registerMetaHarvestLevel(colorGate6, "pickaxe", 1);
//			registerBlockWithItem(colorGate7, "color_gate_7", r);
//			FPBlocks.registerMetaHarvestLevel(colorGate7, "pickaxe", 1);	
//			registerBlockWithItem(colorGate8, "color_gate_8", r);
//			FPBlocks.registerMetaHarvestLevel(colorGate8, "pickaxe", 1);	
//			registerBlockWithItem(colorGate9, "color_gate_9", r);
//			FPBlocks.registerMetaHarvestLevel(colorGate9, "pickaxe", 1);	
//			registerBlockWithItem(colorGate10, "color_gate_10", r);
//			FPBlocks.registerMetaHarvestLevel(colorGate10, "pickaxe", 1);	
//			registerBlockWithItem(colorGate11, "color_gate_11", r);
//			FPBlocks.registerMetaHarvestLevel(colorGate11, "pickaxe", 1);		
//			registerBlockWithItem(colorGate12, "color_gate_12", r);
//			FPBlocks.registerMetaHarvestLevel(colorGate12, "pickaxe", 1);		
//			registerBlockWithItem(colorGate13, "color_gate_13", r);
//			FPBlocks.registerMetaHarvestLevel(colorGate13, "pickaxe", 1);		
//			registerBlockWithItem(colorGate14, "color_gate_14", r);
//			FPBlocks.registerMetaHarvestLevel(colorGate14, "pickaxe", 1);	
//			registerBlockWithItem(colorGate15, "color_gate_15", r);
//			FPBlocks.registerMetaHarvestLevel(colorGate15, "pickaxe", 1);
//		}
//		
//		registerBlockWithItem(metalGate, "metal_gate", r);
//		FPBlocks.registerMetaHarvestLevel(metalGate, "pickaxe", 1);
//		
//		
//		registerBlockWithItem(colorIronFence, "color_iron_fence", r);
//		FPBlocks.registerMetaHarvestLevel(colorIronFence, "pickaxe", 1);
//		
//		registerBlockWithItem(eisenleiter, "eisenleiter", r);
//		FPBlocks.registerMetaHarvestLevel(eisenleiter, "pickaxe", 1);
//		
//		registerBlockWithItem(antenne, "antenne", r);
//		FPBlocks.registerMetaHarvestLevel(antenne, "pickaxe", 1);
//		
//		//registerBlock(funkcomputer_weiss_on, ItemMetaMultiTex.class, "funkcomputer_weiss_on");
//		//FPBlocks.registerMetaHarvestLevel(funkcomputer_weiss_on, "pickaxe", 1);
//		
//		//registerBlock(funkcomputer_weiss_off, ItemMetaMultiTex.class, "funkcomputer_weiss_off");
//		//FPBlocks.registerMetaHarvestLevel(funkcomputer_weiss_off, "pickaxe", 1);
//		
//		registerBlockWithItem(colorNeonLamp_on, "color_neon_on", r);
//		FPBlocks.registerMetaHarvestLevel(colorNeonLamp_on, "pickaxe", 1);
//		
//		registerBlockWithItem(colorNeonLamp_off, "color_neon_off", r);
//		FPBlocks.registerMetaHarvestLevel(colorNeonLamp_off, "pickaxe", 1);
//		
//		registerBlockWithItem(colorPlasmaLamp_on, "color_plasma_on", r);
//		FPBlocks.registerMetaHarvestLevel(colorPlasmaLamp_on, "pickaxe", 1);
//		
//		registerBlockWithItem(colorPlasmaLamp_off, "color_plasma_off", r);
//		FPBlocks.registerMetaHarvestLevel(colorPlasmaLamp_off, "pickaxe", 1);
//		
//		//registerBlock(spaceshipmaker, ItemMetaMultiTex.class, "spaceshipmaker");
//		//FPBlocks.registerMetaHarvestLevel(spaceshipmaker, "pickaxe", 1);
//		
//		//registerBlock(microbox, ItemMetaMultiTex.class, "microbox");
//		//FPBlocks.registerMetaHarvestLevel(microbox, "pickaxe", 1);
//		
//		registerBlockWithItem(spacedoor, "spacedoor", r);
//		FPBlocks.registerMetaHarvestLevel(spacedoor, "pickaxe", 1);
//		
//		registerBlockWithItem(ionCollector, "ion_collector", r);
//		FPBlocks.registerMetaHarvestLevel(ionCollector, "pickaxe", 1);
//
//		registerBlockWithItem(wire, "wire", r);
//		FPBlocks.registerMetaHarvestLevel(wire, "pickaxe", 1);
//		
//		registerBlockWithItem(wandrobe, "wandrope", r);
//		FPBlocks.registerMetaHarvestLevel(wandrobe, "pickaxe", 1);
//		
//		erse.setRegistryName(new ResourceLocation(Constants.MOD_ID,"erse_plant"));
//		r.register(erse);
//		
//		registerBlockWithItem(erze, "erze", r);
//		
//		mendelBerry.setRegistryName(new ResourceLocation(Constants.MOD_ID,"mendel_berry_plant"));
//		r.register(mendelBerry);
//		
//		topinambur.setRegistryName(new ResourceLocation(Constants.MOD_ID,"topinambur_plant"));
//		r.register(topinambur);
//		
//		registerBlockWithItem(monorail, "monorail", r);
//		registerBlockWithItem(monorailStation, "monorail_station", r);
//		registerBlockWithItem(monorail_waypoint, "monorail_waypoint", r);
//		registerBlockWithItem(monorail_booster, "monorail_booster", r);
//		registerBlockWithItem(monorail_charger, "monorail_charger", r);
//		registerBlockWithItem(monorail_detector, "monorail_detector", r);
//		
//		registerBlockWithItem(neonsand, "neon_sand", r);
//		FPBlocks.registerMetaHarvestLevel(neonsand, "shovel", 0);
//		
//		registerBlockWithItem(cristal, "cristall", r);
//		
//		registerBlockWithItem(prismid, "prismid", r);
//		FPBlocks.registerMetaHarvestLevel(prismid, "pickaxe", 2);
//		
//		registerBlockWithItem(prismid_stone, "prismid_stone", r);
//		FPBlocks.registerMetaHarvestLevel(prismid_stone, "pickaxe", 2);
//		
//		registerBlockWithItem(optiBench, "opti_bench", r);
//		FPBlocks.registerMetaHarvestLevel(optiBench, "pickaxe", 1);
//		
//		registerBlockWithItem(eMagnet, "elektro_magnet", r);
//		FPBlocks.registerMetaHarvestLevel(eMagnet, "pickaxe", 1);
//		
//		registerBlockWithItem(Magnet, "magnet", r);
//		FPBlocks.registerMetaHarvestLevel(Magnet, "pickaxe", 1);
//		
//		registerBlockWithItem(pulsit, "pulsit", r);
//		FPBlocks.registerMetaHarvestLevel(pulsit, "pickaxe", 1);
//		
//		registerBlockWithItem(pucher, "pucher", r);
//		FPBlocks.registerMetaHarvestLevel(pucher, "pickaxe", 1);
//		
//		registerBlockWithItem(fishBlock, "fish_block", r);
//		FPBlocks.registerMetaHarvestLevel(fishBlock, "pickaxe", 1);
//		
//		registerBlockWithItem(industrieOfen, "industrie_ofen", r);
//		FPBlocks.registerMetaHarvestLevel(industrieOfen, "pickaxe", 1);
//		
//		registerBlockWithItem(blockBreaker, "block_breaker", r);
//		FPBlocks.registerMetaHarvestLevel(blockBreaker, "pickaxe", 1);
//		
//		registerBlockWithItem(metalTreppe, "metal_treppe", r);
//		FPBlocks.registerMetaHarvestLevel(metalTreppe, "pickaxe", 1);
//		
//		registerBlock((BlockSlab) metalSlap0, ItemMetaSlap.class, "metal_slap0", (BlockSlab)metalSlap0, (BlockSlab)metalSlap1, r);
//		registerBlock((BlockSlab) metalSlap1, ItemMetaSlap.class, "metal_slap1",  (BlockSlab)metalSlap0, (BlockSlab)metalSlap1, r);		
//		
//		FPBlocks.registerMetaHarvestLevel(metalSlap0, "pickaxe", 1);
//		FPBlocks.registerMetaHarvestLevel(metalSlap1, "pickaxe", 1);
//		
//		registerBlockWithItem(metalFence, "metal_fence", r);
//		FPBlocks.registerMetaHarvestLevel(metalFence, "pickaxe", 1);
//		
//		registerBlockWithItem(partpress, "partpress", r);
//		FPBlocks.registerMetaHarvestLevel(partpress, "pickaxe", 1);
//		
//		registerBlockWithItem(eFurnace, "e_furnace", r);
//		FPBlocks.registerMetaHarvestLevel(eFurnace, "pickaxe", 1);
//		
//		registerBlockWithItem(solarpanel, "solarpanel", r);
//		FPBlocks.registerMetaHarvestLevel(solarpanel, "pickaxe", 1);
//		
//		registerBlockWithItem(AssemblyTable, "assembly_table", r);
//		FPBlocks.registerMetaHarvestLevel(AssemblyTable, "pickaxe", 1);
//		
//		registerBlockWithItem(crusher, "crusher", r);
//		FPBlocks.registerMetaHarvestLevel(crusher, "pickaxe", 1);
//		
//		registerBlockWithItem(entityEater, "entity_eater", r);
//		FPBlocks.registerMetaHarvestLevel(entityEater, "pickaxe", 1);
//		
//		registerBlockWithItem(baterieBox, "baterie_box", r);
//		FPBlocks.registerMetaHarvestLevel(baterieBox, "pickaxe", 1);
//		
//		registerBlockWithItem(externCooler, "extern_cooler", r);
//		FPBlocks.registerMetaHarvestLevel(externCooler, "pickaxe", 1);
//
//		registerBlockWithItem(pipe, "pipe", r);
//		FPBlocks.registerMetaHarvestLevel(pipe, "pickaxe", 1);
//		
//		registerBlockWithItem(droneStation, "drone_station", r);
//		FPBlocks.registerMetaHarvestLevel(droneStation, "pickaxe", 1);
//		
//		registerBlockWithItem(sorter, "sorter", r);
//		FPBlocks.registerMetaHarvestLevel(sorter, "pickaxe", 1);
//		
//		//try {
//			//Class c =Class.forName("buildcraft.api.mj.MjAPI");
//			registerBlockWithItem(NeonEngine, "neon_engine", r);
//			FPBlocks.registerMetaHarvestLevel(NeonEngine, "pickaxe", 1);
//		//} catch(ClassNotFoundException e){}
//		
//		registerBlockWithItem(claime, "claime", r);
//		
//		registerBlockWithItem(plasmaGenerator, "plasma_generator_on", r);
//		FPBlocks.registerMetaHarvestLevel(plasmaGenerator, "pickaxe", 1);
//
//		
//		registerBlockWithItem(neonBricks, "neon_bricks", r);
//		FPBlocks.registerMetaHarvestLevel(neonBricks, "pickaxe", 1);
//		
//		registerBlockWithItem(triebwerk, "triebwerk", r);
//		FPBlocks.registerMetaHarvestLevel(triebwerk, "pickaxe", 1);
//		
//		registerBlockWithItem(boardComputer, "board_computer", r);
//		FPBlocks.registerMetaHarvestLevel(boardComputer, "pickaxe", 1);
//		
//		registerBlockWithItem(stone, "stone", r);
//		FPBlocks.registerMetaHarvestLevel(stone, "pickaxe", 1);
//		registerBlockWithItem(sand, "sand", r);
//		FPBlocks.registerMetaHarvestLevel(sand, "shovel", 0);
//		registerBlockWithItem(gravel, "gravel", r);
//		FPBlocks.registerMetaHarvestLevel(gravel, "shovel", 0);
//		registerBlockWithItem(dirt, "dirt", r);
//		FPBlocks.registerMetaHarvestLevel(dirt, "shovel", 0);
//		registerBlockWithItem(grass, "grass", r);
//		FPBlocks.registerMetaHarvestLevel(grass, "shovel", 0);
//		registerBlockWithItem(planks, "planks", r);
//		FPBlocks.registerMetaHarvestLevel(planks, "axe", 0);
//		//registerBlock(sandstone, ItemMetaMultiTex.class, "sandstone");
//		registerBlockWithItem(mushroom, "space_mushroom", r);
//		
//		registerBlockWithItem(beam, "beam", r);
//		FPBlocks.registerMetaHarvestLevel(beam, "pickaxe", 1);
//		
//		//TODO registerBlock(neonGas, ItemMetaMultiTex.class, "neonGas");
//		registerBlockWithItem(blockWithHole, "block_with_hole", r);
//		
//		registerBlockWithItem(wasserTurbine, "wasser_turbine", r);
//		FPBlocks.registerMetaHarvestLevel(wasserTurbine, "pickaxe", 1);
//		
//		registerBlockWithItem(brennstoffGenerator, "brennstoff_generator", r);
//		FPBlocks.registerMetaHarvestLevel(brennstoffGenerator, "pickaxe", 1);
//		
//		registerBlockWithItem(waterCooler, "water_cooler", r);
//		FPBlocks.registerMetaHarvestLevel(waterCooler, "pickaxe", 1);
//		
//		registerBlockWithItem(externalCore, "external_core", r);
//		FPBlocks.registerMetaHarvestLevel(externalCore, "pickaxe", 1);
//		
//		registerBlockWithItem(techtable, "techtable", r);
//		FPBlocks.registerMetaHarvestLevel(techtable, "pickaxe", 1);
//		
//		
//		ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "glowmelo");
//		glowmelo.setRegistryName(res);
//		r.register(glowmelo);
//		
//		//Item tex = new ItemEatableBlock(glowmelo);
//		//GameRegistry.register(tex);
//		
//		registerBlockWithItem(sapling, "sapling", r);
//		
//		registerBlockWithItem(leaves, "leaves", r);
//		
//		registerBlockWithItem(modul1, "modul_1", r);
//		FPBlocks.registerMetaHarvestLevel(modul1, "pickaxe", 1);
//		
//		registerBlockWithItem(modul2, "modul_2", r);
//		FPBlocks.registerMetaHarvestLevel(modul2, "pickaxe", 1);
//		
//		registerBlockWithItem(modul3, "modul_3", r);
//		FPBlocks.registerMetaHarvestLevel(modul3, "pickaxe", 1);
//		
//		registerBlockWithItem(scannerBlock, "scanner_block", r);
//		FPBlocks.registerMetaHarvestLevel(scannerBlock, "pickaxe", 1);
//		
//		registerBlockWithItem(forscherBlock, "forscher", r);
//		FPBlocks.registerMetaHarvestLevel(forscherBlock, "pickaxe", 1);
//		
//		registerBlockWithItem(laserTransmitter, "laser_transmitter", r);
//		FPBlocks.registerMetaHarvestLevel(laserTransmitter, "pickaxe", 1);
//		
//		registerBlockWithItem(wood, "wood", r);
//		FPBlocks.registerMetaHarvestLevel(wood, "axe", 0);
//		
//		registerBlockWithItem(blockPlacer, "block_placer", r);
//		FPBlocks.registerMetaHarvestLevel(blockPlacer, "pickaxe", 1);
//		
//		registerBlockWithItem(industrialNeonFurnace, "ind_neon_furn", r);
//		FPBlocks.registerMetaHarvestLevel(industrialNeonFurnace, "pickaxe", 1);
//		
//		registerBlockWithItem(zentrifuge, "zentrifuge", r);
//		FPBlocks.registerMetaHarvestLevel(zentrifuge, "pickaxe", 1);
//		
//		registerBlockWithItem(recycler, "recycler", r);
//		FPBlocks.registerMetaHarvestLevel(recycler, "pickaxe", 1);
//
//		registerBlockWithItem(flashserver, "flash_server", r);
//		FPBlocks.registerMetaHarvestLevel(flashserver, "pickaxe", 1);
//		
//		registerBlockWithItem(fuelcell, "fuelcell", r);
//		FPBlocks.registerMetaHarvestLevel(fuelcell, "pickaxe", 1);
//		
//		registerBlockWithItem(ftlDrive, "ftl_drive", r);
//		FPBlocks.registerMetaHarvestLevel(ftlDrive, "pickaxe", 1);
//		
//		registerBlockWithItem(advancedBoardcomputer, "advanced_boardcomputer", r);
//		FPBlocks.registerMetaHarvestLevel(advancedBoardcomputer, "pickaxe", 1);
//		
//		registerBlockWithItem(modularDoor, "modular_door", r);
//		FPBlocks.registerMetaHarvestLevel(modularDoor, "pickaxe", 1);
//		
//		compositeDoor.setRegistryName(Constants.MOD_ID,"composite_door");
//		r.register(compositeDoor);
//		FPBlocks.registerMetaHarvestLevel(compositeDoor, "pickaxe", 1);
//		
////		registerBlockWithItem(moduleSpeedUpgrade, "module_speed_upgrade");
////		FPBlocks.registerMetaHarvestLevel(moduleSpeedUpgrade, "pickaxe", 1);
//		
//		res = new ResourceLocation(Constants.MOD_ID, "fireflies");
//		fireflies.setRegistryName(res);
//		r.register(fireflies);
//		
//		res = new ResourceLocation(Constants.MOD_ID, "falling_tree");
//		fallingTree.setRegistryName(res);
//		r.register(fallingTree);
//		
//		registerBlockWithItem(saplingHolder, "sapling_holder", r);
//		FPBlocks.registerMetaHarvestLevel(saplingHolder, "pickaxe", 1);
//		
//		
//		registerBlockWithItem(fluidTube, "fluid_tube", r);
//		FPBlocks.registerMetaHarvestLevel(fluidTube, "pickaxe", 1);
//		
//		registerBlockWithItem(fluidPump, "fluid_pump", r);
//		FPBlocks.registerMetaHarvestLevel(fluidPump, "pickaxe", 1);
//		
//		registerBlockWithItem(wirelessRedstoneReceiver, "wr_receiver", r);
//		FPBlocks.registerMetaHarvestLevel(wirelessRedstoneReceiver, "pickaxe", 1);
//		
//		registerBlockWithItem(wirelessRedstoneTransmitter, "wr_transmitter", r);
//		FPBlocks.registerMetaHarvestLevel(wirelessRedstoneTransmitter, "pickaxe", 1);
//		
//		registerBlockWithItem(wirelessRedstoneTransmitterInverted, "wr_transmitter_i", r);
//		FPBlocks.registerMetaHarvestLevel(wirelessRedstoneTransmitterInverted, "pickaxe", 1);
//		
//		registerBlockWithItem(dungeonSpawner, "dungeon_spawner", r);
//		
//		registerBlockWithItem(quantanium, "quantanium", r);
//		FPBlocks.registerMetaHarvestLevel(quantanium, "pickaxe", 1);
//		
//		registerBlockWithItem(modul1calc, "modul1calc", r);
//		FPBlocks.registerMetaHarvestLevel(modul1calc, "pickaxe", 1);
//		
//		registerBlockWithItem(fluidTank, "fluid_tank", r);
//		FPBlocks.registerMetaHarvestLevel(fluidTank, "pickaxe", 1);
//		
//		registerBlockWithItem(fluidIntake, "fluid_intake", r);
//		FPBlocks.registerMetaHarvestLevel(fluidIntake, "pickaxe", 1);
//		
//		registerBlockWithItem(hugemycel, "huge_mycel", r);
//		registerBlockWithItem(menelaus_mushroom, "menelaus_mushroom", r);
//		
//		registerBlockWithItem(filter_assembler, "filter_assembler", r);
//		FPBlocks.registerMetaHarvestLevel(filter_assembler, "pickaxe", 1);
//		
//		registerBlockWithItem(force_field, "force_field", r);
//		
//		registerBlockWithItem(dungeon_core, "dungeon_core", r);
//		
//		registerBlockWithItem(fp_lever, "fp_lever", r); 
//		registerBlockWithItem(fp_button, "button", r); 
//		
//		registerBlockWithItem(monorail_oneway, "monorail_oneway", r);
//		
//		//TODO: maybe add ironNugget, -> super Crusher maks ironDust
//		//HelperOreDict.FuturepackConveter.addOre("gemIron", new ItemStack(wood, 1, 0));
//		
//		//Don't change Registry and simple not generate it
//		registerBlockWithItem(erzBlocke, "erz_blocke", r);
//		registerBlockWithItem(erzBlocke2, "erz_blocke2", r);
//		
//		FPBlocks.registerMetaHarvestLevel(erzBlocke, "pickaxe", 1);
//		FPBlocks.registerMetaHarvestLevel(erzBlocke2, "pickaxe", 1);
//		
//		registerBlockWithItem(bedrock_rift, "bedrock_rift", r);
//		
//		registerBlockWithItem(rs_timer, "rs_timer", r);
//		FPBlocks.registerMetaHarvestLevel(rs_timer, "pickaxe", 1);
//		
//		registerBlockWithItem(composite_chest, "composite_chest", r);
//		
//		registerBlockWithItem(deep_core_miner, "deep_core_miner", r);
//		FPBlocks.registerMetaHarvestLevel(deep_core_miner, "pickaxe", 1);
//		
//		registerBlockWithItem(syncronizer, "syncronizer", r);
//		FPBlocks.registerMetaHarvestLevel(syncronizer, "pickaxe", 1);
//		
//		registerBlockWithItem(rf2ne, "rf_ne_converter", r);
//		FPBlocks.registerMetaHarvestLevel(rf2ne, "pickaxe", 1);
//		
//		registerBlockWithItem(optiAssembler, "opti_assembler", r);
//		FPBlocks.registerMetaHarvestLevel(optiAssembler, "pickaxe", 1);
//		
//		registerBlockWithItem(insertNode, "insert_node", r);
//		FPBlocks.registerMetaHarvestLevel(insertNode, "pickaxe", 1);
//		
//		registerBlockWithItem(fermentationBarrel, "fermentationBarrel", r);
//		FPBlocks.registerMetaHarvestLevel(fermentationBarrel, "pickaxe", 1);
//		
//		registerBlockWithItem(gasturbine, "gasturbine", r);
//		FPBlocks.registerMetaHarvestLevel(gasturbine, "pickaxe", 1);
//		
//		
//		oxades.setRegistryName(Constants.MOD_ID, "oxades");
//		r.register(oxades);
//		
//		//OreDict registry moved to items
//	}


//	private static void registerBlock(BlockSlabMultiTexture bl, String string, BlockSlabMultiTexture bs1, BlockDekoMetaGlass bs2, IForgeRegistry<Block> reg)
//	{
//		ResourceLocation res = new ResourceLocation(Constants.MOD_ID, string);
//		bl.setRegistryName(res);
//		reg.register(bl);
//		ItemMetaSlap tex = new ItemMetaSlap(bl, bs1, bs2);
//		tex.setRegistryName(res);
//		itemBlocks.add(tex);
//	}
//
//
//	private static void registerBlock( BlockSlab bl, Class<ItemMetaSlap> class1, String string, BlockSlab bs1, BlockSlab bs2, IForgeRegistry<Block> reg)
//	{
//		//schei� slab m�ll den eh niemand haben will -.- 
//		ResourceLocation res = new ResourceLocation(Constants.MOD_ID, string);
//		bl.setRegistryName(res);
//		reg.register(bl);
//		ItemMetaSlap tex = new ItemMetaSlap(bl, bl, bs1, bs2);
//		tex.setRegistryName(res);
//		itemBlocks.add(tex);
//	}
//
//
//	private static void registerBlockWithItem(Block bl,	String string, IForgeRegistry<Block> reg)
//	{
//		ResourceLocation res = new ResourceLocation(Constants.MOD_ID, string);
//		bl.setRegistryName(res);
//		reg.register(bl);
//		ItemMetaMultiTex tex = new ItemMetaMultiTex(bl);
//		tex.setRegistryName(res);
//		itemBlocks.add(tex);
//	}


//	//@ TODO: OnlyIn(Dist.CLIENT)
//	public static void setupRendering()
//	{		
//		try
//		{			
//			Field[] fields = FPBlocks.class.getFields();
//			for(Field f : fields)
//			{
//				if(Modifier.isStatic(f.getModifiers()))
//				{
//					Object o = f.get(null);
//					if(o instanceof Block)
//					{
//						Item item = Item.getItemFromBlock((Block) o);
//						
//						if(item==null || item.getClass() == ItemAir.class)
//						{
//							FPLog.logger.error("Block %s has no Item!", o);						
//							continue;
//						}	
//						
//						if(o instanceof IItemMetaSubtypes)
//						{
//							IItemMetaSubtypes meta = (IItemMetaSubtypes) o;
//							for(int i=0;i<meta.getMaxMetas();i++)
//							{
//								registerItem(meta.getMetaName(i), item, i);
//							}
//						}
//						else
//						{
//							registerItem(item.getTranslationKey().substring(5), item, 0);
//						}
//					}
// 				}
//			}
//			
////			Item fluid = Item.getItemFromBlock(neonLiquid);
////			ModelBakery.registerItemVariants(fluid);
////			
////			final ModelResourceLocation ne = new ModelResourceLocation(Constants.MOD_ID + ":" + "neon_liquid", "fluid");
////			ModelLoader.setCustomMeshDefinition(fluid, new ItemMeshDefinition()
////			{
////				public ModelResourceLocation getModelLocation(ItemStack stack)
////				{
////					return ne;
////				}
////			});
//			
//		}
//			
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
//	}
//	
//	//@ TODO: OnlyIn(Dist.CLIENT)
//	private static class StateMapperLeaves extends StateMapperBase
//	{
//		@Override
//		protected ModelResourceLocation getModelResourceLocation(IBlockState state)
//		{
//			String s = Block.REGISTRY.getNameForObject(state.getBlock()).toString();
//			
//			 Map<IProperty<?>, Comparable<?>> map = Maps.<IProperty<?>, Comparable<?>>newLinkedHashMap(state.getProperties());
//			
//			 map.remove(BlockLeaves.CHECK_DECAY);
//			 map.remove(BlockLeaves.DECAYABLE);
//			 
//			 if(state.getValue(BlockFpLeaves.subtype)!=0)
//			 {
//				 map.remove(BlockFpLeaves.LIGHT);
//			 }
//			 
//			return new ModelResourceLocation(s, this.getPropertyString(map));
//		}
//	}
//	
//	//@ TODO: OnlyIn(Dist.CLIENT)
//	private static class StateMapperFluid implements IStateMapper
//	{
//		@Override
//		public Map<IBlockState, ModelResourceLocation> putStateModelLocations( Block blockIn)
//		{
//			HashMap<IBlockState, ModelResourceLocation> map = new HashMap<IBlockState, ModelResourceLocation>();
//			for(IBlockState state : blockIn.getBlockState().getValidStates())
//			{
//				map.put(state, getModelResourceLocation(state));
//			}
//			return map;
//		}
//		
//		protected ModelResourceLocation getModelResourceLocation(IBlockState state)
//		{
//			return new ModelResourceLocation(Block.REGISTRY.getNameForObject(state.getBlock()), "fluid");
//		}
//	}
//	
//	//@ TODO: OnlyIn(Dist.CLIENT)
//	public static void setupPreRendering()
//	{
//		//final ModelResourceLocation ne = new ModelResourceLocation(Constants.MOD_ID + ":" + "neon_liquid", "fluid");
//		
//		ModelLoader.setCustomStateMapper(leaves, new StateMapperLeaves());
//		
//		
//		ModelLoader.setCustomStateMapper(neonLiquid, new StateMapperFluid());
//		//ModelLoader.setCustomStateMapper(bitripentiumLiquid, new StateMapperFluid());
//		ModelLoader.setCustomStateMapper(saltWater, new StateMapperFluid());
//		//ModelLoader.setCustomStateMapper(biogas, new StateMapperFluid());
//		ModelLoader.setCustomStateMapper(compositeDoor, (new StateMap.Builder()).ignore(new IProperty[] {BlockDoor.POWERED}).build());
//	}
//	
//	//@ TODO: OnlyIn(Dist.CLIENT)
//	private static void registerItem(String s, Item i, int meta)
//	{
//		RenderItem render = Minecraft.getInstance().getRenderItem();
//		ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "blocks/" + toLoverCase(s));
//		ModelLoader.setCustomModelResourceLocation(i, meta, new ModelResourceLocation(res, "inventory"));
//		//ModelBakery.registerItemVariants(i, res);
//		//FPLog.logger.debug(String.format("Add Item %s(%s) with %s",i,meta,s));
//		//render.getItemModelMesher().register(i, meta, new ModelResourceLocation(res, "inventory"));
//	}
//	
	private static String toLoverCase(String s)
	{
		for(int i='A';i<='Z';i++)
		{
			int index = s.indexOf(i);
			if(index > 0)
			{
				char c = s.charAt(index -1);
				String s2 = c >= 'a' && c <= 'z' ? "_" : "";
				s = s.replace( "" + (char)i, (s2 +(char)i).toLowerCase() );
			}
			else
			{
				s = s.replace( "" + (char)i, ("" +(char)i).toLowerCase() );
			}
				
		}
		return s;
	}
	
//	public static void registerMetaHarvestLevel(Block block, String tool, int level)
//	{	
//		block.setHarvestLevel(tool, level);
//	}
	
	//BlockHelpers Start

//	static Comparator<NeonObject> comp = new Comparator<NeonObject>()
//	{
//		@Override
//		public int compare(NeonObject arg0, NeonObject arg1)
//		{
//			if(arg0.neon.getType() == EnumPowerMode.USE && arg1.neon.getType() != EnumPowerMode.USE)
//			{
//				return -1;
//			}
//			if(arg1.neon.getType() == EnumPowerMode.USE && arg0.neon.getType() != EnumPowerMode.USE)
//			{
//				return 1;
//			}
//			float f1 = arg0.neon.getPower()/arg0.neon.getMaxPower();
//			float f2 = arg1.neon.getPower()/arg1.neon.getMaxPower();
//			if(f1==f2)
//				return 0;
//			return f1 < f2 ? -1 : 1;
//
//		}
//	};
	
	
//	
//	private static final IBlockSelector SupportSelector = new IBlockSelector()
//	{
//		
//		@Override
//		public boolean isValidBlock(World w, BlockPos pos, Material m, boolean dia, ParentCoords parent) 
//		{
//			if(dia)
//				return false;
//			TileEntity tile = w.getTileEntity(pos);
//			if(tile!=null && tile.hasWorld() && tile instanceof ITileSupportAble)
//			{
//				ITileSupportAble ki = (ITileSupportAble) tile;
//				return ki.isSupportAble(getSide(pos,parent));
//			}
//			return false;
//		}
//		
//		@Override
//		public boolean canContinue(World w, BlockPos pos, Material m,boolean dia, ParentCoords parent)
//		{
//			ITileSupportAble ki = (ITileSupportAble) w.getTileEntity(pos);
//			return ki.getSupportType() == EnumSupportType.WIRE;
//		}
//	};
//	
//	private static final IBlockValidator SupportUser = new IBlockValidator()
//	{
//		
//		@Override
//		public boolean isValidBlock(World w, ParentCoords pos) 
//		{
//			ITileSupportAble ki = (ITileSupportAble) w.getTileEntity(pos);
//			if(ki==null)
//				return false;
//			return ki.getSupportType() == EnumSupportType.USER && ki.needSupport();
//		}
//	};
//	
//	public static boolean sendSupportPoints(World w, BlockPos pos, ITileSupportAble base)
//	{
//		if(base.getSupport()<=0)
//			return false;
//		if(w.isRemote)
//			return false;
//		
////		FPSpaceShipSelector s = new FPSpaceShipSelector(w, AISelector);
////		s.selectBlocks(pos);
//		FPBlockSelector s = FPSelectorHelper.getSelector((WorldServer) w, pos, SupportSelector);
//		Collection<ParentCoords> users = s.getValidBlocks(SupportUser);
//		
//		boolean success = false;
//		
//		for(ParentCoords cord : users)
//		{
//			ITileSupportAble ki = (ITileSupportAble) w.getTileEntity(cord);
//			if(ki==null)
//			{
//				continue;
//			}
//			if(base.getSupport()>0 && base.canSendSupport(ki))
//			{
//				ki.addSupport(1);
//				base.useSupport(1);
//				success = true;
//				ParentCoords c = cord;
//				while(c!=null)
//				{
//					ITileSupportAble wire = (ITileSupportAble) w.getTileEntity(c);
//					if(wire!=null)
//						wire.support();
//
//					c = c.getParent();
//				}
//			}
//		}
//		return success;
//	}

//	//@ TODO: OnlyIn(Dist.CLIENT)
//	public static void loadAdditionalTextures(TextureStitchEvent event) 
//	{
//		TextureMap m = event.getMap();
//		m.registerSprite(biogasFluid.getStill());
//		m.registerSprite(bitripentiumFluid.getStill());
//		m.registerSprite(bitripentiumFluid.getFlowing());
//	}
}
