package futurepack.extensions.minetweaker.implementation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import crafttweaker.annotations.ZenRegister;
import futurepack.common.research.ResearchLoader;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenRegister
@ZenClass("mods.futurepack.research")
public class ResearchBridge 
{

	@ZenMethod
	public static void addResearches(InputStream in)
	{
		ResearchLoader.instance.addResearchesFromReader(new InputStreamReader(in, StandardCharsets.UTF_8));
	}

	@ZenMethod
	public static void addResearchFile(String s)
	{
		try
		{
			InputStream in = new FileInputStream(new File(s));
			addResearches(in);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} 
	}
	
}