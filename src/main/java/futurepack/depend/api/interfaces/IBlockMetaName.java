package futurepack.depend.api.interfaces;

import net.minecraft.world.item.ItemStack;

public interface IBlockMetaName 
{
	public String getMetaNameSub(ItemStack is);
}
