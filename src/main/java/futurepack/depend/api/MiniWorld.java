package futurepack.depend.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.function.Predicate;
import java.util.stream.Stream;

import futurepack.depend.api.helper.HelperSerialisation;
import net.minecraft.client.Minecraft;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Registry;
import net.minecraft.core.RegistryAccess;
import net.minecraft.core.Vec3i;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.LightLayer;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeManager;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.border.WorldBorder;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.chunk.ChunkStatus;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.levelgen.Heightmap.Types;
import net.minecraft.world.level.lighting.LevelLightEngine;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.fml.DistExecutor;

/**
 * This is just for holding the Wolrd Data. Currently used for the Falling Tree Rendering
 */
public class MiniWorld implements LevelReader, INBTSerializable<CompoundTag>
{
	public long SEED;
	
	public BlockEntity[][][] tiles;//
	public BlockState[][][] states;//
	public Biome[][] bioms;//
	public Integer[][][][] redstone;//
	public Integer[][][] skylight;//
	public Integer[][][] blocklight;//
	
	public boolean extendedLevels;//
	
	public BlockPos start;//
	public int height,width,depth;//
	
	public Direction face;//
	public Vec3 rotationpoint;//
	
	/**In degrees*/
	public float rot;
	
	public ArrayList<BlockPos> validBlocks; //important for rendering
	
	public MiniWorld(BlockPos start, Vec3i whd, long seed)
	{
		this.start = start;
		this.SEED = seed;
		width=whd.getX();
		height=whd.getY();
		depth=whd.getZ();
		tiles = new BlockEntity[width+1][height+1][depth+1];
		states = new BlockState[width+1][height+1][depth+1];
		bioms = new Biome[width+1][depth+1];
		redstone = new Integer[width+1][height+1][depth+1][6];
		skylight = new Integer[width+1][height+1][depth+1];
		blocklight = new Integer[width+1][height+1][depth+1];
		
		for(Integer[][] j : skylight)
		{
			for(Integer[] jj : j)
			{
				Arrays.fill(jj, -1);
			}
		}
		for(Integer[][] j : blocklight)
		{
			for(Integer[] jj : j)
			{
				Arrays.fill(jj, -1);
			}
		}
		
			
	}
	
	private Level w = null;
	public int skylightSubtracted = 1;
	private short[][] heightMap;
		
	public MiniWorld(Level tileEntityWorld, CompoundTag nbt)
	{
		if(tileEntityWorld instanceof ServerLevel)
			this.SEED = ((ServerLevel)tileEntityWorld).getSeed();
		w = tileEntityWorld;
		skylightSubtracted = w.getSkyDarken();
		deserializeNBT(nbt);
	}
	
	@Override
	public BlockEntity getBlockEntity(BlockPos pos)
	{
		if(isInBounds(pos))
		{
			return getObject(tiles, pos);
		}
		return null;
	}
	
	/*
	 * FIXME: Auskommentiert
	@Override
	public int getCombinedLight(BlockPos pos, int val)
	{
		if(isInBounds(pos))
		{
			int j = getObject(skylight, pos);
			int k = getObject(blocklight, pos);
			
			if (k < val)
			{
				k = val;
			}
			
			return j << 20 | k << 4;
		}
		return skylight[0][0][0] << 20 | blocklight[0][0][0] << 4;	
	}*/

	@Override
	public BlockState getBlockState(BlockPos pos)
	{
		if(isInBounds(pos))
		{
			return getObject(states, pos);
		}
		return Blocks.AIR.defaultBlockState();
	}

	@Override
	public boolean isEmptyBlock(BlockPos pos)
	{
		BlockState state = getBlockState(pos);
		return state.isAir();
	}

	@Override
	public Biome getBiome(BlockPos pos)
	{
		if(isInBounds(pos))
		{
			return getObject(bioms, pos);
		}
		return bioms[0][0];
	}

	@Override
	public int getDirectSignal(BlockPos pos, Direction direction)
	{
		if(isInBounds(pos))
		{
			return getObject(redstone, pos)[direction.get3DDataValue()];
		}
		return 0;
	}
		
	private boolean isInBounds(BlockPos pos)
	{
		int x = pos.getX() - start.getX();
		int y = pos.getY() - start.getY();
		int z = pos.getZ() - start.getZ();
		
		if(x>=0 && x<width)
		{
			if(y>=0 && y<height)
			{
				if(z>=0 && z<depth)
				{
					return true;
				}
			}
		}
			
		return false;
	}	
		
	private <T> T getObject(T[][][] var, BlockPos pos)
	{
		int x = pos.getX() - start.getX();
		int y = pos.getY() - start.getY();
		int z = pos.getZ() - start.getZ();
			
		x = x<0 ? 0 : (x >= var.length ? var.length-1 : x);
		y = y<0 ? 0 : (y >= var[x].length ? var[x].length-1 : y);
		z = z<0 ? 0 : (z >= var[x][y].length ? var[x][y].length-1 : z);
		
		
		return var[x][y][z];
	}
		
	public <T> void setObject(T[][][] var, BlockPos pos, T val)
	{
		int x = pos.getX() - start.getX();
		int y = pos.getY() - start.getY();
		int z = pos.getZ() - start.getZ();
			
		var[x][y][z] = val;
	}
	
	public <T> void setObjectSafe(T[][][] var, BlockPos pos, T val)
	{
		int x = pos.getX() - start.getX();
		int y = pos.getY() - start.getY();
		int z = pos.getZ() - start.getZ();
			
		
		if(x>0 && y>0 && z>0)
		{
			if(x <= width && y <= height && z <= depth)
			{
				var[x][y][z] = val;
			}
		}
	}
	
	private <T> T getObject(T[][] var, BlockPos pos)
	{
		int x = pos.getX() - start.getX();
		int z = pos.getZ() - start.getZ();
			
		return var[x][z];
	}
		
	public <T> void setObject(T[][] var, BlockPos pos, T val)
	{
		int x = pos.getX() - start.getX();
		int z = pos.getZ() - start.getZ();
			
		var[x][z] = val;
	}
	
	public void setRotation(float f)
	{
		this.rot = f;
	}

	@Override
	public CompoundTag serializeNBT()
	{
		CompoundTag nbt = new CompoundTag();
		nbt.putIntArray("stsf", new int[]{width,height,depth,start.getX(),start.getY(),start.getZ(),face.get3DDataValue()});
		ListTag tile = new ListTag();
		int[] ints = new int[(width+1)*(height+1)*(depth+1)];
		int[] light = new int[(width+1)*(height+1)*(depth+1)];
		int[] red = new int[(width+1)*(height+1)*(depth+1)];
		HelperSerialisation.Factory<Biome> fac = new HelperSerialisation.Factory<Biome>(getDynRegistries(w).registryOrThrow(Registry.BIOME_REGISTRY));
		
		for(int x=0;x<=width;x++)
		{
			for(int z=0;z<=depth;z++)
			{
				for(int y=0;y<=height;y++)
				{
				
					BlockEntity ti = tiles[x][y][z];
					if(ti!=null)
					{
						tile.add(ti.save(new CompoundTag()));
					}
					int pos = x*height*depth+ z*height +y;
					if(states[x][y][z]==null)
						ints[pos]=0;
					else
						ints[pos] = Block.getId(states[x][y][z]);
					int j = toInt(skylight[x][y][z]);
					int k = toInt(blocklight[x][y][z]);
					light[pos] = j << 20 | k << 4;
					red[pos] = redstoneInt(redstone[x][y][z]);
				}
//				int pos = x*depth+ z;
				fac.add(getBiome(start.offset(x, 0, z)));
			}
		}
		nbt.put("tiles", tile);
		nbt.putIntArray("stage", ints);
		nbt.putIntArray("light", light);
		nbt.putIntArray("red", red);
		nbt.put("biomes", fac.store()); //FIXME: Dringend erzeugt Softcrashes
		
		nbt.putBoolean("extLvl", extendedLevels);
		nbt.putIntArray("double", new int[]{Float.floatToIntBits((float)rotationpoint.x),Float.floatToIntBits((float) rotationpoint.y),Float.floatToIntBits((float) rotationpoint.z)} );
		
		nbt.putLong("seed", SEED);
		
		return nbt;
	}
	
	private int redstoneInt(Integer[] ints)
	{
		return toInt(ints[0])&15 | (toInt(ints[1])&15)<<4 | (toInt(ints[2])&15)<<8 | (toInt(ints[3])&15)<<12 | (toInt(ints[4])&15)<<16 | (toInt(ints[5])&15)<<20;
	}
	
	private int toInt(Integer i)
	{
		return i==null?0:i;
	}

	
	@SuppressWarnings("deprecation")
	public static RegistryAccess getDynRegistries(Level w)
	{
		if(w==null)
		{
			return null;
		}
		else if(w.isClientSide)
		{
			return DistExecutor.callWhenOn(Dist.CLIENT, () -> new Callable<RegistryAccess>()
			{
				@Override
				public RegistryAccess call() throws Exception
				{
					LocalPlayer pl = Minecraft.getInstance().player;
					return pl.connection.registryAccess();
				}
			});
		}
		else
		{
			return w.getServer().registryAccess();
		}
	}
	
	
	@Override
	public void deserializeNBT(CompoundTag nbt)
	{
		int[] stsf = nbt.getIntArray("stsf");
		width=stsf[0];height=stsf[1];depth=stsf[2];
		start = new BlockPos(stsf[3], stsf[4], stsf[5]);
		face = Direction.from3DDataValue(stsf[6]);
		extendedLevels = nbt.getBoolean("extLvl");
		int[] dou = nbt.getIntArray("double");
		rotationpoint = new Vec3(Float.intBitsToFloat(dou[0]),Float.intBitsToFloat(dou[1]),Float.intBitsToFloat(dou[2]));
		
		tiles = new BlockEntity[width+1][height+1][depth+1];
		states = new BlockState[width+1][height+1][depth+1];
		bioms = new Biome[width+1][depth+1];
		redstone = new Integer[width+1][height+1][depth+1][6];
		skylight = new Integer[width+1][height+1][depth+1];
		blocklight = new Integer[width+1][height+1][depth+1];
		
		SEED = nbt.getLong("seed");
		
		int[] ints = nbt.getIntArray("stage");
		int[] light = nbt.getIntArray("light");
		int[] red = nbt.getIntArray("red");
		
		Biome[] biomecashe = new HelperSerialisation.Factory<Biome>(getDynRegistries(w).registryOrThrow(Registry.BIOME_REGISTRY)).load(nbt.getCompound("biomes"), Biome[]::new);
		
		for(int x=0;x<=width;x++)
		{
			for(int z=0;z<=depth;z++)
			{
				for(int y=0;y<=height;y++)
				{				
					int pos = x*height*depth+ z*height +y;
					states[x][y][z] = Block.stateById(ints[pos]);
					if(light.length==0)
					{
						skylight[x][y][z] = 15;
						blocklight[x][y][z]	= 0;  
					}
					else
					{
						skylight[x][y][z] = light[pos] >>20;
						blocklight[x][y][z]	= light[pos]>>4;  
					}
					if(red.length==0)
					{
						redstone[x][y][z] = new Integer[]{0,0,0,0,0,0};
					}
					else
						redstone[x][y][z] = redstoneInt(red[pos]);
				}
				int pos = x*depth+ z;
				bioms[x][z] = biomecashe[pos];
			}
		}
		ListTag tile = nbt.getList("tiles", 10);
		for(int i=0;i<tile.size();i++)
		{
			CompoundTag tag = tile.getCompound(i);
			BlockPos tilePos = new BlockPos(tag.getInt("x"), tag.getInt("y"), tag.getInt("z"));
			BlockState state = getObject(states, tilePos);
			BlockEntity te = BlockEntity.loadStatic(tilePos, state, tag);
			te.setLevel(w);
			setObject(tiles, te.getBlockPos(), te);
		}
	}
	
	private Integer[] redstoneInt(int ints)
	{
		Integer[] t = new Integer[6];
		t[0] = ints & 15;
		t[1] = (ints>>4) & 15;
		t[2] = (ints>>8) & 15;
		t[3] = (ints>>12) & 15;
		t[4] = (ints>>16) & 15;
		t[5] = (ints>>20) & 15;
		return t;
	}

	@Override
	public int getBrightness(LightLayer type, BlockPos pos)
	{
		switch(type)
		{
			case BLOCK:
				return getObject(blocklight, pos);
			case SKY:
				return getObject(skylight, pos);
			default:
				return 0;
		}
	}

	@Override
	public int getRawBrightness(BlockPos pos, int amount)
	{
		int j, k;
		if(isInBounds(pos))
		{
			j = getObject(skylight, pos);
			k = getObject(blocklight, pos);
		}
		else
		{
			j = skylight[0][0][0];
			k = blocklight[0][0][0];
		}
		k-= amount;
		
		if (k < 0)
		{
			k = 0;
		}	
		return j << 20 | k << 4;
	}

//	@Override
//	public boolean canSeeSky(BlockPos pos)
//	{
//		return getHeight(Type.LIGHT_BLOCKING, pos.getX(), pos.getZ()) >= pos.getY();
//	}

	private int getHeightAt(int x, int z)
	{
		x -= start.getX();
		z -= start.getZ();
		if(heightMap==null)
		{
			heightMap = new short[width+1][depth+1];
			for(int j=0;j<=width;j++)
			{
				for(int l=0;l<=depth;l++)
				{
					for(int k=height;k>=0;k--)
					{
						BlockState st = states[j][k][l];
						if(st.isAir())
						{
							continue;
						}
						else
						{
							heightMap[j][l] = (short)k;
							break;
						}
					}
				}
			}
		}
		if(x>0 && x<=width && z>0 && z<=depth)
			return heightMap[x][z] + start.getY();
		
		return 0;
	}
	
	@Override
	public int getHeight(Types heightmapType, int x, int z)
	{
		return getHeightAt(x, z);
	}

	@Override
	public int getSkyDarken()
	{
		return skylightSubtracted;
	}

	private WorldBorder border;
	
	@Override
	public WorldBorder getWorldBorder()
	{
		return border;
	}

	@Override
	public boolean isUnobstructed(Entity entityIn, VoxelShape shape)//copied from world
	{
		return true;
	}

//	@Override
//	public List<Entity> getEntitiesWithinAABBExcludingEntity(Entity entityIn, AxisAlignedBB bb)
//	{
//		return Collections.emptyList();
//	}

	@Override
	public boolean isClientSide()
	{
		return true;
	}

	@Override
	public int getSeaLevel()
	{
		if(w!=null)
			return w.getSeaLevel();
		else
			return 60;
	}

	@Override
	public FluidState getFluidState(BlockPos pos)
	{
		return Fluids.EMPTY.defaultFluidState();
	}

	public void setWorld(Level original)
	{
		this.w = original;
		if(border == null)
			this.border = w.getWorldBorder();
	}

	@Override
	public ChunkAccess getChunk(int x, int z, ChunkStatus requiredStatus, boolean nonnull) 
	{
		return null;
	}

	@Override
	public boolean hasChunk(int x, int z) 
	{
//		if(allowEmpty)
//			return true;
//		else
		{
			int x0 = start.getX() >> 4;
			int z0 = start.getZ() >> 4;
			int x1 = (start.getX() + width) >> 4;
			int z1 = (start.getZ() + depth) >> 4;
			
			if(x0 <= x && x <= x1)
			{
				if(z0 <= z && z <= z1)
				{
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public BlockPos getHeightmapPos(Types heightmapType, BlockPos pos) 
	{
		return new BlockPos(pos.getX(), getHeightAt(pos.getX(), pos.getZ()), pos.getZ());
	}

	@Override
	public LevelLightEngine getLightEngine() 
	{
		return w.getLightEngine();
	}

	@Override
	public BiomeManager getBiomeManager() 
	{
		return w.getBiomeManager();
	}

	@Override
	public Biome getUncachedNoiseBiome(int x, int y, int z) 
	{
		return w.getUncachedNoiseBiome(x, y, z);
	}

	@Override
	public float getShade(Direction p_230487_1_, boolean p_230487_2_)
	{
		boolean flag = Minecraft.getInstance().level.effects().constantAmbientLight();

		if (!p_230487_2_) 
		{
			return flag ? 0.9F : 1.0F;
		}
		else
		{
			switch(p_230487_1_) 
			{
			case DOWN:
				return flag ? 0.9F : 0.5F;
			case UP:
				return flag ? 0.9F : 1.0F;
			case NORTH:
			case SOUTH:
				return 0.8F;
			case WEST:
			case EAST:
				return 0.6F;
			default:
				return 1.0F;
			}
		}
	}

	@Override
	public Stream<VoxelShape> getEntityCollisions(Entity p_230318_1_, AABB p_230318_2_, Predicate<Entity> p_230318_3_) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DimensionType dimensionType() 
	{
		return w.dimensionType();
	}
			
}
