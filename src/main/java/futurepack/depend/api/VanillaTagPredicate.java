package futurepack.depend.api;

import java.util.List;

import futurepack.api.ItemPredicateBase;
import futurepack.api.helper.HelperTags;
import futurepack.depend.api.helper.HelperOreDict;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.Tag;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Blocks;

public class VanillaTagPredicate extends ItemPredicateBase
{
	private final Tag<Item> tag;
	private final ResourceLocation name;
	private final int stackSize;
	
	public VanillaTagPredicate(String tagName)
	{
		this(tagName, 1);
	}
	
	public VanillaTagPredicate(String tagName, int size)
	{
		this(new ResourceLocation(tagName), size);
	}
	
	public VanillaTagPredicate(ResourceLocation res, int stackSize)
	{
		this(res, HelperOreDict.getOptionalTag(ItemTags.getAllTags(), res), stackSize);
	}
	
	public VanillaTagPredicate(ResourceLocation res, Tag<Item> tag, int stackSize)
	{
		this.tag = tag;
		this.name = res;
		this.stackSize = stackSize;
	}
	
	@Override
	public boolean apply(ItemStack input)
	{
		return input.is(tag);
	}

	@Override
	public ItemStack getRepresentItem()
	{
		if(tag.getValues().isEmpty())
		{
			ItemStack stack = new ItemStack(Blocks.STONE);
			stack.setHoverName(new TextComponent("Item Tag '" + name + "'"));
			return stack;
		}
		Item item = tag.getValues().iterator().next();
		return new ItemStack(item, stackSize);
	}

	@Override
	public int getMinimalItemCount(ItemStack input)
	{
		return stackSize;
	}

	@Override
	public List<ItemStack> collectAcceptedItems(List<ItemStack> list)
	{
		
		
		tag.getValues().stream().map(i -> new ItemStack(i, stackSize)).forEach(list::add);
		
		if(list.isEmpty() && !HelperTags.areTagsLoaded())
		{
			ItemStack stack = new ItemStack(Blocks.STONE);
			stack.setHoverName(new TextComponent("Item Tags are still loading,  '" + name + "'"));
			list.add(stack);
		}
		return list;
	}
	
	@Override
	public String toString()
	{
		return "'"+name+"'@"+stackSize;
	}
	
	public void write(FriendlyByteBuf buf)
	{
		buf.writeUtf(name.toString());
		buf.writeVarInt(stackSize);
	}
	
	public static VanillaTagPredicate read(FriendlyByteBuf buf)
	{
		return new VanillaTagPredicate(buf.readUtf(), buf.readVarInt());
	}
}
