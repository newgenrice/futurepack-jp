package futurepack.depend.api.helper;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Random;
import java.util.WeakHashMap;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.blaze3d.vertex.VertexFormat;
import com.mojang.math.Matrix4f;
import com.mojang.math.Vector3f;

import futurepack.client.render.block.RenderLogistic;
import futurepack.client.render.hologram.TemporaryWorld;
import futurepack.common.block.logistic.frames.BlockManeuverThruster;
import futurepack.depend.api.MiniWorld;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemBlockRenderTypes;
import net.minecraft.client.renderer.LevelRenderer;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.block.BlockRenderDispatcher;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockAndTintGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.client.model.data.IModelData;

public class HelperRenderBlocks
{
	public static void renderBlock(BlockState state, BlockPos pos, BlockAndTintGetter acces, PoseStack matrixStackIn, MultiBufferSource bufferTypeIn)
	{ 
		if(state == null) 
		{
			return;
		}
		BlockRenderDispatcher renderer = Minecraft.getInstance().getBlockRenderer();
		renderBlock(state, renderer, matrixStackIn, bufferTypeIn, net.minecraftforge.client.model.data.EmptyModelData.INSTANCE, acces, pos);
	}
	
	public static void renderBlock(BlockState blockStateIn, BlockRenderDispatcher renderer, PoseStack matrixStackIn, MultiBufferSource bufferTypeIn, IModelData modelData, BlockAndTintGetter reader, BlockPos pos)
	{
		for (net.minecraft.client.renderer.RenderType type : net.minecraft.client.renderer.RenderType.chunkBufferLayers()) 
		{
            if (ItemBlockRenderTypes.canRenderInLayer(blockStateIn, type)) 
            {
               net.minecraftforge.client.ForgeHooksClient.setRenderLayer(type);
               renderBlock(blockStateIn, renderer, matrixStackIn, modelData, reader, pos, bufferTypeIn.getBuffer(type));
            }
		}
		net.minecraftforge.client.ForgeHooksClient.setRenderLayer(null);
	}
	
	public static void renderBlock(BlockState blockStateIn, BlockRenderDispatcher renderer, PoseStack matrixStackIn, IModelData modelData, BlockAndTintGetter reader, BlockPos pos, VertexConsumer builder)
	{
		RenderShape blockrendertype = blockStateIn.getRenderShape();
		
		if (blockrendertype == RenderShape.MODEL)
		{
			BakedModel ibakedmodel = renderer.getBlockModel(blockStateIn);
			
			int pCombinedLight;
			if (reader != null) {
				pCombinedLight = LevelRenderer.getLightColor(reader, pos);
			} else {
				pCombinedLight = 15728880;
			}

			//this is from Falling Sand renderer
//			renderer.getModelRenderer().renderModel(reader, ibakedmodel, blockStateIn, pos, matrixStackIn, builder, false, new Random(), blockStateIn.getSeed(pos), OverlayTexture.NO_OVERLAY, modelData);    
			//renderer.getModelRenderer().renderModel(matrixStackIn.last(), builder, blockStateIn, ibakedmodel, 1.0F, 1.0F, 1.0F, pCombinedLight, OverlayTexture.NO_OVERLAY, modelData);
			
			renderer.getModelRenderer().tesselateBlock(reader, ibakedmodel, blockStateIn, pos, matrixStackIn, builder, false, new Random(), pCombinedLight, pCombinedLight, modelData);
		}
	}
	
	public static void renderBlockSlow(BlockState state, BlockPos at, Level w, PoseStack matrixStack, MultiBufferSource bufferTypeIn)
	{
		//FixME: check how redstone for redstone neon cables is rendered
		
//		GlStateManager.disableLighting();
		w.getProfiler().push("setingRenderMatrix");
		
//		Lighting.setupFor3DItems();
//		GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GlStateManager._enableBlend();
		
//		if (Minecraft.useAmbientOcclusion())
//		{
//			GlStateManager._shadeModel(GL11.GL_SMOOTH);
//		}
//		else
//		{
//			GlStateManager._shadeModel(GL11.GL_FLAT);
//		}
		
		
		matrixStack.translate(-at.getX(), -at.getY(), -at.getZ());
		
		LevelReader acc = new TemporaryWorld(w, at, state);
		
		w.getProfiler().popPush("renderingBlock");
		
		renderBlock(state, at, acc, matrixStack, bufferTypeIn);
		
		w.getProfiler().popPush("restoringRenderMatrix");
		
		matrixStack.translate(at.getX(), at.getY(), at.getZ());
//		Lighting.setupForFlatItems();
//		GlStateManager.enableLighting();
		
		w.getProfiler().pop();
	}
	
	/*public static MiniWorld create(World original, BlockPos start, BlockPos end, Direction face, Vector3d rotpoint)
	{
		MiniWorld w = new MiniWorld(start, end.subtract(start), ((ISeedReader)original).getSeed());
		w.face = face;
		w.rotationpoint = rotpoint;
		
		for(BlockPos pos : (BlockPos.betweenClosed(start, end)))
		{
			w.setObject(w.states, pos, original.getBlockState(pos));
			w.setObject(w.tiles, pos, original.getBlockEntity(pos));
			w.setObject(w.bioms, pos, original.getBiome(pos));
			
			Integer[] red = new Integer[6];
			for(int i=0;i<red.length;i++)
			{
				red[i] = original.getDirectSignal(pos, Direction.from3DDataValue(i));
			}
			w.setObject(w.redstone, pos, red);
			
			w.setObject(w.skylight, pos,  original.getBrightness(LightType.SKY, pos));
			w.setObject(w.blocklight, pos, original.getBrightness(LightType.BLOCK, pos));
			
		}
		return w;
	}*/
	
//	public static void renderWorld(MiniWorld w)
//	{
//		GlStateManager.disableLighting();
//		
//		//matrixStack.translate(0, 1, 0);
//		matrixStack.pushPose();
//		GL11.glRotatef(w.rot, w.face.getXOffset(),  w.face.getYOffset(),  w.face.getZOffset());
//		//matrixStack.translate(0, -1, 0);
////		
////		matrixStack.translate(-w.start.getX(), -w.start.getY(), -w.start.getZ());
//		
//		
//		
//		Minecraft.getInstance().getTextureManager().bindTexture(AtlasTexture.LOCATION_BLOCKS_TEXTURE);
//		Tessellator tes = Tessellator.getInstance();
//		tes.getBuffer().begin(VertexFormat.Mode.QUADS, DefaultVertexFormats.BLOCK);
//		
//		renderFastBase(w, tes.getBuffer(), new Random(w.SEED));
//		
//		tes.draw();
//		
//		matrixStack.popPose();
//		GlStateManager.enableLighting();
//	} 
	
	public static void renderFast(MiniWorld w, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn)
	{
		matrixStackIn.pushPose();
		RenderType type = RenderType.cutout();
		type.setupRenderState();
		
		//TODO: render everything in correct pass so colored glass works
		
		PoseStack baseMatrix = new PoseStack();
		Runnable data = bakeMiniWorld(w, baseMatrix);
		float[] rot = new float[]{w.face.getStepX(),  w.face.getStepY(),  w.face.getStepZ()};
		rot[0]*=Math.toRadians(w.rot);
		rot[1]*=Math.toRadians(w.rot);
		rot[2]*=Math.toRadians(w.rot);
		
		matrixStackIn.mulPose(Vector3f.XP.rotation(rot[0]));
		matrixStackIn.mulPose(Vector3f.YP.rotation(rot[1]));
		matrixStackIn.mulPose(Vector3f.ZP.rotation(rot[2]));
		
		Matrix4f mat = matrixStackIn.last().pose();
//		FloatBuffer fb = MemoryTracker.create(16 * 4).asFloatBuffer();
//		mat.store(fb);
//		GL11.glLoadMatrixf(fb);
		
		RenderSystem.getModelViewMatrix().load(mat);
		
//		RenderSystem.setShaderTexture(0, TextureAtlas.LOCATION_BLOCKS);
		
//		BufferBuilder builder = Tesselator.getInstance().getBuilder();
//				
//		builder.begin(type.mode(), type.format());
//		BufferBuilder.State state = data.getState();
//		builder.restoreState(state);
//		Tesselator.getInstance().end();
		
		data.run();
		
		type.clearRenderState();
		matrixStackIn.popPose();
		RenderSystem.applyModelViewMatrix();
	}
	
	public static void renderFastBase(MiniWorld w, BufferBuilder buf, PoseStack matrixStackIn)
	{
		BlockRenderDispatcher renderer = Minecraft.getInstance().getBlockRenderer();
		
		double dx = -w.start.getX() -w.rotationpoint.x;
		double dy = -w.start.getY() -w.rotationpoint.y;
		double dz = -w.start.getZ() -w.rotationpoint.z;
		
		matrixStackIn.pushPose();
		matrixStackIn.translate(0.5, 0, 0.5);
		matrixStackIn.translate(dx, dy, dz);
		
		ArrayList<BlockPos> vecs = w.validBlocks;
		if(vecs==null)
		{
			vecs = new ArrayList<BlockPos>();
			int bx = w.start.getX();
			int by = w.start.getY();
			int bz = w.start.getZ();
			BlockPos.MutableBlockPos pos = w.start.mutable();
			for(int x=0;x<w.width;x++)
			{
				for(int y=0;y<w.height;y++)
				{
					for(int z=0;z<w.depth;z++)
					{
						pos.set(bx+x, by+y, bz+z);
						
						BlockState b = w.getBlockState(pos);
						if(b.getBlock()==Blocks.AIR)
							continue;
						if(b.isAir())
							continue;
						
						vecs.add(pos.immutable());
						
						//renderBlock(b, renderer, matrixStackIn, combinedLightIn, combinedOverlayIn, net.minecraftforge.client.model.data.EmptyModelData.INSTANCE, w, pos, buf);
					}
				}
			}
			w.validBlocks = vecs;
		}
//		else
//		{
			for(BlockPos vec : vecs)
			{
				BlockState b = w.getBlockState(vec);
				Block bb = b.getBlock();
				if(bb==Blocks.AIR || bb == Blocks.VOID_AIR || bb==Blocks.CAVE_AIR)
					continue;
				if(bb instanceof BlockManeuverThruster)
				{
					b = b.setValue(BlockManeuverThruster.powered, true);
				}
				matrixStackIn.pushPose();
				matrixStackIn.translate(vec.getX(), vec.getY(), vec.getZ());
				renderBlock(b, renderer, matrixStackIn, net.minecraftforge.client.model.data.EmptyModelData.INSTANCE, w, vec, buf);
				matrixStackIn.popPose();
			}
//		}
		matrixStackIn.popPose();
	}
	
	private static WeakHashMap<MiniWorld, Runnable> vertexCashe = new WeakHashMap<MiniWorld, Runnable>();
	
	private static Runnable bakeMiniWorld(MiniWorld w, PoseStack matrixStackIn)
	{
		Runnable runner = null;
		runner = vertexCashe.get(w);
//		if(buffer!=null)
//		{
//			buffer = null;
//		}
		if(runner==null)
		{
			int size = w.width * w.height * w.depth * 6 * DefaultVertexFormat.BLOCK.getIntegerSize() * 4;
			BufferBuilder buffer = new BufferBuilder(size);
			buffer.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.BLOCK);
			
			renderFastBase(w, buffer, matrixStackIn);
			
//			buffer.finishDrawing(); -- removed finish drawing becuase now it works; with finsish drawing buffer#getState breaks()
			
			if(buffer.building())
				buffer.end();
			runner = RenderLogistic.createStaticRenderCall(buffer);
			vertexCashe.put(w, runner);
		}		
		return runner;
	}
	
//	private static void rotateStaticData(BufferBuilder target, BufferBuilder data, float[] rotation, float[] translation)
//	{	
//		if(target.getVertexFormat() != data.getVertexFormat())
//		{
//			return; //this should not happen!
//		}
//		
//		float[] rot = new float[6];
//		rot[0] = (float) Math.sin(rotation[0]);
//		rot[1] = (float) Math.sin(rotation[1]);
//		rot[2] = (float) Math.sin(rotation[2]);
//		
//		rot[3] = (float) Math.cos(rotation[0]);
//		rot[4] = (float) Math.cos(rotation[1]);
//		rot[5] = (float) Math.cos(rotation[2]);
//				
//		if(data.getVertexFormat() == DefaultVertexFormats.BLOCK)
//		{
//			VertexFormat format = data.getVertexFormat();
//			
//			ByteBuffer read = data.getByteBuffer().asReadOnlyBuffer();
//			read.position(0);
//			
//			if(rotation[0]!=0 || rotation[1]!=0 || rotation[2]!=0 || rotation[0]!=0 || rotation[1]!=0 || rotation[2]!=0)
//			{
//				ByteBuffer write = ByteBuffer.allocate(data.getVertexCount() * format.getSize());
//				write.order(ByteOrder.LITTLE_ENDIAN);
//				read.order(ByteOrder.LITTLE_ENDIAN);
//
//				float[] pos = new float[3];
//				
//				write.put(read);
//				
//				for(int i=0;i<data.getVertexCount();i++)
//				{
//					read.position(i*format.getSize());
//					pos[0] = read.getFloat();
//					pos[1] = read.getFloat();
//					pos[2] = read.getFloat();
//					
//					write.position(i * format.getSize());
//					
//					float f1,f2;
//					f1 = pos[1] * rot[3] - pos[2] * rot[0];///this is the roation
//					f2 = pos[1] * rot[0] + pos[2] * rot[3];
//					pos[1]=f1;
//					pos[2]=f2;
//					
//					f1 = pos[0] * rot[4] + pos[2] * rot[1];
//					f2 = -pos[0] * rot[1] + pos[2] * rot[4];
//					pos[0]=f1;
//					pos[2]=f2;
//					
//					f1 = pos[0] * rot[5] - pos[1] * rot[2];
//					f2 = pos[0] * rot[2] + pos[1] * rot[5];
//					pos[0]=f1;
//					pos[1]=f2;
//					
//					pos = translate(pos, translation);
//					
//					write.putFloat(pos[0]);
//					write.putFloat(pos[1]);
//					write.putFloat(pos[2]);
//				}
//				write.order(ByteOrder.BIG_ENDIAN);
//				read.order(ByteOrder.BIG_ENDIAN);
//				write.position(0);
//				
//				insertRenderData(target, write, data.getVertexCount());
//			}
//			else
//			{
//				//byte[] bytes = new byte[read.limit()];
//				//read.get(bytes);
//				
//				insertRenderData(target, read, data.getVertexCount());				
//			}
//			
//			//vboUploader.draw(data); is this is used in renders Correctly
//			
//			return;
//		}
//	}
	
//	private static float[] translate(float[] pos, float[] translation)
//	{
//		pos[0] += translation[0];
//		pos[1] += translation[1];
//		pos[2] += translation[2];
//		return pos;
//	}
//	
//	
//	private static Field f_byteBuffer, f_vertexCount;
//	private static Method m_growBuffer;
//	private static boolean error = false;
//	
//	static
//	{
//		Class<BufferBuilder> cbuf = BufferBuilder.class;
//		
//		f_byteBuffer = DirtyHacks.findField(cbuf, "byteBuffer", "buffer", ByteBuffer.class);
//		f_vertexCount = DirtyHacks.findField(cbuf, "vertexCount", "vertices", int.class);
//		f_byteBuffer.setAccessible(true);
//		f_vertexCount.setAccessible(true);
//		
//		m_growBuffer = DirtyHacks.findMethod(cbuf, "growBuffer", "ensureCapacity", int.class);
//		m_growBuffer.setAccessible(true);
//	}
//	
//	public static void insertRenderData(BufferBuilder target, ByteBuffer data, int vertexCount)
//	{
//		if(!error)
//		{
//			try
//			{
//				target.putBulkData(data);
//			}
//			catch(NoSuchMethodError e)
//			{
//				error = true;
//				Minecraft.getInstance().player.sendMessage(new StringTextComponent("You are propably using an outdated forge, or optifine. Please update." ));
//			}
//		}
//		else
//		{
//			try {
//				m_growBuffer.invoke(target, data.limit());
//				
//				ByteBuffer renderBuf = (ByteBuffer) f_byteBuffer.get(target);
//				int vertecies = target.getVertexCount();
//				
//				VertexFormat format = target.getVertexFormat();
//				int pos = vertecies * format.getSize();
//				
//				renderBuf.position(pos);
//				renderBuf.put(data);
//				
//				f_vertexCount.set(target, vertecies + vertexCount);
//				
//			} catch (IllegalArgumentException e) {
//				e.printStackTrace();
//			} catch (IllegalAccessException e) {
//				e.printStackTrace();
//			} catch (InvocationTargetException e) {
//				e.printStackTrace();
//			}
//		}
//	}
	
	
	private static <T> void deepCompare(Class<T> clazz, T c1, T c2, int sub) throws Exception
	{
		Field[] flds = clazz.getDeclaredFields();
		for(Field f : flds)
		{
			f.setAccessible(true);
			
			Object o1 = f.get(c1);
			Object o2 = f.get(c2);
			
			boolean dif = false;
			
			if(o1!=null && o2!=null)
			{
				dif = !o1.equals(o2);
			}
			else
			{
				dif = o1!=null || o2!=null;
			}
			String s = "";
			for(int i=0;i<sub;i++)
				s += "\t";
				
			if(dif)
			{		
				s += "Field: " + f + " 1: '" + o1 + "' is not 2: '" + o2+"'";
				System.out.println(s);
				if(o1 instanceof ByteBuffer)
				{
					compareByteBuffer((ByteBuffer)o1, (ByteBuffer)o2);
				}
				else
				{
					deepCompare((Class)f.getType(), o1, o2, sub+1);
				}
			}
			else
			{
				System.out.println(s+ "Same " + f+ "\t " + o1);
			}
		}
	}
	
	private static <T> void deepCompare(Class<T> clazz, T c1, T c2) throws Exception
	{
		deepCompare(clazz,c1,c2,0);
	}
	
	private static void compareByteBuffer(ByteBuffer buf1, ByteBuffer buf2)
	{
		buf1.position(0);
		buf2.position(0);
		
		if(buf1.limit() != buf2.limit())
		{
			System.out.println("Buffers got different Limits: " + buf1.limit() + " and " + buf2.limit());
			return;
		}
		for(int i=0;i<buf1.limit();i++)
		{
			byte b1 = buf1.get();
			byte b2 = buf2.get();
			
			if(b1!=b2)
			{
				System.out.println("Byte mismatch at " + i + ", got " + b1 + " and " + b2);
			}
		}
	}
}

