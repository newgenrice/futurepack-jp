package futurepack.depend.api.helper;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import futurepack.api.FacingUtil;
import futurepack.api.PacketBase;
import futurepack.api.interfaces.INetworkUser;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import futurepack.common.FPLog;
import futurepack.common.block.modification.TileEntityModificationBase;
import futurepack.common.modification.EnumChipType;
import futurepack.common.network.FunkPacketExperienceDistribution;
import futurepack.common.network.NetworkManager;
import futurepack.depend.api.StableConstants;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.NbtIo;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.Container;
import net.minecraft.world.WorldlyContainer;
import net.minecraft.world.entity.ExperienceOrb;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeManager;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.item.crafting.SmeltingRecipe;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemHandlerHelper;
import net.minecraftforge.items.wrapper.InvWrapper;
import net.minecraftforge.items.wrapper.SidedInvWrapper;

public class HelperInventory 
{

	public static class SlotContent
	{
		public IItemHandler inv;
		public int slot;
		public ItemStack item;
		public ItemEntity entity;
		
		public SlotContent(IItemHandler p1, int p2, ItemStack p3, ItemEntity p4)
		{
			inv = p1;
			slot = p2;
			item = p3.copy();
			entity = p4;
			if(p3.isEmpty())
			{
				throw new NullPointerException("ItemStack cant be Empty!");
			}
		}
		
		public SlotContent(ItemEntity item)
		{
			this(null, -1, item.getItem(), item);
		}
		
		public SlotContent(IItemHandler inv, int slot)
		{
			this(inv, slot, inv.getStackInSlot(slot), null);
		}
		
		public void remove()
		{
			if(inv!=null)
			{
				inv.extractItem(slot, item.getCount(), false);
			}
			if(entity!=null)
			{
				entity.getItem().shrink(item.getCount());
				if(entity.getItem().getCount() <= 0)
				{
					entity.discard();
				}			
			}
		}
		
		public void updateSrc()
		{
			if(inv!=null)
			{
				ItemStack item = inv.getStackInSlot(slot);
				if(item!=null && item.getCount() > this.item.getCount())
				{
					inv.extractItem(slot, item.getCount() - this.item.getCount(), false);
				}
			}
		}
		
		@Override
		public String toString()
		{
			return item + " in " + (entity!=null ? entity.toString() : inv + "@"+slot);
		}
	}
	
	
	private static void tryInsert(Container inv, ItemStack it)
	{
		
	}

	public static boolean areItemsEqualNoSize(ItemStack it1, ItemStack it2) 
	{
		return it1.sameItem(it2) && ItemStack.tagMatches(it1, it2);
	}
	
	/**This method fills the items given by <b>items</b> into the <b>inventory</b> starting at the <b>start</b> index and only use the slots given through <b>length</b>
	 * @return am ItemStack[] with unplaceable itmes
	 */
	public static ItemStack[] placeSomewhereOverfillingItems(NonNullList<ItemStack> inventory, int start, int length, ItemStack[] items, int maxinv)
	{
		ArrayList<ItemStack> list = new ArrayList<ItemStack>(items.length);
		for(ItemStack it : items)
		{
			if(it==null || it.isEmpty())
				continue;
			ItemStack result = placeInInventory(inventory, start, length, it, maxinv);
			if(result!=null && !result.isEmpty())
			{
				list.add(result);
			}
		}
		
		if(list.isEmpty())
			return null;
		else
			return list.toArray(new ItemStack[list.size()]);
	}
	
	@Nullable
	public static ItemStack placeInInventory(NonNullList<ItemStack> inventory, int start, int length, ItemStack it, int maxinv)
	{
		it = it.copy();
		boolean innull = false;
		for(int i=start;i<length;i++)
		{
			if(inventory.get(i).isEmpty() && innull)
			{
				int max = it.getMaxStackSize();
				int size = it.getCount();				
				int change = Math.min(size, Math.min(max, maxinv));
				
				if(size > change)
				{
					inventory.set(i, it.copy());
					inventory.get(i).setCount(change);
					it.shrink(change);
				}
				else
				{
					inventory.set(i, it);
					return null;
				}
			}
			else if(!inventory.get(i).isEmpty())
			{
				if(areItemsEqualNoSize(inventory.get(i), it))
				{
					int max = it.getMaxStackSize();
					int size = inventory.get(i).getCount() + it.getCount();				
					int change = Math.min(size, Math.min(max, maxinv));
					
					if(size > change)
					{
						int dif = change - inventory.get(i).getCount();
						inventory.get(i).setCount(change);
						it.shrink(change);
					}
					else
					{
						inventory.get(i).setCount(change);
						return null;
					}
				}
			}
			
			if(it.getCount()<=0)
				return null;
			if(!innull && i+1==length)
			{
				i=start;
				innull = true;
			}
		}
		
		return null;
	}
	
	@Nullable
	public static IItemHandler getHandler(BlockEntity tile, Direction face)
	{
		if(tile==null)
		{	
			return null;
		}	
		else
		{
			LazyOptional<IItemHandler> opt = tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, face);
			return opt.orElseGet(() -> 
			{
				if(tile instanceof WorldlyContainer)
				{
					return new SidedInvWrapper((WorldlyContainer) tile, face);
				}
				else if(tile instanceof Container)
				{
					return new InvWrapper((Container) tile);
				}
				return null;//Yes a nonnull supplier returning null, seems wrong but it is right :P
			});
		}
	}
	
	/**
	 * 
	 * @param tile the TilEntity to inser tinto
	 * @param jkl the Block were the Items get inserted
	 * @param face the face from which the Items get inserted
	 * @param items the items getting inserted
	 * @return a List of the removed items
	 */
	public static List<SlotContent> insertItems(BlockEntity tile, Direction face, List<SlotContent> items)
	{
		ArrayList<SlotContent> removed = new ArrayList<HelperInventory.SlotContent>();
		IItemHandler handler = getHandler(tile, face);
		if(handler!=null)
		{
			for(SlotContent cont : items)
			{
				ItemStack item = ItemHandlerHelper.insertItem(handler, cont.item, false);
				if(!item.isEmpty())
				{
					//System.out.println("There could be something wrong happend with the Items at " + jkl);
					
					ItemStack original = cont.item.copy();
					
					cont.item = original.copy();
					cont.item.setCount(item.getCount()); //change SlotContent to what is left amount
					
					ItemStack cpy = original.copy();
					cpy.shrink(item.getCount()); //cpy is inserted amount
					if(cpy.getCount() > 0)
					{
						removed.add(new SlotContent(cont.inv, cont.slot, cpy, cont.entity));
					}		
				}
				else
				{
					removed.add(cont);
				}
			}
		}
		items.removeAll(removed);
		
		
		return removed;
	}
	
	/**
	 * 
	 * @param w the World
	 * @param jkl the Block were the Items get inserted
	 * @param face the face from which the Items get inserted
	 * @param items the items getting inserted
	 * @return a List of the removed items
	 */
	public static List<SlotContent> insertItems(Level w, BlockPos jkl, Direction face, List<SlotContent> items)
	{
		return insertItems(w.getBlockEntity(jkl), face, items);
	}
	
	public static List<SlotContent> ejectItemsIntoWorld(Level w, BlockPos jkl, List<SlotContent> items)
	{
		ArrayList<SlotContent> removed = new ArrayList<HelperInventory.SlotContent>();	
		if(w.isEmptyBlock(jkl))
		{
			for(SlotContent slot : items)
			{
				if(!w.isClientSide)
				{
					ItemStack it = slot.item;
					ItemEntity item = new ItemEntity(w, jkl.getX()+0.5,  jkl.getY()+0.5,  jkl.getZ()+0.5, it);
					w.addFreshEntity(item);
				}
				removed.add(slot);
			}
			items.removeAll(removed);
		}
		return removed;
	}
	
	public static void transferItemStacks(IItemHandler from, IItemHandler to)
	{
		for(int i=0;i<from.getSlots();i++)
		{
			ItemStack stack = from.extractItem(i, 64, true);
			if(!stack.isEmpty())
			{
				ItemStack remain = ItemHandlerHelper.insertItem(to, stack, true);
				if(remain == stack)
				{
					continue;
				}
				else if(remain.isEmpty())
				{
					stack = from.extractItem(i, 64, false);
					remain = ItemHandlerHelper.insertItem(to, stack, false);
					if(!remain.isEmpty())
					{
						FPLog.logger.error("An ItemStack got deleted! Could not insert %s into %s", remain, to);
					}
				}
				else
				{
					stack = from.extractItem(i, stack.getCount() - remain.getCount(), false);
					if(stack.getCount()>0)
					{
						remain = ItemHandlerHelper.insertItem(to, stack, false);
						if(!remain.isEmpty())
						{
							FPLog.logger.error("An ItemStack got deleted! Could not insert %s into %s", remain, to);
						}
					}
				}
			}
		}
		
	}

	public static void placeNBT(Level w, BlockPos pos, LivingEntity liv, ItemStack it)
	{
		if(it.hasTag() && it.getTag().contains("tile"))
        {
        	BlockEntity tile = w.getBlockEntity(pos);
        	if(tile != null)
        	{
        		CompoundTag nbt = it.getTag().getCompound("tile");
        		nbt.putInt("x", pos.getX());
        		nbt.putInt("y", pos.getY());
        		nbt.putInt("z", pos.getZ());
        		tile.load(nbt);
        	}       	
        }
	}
	
	public static void dropNBT(Level w, BlockPos pos, BlockState state)
	{
		if(w.restoringBlockSnapshots)
			return;
		
		BlockEntity tile =w.getBlockEntity(pos);
		if(tile==null)
			return;
					
		BlockState currentState = w.getBlockState(pos);
		
		if(tile instanceof Container)
		{
			Container inv = (Container) tile;
			for(int i=0;i<inv.getContainerSize();i++)
			{
				ItemStack it = inv.getItem(i);
				if(it!=null && it.hasTag() && shouldDrop(it.getTag()))
				{
					inv.setItem(i, ItemStack.EMPTY);
					ItemEntity eitem = new ItemEntity(w, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5,it);
					w.addFreshEntity(eitem);
				}
			}
		}
		else
		{
			IItemHandler handler = HelperInventory.getHandler(tile, Direction.DOWN);
			if(handler!=null)
			{
				for(int i=0;i<handler.getSlots();i++)
				{
					ItemStack it = handler.getStackInSlot(i);
					if(it!=null && it.hasTag() && shouldDrop(it.getTag()))
					{
						if(handler instanceof IItemHandlerModifiable)
						{
							((IItemHandlerModifiable) handler).setStackInSlot(i, ItemStack.EMPTY);
							ItemEntity eitem = new ItemEntity(w, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5,it);
							w.addFreshEntity(eitem);
						}
						else
						{
							ItemStack is = handler.extractItem(i, it.getCount(), false);
							if(is!=null)
							{
								ItemEntity eitem = new ItemEntity(w, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5,is);
								w.addFreshEntity(eitem);
							}
						}						
					}
				}
			}
		}
		
		BlockEntity tile2 = ((EntityBlock)state.getBlock()).newBlockEntity(pos, state);
		if(tile2 == null)
		{
			System.out.printf("Old: %s %s \tNew: %s %s\n ", tile, state, tile2, currentState);
			return;
		}
		tile2.setLevel(w);
		
		ItemStack it = new ItemStack(state.getBlock(), 1);
		CompoundTag nbt = new CompoundTag();
		tile.save(nbt);
		
		CompoundTag copy = new CompoundTag();
		tile2.save(copy);
		if(!nbt.equals(copy))
		{
			it.setTag(new CompoundTag());
			it.getTag().put("tile", nbt);
		}
		ItemEntity eitem = new ItemEntity(w, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5,it);
		w.addFreshEntity(eitem);
		
		//cleaning TileEntity
		tile.load(copy);
	}
	
	private static boolean shouldDrop(CompoundTag nbt)
	{
		int maxSize = 38836; //max byte size per item
		
		ByteArrayOutputStream out = new ByteArrayOutputStream(maxSize);
		DataOutputStream dout = new DataOutputStream(out);
		try
		{
			NbtIo.write(nbt, dout);
			dout.flush();
			dout.close();
			
			return maxSize < out.size();
		}
		catch (IOException e) {
			e.printStackTrace();
		}	
		
		return nbt.contains("tile");
	}

	public static void storeInventory(String name, CompoundTag nbt, Container filter_items) 
	{
		ListTag nbtTagList = new ListTag();
        for (int i = 0; i < filter_items.getContainerSize(); i++)
        {
            if (!filter_items.getItem(i).isEmpty())
            {
                CompoundTag itemTag = new CompoundTag();
                itemTag.putInt("Slot", i);
                filter_items.getItem(i).save(itemTag);
                nbtTagList.add(itemTag);
            }
        }
        nbt.put(name, nbtTagList);
	}
	
	public static void loadInventory(String name, CompoundTag nbt, Container filter_items) 
	{
		ListTag tagList = nbt.getList(name, StableConstants.NBT.TAG_COMPOUND);
		for (int i = 0; i < tagList.size(); i++)
		{
			CompoundTag itemTags = tagList.getCompound(i);
			int slot = itemTags.getInt("Slot");

			if (slot >= 0 && slot < filter_items.getContainerSize())
			{
				filter_items.setItem(slot, ItemStack.of(itemTags));
			}
		}
	}

	public static void doItemExtract(TileEntityModificationBase base)
	{
		if(base.getChipPower(EnumChipType.TRANSPORT)>0)
		{
			for(Direction face : FacingUtil.VALUES)
			{	
				IItemHandler inv = getHandler(base, face);
				if(inv != null )
				{
					float xp = base.getChipPower(EnumChipType.ULTIMATE) * 0.5F;
					boolean netty = base.getChipPower(EnumChipType.NETWORK)>0;
					HelperInventory.tryExtractIntoInv(base.getLevel(), base.getBlockPos(), face, inv, xp, netty);
				}
			}
		}
	}

	public static int[] getOpenSlots(Container inv, Direction face)
	{
		int[] slots;
		if(inv instanceof WorldlyContainer)
		{
			slots = ((WorldlyContainer) inv).getSlotsForFace(face);
		}
		else
		{
			slots = new int[inv.getContainerSize()];
			for(int i=0;i<slots.length;i++)
			{
				slots[i] = i;
			}
		}	
		return slots;
	}

	public static void tryExtractIntoInv(final Level w, BlockPos pos, Direction face, IItemHandler invBase, float doXP, boolean netty)
	{
		tryExtractIntoInv(w, pos, face, invBase, doXP, netty, invBase.getSlots());
	}
	public static void tryExtractIntoInv(final Level w, BlockPos pos, Direction face, IItemHandler invBase, float doXP, boolean netty, int stacks)
	{
		if(w.isClientSide)
			return; //do not do something on the Client; See ISSUE #534
		BlockEntity ent = w.getBlockEntity(pos.relative(face));
		if(ent!=null)
		{
			IItemHandler handler = getHandler(ent, face.getOpposite());
			if(handler==null)
				return;
			
			for(int j=0;j<invBase.getSlots();j++)
			{
				int i = (w.random.nextInt(invBase.getSlots()) + j) % invBase.getSlots();
				ItemStack it;
				try
				{
					it = invBase.extractItem(i, 64, true);
				}
				catch(NullPointerException e)
				{
					FPLog.logger.warn("Another Block crashed while interacting with it, breaking this block to avoid crashes!");
					FPLog.logger.catching(org.apache.logging.log4j.Level.WARN, e);
					w.destroyBlock(ent.getBlockPos(), true);
					return;
				}
				if(it!=null && !it.isEmpty())
				{			
					if(stacks<=0)
						return;
					
					stacks--;
					
					float f = 0;
					ItemStack rest;
					try
					{
						rest = ItemHandlerHelper.insertItem(handler, it, true);
					}
					catch(NullPointerException e)
					{
						FPLog.logger.warn("Another Block crashed while interacting with it, breaking this block to avoid crashes!");
						FPLog.logger.catching(org.apache.logging.log4j.Level.WARN, e);
						w.destroyBlock(ent.getBlockPos(), true);
						return;
					}
					if(rest==null || rest.isEmpty())
					{
						it = invBase.extractItem(i, 64, false);
						f = getExpFromItem(w.getRecipeManager(), it::sameItem) + doXP;
						f *= it.getCount();
						rest = ItemHandlerHelper.insertItem(handler, it, false);
					}
					else if(rest.getCount() < it.getCount())
					{
						it = invBase.extractItem(i, it.getCount() - rest.getCount(), false);
						f = getExpFromItem(w.getRecipeManager(), it::sameItem) + doXP;
						f *= it.getCount();
						rest = ItemHandlerHelper.insertItem(handler, it, false);	
						rest = ItemHandlerHelper.insertItem(invBase, rest, false);	
					}
					else
					{
						rest = ItemStack.EMPTY;
					}
					
					if(doXP>0 && f>0)
					{
						int val = (int) f;
						if(f>val)
							val++;
						
						if(netty)
						{
							FunkPacketExperienceDistribution exp = new FunkPacketExperienceDistribution(pos, new ITileNetwork()
							{
								@Override
								public void onFunkPacket(PacketBase pkt) { }
								
								@Override
								public boolean isWire()
								{
									return false;
								}
								
								@Override
								public boolean isNetworkAble()
								{
									return true;
								}
							}, val);
							if(!sendPacket(w, pos, exp))
							{
								Vec3 vec = Vec3.atLowerCornerOf(pos.relative(face)).add(0.5,0.5,0.5);
								ExperienceOrb orb = new ExperienceOrb(w, vec.x, vec.y, vec.z, val);
								w.addFreshEntity(orb);
							}
						}
						else
						{
							Vec3 vec = Vec3.atLowerCornerOf(pos.relative(face)).add(0.5,0.5,0.5);
							ExperienceOrb orb = new ExperienceOrb(w, vec.x, vec.y, vec.z, val);
							w.addFreshEntity(orb);
						}
					}
					
					if(!rest.isEmpty())
					{
						FPLog.logger.warn("Item transfer ended in Item loss! Lost " + rest);
					}
				}
			}
		}
	}

	public static float getExpFromItem(RecipeManager manger, Predicate<ItemStack> pred)
	{
		Optional<SmeltingRecipe> recipe = manger.byType(RecipeType.SMELTING).values().stream().filter(r -> pred.test(r.getResultItem())).map(r -> (SmeltingRecipe)r).findAny();
		if(recipe.isPresent())
		{
			return recipe.get().getExperience();
		}
		else
		{
			return 0F;
		}
	}

	public static boolean sendPacket(final Level w, BlockPos pos, PacketBase exp)
	{
		for(Direction direction : FacingUtil.VALUES)
	    {
			
			
			final BlockPos xyz = pos.relative(direction);
	    	BlockEntity t = w.getBlockEntity(xyz);
	    	if(t instanceof ITileNetwork && ((ITileNetwork) t).isNetworkAble())
	    	{
	    		NetworkManager.sendPacketThreaded(new INetworkUser()
	        	{				
					@Override
					public void postPacketSend(PacketBase pkt)
					{
						if(pkt instanceof FunkPacketExperienceDistribution)
						{
							int left = ((FunkPacketExperienceDistribution) pkt).XP;
							if(left>0)
							{
								Vec3 vec = Vec3.atLowerCornerOf(xyz).add(0.5,0.5,0.5);
								ExperienceOrb orb = new ExperienceOrb(w, vec.x, vec.y, vec.z, left);
								if(!w.isClientSide)
								{
									((ServerLevel)w).getServer().submitAsync(() -> w.addFreshEntity(orb));
								}
								
							}
							
						}
					}
					
					@Override
					public Level getSenderWorld()
					{
						return w;
					}
					
					@Override
					public BlockPos getSenderPosition()
					{
						return xyz;
					}
				}, exp);
	    		return true;
	    	}       	
	    }
		return false;
	}

	public static boolean canStack(ItemStack it1, ItemStack it2)
	{
		return it1!=null && it2!=null && areItemsEqualNoSize(it1, it2);
	}
}
