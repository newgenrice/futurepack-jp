package futurepack.depend.api.helper;

import futurepack.api.interfaces.IItemNeon;
import futurepack.api.interfaces.IItemSupport;
import futurepack.common.block.ItemMoveTicker;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;

public class HelperItems 
{
	public static Component getTooltip(ItemStack it, IItemNeon neon)
	{
		return new TranslatableComponent("tooltip.futurepack.item.neon", neon.getNeon(it), neon.getMaxNeon(it));
	}
	
	public static Component getTooltip(ItemStack it, IItemSupport sp)
	{
		return new TranslatableComponent("tooltip.futurepack.item.support", sp.getSupport(it), sp.getMaxSupport(it));
	}

	public static void moveItemTo(Level world, Vec3 destination, Vec3 itemPos) 
	{
		if(world.isClientSide)
			throw new IllegalStateException("This method is not allowed on Clients!");

		new ItemMoveTicker(world, destination, itemPos);
	}
	
	public static void disableItemSpawn()
	{
		HelperEntities.disableItemSpawn();
	}
	
	public static void enableItemSpawn()
	{
		HelperEntities.enableItemSpawn();
	}
}
