package futurepack.depend.api.helper;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.Property;
import net.minecraftforge.registries.ForgeRegistries;


public class HelperHologram
{	
//	/**
//	 * @param A TileEntity implementing IHologramAble
//	 */
//	
//	public static void renderHologram(BlockEntity t)
//	{
//		HologramClient.renderHologram(t);
//	}
	
//	/**
//	 * Uses Tesselator to render Hologram
//	 * 
//	 * @param tile
//	 * @param x
//	 * @param y
//	 * @param z
//	 * @param buf this must be the {@link net.minecraft.client.renderer.BufferBuilder}
//	 */
//	public static void renderHologramFAST(BlockEntity tile, double x, double y, double z, Object buf)
//	{
//		HologramClient.renderHologramFAST(tile, x, y, z, (BufferBuilder) buf);
//	}
//	
	public static boolean isHologramDebug()
	{
		return HologramClient.isHologramDebug();
	}
	
//	public static boolean hasFastRenderer(ITileHologramAble holo)
//	{
//		return HologramClient.hasFastRenderer(holo);
//	}
	
	public static void saveInItem(ItemStack it, BlockState state)
	{
		if(!it.hasTag())
		{
			it.setTag(new CompoundTag());
		}
		
		it.getTag().put("hologram", toNBT(state));
	}
	
	public static BlockState loadFormItem(ItemStack it)
	{
		if(!it.hasTag())
		{
			return null;
		}
		
		CompoundTag tag = it.getTag().getCompound("hologram");
		
		if(tag==null)
		{
			return null;
		}
		
		return fromNBT(tag);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static CompoundTag toNBT(BlockState state)
	{
		CompoundTag nbt = new CompoundTag();
		nbt.putString("res", state.getBlock().getRegistryName().toString());
		
		ImmutableMap<Property<?>, Comparable<?>> map = state.getValues();
		ImmutableSet<Entry<Property<?>, Comparable<?>>> set = map.entrySet();
		for(Entry<Property<?>, Comparable<?>> e : set)
		{
			Property p = e.getKey();
			nbt.putString(p.getName(), p.getName(e.getValue()));
		}
		
		return nbt;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static BlockState fromNBT(CompoundTag nbt)
	{
		Block b = ForgeRegistries.BLOCKS.getValue(new ResourceLocation(nbt.getString("res")));
	
		BlockState state = b.defaultBlockState();
		
		ImmutableMap<Property<?>, Comparable<?>> map = state.getValues();//func_177228_b
		ImmutableSet<Entry<Property<?>, Comparable<?>>> set = map.entrySet();
		for(Entry<Property<?>, Comparable<?>> e : set)
		{
			
			Property p = e.getKey();
			
			if(nbt.contains(p.getName()))
			{
				String val = nbt.getString(p.getName());
				if(val!=null)
				{
					Collection<Comparable> cc = p.getPossibleValues();//getPossibleValues
					for(Comparable ca : cc)
					{
						if(val.equals(p.getName(ca)))
						{
							state = state.setValue(p, ca);
							break;
						}
					}
				}			
			}
		}
		
		return state;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static String toStateString(BlockState state)
	{
		StringBuilder build = new StringBuilder(state.getBlock().getRegistryName().toString());
		build.append(':');
		
		ImmutableMap<Property<?>, Comparable<?>> map = state.getValues();
		ImmutableSet<Entry<Property<?>, Comparable<?>>> set = map.entrySet();
		boolean needDelimer = false;
		for(Entry<Property<?>, Comparable<?>> e : set)
		{
			if(needDelimer)
				build.append(';');
			
			Property p = e.getKey();
			build.append(p.getName()+"="+p.getName(e.getValue()));
			needDelimer = true;
		}
		return build.toString();
	}
	
	public static BlockState fromStateString(String s)
	{
		String[] parts = s.toLowerCase().split(":");
		if(parts.length==2)
		{
			ResourceLocation res = new ResourceLocation(parts[0], parts[1]);
			Block bl = ForgeRegistries.BLOCKS.getValue(res);
			if(!bl.getRegistryName().equals(res))
				throw new IllegalArgumentException("Block " + res +" not found, got " + bl.getRegistryName());
			
			return bl.defaultBlockState();
		}
		else if(parts.length==3)
		{
			ResourceLocation res = new ResourceLocation(parts[0], parts[1]);
			Block bl = ForgeRegistries.BLOCKS.getValue(res);
			if(!bl.getRegistryName().equals(res))
				throw new IllegalArgumentException("Block " + res +" not found, got " + bl.getRegistryName());
			
			BlockState state = bl.defaultBlockState();
			String[] props = parts[2].split(";");
			Map<String, String> entrys = new TreeMap<>();
			for(String setrys : props)
			{
				String[] keymap = setrys.split("=", 2);
				entrys.put(keymap[0], keymap[1]);
			}
			
			ImmutableMap<Property<?>, Comparable<?>> map = state.getValues();//func_177228_b
			ImmutableSet<Entry<Property<?>, Comparable<?>>> set = map.entrySet();
			
			for(Entry<Property<?>, Comparable<?>> e : set)
			{
				Property p = e.getKey();
				
				String val = entrys.getOrDefault(p.getName(), null);
				if(val!=null)
				{
					Collection<Comparable> cc = p.getPossibleValues();//getPossibleValues
					for(Comparable ca : cc)
					{
						if(val.equals(p.getName(ca)))
						{
							state = state.setValue(p, ca);
							entrys.remove(p.getName());
							break;
						}
					}		
				}
			}
			return state; 
		}
		else
		{
			throw new IllegalArgumentException("Unsupported BlockState " + s);
		}
	}

}
