package futurepack.extensions.jei.recycler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import futurepack.api.ItemPredicateBase;
import futurepack.common.recipes.recycler.IRecyclerRecipe;
import net.minecraft.world.item.ItemStack;

public class RecyclerJeiFakeRecipe implements IRecyclerRecipe 
{
	ItemStack tool;
	public ArrayList<ItemStack> input;
	public ArrayList<ItemStack> output;
	String text;
	
	public RecyclerJeiFakeRecipe(ItemStack tool, ArrayList<ItemStack> input, ArrayList<ItemStack> output, String text) 
	{
		this.tool = tool;
		this.input = input;
		this.output = output;
		this.text = text;
	}
	
	@Override
	public ItemStack[] getMaxOutput() 
	{
		return null;
	}

	@Override
	public ItemPredicateBase getInput()
	{
		return null;
	}
	
	public ArrayList<ItemStack> getInputs() 
	{
		return input;
	}

	@Override
	public List<ItemStack> getToolItemStacks() 
	{
		return Collections.singletonList(tool);
	}

	@Override
	public float[] getChances() 
	{
		return null;
	}

	@Override
	public String getJeiInfoText() 
	{
		return text;
	}

	public List<ItemStack> getOutputs() 
	{
		return output;
	}

}
