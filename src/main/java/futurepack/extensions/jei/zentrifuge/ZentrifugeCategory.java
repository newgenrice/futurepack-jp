package futurepack.extensions.jei.zentrifuge;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.common.block.modification.ModifiableBlocks;
import futurepack.common.recipes.centrifuge.ZentrifugeRecipe;
import futurepack.extensions.jei.BaseRecipeCategory;
import futurepack.extensions.jei.FuturepackUids;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.drawable.IDrawableAnimated;
import mezz.jei.api.gui.drawable.IDrawableStatic;
import mezz.jei.api.gui.ingredient.IGuiItemStackGroup;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;

public class ZentrifugeCategory extends BaseRecipeCategory<ZentrifugeRecipe>
{
	private IDrawableAnimated arrow;
	
	public ZentrifugeCategory(IGuiHelper gui)
	{
		super(gui, ModifiableBlocks.zentrifuge_w, FuturepackUids.ZENTRIFUGE, 90-24, 55-8);
	}
	
	@Override
	public void draw(ZentrifugeRecipe recipe, PoseStack matrixStack, double mouseX, double mouseY) 
	{
		arrow.draw(matrixStack, 91-24, 59-8);
		Minecraft.getInstance().font.draw(matrixStack, I18n.get("futurepack.jei.neededsupport",recipe.getNeededSupport()), 60, 2, Color.gray.getRGB());
		super.draw(recipe, matrixStack, mouseX, mouseY);
	}

	@Override
	public Class<? extends ZentrifugeRecipe> getRecipeClass() 
	{
		return ZentrifugeRecipe.class;
	}

	@Override
	public void setIngredients(ZentrifugeRecipe rec, IIngredients ingredients) 
	{
		List<ItemStack> l = rec.getInput().collectAcceptedItems(new ArrayList<>());
		ingredients.setInputLists(VanillaTypes.ITEM, Collections.singletonList(l));
		
		List<ItemStack> outputs = new ArrayList<ItemStack>();
		for(ItemStack it:rec.getOutput())
		{
			outputs.add(it);
		}
		ingredients.setOutputs(VanillaTypes.ITEM, outputs);
	}

	@Override
	public void setRecipe(IRecipeLayout recipeLayout, ZentrifugeRecipe recipe, IIngredients ingredients) 
	{
		IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();

		guiItemStacks.init(0, true, 61-24, 33-8);
		
		guiItemStacks.init(1, false, 124-24, 8-8);
		guiItemStacks.init(2, false, 133-24, 33-8);	
		guiItemStacks.init(3, false, 124-24, 58-8);
		
		guiItemStacks.set(ingredients);
	}

	@Override
	protected IDrawable createBackground(IGuiHelper gui) 
	{
		ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/zentrifuge.png");
		IDrawableStatic draw = gui.createDrawable(res, 176, 0, 24, 17);
		arrow = gui.createAnimatedDrawable(draw, 40, IDrawableAnimated.StartDirection.LEFT, false);
		return gui.createDrawable(res, 24, 8, 127, 68);
	}

	@Override
	public boolean isResearched(ZentrifugeRecipe rec) 
	{
		return rec.isLocalResearched();
	}

}
