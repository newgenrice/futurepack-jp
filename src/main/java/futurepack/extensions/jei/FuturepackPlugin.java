package futurepack.extensions.jei;

import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import futurepack.api.Constants;
import futurepack.common.FPLog;
import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.common.block.inventory.WaterCoolerManager;
import futurepack.common.block.modification.ModifiableBlocks;
import futurepack.common.gui.inventory.GuiAssemblyRezeptCreator;
import futurepack.common.gui.inventory.GuiAssemblyTable;
import futurepack.common.gui.inventory.GuiConstructionTable;
import futurepack.common.gui.inventory.GuiCrusher;
import futurepack.common.gui.inventory.GuiIndFurnace;
import futurepack.common.gui.inventory.GuiIndNeonFurnace;
import futurepack.common.gui.inventory.GuiTechTable;
import futurepack.common.gui.inventory.GuiZentrifuge;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.recipes.ISyncedRecipeManager;
import futurepack.common.recipes.RecipeManagerSyncer;
import futurepack.common.recipes.airbrush.AirbrushRegistry;
import futurepack.common.recipes.crafting.ShapedRecipeWithResearch;
import futurepack.common.recipes.crafting.ShapelessRecipeWithResearch;
import futurepack.extensions.jei.airbrush.AirbrushCategory;
import futurepack.extensions.jei.assembly.AssemblyCategory;
import futurepack.extensions.jei.crafting.ShapedResearchCraftingExtension;
import futurepack.extensions.jei.crafting.ShapelessResearchCraftingExtension;
import futurepack.extensions.jei.crusher.CrusherCategory;
import futurepack.extensions.jei.indfurnace.IndustrialCategory;
import futurepack.extensions.jei.indneonfurnace.IndNeonCategorie;
import futurepack.extensions.jei.partpress.PartPressCategory;
import futurepack.extensions.jei.recycler.RecyclerCategory;
import futurepack.extensions.jei.watercooler.WaterCoolerCategory;
import futurepack.extensions.jei.zentrifuge.ZentrifugeCategory;
import mezz.jei.api.IModPlugin;
import mezz.jei.api.JeiPlugin;
import mezz.jei.api.constants.VanillaRecipeCategoryUid;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.helpers.IJeiHelpers;
import mezz.jei.api.recipe.IRecipeManager;
import mezz.jei.api.recipe.category.extensions.IExtendableRecipeCategory;
import mezz.jei.api.recipe.category.extensions.vanilla.crafting.ICraftingCategoryExtension;
import mezz.jei.api.registration.IRecipeCatalystRegistration;
import mezz.jei.api.registration.IRecipeCategoryRegistration;
import mezz.jei.api.registration.IRecipeRegistration;
import mezz.jei.api.registration.IRecipeTransferRegistration;
import mezz.jei.api.registration.IVanillaCategoryExtensionRegistration;
import mezz.jei.api.runtime.IJeiRuntime;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.CraftingRecipe;

@JeiPlugin
public class FuturepackPlugin implements IModPlugin
{
	public static IJeiHelpers jei;
	public static IRecipeManager registry;
	
	private static WeakReference<FuturepackPlugin> WEAK_INSTANCE = null;
	
	private Set<ISyncedRecipeManager<?>> bufferedManagers = Collections.synchronizedSet(new HashSet<>());
	private boolean airBrushRecipesInit = false;
	
	public FuturepackPlugin() 
	{
		if(PluginRecipeListener.INSTANCE == null)
		{
			PluginRecipeListener.INSTANCE = new PluginRecipeListener();
			RecipeManagerSyncer.INSTANCE.registerRecipeListener(PluginRecipeListener.INSTANCE);
		}
		
		WEAK_INSTANCE = new WeakReference<FuturepackPlugin>(this);
	}
	
	@Override
	public void onRuntimeAvailable(IJeiRuntime jeiRuntime)
	{
		registry = jeiRuntime.getRecipeManager();
		bufferedManagers.forEach(this::addRecipes);
		bufferedManagers.clear();
		
		if(!airBrushRecipesInit)
		{
			airBrushRecipesInit = true;
			AirbrushRegistry.asJEIRecipes().forEach(r -> registry.addRecipe(r, FuturepackUids.AIRBRUSH));
		}
//		new RuntimeEditor(registry);
	}
	
	@Override
	public void registerCategories(IRecipeCategoryRegistration registry)
	{
		IGuiHelper guiHelper = registry.getJeiHelpers().getGuiHelper();
		
//		registry.addRecipeCategories(new ResearchRecipeCategory(guiHelper));
		registry.addRecipeCategories(new AssemblyCategory(guiHelper));
		registry.addRecipeCategories(new ZentrifugeCategory(guiHelper));
		registry.addRecipeCategories(new IndustrialCategory(guiHelper));
		registry.addRecipeCategories(new IndNeonCategorie(guiHelper));
		registry.addRecipeCategories(new CrusherCategory(guiHelper));
		registry.addRecipeCategories(new AirbrushCategory(guiHelper));
		registry.addRecipeCategories(new RecyclerCategory(guiHelper));
		registry.addRecipeCategories(new WaterCoolerCategory(guiHelper));
		registry.addRecipeCategories(new PartPressCategory(guiHelper));
	}
	
	@Override
	public void registerRecipeTransferHandlers(IRecipeTransferRegistration registration) 
	{
		registration.addRecipeTransferHandler(GuiTechTable.ContainerTechTable.class,  VanillaRecipeCategoryUid.CRAFTING, 0, 9, 10, 54);
		registration.addRecipeTransferHandler(GuiTechTable.ContainerTechTable.class, FuturepackUids.CRAFTING, 0, 9, 10, 54);
		registration.addRecipeTransferHandler(GuiAssemblyTable.ContainerAssemblyTable.class, FuturepackUids.ASSEMBLY, 1, 3, 6, 36);
		registration.addRecipeTransferHandler(GuiCrusher.ContainerCrusher.class, FuturepackUids.CRUSHER, 0, 1, 2, 36);
		registration.addRecipeTransferHandler(GuiIndFurnace.ContainerIndFurnace.class, FuturepackUids.INDUTRIALFURNACE, 1, 3, 8, 36);
		registration.addRecipeTransferHandler(GuiIndNeonFurnace.ContainerIndNeonFurnace.class, FuturepackUids.INDUSTRIALNEONFURNACE, 0, 3, 6, 36);
		registration.addRecipeTransferHandler(GuiZentrifuge.ContainerZentrifuge.class, FuturepackUids.ZENTRIFUGE, 0, 1, 4, 36);
		
		registration.addRecipeTransferHandler(GuiConstructionTable.ContainerConstructionTable.class,  VanillaRecipeCategoryUid.CRAFTING, 3, 9, 12, 36);
		registration.addRecipeTransferHandler(GuiAssemblyRezeptCreator.ContainerAssemblyRezeptCreator.class, FuturepackUids.ASSEMBLY, 0, 3, 6, 36);
	}
	
	@Override
	public void registerRecipeCatalysts(IRecipeCatalystRegistration registry) 
	{
		registry.addRecipeCatalyst(new ItemStack(InventoryBlocks.techtable), FuturepackUids.CRAFTING, VanillaRecipeCategoryUid.CRAFTING);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.opti_bench_w), FuturepackUids.CRAFTING, VanillaRecipeCategoryUid.CRAFTING);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.opti_bench_g), FuturepackUids.CRAFTING, VanillaRecipeCategoryUid.CRAFTING);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.opti_bench_b), FuturepackUids.CRAFTING, VanillaRecipeCategoryUid.CRAFTING);
//		
		registry.addRecipeCatalyst(new ItemStack(InventoryBlocks.assembly_table_w), FuturepackUids.ASSEMBLY);
		registry.addRecipeCatalyst(new ItemStack(InventoryBlocks.assembly_table_g), FuturepackUids.ASSEMBLY);
		registry.addRecipeCatalyst(new ItemStack(InventoryBlocks.assembly_table_b), FuturepackUids.ASSEMBLY);
		registry.addRecipeCatalyst(new ItemStack(InventoryBlocks.industrial_furnace), FuturepackUids.INDUTRIALFURNACE);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.industrial_neon_furnace_w), FuturepackUids.INDUSTRIALNEONFURNACE);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.industrial_neon_furnace_g), FuturepackUids.INDUSTRIALNEONFURNACE);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.industrial_neon_furnace_b), FuturepackUids.INDUSTRIALNEONFURNACE);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.zentrifuge_w), FuturepackUids.ZENTRIFUGE);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.zentrifuge_g), FuturepackUids.ZENTRIFUGE);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.zentrifuge_b), FuturepackUids.ZENTRIFUGE);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.crusher_w), FuturepackUids.CRUSHER);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.crusher_g), FuturepackUids.CRUSHER);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.crusher_b), FuturepackUids.CRUSHER);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.recycler_w), FuturepackUids.RECYCLER);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.recycler_g), FuturepackUids.RECYCLER);
		registry.addRecipeCatalyst(new ItemStack(ModifiableBlocks.recycler_b), FuturepackUids.RECYCLER);	
		registry.addRecipeCatalyst(new ItemStack(ToolItems.airBrush), FuturepackUids.AIRBRUSH);
		registry.addRecipeCatalyst(new ItemStack(InventoryBlocks.water_cooler), FuturepackUids.WATERCOOLER);	
		registry.addRecipeCatalyst(new ItemStack(InventoryBlocks.part_press), FuturepackUids.PARTPRESS);
	}
	
	@Override
	public void registerRecipes(IRecipeRegistration registry) 
	{
//		registry.addRecipes(FPAssemblyManager.instance.recipes, FuturepackUids.ASSEMBLY);
//		registry.addRecipes(FPCrushingManager.instance.recipes, FuturepackUids.CRUSHER);
//		registry.addRecipes(FPIndustrialFurnaceManager.instance.recipes, FuturepackUids.INDUTRIALFURNACE);
//		registry.addRecipes(FPIndustrialNeonFurnaceManager.instance.recipes, FuturepackUids.INDUSTRIALNEONFURNACE);
//		registry.addRecipes(FPZentrifugeManager.instance.recipes, FuturepackUids.ZENTRIFUGE);
//		registry.addRecipes(FPRecyclerShredderManager.instance.getRecipes(), FuturepackUids.RECYCLER);
//		registry.addRecipes(FPRecyclerLaserCutterManager.instance.getRecipes(), FuturepackUids.RECYCLER);
//		registry.addRecipes(FPRecyclerTimeManipulatorManager.instance.getRecipes(), FuturepackUids.RECYCLER);
		
		if(AirbrushRegistry.asJEIRecipes()!=null)
		{
			airBrushRecipesInit = true;
			registry.addRecipes(AirbrushRegistry.asJEIRecipes().collect(Collectors.toList()), FuturepackUids.AIRBRUSH);
		}
			
		else
			airBrushRecipesInit = false;
		registry.addRecipes(WaterCoolerManager.INSTANCE.recipes.values(), FuturepackUids.WATERCOOLER);
		FakeRecipeGenerator.registerRecyclerAnalyserRecipes(registry);
		FakeRecipeGenerator.registerPartPressRecipes(registry);
	}
	
	
	@Override
	public void registerVanillaCategoryExtensions(IVanillaCategoryExtensionRegistration registration) 
	{
		IExtendableRecipeCategory<CraftingRecipe, ICraftingCategoryExtension> craftingCategory = registration.getCraftingCategory();
		craftingCategory.addCategoryExtension(ShapedRecipeWithResearch.class, ShapedResearchCraftingExtension::new);
		craftingCategory.addCategoryExtension(ShapelessRecipeWithResearch.class, ShapelessResearchCraftingExtension::new);
	}

	@Override
	public ResourceLocation getPluginUid() 
	{
		return new ResourceLocation(Constants.MOD_ID, "jei");
	}

	
	public void addRecipes(ISyncedRecipeManager<?> t) 
	{
		if(registry != null)
		{
			ResourceLocation res = t.getName();
		
			if(res!=null)
			{
				Collection<?> col = t.getRecipes();
				col.forEach(recipe -> registry.addRecipe(recipe, res));
				FPLog.logger.info("Added %s recipes for %s", col.size(), res);
			}
		}
		else
		{
			bufferedManagers.add(t);
		}
	}
	
//	@Override
//	public void register(IModRegistry registry)
//	{
//		jei = registry.getJeiHelpers();
//		IGuiHelper guiHelper = jei.getGuiHelper();
//
//		registry.handleRecipes(ShapedRecipeWithResearch.class, r ->{ return new ShapedResearchWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, "minecraft.crafting");
//		registry.handleRecipes(RecipeMaschin.class, r ->{ return new ShapedResearchWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, "minecraft.crafting");
//		registry.handleRecipes(RecipeOwnerBased.class, r ->{ return new ShapedResearchWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, "minecraft.crafting");
//		registry.handleRecipes(ShapelessRecipeWithResearch.class, r ->{ return new ShapelessResearchWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, "minecraft.crafting");
//		
//		registry.handleRecipes(AssemblyRecipe.class, r ->{ return new AssemblyWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, FuturepackUids.ASSEMBLY);
//		registry.handleRecipes(ZentrifugeRecipe.class, r ->{ return new ZentrifugeWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, FuturepackUids.ZENTRIFUGE);
//		registry.handleRecipes(IndRecipe.class, r ->{ return new IndustrialWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, FuturepackUids.INDUTRIALFURNACE);
//		registry.handleRecipes(IndNeonRecipe.class, r ->{ return new IndNeonWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, FuturepackUids.INDUSTRIALNEONFURNACE);
//		registry.handleRecipes(CrushingRecipe.class, r ->{ return new CrusherWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, FuturepackUids.CRUSHER);
//		registry.handleRecipes(AirbrushRecipeJEI.class, r ->{ return new AirbrushWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, FuturepackUids.AIRBRUSH);
//		registry.handleRecipes(IRecyclerRecipe.class, r ->{ return new RecyclerWrapper(FuturepackPlugin.jei.getGuiHelper(), r);}, FuturepackUids.RECYCLER);
//		registry.handleRecipes(WaterCoolerManager.CoolingEntry.class, r ->{ return new WaterCoolerWrapper(r);}, FuturepackUids.WATERCOOLER);
//		registry.handleRecipes(PartPressJeiFakeRecipe.class, r ->{ return new PartPressWrapper(r);}, FuturepackUids.PARTPRESS);
//
//
////		IRecipeRegistryPlugin
////		registry.addRecipeRegistryPlugin(recipeRegistryPlugin);
//	}
	
//	public static class RuntimeEditor implements IRecipeEditor
//	{
//		private final IRecipeRegistry registry;
//
//		public RuntimeEditor(IRecipeRegistry registry)
//		{
//			this.registry = registry;
//			RecipeRuntimeEditor.editor = this;
//		}
//		
//		@Override
//		public void addRecipe(Object o)
//		{
//			registry.addRecipe(o);
//		}
//
//		@Override
//		public void removeRecipe(Object o)
//		{
//			registry.removeRecipe(o);
//		}
//	}
	
	static class PluginRecipeListener implements Consumer<ISyncedRecipeManager<?>>
	{
		static PluginRecipeListener INSTANCE;
		
		@Override
		public void accept(ISyncedRecipeManager<?> t) 
		{
			FuturepackPlugin plugin = FuturepackPlugin.WEAK_INSTANCE.get();
			
			if(plugin != null)
				plugin.addRecipes(t);
			
		}	
	};
	
}
