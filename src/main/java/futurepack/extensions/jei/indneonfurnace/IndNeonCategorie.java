package futurepack.extensions.jei.indneonfurnace;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.api.ItemPredicateBase;
import futurepack.common.block.modification.ModifiableBlocks;
import futurepack.common.recipes.industrialfurnace.IndNeonRecipe;
import futurepack.extensions.jei.BaseRecipeCategory;
import futurepack.extensions.jei.FuturepackUids;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.drawable.IDrawableAnimated;
import mezz.jei.api.gui.drawable.IDrawableAnimated.StartDirection;
import mezz.jei.api.gui.drawable.IDrawableStatic;
import mezz.jei.api.gui.ingredient.IGuiItemStackGroup;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;

public class IndNeonCategorie extends BaseRecipeCategory<IndNeonRecipe>
{
	private IDrawableAnimated arrow;
	
	public IndNeonCategorie(IGuiHelper gui)
	{
		super(gui, ModifiableBlocks.industrial_neon_furnace_w, FuturepackUids.INDUSTRIALNEONFURNACE, 92-19, 32-10);
	}
	

	
	@Override
	public void draw(IndNeonRecipe recipe, PoseStack matrixStack, double mouseX, double mouseY) 
	{
		arrow.draw(matrixStack, 93-19, 35-10);
		Minecraft.getInstance().font.draw(matrixStack, I18n.get("futurepack.jei.neededsupport",recipe.getSupport()), 64, 2, Color.gray.getRGB());
		super.draw(recipe, matrixStack, mouseX, mouseY);
	}

	@Override
	public Class<? extends IndNeonRecipe> getRecipeClass() 
	{
		return IndNeonRecipe.class;
	}

	@Override
	public void setIngredients(IndNeonRecipe rec, IIngredients ingredients) 
	{
		List<List<ItemStack>> inputs = new ArrayList<List<ItemStack>>();
		for(ItemPredicateBase it : rec.getInputs())
		{
			inputs.add(it.collectAcceptedItems(new ArrayList<>()));
		}
		ingredients.setInputLists(VanillaTypes.ITEM, inputs);
		
		ingredients.setOutput(VanillaTypes.ITEM, rec.getOutput());
	}

	@Override
	public void setRecipe(IRecipeLayout recipeLayout, IndNeonRecipe recipe, IIngredients ingredients) 
	{
		IGuiItemStackGroup group = recipeLayout.getItemStacks();
		
		group.init(0, true, 25-19, 33-10);
		group.init(1, true, 43-19, 33-10);
		group.init(2, true, 61-19, 33-10);
		
		group.init(3, false, 133-19, 15-10);
		
		group.set(ingredients);
	}

	@Override
	protected IDrawable createBackground(IGuiHelper gui) 
	{
		ResourceLocation res =new ResourceLocation(Constants.MOD_ID, "textures/gui/neonindustrialfurnace.png");
		IDrawable bg = gui.createDrawable(res, 19, 10, 132, 69);
		IDrawableStatic base = gui.createDrawable(res, 176, 14, 24, 17);
		arrow = gui.createAnimatedDrawable(base, 400, StartDirection.LEFT, false);
		return bg;
	}

	@Override
	public boolean isResearched(IndNeonRecipe rec) 
	{
		return rec.isLocalResearched();
	}

}
