package futurepack.extensions.jei.assembly;

import java.util.ArrayList;
import java.util.List;

import futurepack.api.Constants;
import futurepack.api.ItemPredicateBase;
import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.common.recipes.assembly.AssemblyRecipe;
import futurepack.extensions.jei.BaseRecipeCategory;
import futurepack.extensions.jei.FuturepackUids;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IGuiItemStackGroup;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;

public class AssemblyCategory extends BaseRecipeCategory<AssemblyRecipe>
{
	public AssemblyCategory(IGuiHelper gui)
	{
		super(gui, InventoryBlocks.assembly_table_w, FuturepackUids.ASSEMBLY, 0, 32);
	}
	
	@Override
	protected IDrawable createBackground(IGuiHelper gui) 
	{
		ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/assembler.png");
		return gui.createDrawable(res, 47, 21, 80, 54);
	}

	@Override
	public Class<? extends AssemblyRecipe> getRecipeClass() 
	{
		return AssemblyRecipe.class;
	}

	@Override
	public void setIngredients(AssemblyRecipe recipe, IIngredients ingredients) 
	{
		List<List<ItemStack>> inputs = new ArrayList<List<ItemStack>>();
		for(ItemPredicateBase in : recipe.getInput())
		{
			inputs.add(in.collectAcceptedItems(new ArrayList<ItemStack>()));
		}
		ingredients.setInputLists(VanillaTypes.ITEM, inputs);
		
		ingredients.setOutput(VanillaTypes.ITEM, recipe.getOutput());
	}

	@Override
	public void setRecipe(IRecipeLayout recipeLayout, AssemblyRecipe recipe, IIngredients ingredients) 
	{
		IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();
		
		guiItemStacks.init(0, true, 14, 0);
		guiItemStacks.init(1, true, 32, 0);
		guiItemStacks.init(2, true, 50, 0);
		
		guiItemStacks.init(3, false, 33, 32);
		
		guiItemStacks.set(ingredients);
	}

	@Override
	public boolean isResearched(AssemblyRecipe t) 
	{
		return t.isLocalResearched();
	}

}
