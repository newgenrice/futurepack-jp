package futurepack.extensions.jei.crafting;

import javax.annotation.Nullable;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.recipes.crafting.ShapelessRecipeWithResearch;
import futurepack.extensions.jei.BaseRecipeCategory;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.category.extensions.vanilla.crafting.ICraftingCategoryExtension;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.common.util.Size2i;

public class ShapelessResearchCraftingExtension<T extends ShapelessRecipeWithResearch> implements ICraftingCategoryExtension 
{
	protected final T recipe;

	private float scale = 21F / 256F;
	private int x, y;

	public ShapelessResearchCraftingExtension(T recipe) 
	{
		this.recipe = recipe;
		x = 61;
		y = 16;
	}

	@Override
	public void setIngredients(IIngredients ingredients) 
	{
		ingredients.setInputIngredients(recipe.getIngredients());
		ingredients.setOutput(VanillaTypes.ITEM, recipe.getResultItem());
	}

	@Nullable
	@Override
	public ResourceLocation getRegistryName() 
	{
		return recipe.getId();
	}

	@Nullable
	@Override
	public Size2i getSize() 
	{
		return null;
	}

	@Override
	public void drawInfo(int recipeWidth, int recipeHeight, PoseStack matrixStack, double mouseX, double mouseY) 
	{
		if (!isResearched()) 
		{
			matrixStack.pushPose();
			matrixStack.scale(scale, scale, 1);
			RenderSystem.setShaderColor(1f, 1f, 1f, 1f);
			BaseRecipeCategory.blockedIcon.draw(matrixStack, (int) (x / scale), (int) (y / scale));
			matrixStack.popPose();
		}
	}

	public boolean isResearched() 
	{
		return recipe.isLocalResearched();
	}

}
