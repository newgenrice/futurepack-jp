package futurepack.extensions.jei.crusher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import futurepack.api.Constants;
import futurepack.common.block.modification.ModifiableBlocks;
import futurepack.common.recipes.crushing.CrushingRecipe;
import futurepack.extensions.jei.BaseRecipeCategory;
import futurepack.extensions.jei.FuturepackUids;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IGuiItemStackGroup;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;

public class CrusherCategory extends BaseRecipeCategory<CrushingRecipe>
{

	public CrusherCategory(IGuiHelper gui)
	{
		super(gui, ModifiableBlocks.crusher_w, FuturepackUids.CRUSHER, 80-47, 31-21);
	}

	@Override
	public Class<? extends CrushingRecipe> getRecipeClass() 
	{
		return CrushingRecipe.class;
	}

	@Override
	public void setIngredients(CrushingRecipe rec, IIngredients ingredients) 
	{
		List<ItemStack> l = rec.getInput().collectAcceptedItems(new ArrayList<>());
		ingredients.setInputLists(VanillaTypes.ITEM, Collections.singletonList(l));
		ingredients.setOutput(VanillaTypes.ITEM, rec.getOutput());
	}

	@Override
	public void setRecipe(IRecipeLayout recipeLayout, CrushingRecipe recipe, IIngredients ingredients) 
	{
		IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();

		guiItemStacks.init(0, true, 0, 4);
		guiItemStacks.init(1, false, 60, 4);
		
		guiItemStacks.set(ingredients);
	}

	@Override
	protected IDrawable createBackground(IGuiHelper gui) 
	{
		ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/super_crusher.png");
		return gui.createDrawable(res, 55, 30, 82, 27);
	}

	@Override
	public boolean isResearched(CrushingRecipe rec) 
	{
		return rec.isLocalResearched();
	}
	
}
