package futurepack.extensions.crafttweaker.impl;

import org.openzen.zencode.java.ZenCodeType;

import com.blamejared.crafttweaker.api.CraftTweakerAPI;
import com.blamejared.crafttweaker.api.annotation.ZenRegister;
import com.blamejared.crafttweaker.api.ingredient.IIngredient;
import com.blamejared.crafttweaker.api.item.IItemStack;

import futurepack.common.recipes.industrialfurnace.FPIndustrialFurnaceManager;
import futurepack.common.recipes.industrialfurnace.FPIndustrialNeonFurnaceManager;
import futurepack.common.recipes.industrialfurnace.IndNeonRecipe;
import futurepack.extensions.crafttweaker.CrafttweakerExtension;
import futurepack.extensions.crafttweaker.RecipeActionBase;

@ZenRegister
@ZenCodeType.Name("mods.futurepack.neonindustrial")
public class CTIndustrialNeonFurnace
{
	static class IndNeonRecipeAddAction extends RecipeActionBase<IndNeonRecipe>
	{
		private IndNeonRecipe r;
		
		public IndNeonRecipeAddAction(String id, IItemStack output, IIngredient[] input, int support)
		{
			super(Type.ADD, () -> FPIndustrialNeonFurnaceManager.instance);
			
			r = new IndNeonRecipe(id, support, output.getInternal(), CrafttweakerExtension.convert(input));
		}

		@Override
		public IndNeonRecipe createRecipe()
		{
			return r;
		}
		
	}
	
	static class IndNeonRecipeRemoveAction extends RecipeActionBase<IndNeonRecipe>
	{
		private IItemStack[] input;
		
		public IndNeonRecipeRemoveAction(IItemStack[] input)
		{
			super(Type.ADD, () -> FPIndustrialNeonFurnaceManager.instance);
			
			this.input = input;
		}

		@Override
		public IndNeonRecipe createRecipe()
		{
			return ((FPIndustrialNeonFurnaceManager)recipeManager.get()).getMatchingRecipes(CrafttweakerExtension.convert(input), null)[0];
		}
		
	}
	
	@ZenCodeType.Method
	public static void add(String id, IItemStack output, IIngredient[] input, int support)
	{
		CraftTweakerAPI.apply(new IndNeonRecipeAddAction(id, output, input, support));
	}
	
	@ZenCodeType.Method
	public static void remove(IItemStack[] input)
	{
		CraftTweakerAPI.apply(new IndNeonRecipeRemoveAction(input));
	}

}
