package futurepack.extensions.crafttweaker.impl;

import org.openzen.zencode.java.ZenCodeType;

import com.blamejared.crafttweaker.api.CraftTweakerAPI;
import com.blamejared.crafttweaker.api.annotation.ZenRegister;
import com.blamejared.crafttweaker.api.ingredient.IIngredient;
import com.blamejared.crafttweaker.api.item.IItemStack;

import futurepack.common.recipes.assembly.AssemblyRecipe;
import futurepack.common.recipes.assembly.FPAssemblyManager;
import futurepack.extensions.crafttweaker.CrafttweakerExtension;
import futurepack.extensions.crafttweaker.RecipeActionBase;

@ZenRegister
@ZenCodeType.Name("mods.futurepack.assembly")
public class CTAssembly
{

	static class AssemblyRecipeAddAction extends RecipeActionBase<AssemblyRecipe>
	{
		private AssemblyRecipe r;
		
		public AssemblyRecipeAddAction(String id, IItemStack output, IIngredient[] input)
		{
			super(Type.ADD, () -> FPAssemblyManager.instance);
			
			r = new AssemblyRecipe(id, output.getInternal(), CrafttweakerExtension.convert(input));
		}

		@Override
		public AssemblyRecipe createRecipe()
		{
			return r;
		}
		
	}
	
	static class AssemblyRecipeRemoveAction extends RecipeActionBase<AssemblyRecipe>
	{
		private IItemStack[] input;
		
		public AssemblyRecipeRemoveAction(IItemStack[] input)
		{
			super(Type.ADD, () -> FPAssemblyManager.instance);
			
			this.input = input;
		}

		@Override
		public AssemblyRecipe createRecipe()
		{
			return ((FPAssemblyManager)recipeManager.get()).getMatchingRecipe(CrafttweakerExtension.convert(input));
		}
		
	}
	
	@ZenCodeType.Method
	public static void add(String id, IItemStack output, IIngredient[] input)
	{
		CraftTweakerAPI.apply(new AssemblyRecipeAddAction(id, output, input));
	}
	
	@ZenCodeType.Method
	public static void remove(IItemStack[] input)
	{
		CraftTweakerAPI.apply(new AssemblyRecipeRemoveAction(input));
	}
}
