package futurepack.extensions.crafttweaker.impl;

import org.openzen.zencode.java.ZenCodeType;

import com.blamejared.crafttweaker.api.annotation.ZenRegister;
import com.blamejared.crafttweaker.api.item.IItemStack;

import futurepack.common.gui.RenderableItem;
import futurepack.common.research.ResearchManager;
import futurepack.common.research.ResearchPage;
import net.minecraft.resources.ResourceLocation;

@ZenRegister
@ZenCodeType.Name("mods.futurepack.research")
public class CTResearch
{
	@ZenCodeType.Method
	public static ResearchWrapper getResearch(String name)
	{
		return new ResearchWrapper(ResearchManager.getResearch(name));
	}
	
	@ZenCodeType.Method
	public static ResearchPageWrapper getOrCreateResearchPage(String name)
	{
		return new ResearchPageWrapper(ResearchPage.getPageSave(name));
	}
	
	@ZenCodeType.Method
	public static ResearchWrapper createResearch(String name, int x, int y, IItemStack icon, ResearchPageWrapper page)
	{
		return new ResearchWrapper(ResearchManager.createResearch(new ResourceLocation(name), x, y, new RenderableItem(icon.getInternal()), page.p));
	}

}
