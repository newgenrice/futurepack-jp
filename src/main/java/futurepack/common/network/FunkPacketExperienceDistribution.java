package futurepack.common.network;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import net.minecraft.core.BlockPos;

public class FunkPacketExperienceDistribution extends PacketBase
{
	public int XP;
	
	public FunkPacketExperienceDistribution(BlockPos src, ITileNetwork net, int xp)
	{
		super(src, net);
		XP = xp;
	}

}
