package futurepack.common.network;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import net.minecraft.core.BlockPos;

public class FunkPacketSupport extends PacketBase
{
	public final int needed;
	public int collected = 0;
	
	
	public FunkPacketSupport(BlockPos src, ITileNetwork net, int support)
	{
		super(src, net);
		needed = support;
	}

}
