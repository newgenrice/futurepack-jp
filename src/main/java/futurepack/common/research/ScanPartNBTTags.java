package futurepack.common.research;

import futurepack.api.interfaces.IScanPart;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.phys.BlockHitResult;

public class ScanPartNBTTags implements IScanPart {

	@Override
	public Component doBlock(Level w, BlockPos pos, boolean inGUI, BlockHitResult res)
	{
		BlockEntity[] t = {null};
		w.getServer().submit(() -> {
			t[0] = w.getBlockEntity(pos);
		}).join();
		BlockEntity tile = t[0];
		if(tile != null)
		{
			CompoundTag nbt = new CompoundTag();
			tile.save(nbt);
			return NBTtoChat(nbt);
		}
		return null;
	}

	@Override
	public Component doEntity(Level w, LivingEntity e, boolean inGUI)
	{
		CompoundTag nbt = new CompoundTag();
		e.saveWithoutId(nbt);
		nbt.remove("Attributes");
		return NBTtoChat(nbt);
	}
	
	private static TextComponent NBTtoChat(CompoundTag nbt)
	{
		StringBuilder build = new StringBuilder();
		char[] c = nbt.toString().toCharArray();
		
		boolean insideString  = false;
		String sub = "";
		for(int i=0;i<c.length;i++)
		{
			build.append(c[i]);
			
			if(c[i] == '"')
			{
				insideString = !insideString;
			}
			else if(!insideString)
			{
				switch (c[i])
				{
				case ',':
					build.append('\n'+sub);
					break;
				case '{':
					sub+=" ";
					build.append('\n'+sub);
					break;
				case '}':
					sub = sub.substring(1);
					build.insert(build.length()-1, "\n"+sub);
					break;
				case '[':
					sub+=" ";
					build.append('\n' +sub);
					break;
				case ']':
					sub = sub.substring(1);
					build.insert(build.length()-1, "\n"+sub);
					break;
				}
			}
		}
			
		return new TextComponent(build.toString());
	}

}
