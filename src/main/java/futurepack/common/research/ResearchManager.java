package futurepack.common.research;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import futurepack.api.ItemPredicateBase;
import futurepack.api.interfaces.IGuiRenderable;
import futurepack.common.FPLog;
import futurepack.common.gui.RenderableItem;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;

public class ResearchManager
{
	public static ResearchManager instance;
	
	public static void init()
	{
		instance = new ResearchManager();
	}
	
	////@ TODO: OnlyIn(Dist.CLIENT)
	/**Map: name of the Research , Research*/
	private HashMap<String, Research> researchMap;
	public static final Research ERROR = new Research(new ResourceLocation("error", "error"), 0, 100, 100, new RenderableItem(new ItemStack(Blocks.DIRT, 64)), ResearchPage.base);
	private ArrayList<Research> idMapping;

	public ResearchManager()
	{
		researchMap = new HashMap<String, Research>();
		idMapping = new ArrayList<Research>();
	}
	
	public static Set<String> getAllReseraches()
	{
		return getInstance().researchMap.keySet();
	}

	public static Research getById(int id)
	{	
		return getInstance().idMapping.get(id);
	}

	public String getHeading(String name)
	{
		Research r = researchMap.get(removeDomain(name));
		if(r!=null)
		{
			return r.getLocalizedName().getString();
		}
		return "";
	}

	public static Research getResearch(String research)
	{
		Research r = getInstance().researchMap.get(removeDomain(research));
		if(r==null)
		{
			FPLog.logger.error("Research '" + research + "' is null! returning backup Research");
			r=ERROR;
		}
		return r;
	}
	
	private static ResearchManager getInstance()
	{
		if(instance == null)
		{
			DistExecutor.runWhenOn(Dist.CLIENT, () -> ResearchLoader.instance::init);
		}
		return instance;
	}
	
	public static Research getResearchOrElse(String research, Research fallback)
	{
		return getInstance().researchMap.getOrDefault(removeDomain(research), fallback);
	}
	
	private static String removeDomain(String s)
	{
		int pos = s.indexOf(':');
		if(pos == -1)
			return s;
		else
		{
			return s.substring(pos+1);
		}
	}

	public static int getResearchCount()
	{
		if(instance==null)
			return 0;
		
		return instance.idMapping.size();
	}

	

	

	public static Research createResearch(ResourceLocation res, int x, int y, IGuiRenderable icon, ResearchPage base)
	{
		if(instance == null)
		{
			throw new IllegalStateException("Research System has not been initialisated yet!");
		}
		if(instance.researchMap.containsKey(res.getPath()))
		{
			throw new IllegalArgumentException("ID " +res.getPath()+ " already registred!");
		}
		if("minecraft".equals(res.getNamespace()))
		{
			throw new IllegalArgumentException("'minecraft' is not a valid research namespace");
		}
		Research r = new Research(res, instance.idMapping.size(), x, y, icon,  base);
		instance.idMapping.add(r);
		instance.researchMap.put(res.getPath(), r);
		TagDictReplacer.fixResearches();
		return r;
	}
	/*
	 * Achievment Map "futurepack:research.json"
	 * [
	 *   {
	 *     id:"test",
	 *     x:0,
	 *     y:0,
	 *     "parents":["achievement.mineWood"],
	 *     icon:{id:"minecraft:dirt",Damage:0,Count:1,tag:{}},
	 *     enables:[{}],
	 *     need:{id:minecraft:cobblestone,Damage:0,Count:64},
	 *     level:1,
	 *     time:20
	 *   }
	 * ]
	 * 
	 * 
	 * in lang file "futurepack:lang/en_US.lang":
	 * research.test=Test in English
	 * 
	 * 
	 * Achievment file "futurepack:lang/test.json":
	 * {
	 *   "en_US":["Test Text","New Paragraph"]
	 * }
	 * 
	 */

	
	protected void checkResearchCorrectness()
	{
		ArrayList<Research> faulty = new ArrayList<Research>();
		
		for(Research r : researchMap.values())
		{
			ItemPredicateBase[] needs = r.getNeeded();
			int resources = 0;
			if(r.getTime() > 0)
				resources++;
			if(r.getNeonenergie() > 0)
				resources++;
			if(r.getSupport() > 0)
				resources++;
			if(r.getExpLvl() > 0)
				resources++;
			
			if(resources != needs.length)
			{
				if(resources ==1 && needs.length==0 && r.getTime() > 0)
				{
					//unobtainables with extern trigger
				}
				else
				{
					faulty.add(r);
				}
				
			}
				
		}
		
		if(!faulty.isEmpty())
		{
			FPLog.logger.error("Found %s faulty researches", faulty.size());
			for(Research r : faulty)
			{
				FPLog.logger.error("\t %s", r);
			}
			throw new RuntimeException("Found "+faulty.size()+" faulty researches see log for details.");
		}
	}
}
