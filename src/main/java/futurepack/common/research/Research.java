package futurepack.common.research;


import java.util.Arrays;

import futurepack.api.Constants;
import futurepack.api.ItemPredicateBase;
import futurepack.api.interfaces.IGuiRenderable;
import futurepack.api.interfaces.IResearch;
import futurepack.depend.api.EnumAspects;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;

public class Research implements IResearch, Comparable<Research>
{
	private static final ResourceLocation slot = new ResourceLocation(Constants.MOD_ID, "textures/gui/slot_bg.png");
	private static final ResourceLocation slotErk = new ResourceLocation(Constants.MOD_ID, "textures/gui/slot_erk_bg.png");
	private static final ResourceLocation slotSo = new ResourceLocation(Constants.MOD_ID, "textures/gui/slot_so_bg.png");
	
	private final String name;
	private final String domain;
	private final int x,y;
	public final int id;
	public final ResearchPage page;
	
	private final IGuiRenderable icon;
	private Research[] parents=null;
	private ResourceLocation[] grandparents=null;
	
	
	private ItemStack[] enables=null;
	private ItemPredicateBase[] needed=null;
	
	protected int techLevel = 0;
	protected EnumAspects[] aspects;
	
	/**
	 * The time in Ticks needed for researching, 0 = automatic.
	 */
	protected int time = 0; 
	protected int expLvl = 0;
	protected int support = 0;
	protected int neonenergie = 0;
	protected int visibilityLevel = 0;
	
	private ItemStack[] rewards;
	
	public Research(String name, String domain, int id, int px, int py, IGuiRenderable picon, ResearchPage page)
	{
		this.name = name;
		this.domain = domain;
		this.id = id;
		x = px;
		y = py;
		icon = picon;
		this.page = page;
		page.registerResearch(this);
	}
	
	public Research(ResourceLocation res, int id, int px, int py, IGuiRenderable picon, ResearchPage page)
	{
		this(res.getPath(), res.getNamespace(), id, px, py, picon, page);
	}
	
	public void setParents(Research[] p)
	{
		if(p.length==0)
			return;
		
		parents = p;
	}
	
	public void setGrandparents(ResourceLocation[] p)
	{
		if(p.length==0)
			return;
		
		grandparents = p;
	}
	
	public void setEnabled(ItemStack[] it)
	{
		if(it.length==0)
			return;
		
		if(enables==null)
			enables=it;
	}
	
	public void setNeeded(ItemPredicateBase[] it)
	{
		if(it==null)
			return;
		
		if(it.length==0)
			return;
		
		if(needed==null)
			needed=it;
	}
	
	
	@Override
	public Research[] getParents()
	{
		if(parents==null)
		{
			return new Research[0];
		}
		return parents;
	}
	
	@Override
	public ResourceLocation[] getGrandparents()
	{
		if(grandparents==null)
		{
			return new ResourceLocation[0];
		}
		return grandparents;
	}
	
	@Override
	//@ TODO: OnlyIn(Dist.CLIENT)
	public Component getLocalizedName()
	{
		return new TranslatableComponent(getTranslationKey());
	}
	
	@Override
	public String getTranslationKey()
	{
		return "research." + name;
	}
	
	@Override
	public String getName()
	{
		return name;
	}
	
	@Override
	public int getX()
	{
		return x;
	}
	
	@Override
	public int getY()
	{
		return y;
	}
	
	@Override
	public IGuiRenderable getIcon()
	{
		return icon;
	}
	
	@Override
	public ItemStack[] getEnables()
	{
		if(enables==null)
		{
			return new ItemStack[0];
		}
		return enables;
	}
	
	@Override
	public ItemPredicateBase[] getNeeded()
	{
		if(needed==null)
		{
			return new ItemPredicateBase[0];
		}
		return needed;
	}
	
	@Override
	public int hashCode() 
	{
		return name.hashCode();
	}
	
	public boolean areAspectsMatching(boolean[] bools)
	{
		if(aspects == null)
		{
			throw new IllegalStateException("Research "+ toString() + " is broken and has no Aspects!", new NullPointerException("Research.aspects is null!"));
		}
		for(int i=0;i<bools.length;i++)
		{		
			boolean b = false;
			for(int j=0;j<this.aspects.length;j++)
			{
				if(this.aspects[j].ordinal() == i)
				{
					b = true;
					break;
				}
			}
			if(b != bools[i])
			{
				return false;
			}
				
		}
		return true;
	}
	
	public boolean isPartwhiseCorrect(boolean[] bools)
	{
		if(aspects == null)
		{
			throw new IllegalStateException("Research "+ toString() + " is broken and has no Aspects!", new NullPointerException("Research.aspects is null!"));
		}
		for(int i=0;i<bools.length;i++)
		{		
			if(!bools[i])
				continue;
				
			boolean b = false;
			for(int j=0;j<this.aspects.length;j++)
			{
				if(this.aspects[j].ordinal() == i)
				{
					b = true;
					break;
				}
			}
			if(!b)
			{
				return false;
			}
		}
		return true;
	}
	
	@Override
	public int getExpLvl()
	{
		return expLvl;
	}

	@Override
	public int getSupport()
	{
		return support;
	}

	@Override
	public int getNeonenergie()
	{
		return neonenergie;
	}

	@Override
	public int getTime()
	{
		return time;
	}

	@Override
	public int getTecLevel()
	{
		return techLevel;
	}

	@Override
	public ResourceLocation getBackground()
	{
		return grandparents!=null? slotSo : enables==null?slotErk : slot;
	}

	/**
	 * 0 allways vivisble
	 * 1 visible if researchable
	 * 2 visible if researched
	 * 4 never vivisble
	 * @param hasRes
	 * @param canRes
	 * @return
	 */
	@Override
	public boolean isVisible(boolean hasRes, boolean canRes)
	{
		if(visibilityLevel == 4)
			return false;
		else
			return visibilityLevel <= ( (hasRes?2:0) | (canRes?1:0) );
	}

	public String getDomain()
	{
		return domain;
	}

	@Override
	public int compareTo(Research o) 
	{
		if(o==null)
			return this.id;
		
		return this.id - o.id;
	}
	
	@Override
	public String toString() 
	{
		return String.format("Reserach %s:%s(%s)", domain,name,id);
	}
	
	
	public String toFullString()
	{
		//System.out.print(name);
		String[] p = null;
		if(parents!=null)
		{
			p = new String[parents.length];
			for(int i=0;i<parents.length;i++)
			{
				p[i] = parents[i].domain + ":" + parents[i].name;
			}
		}
		String[] pp = null;
		if(grandparents!=null)
		{
			pp = new String[grandparents.length];
			for(int i=0;i<grandparents.length;i++)
			{
				pp[i] = grandparents[i].toString();
			}
		}
		
		return String.format("Reserach %s:%s(%s) { x:%s, y:%s, Icon:%s, Parents:%s, Grandparent:%s, Needed:%s, Enabled:%s, Aspects:%s }",domain,name,id,x,y,icon,Arrays.toString(p),Arrays.toString(pp),Arrays.toString(needed),Arrays.toString(enables), Arrays.toString(aspects));
	}
	
	
	public void setRewards(ItemStack[] it)
	{
		if(it.length==0)
			return;
		
		if(rewards==null)
			rewards=it;
	}
	
	@Override
	public ItemStack[] getRewards()
	{
		if(rewards==null)
		{
			return new ItemStack[0];
		}
		return rewards;
	}
}
