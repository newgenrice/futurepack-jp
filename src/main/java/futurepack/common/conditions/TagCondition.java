package futurepack.common.conditions;

import java.util.function.BooleanSupplier;

import com.google.gson.JsonElement;

import futurepack.depend.api.helper.HelperOreDict;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;

public class TagCondition implements BooleanSupplier
{
	private static ResourceLocation tagName;

	public TagCondition(ResourceLocation tagName)
	{
		this.tagName = tagName;
	}

	//doing this late Tags are loaded
	
	@Override
	public boolean getAsBoolean()
	{
		return HelperOreDict.getOptionalTag(ItemTags.getAllTags(), tagName).getValues().isEmpty() == false;
	}

	public static TagCondition fromJson(JsonElement elm)
	{
		return new TagCondition(new ResourceLocation(elm.getAsString()));
	}
}
