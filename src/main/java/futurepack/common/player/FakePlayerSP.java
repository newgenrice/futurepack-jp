package futurepack.common.player;

import futurepack.api.interfaces.IAirSupply;
import futurepack.common.DirtyHacks;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.client.ClientRecipeBook;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.multiplayer.ClientPacketListener;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.core.Direction;
import net.minecraft.stats.StatsCounter;
import net.minecraft.tags.FluidTags;
import net.minecraft.tags.Tag;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.material.Fluid;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public class FakePlayerSP extends LocalPlayer
{
	private LocalPlayer wrapped;

	public FakePlayerSP(Minecraft mcIn, ClientLevel worldIn, ClientPacketListener netHandler, StatsCounter statFile, ClientRecipeBook book, boolean clientSneakState, boolean clientSprintState)
	{
		super(mcIn, worldIn, netHandler, statFile, book, clientSneakState, clientSneakState);
		wrapped = null;
	}
	
	public FakePlayerSP(LocalPlayer sp)
	{
		this(Minecraft.getInstance(), (ClientLevel) sp.getCommandSenderWorld(), sp.connection, sp.getStats(), sp.getRecipeBook(), sp.isShiftKeyDown(), sp.isSprinting());
		DirtyHacks.replaceData(this, sp, LocalPlayer.class);
		DirtyHacks.replaceData(this, sp, Player.class);
		DirtyHacks.replaceData(this, sp, LivingEntity.class);
		DirtyHacks.replaceData(this, sp, Entity.class);
		wrapped = sp;
	}
	
	@Override
	public boolean isEyeInFluid(Tag<Fluid> tagIn)
	{
		if(FluidTags.WATER == tagIn)
		{
			if(!AtmosphereManager.hasEntityOxygen(this))
			{
				return true;
			}
		}
		return super.isEyeInFluid(tagIn);
	}
	
	@Override
	public int getAirSupply()
	{
		IAirSupply supp = getCapability(AtmosphereManager.cap_AIR, null).orElseGet(DefaultAir::new);
		return supp.getAir();
	}
	
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(wrapped!=null)
		{
			return wrapped.getCapability(capability, facing);
		}
		else
			return super.getCapability(capability, facing);
	}
	
	private class DefaultAir implements IAirSupply
	{

		@Override
		public int getAir()
		{
			return FakePlayerSP.super.getAirSupply();
		}

		@Override
		public void addAir(int amount)
		{
			FakePlayerSP.super.setAirSupply(getAir() + amount);
		}

		@Override
		public void reduceAir(int amount)
		{
			addAir(-amount);
		}

		@Override
		public int getMaxAir()
		{
			return FakePlayerSP.super.getMaxAirSupply();
		}
		
	}
}
