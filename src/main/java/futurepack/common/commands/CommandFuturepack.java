package futurepack.common.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;

import futurepack.api.Constants;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.research.Research;
import futurepack.common.research.ResearchManager;
import net.minecraft.ChatFormatting;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementProgress;
import net.minecraft.commands.CommandRuntimeException;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.commands.arguments.EntityArgument;
import net.minecraft.commands.arguments.selector.EntitySelector;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.server.level.ServerPlayer;

public class CommandFuturepack
{

	public static void register(CommandDispatcher<CommandSourceStack> dispatcher) 
	{
		dispatcher.register(Commands.literal("futurepack").requires((pl) -> 
		{
			return pl.hasPermission(2);
		})
			.then(Commands.literal("research")
				.then(Commands.argument("player", EntityArgument.player())
					.then(Commands.literal("reset")
						.executes(ctx -> {
							CommandFuturepack.reset(ctx.getSource(), ctx.getArgument("player", EntitySelector.class).findSinglePlayer(ctx.getSource()));
							return Command.SINGLE_SUCCESS;
						})
					)
					.then(Commands.literal("unlock")
						.then(Commands.argument("research", StringArgumentType.word())
							.executes(ctx -> {
								unlockResearch(ctx.getSource(), ctx.getArgument("player", EntitySelector.class).findSinglePlayer(ctx.getSource()), ctx.getArgument("research", String.class), false);
								return Command.SINGLE_SUCCESS;
							})
							.then(Commands.literal("single")
								.executes(ctx -> {
									unlockResearch(ctx.getSource(), ctx.getArgument("player", EntitySelector.class).findSinglePlayer(ctx.getSource()), ctx.getArgument("research", String.class), true);
									return Command.SINGLE_SUCCESS;
								})
							)
						)
					)
				)
			)
				
		);
	}
	
	//Workers
	
	private static void reset(CommandSourceStack sender, ServerPlayer target)
	{
		try
		{
			CustomPlayerData data= CustomPlayerData.getDataFromPlayer(target);
			data.reset();
			sender.sendSuccess(new TranslatableComponent("command.fp.research.reset.feedback", new Object[0]), true);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private static void unlockResearch(CommandSourceStack sender, ServerPlayer target, String name, Boolean unsafe) throws CommandRuntimeException {
		
		CustomPlayerData data= CustomPlayerData.getDataFromPlayer(target);
		
		if(name.equalsIgnoreCase("all"))
		{	
			//force advancements
	        for (Advancement advancement : target.getServer().getAdvancements().getAllAdvancements())
	        {
	           if(advancement.getId().getNamespace().equals(Constants.MOD_ID))
	           {
	        	   AdvancementProgress prog = target.getAdvancements().getOrStartProgress(advancement);
	        	   if(prog.isDone())
	        		   continue;
	        	   for(String s : prog.getRemainingCriteria())
	        	   {
	        		   target.getAdvancements().award(advancement, s);
	        	   }
	           }
	        }
			
            //unlock researches
			for(String s : ResearchManager.getAllReseraches())
			{
				Research r = ResearchManager.getResearch(s);
				enable(data, r);
			}	
		}
		else if(name.equalsIgnoreCase("basics"))
		{	
			String bas[] = {"techtabel", "get_started"};
			for(String s : bas)
			{
				Research r = ResearchManager.getResearch(s);
				enable(data, r);
			}	
		}
		else
		{
			Research r = ResearchManager.getResearch(name);
			if(r.getName() == "ERROR")
				sender.sendFailure(new TranslatableComponent("command.fp.research.noresearch", name).setStyle(Style.EMPTY.withColor(ChatFormatting.RED)));
			if(unsafe)
				data.addResearchUnsafe(r);
			else
				enable(data, r);
			name = r.getTranslationKey();
		}

		sender.sendSuccess(new TranslatableComponent("command.fp.research.unlock.success", new TranslatableComponent(name)), true);
		
	}
	
	private static void enable(CustomPlayerData toAdd, Research r)
	{
		Research[] rr = r.getParents();
		
		if(rr!=null)
		{
			for(Research r3 : rr)
			{
				enable(toAdd, r3);
			}
		}		
		toAdd.addResearch(r);
	}
	
}
