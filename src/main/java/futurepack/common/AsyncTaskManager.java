package futurepack.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

public class AsyncTaskManager 
{
	public static <T> ForkJoinTask<T> addTask(Stage<T> stage, Runnable r, T value)
	{
		ForkJoinTask<T> t = getExecutor().submit(r, value);
		stage.add(t);
		return t;
	}
	
	public static <T> ForkJoinTask<T> addTask(Stage<T> stage, Callable<T> call)
	{
		ForkJoinTask<T> t = getExecutor().submit(call);
		stage.add(t);
		return t;
	}
	
	public static ForkJoinPool getExecutor()
	{
		return ForkJoinPool.commonPool();
	}
	
	
	static abstract interface Stage<T>
	{
		public abstract void add(ForkJoinTask<T> task);
		
		public abstract List<T> join();
		
		public abstract boolean joinWithStats();
	}
	
	private static class BasicStage implements Stage<Boolean>
	{
		private final Object LOCK = new Object();
		private List<ForkJoinTask<Boolean>> tasks = null;
		private final String name;
		
		public BasicStage(String name) 
		{
			this.name = name;
		}
		
		@Override
		public void add(ForkJoinTask<Boolean> task)
		{
			synchronized(LOCK)
			{
				if(tasks==null)
				{
					tasks = new ArrayList<ForkJoinTask<Boolean>>();
				}
				tasks.add(task);
			}
		}
		
		@Override
		public List<Boolean> join()
		{
			List<ForkJoinTask<Boolean>> tasks;
			synchronized(LOCK)
			{
				tasks = this.tasks;
				this.tasks = null;
			}
			
			if(tasks==null)
			{
				return Collections.emptyList();
			}
			
			ArrayList<Boolean> results = new ArrayList<Boolean>(tasks.size());
			tasks.forEach(t -> results.add(t.join()));
			return results;
		}
		
		@Override
		public boolean joinWithStats()
		{
			long time = System.currentTimeMillis();
			List<Boolean> result = join();
			time = System.currentTimeMillis() - time;
			int total = result.size();
			result.removeIf(b -> b==null || b.booleanValue());
			int failed = result.size();
			FPLog.logger.info("[%s] Finished %s tasks, %s failed, took %s ms", name,  total, failed, time);
			
			return failed == 0;
		}
	}
	
	public static final Stage<Boolean> ASYNC_RESTARTABLE = new BasicStage("ASYNC");
	
	public static final Stage<Boolean> FILE_IO = new BasicStage("File IO");
	
	public static final Stage<Boolean> RESOURCE_RELOAD = new BasicStage("Reseource Relaod");
	
}
