package futurepack.common;

import futurepack.world.loot.LootFunctionSetupBattery;
import futurepack.world.loot.LootFunctionSetupChip;
import futurepack.world.loot.LootFunctionSetupCore;
import futurepack.world.loot.LootFunctionSetupRam;
import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.storage.loot.Serializer;
import net.minecraft.world.level.storage.loot.functions.LootItemFunction;
import net.minecraft.world.level.storage.loot.functions.LootItemFunctionType;

public class FPLootFunctions 
{
	public static final LootItemFunctionType SETUP_CHIP = registerFunction("fp_setup_chip", new LootFunctionSetupChip.Storage());
	public static final LootItemFunctionType SETUP_CORE = registerFunction("fp_setup_core", new LootFunctionSetupCore.Storage());
	public static final LootItemFunctionType SETUP_RAM = registerFunction("fp_setup_ram", new LootFunctionSetupRam.Storage());
	public static final LootItemFunctionType SETUP_BATTERY = registerFunction("fp_setup_battery", new LootFunctionSetupBattery.Storage());

	/**
	 * Copy of LootFunctionManager
	 * 
	 * @param p_237451_0_
	 * @param p_237451_1_
	 * @return
	 */
	private static LootItemFunctionType registerFunction(String p_237451_0_, Serializer<? extends LootItemFunction> p_237451_1_) 
	{
		return Registry.register(Registry.LOOT_FUNCTION_TYPE, new ResourceLocation(p_237451_0_), new LootItemFunctionType(p_237451_1_));
	}
	
	public static void init()
	{
		FPLog.logger.info("Registered Loot Functions");
	}
}
