package futurepack.common.sync;

import java.util.Arrays;
import java.util.function.Supplier;

import futurepack.client.research.LocalPlayerResearchHelper;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.research.Research;
import futurepack.common.research.ResearchManager;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fmllegacy.network.NetworkEvent;

/**
 * Send to Client with Player Research Data
*/
public class MessageResearchResponse
{
	private int[] local;
	private Integer[] inProg;
	
	public MessageResearchResponse()
	{
		//Only used for Client side
	}
	
	public MessageResearchResponse(MinecraftServer server, CustomPlayerData data)
	{
		inProg = data.getResearchesInProgress(server);
		
		local = new int[data.getAllResearches().size()];
		int pos=0;
		for(String s : data.getAllResearches())
		{
			Research r = ResearchManager.getResearch(s);
			local[pos]=r.id;
			pos++;
		}
		local = compress(local, 0);
	}

	public static MessageResearchResponse decode(FriendlyByteBuf buf) 
	{
		MessageResearchResponse answ = new MessageResearchResponse();
		answ.local = new int[buf.readVarInt()];
		for(int i=0;i<answ.local.length;i++)
		{
			answ.local[i] = buf.readVarInt();
		}
		answ.local = decompress(answ.local, 0);
		
		answ.inProg = new Integer[buf.readVarInt()];
		for(int i=0;i<answ.inProg.length;i++)
		{
			answ.inProg[i] = buf.readVarInt();
		}
		return answ;
	}
	
	public static void encode(MessageResearchResponse msg, FriendlyByteBuf buf) 
	{
		buf.writeVarInt(msg.local.length);
		for(int i : msg.local)
		{
			buf.writeVarInt(i);
		}
		
		buf.writeVarInt(msg.inProg.length);
		for(int i : msg.inProg)
		{
			buf.writeVarInt(i);
		}
	}
	
	public static void consume(MessageResearchResponse message, Supplier<NetworkEvent.Context> ctx) 
	{
		LocalPlayerResearchHelper.setupResearching(message.inProg);
		LocalPlayerResearchHelper.setupData(message.local);
		ctx.get().setPacketHandled(true);
	}
	
	private static int[] compress(int[] data, int start)
	{
		if(start==0)
			Arrays.sort(data);
		
		int begin = start;
		int end = start;
		for(int i=start+1;i<data.length;i++)
		{
			if(data[i] - data[i-1] <= 1)
			{
				end = i;
			}
			else if(end-begin >= 3)
			{
				break;
			}
			else
			{
				begin = i;
				end = i;
			}
		}
		
		if(end-begin >= 3)
		{
			int newLength = 1+begin + 1 + (data.length - end);
			int[] ints = new int[newLength];
			System.arraycopy(data, 0, ints, 0, begin+1);
			ints[begin+1] = -1;
			System.arraycopy(data, end, ints, begin+2, data.length - end);
			return compress(ints, begin+2);
		}
			
		return data;
	}
	
	private static int[] decompress(int[] data, int start)
	{		
		for(int i=start;i<data.length;i++)
		{
			if(data[i] == -1)
			{
				if(i-1 >= 0 && i+1<data.length)
				{
					int beginVal = data[i-1];
					int endVal = data[i+1];
					int amount = endVal - beginVal;
					
					int newLength = data.length -2 + amount;  
					int[] ints = new int[newLength];
					System.arraycopy(data, 0, ints, 0, i-1);
					System.arraycopy(data, i+1, ints, newLength-(data.length-(i+1)), data.length-(i+1));
					
					for(int c=beginVal;c<=endVal;c++)
					{
						ints[i-1] = c;
						i++;
					}
					return decompress(ints, i);
				}
			}
		}
		
		return data;
	}
}
