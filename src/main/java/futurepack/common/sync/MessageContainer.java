package futurepack.common.sync;

import java.util.function.Supplier;

import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import io.netty.buffer.Unpooled;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.level.ServerPlayer;
import net.minecraftforge.fmllegacy.network.NetworkDirection;
import net.minecraftforge.fmllegacy.network.NetworkEvent;

/**
 * Used in some Conatainers for Extra Data (from CLient to Server)
*/
public class MessageContainer 
{
	private FriendlyByteBuf filled;
	
	public MessageContainer(IGuiSyncronisedContainer tile)
	{
		FriendlyByteBuf pb = new FriendlyByteBuf(Unpooled.buffer());
		tile.writeToBuffer(pb);
		filled = pb;
	}
		
	public MessageContainer(FriendlyByteBuf filled) 
	{
		this.filled = filled;
	}
	
	public static MessageContainer decode(FriendlyByteBuf buf) 
	{
		byte[] data = buf.readByteArray();
		return new MessageContainer(new FriendlyByteBuf(Unpooled.wrappedBuffer(data)));
	}

	public static void encode(MessageContainer msg, FriendlyByteBuf buf) 
	{
		byte[] data = new byte[msg.filled.readableBytes()];
		msg.filled.readBytes(data);
		buf.writeByteArray(data);
	}
		
		
	public static void consume(MessageContainer message, Supplier<NetworkEvent.Context> ctx) 
	{
		if(ctx.get().getDirection() == NetworkDirection.PLAY_TO_SERVER)
		{
			ServerPlayer player = ctx.get().getSender();
				
			if(player!=null)
			{
				//System.out.println(player);
				if(player.containerMenu instanceof IGuiSyncronisedContainer)
				{
					IGuiSyncronisedContainer sync = (IGuiSyncronisedContainer) player.containerMenu;
					sync.readFromBuffer(message.filled);
					ctx.get().setPacketHandled(true);
				}
			}
		}
	}
	
}
