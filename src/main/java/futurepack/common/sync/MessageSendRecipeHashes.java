package futurepack.common.sync;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import futurepack.common.recipes.RecipeManagerSyncer;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.fmllegacy.network.NetworkEvent;

public class MessageSendRecipeHashes 
{
	private Map<String, byte[]> nameToHash;
	
	public MessageSendRecipeHashes(Map<String, byte[]> nameToHash)
	{
		this.nameToHash = nameToHash;
	}

	public static void consume(MessageSendRecipeHashes message, Supplier<NetworkEvent.Context> ctx) 
	{
		RecipeManagerSyncer.INSTANCE.onClientResponseHashes(message.nameToHash, ctx.get().getSender());
		ctx.get().setPacketHandled(true);
	}
	
	public static MessageSendRecipeHashes decode(FriendlyByteBuf buf) 
	{
		int size = buf.readVarInt();
		HashMap<String, byte[]> map = new HashMap<String, byte[]>(size);
		for(int i=0;i<size;i++)
		{
			map.put(buf.readUtf(32767), buf.readByteArray());
		}
		return new MessageSendRecipeHashes(map);
	}
		
	public static void encode(MessageSendRecipeHashes msg, FriendlyByteBuf buf) 
	{
		buf.writeVarInt(msg.nameToHash.size());
		msg.nameToHash.forEach((s,b) -> buf.writeUtf(s).writeByteArray(b));
	}
}
