package futurepack.common.sync;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;

import automatic.version.Version;
import futurepack.api.Constants;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.core.BlockPos;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.phys.AABB;
import net.minecraftforge.fmllegacy.network.NetworkRegistry;
import net.minecraftforge.fmllegacy.network.PacketDistributor;
import net.minecraftforge.fmllegacy.network.simple.SimpleChannel;

public class FPPacketHandler 
{
	private static final String NETTY_VERSION = Version.version;
	private static int ID =0;
	
	public static Predicate<String> version = s -> NETTY_VERSION.equals(s);
	public static SimpleChannel CHANNEL_FUTUREPACK = NetworkRegistry.newSimpleChannel(new ResourceLocation(Constants.MOD_ID, "network"), () -> NETTY_VERSION, version, version);
	public static HashMap<UUID,boolean[]> map = new HashMap<UUID,boolean[]>();
	
	
	public static int getNextID()
	{
		return ID++;
	}
	
	public static void init()
	{
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageContainer.class, MessageContainer::encode, MessageContainer::decode, MessageContainer::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageResearchCall.class, MessageResearchCall::encode, MessageResearchCall::decode, MessageResearchCall::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageResearchResponse.class, MessageResearchResponse::encode, MessageResearchResponse::decode, MessageResearchResponse::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageKeyPressed.class, MessageKeyPressed::encode, MessageKeyPressed::decode, MessageKeyPressed::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessagePlayerAir.class, MessagePlayerAir::encode, MessagePlayerAir::decode, MessagePlayerAir::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageOpenGuiClient.class, MessageOpenGuiClient::encode, MessageOpenGuiClient::decode, MessageOpenGuiClient::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageEScanner.class, MessageEScanner::encode, MessageEScanner::decode, MessageEScanner::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageRendering.class, MessageRendering::encode, MessageRendering::decode, MessageRendering::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageAirFilledRoom.class, MessageAirFilledRoom::encode, MessageAirFilledRoom::decode, MessageAirFilledRoom::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageFPData.class, MessageFPData::encode, MessageFPData::decode, MessageFPData::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageItemMove.class, MessageItemMove::encode, MessageItemMove::decode, MessageItemMove::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageEntityForEater.class, MessageEntityForEater::encode, MessageEntityForEater::decode, MessageEntityForEater::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageRequestTerrain.class, MessageRequestTerrain::encode, MessageRequestTerrain::decode,  MessageRequestTerrain::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageSendTerrain.class, MessageSendTerrain::encode, MessageSendTerrain::decode,  MessageSendTerrain::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageHookLines.class, MessageHookLines::encode, MessageHookLines::decode,  MessageHookLines::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageSpaceshipRequest.class, MessageSpaceshipRequest::encode, MessageSpaceshipRequest::decode,  MessageSpaceshipRequest::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageSpaceshipResponse.class, MessageSpaceshipResponse::encode, MessageSpaceshipResponse::decode,  MessageSpaceshipResponse::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageAdditionalContainerData.class, MessageAdditionalContainerData::encode, MessageAdditionalContainerData::decode,  MessageAdditionalContainerData::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageRequestClientRecipeHashes.class, MessageRequestClientRecipeHashes::encode, MessageRequestClientRecipeHashes::decode,  MessageRequestClientRecipeHashes::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageSendRecipeHashes.class, MessageSendRecipeHashes::encode, MessageSendRecipeHashes::decode,  MessageSendRecipeHashes::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageSendRecipes.class, MessageSendRecipes::encode, MessageSendRecipes::decode,  MessageSendRecipes::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageSendAllRecipesCorrect.class, MessageSendAllRecipesCorrect::encode, MessageSendAllRecipesCorrect::decode,  MessageSendAllRecipesCorrect::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageChekpointSelect.class, MessageChekpointSelect::encode, MessageChekpointSelect::decode,  MessageChekpointSelect::consume);
		
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessageRequestJawaulInventory.class, MessageRequestJawaulInventory::encode, MessageRequestJawaulInventory::decode,  MessageRequestJawaulInventory::consume);
		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(FPPacketHandler.getNextID(), MessagePlayerAirTanks.class, MessagePlayerAirTanks::encode, MessagePlayerAirTanks::decode,  MessagePlayerAirTanks::consume);
		
		
		
//		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(MessageMagnetisemUpdate.class, MessageMagnetisemUpdate.class, FPPacketHandler.getID(), Dist.CLIENT);
//		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(MessageRendering.class, MessageRendering.class, FPPacketHandler.getID(), Dist.DEDICATED_SERVER);
//		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(MessageUpdateMetas.class, MessageUpdateMetas.class, FPPacketHandler.getID(), Dist.CLIENT);

//		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(MessageFPData.class, MessageFPData.class, FPPacketHandler.getID(), Dist.CLIENT);

//		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(MessagePlayerAir.class, MessagePlayerAir.class, FPPacketHandler.getID(), Dist.CLIENT);
		
//		FPPacketHandler.CHANNEL_FUTUREPACK.registerMessage(MessageTileDataRequest.class, MessageTileDataRequest.class, FPPacketHandler.getID(), Dist.DEDICATED_SERVER);
		
	}
	
	public static void syncWithServer(IGuiSyncronisedContainer sync)
	{
		FPPacketHandler.CHANNEL_FUTUREPACK.sendToServer(new MessageContainer(sync));
	}
	
	public static <T extends AbstractContainerMenu & ISyncable> void sendToClient(T container, Player pl)
	{
		if(!pl.getCommandSenderWorld().isClientSide())
		{
			try 
			{
				MessageAdditionalContainerData<T> mes = new MessageAdditionalContainerData(container);
				FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with(() -> (ServerPlayer)pl), mes);
			}
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	public static void sendTileEntityPacketToAllClients(BlockEntity tile)
	{
		sendTileEntityPacketToAllClients(tile, new Predicate<ServerPlayer>() //getEntitiesWithinAABBExcludingEntity
		{			
			@Override
			public boolean test(ServerPlayer var1)
			{
				return true;
			}
		}, 20);
	}
	
	public static void sendTileEntityPacketToAllClients(BlockEntity tile, Predicate<ServerPlayer> pred, int range)
	{
		final ClientboundBlockEntityDataPacket pack = tile.getUpdatePacket();
		BlockPos pos = tile.getBlockPos();
		List<ServerPlayer> players = tile.getLevel().getEntitiesOfClass(ServerPlayer.class, new AABB(pos.getX()-range, pos.getY()-range, pos.getZ()-range, pos.getX()+range, pos.getY()+range, pos.getZ()+range), pred);
		for(ServerPlayer var1 : players)
		{
			var1.connection.send(pack);
		}
		
	}
	
//	/**Used for Dust Manager to Client*/
//	public static class MessageUpdateMetas implements IMessage, IMessageHandler<MessageUpdateMetas, IMessage>
//	{
//		public HashMap<String, Integer> IDmap = new HashMap<String, Integer>();
//		
//		public MessageUpdateMetas(HashMap<String, ItemStack> trans)
//		{
//			for(Entry<String, ItemStack> e : trans.entrySet())
//			{
//				if(e.getValue()==null)
//					continue;
//				
//				IDmap.put(e.getKey(), e.getValue().getItemDamage());
//			}
//		}
//		
//		public MessageUpdateMetas()
//		{
//			
//		}
//		
//		@Override
//		public IMessage onMessage(MessageUpdateMetas message, MessageContext ctx)
//		{
//			FPMain.dustManager.replaceEntrys(message.IDmap);
//			return null;
//		}
//
//		@Override
//		public void fromBytes(ByteBuf buf)
//		{
//			IDmap.clear();
//			int lenght = buf.readVarInt();
//			for(int i=0;i<lenght;i++)
//			{
//				byte[] b = new byte[buf.readVarInt()];
//				buf.readBytes(b);
//				int val = buf.readVarInt();
//				String s = new String(b);
//				IDmap.put(s, val);
//			}
//		}
//
//		@Override
//		public void toBytes(ByteBuf buf)
//		{
//			buf.writeVarInt(IDmap.size());
//			for(Entry<String, Integer> e : IDmap.entrySet())
//			{
//				byte[] b = e.getKey().getBytes();
//				buf.writeVarInt(b.length);			
//				buf.writeBytes(b);
//				buf.writeVarInt(e.getValue());
//			}
//		}
//		
//	}
	
	
	
//	/**The Client get the Server properties of Magentic Level of Armor*/
//	public static class MessageMagnetisemUpdate implements IMessage, IMessageHandler<MessageMagnetisemUpdate, IMessage>
//	{
//		private ArrayList<Integer>[] arrays;
//		
//		public MessageMagnetisemUpdate()
//		{
//			
//		}
//		
//		
//		public MessageMagnetisemUpdate(ArrayList<Item> h,ArrayList<Item> c,ArrayList<Item> l,ArrayList<Item> b) 
//		{
//			arrays = new ArrayList[4];
//			arrays[0] = new ArrayList();
//			for(Item i : h)
//			{
//				arrays[0].add(Item.getIdFromItem(i));
//			}
//			arrays[1] = new ArrayList();
//			for(Item i : c)
//			{
//				arrays[1].add(Item.getIdFromItem(i));
//			}
//			arrays[2] = new ArrayList();
//			for(Item i : l)
//			{
//				arrays[2].add(Item.getIdFromItem(i));
//			}
//			arrays[3] = new ArrayList();
//			for(Item i : b)
//			{
//				arrays[3].add(Item.getIdFromItem(i));
//			}
//		}
//		
//		@Override
//		public IMessage onMessage(MessageMagnetisemUpdate m, MessageContext ctx) 
//		{
//			if(m.arrays!=null && m.arrays.length==4)
//			{
//				FPConfig.helmets.clear();
//				FPConfig.chestplates.clear();
//				FPConfig.leggings.clear();
//				FPConfig.boots.clear();
//				
//				for(int i : m.arrays[0])
//				{
//					FPConfig.helmets.add(Item.getItemById(i));
//				}
//				for(int i : m.arrays[1])
//				{
//					FPConfig.chestplates.add(Item.getItemById(i));
//				}
//				for(int i : m.arrays[2])
//				{
//					FPConfig.leggings.add(Item.getItemById(i));
//				}
//				for(int i : m.arrays[3])
//				{
//					FPConfig.boots.add(Item.getItemById(i));
//				}
//			}
//			return null;
//		}
//
//		@Override
//		public void fromBytes(ByteBuf buf) 
//		{
//			arrays = new ArrayList[buf.readVarInt()];
//			for(int i=0;i<arrays.length;i++)
//			{
//				int l = buf.readVarInt();
//				arrays[i] = new ArrayList(l);
//				for(int n=0;n<l;n++)
//				{
//					arrays[i].add(n, buf.readVarInt());
//				}
//			}
//		}
//
//		@Override
//		public void toBytes(ByteBuf buf) 
//		{
//			buf.writeVarInt(arrays.length);
//			for(int i=0;i<arrays.length;i++)
//			{
//				ArrayList<Integer> l = arrays[i];
//				buf.writeVarInt(l.size());
//				for(int n=0;n<l.size();n++)
//				{
//					buf.writeVarInt(l.get(n));
//				}
//			}
//		}
//		
//	}


	
//	/** Requests an Tile updat e from the client */
//	public static class MessageTileDataRequest implements IMessage, IMessageHandler<MessageTileDataRequest, IMessage>
//	{
//		BlockPos tile; 
//		
//		public MessageTileDataRequest()
//		{
//			
//		}
//		
//		public MessageTileDataRequest(BlockPos pos)
//		{
//			tile = pos;
//		}
//		
//		@Override
//		public IMessage onMessage(MessageTileDataRequest message, MessageContext ctx)
//		{
//			EntityPlayerMP mp = ctx.getServerHandler().player;
//			TileEntity tile = mp.world.getTileEntity(message.tile);
//			
//			if(tile != null)
//			{
//				SPacketUpdateTileEntity update = tile.getUpdatePacket();
//				ctx.getServerHandler().sendPacket(update);
//			}
//			return null;
//		}
//
//		@Override
//		public void fromBytes(ByteBuf buf)
//		{
//			tile = new BlockPos(buf.readVarInt(), buf.readVarInt(), buf.readVarInt());
//		}
//
//		@Override
//		public void toBytes(ByteBuf buf)
//		{
//			  buf.writeVarInt(tile.getX());
//			  buf.writeVarInt(tile.getY());
//			  buf.writeVarInt(tile.getZ());  
//		}		
//	}


}
