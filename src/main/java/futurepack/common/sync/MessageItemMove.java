package futurepack.common.sync;

import java.util.Collection;
import java.util.Iterator;
import java.util.function.Supplier;

import futurepack.common.block.ItemMoveTicker;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.fmllegacy.network.NetworkEvent;

/**Used by the ItemMover to sync*/
public class MessageItemMove
{
	int dim;
	Vec3 start,end;
	int[] entities;
	
	public MessageItemMove()
	{
		
	}
	
	public MessageItemMove(Level w , Vec3 toGo, Vec3 start, Collection<ItemEntity> ids)
	{
		this.dim = w.dimension().location().toString().hashCode();
		
		this.start = start;
		this.end = toGo;
		this.entities = new int[ids.size()];
		int i=0;
		Iterator<ItemEntity> iter = ids.iterator();
		while(iter.hasNext())
		{
			entities[i++] = iter.next().getId();
		}
	}
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	private static void onMessage(final MessageItemMove message)
	{
		Runnable run = new Runnable()
		{			
			@Override
			public void run()
			{
				Level w = Minecraft.getInstance().level;
				if(w.dimension().location().toString().hashCode() == message.dim) //check if names are same hash
				{
					while(w.getEntity(message.entities[0])==null)
					{
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					new ItemMoveTicker(w, message.end, message.start, message.entities);
				}				
			}
		};
		Thread t = new Thread(run);
		t.setDaemon(true);
		t.start();
	}

	public static void consume(MessageItemMove message, Supplier<NetworkEvent.Context> ctx) 
	{
		if(message.entities.length>0)
		{
			if(ctx.get().getSender()==null)
				onMessage(message);
		
		}
		ctx.get().setPacketHandled(true);
	}
	
	public static MessageItemMove decode(FriendlyByteBuf buf) 
	{
		MessageItemMove mes = new MessageItemMove();
		mes.dim = buf.readVarInt();
		mes.start = new Vec3(buf.readFloat(), buf.readFloat(), buf.readFloat());
		mes.end = new Vec3(buf.readFloat(), buf.readFloat(), buf.readFloat());
		mes.entities = new int[buf.readVarInt()];
		for(int i=0;i<mes.entities.length;i++)
		{
			mes.entities[i] = buf.readVarInt();
		}
		return mes;
	}
		
	public static void encode(MessageItemMove msg, FriendlyByteBuf buf) 
	{
		buf.writeVarInt(msg.dim);
		buf.writeFloat((float) msg.start.x);
		buf.writeFloat((float) msg.start.y);
		buf.writeFloat((float) msg.start.z);
		buf.writeFloat((float) msg.end.x);
		buf.writeFloat((float) msg.end.y);
		buf.writeFloat((float) msg.end.z);
		buf.writeVarInt(msg.entities.length);
		for(int i=0;i<msg.entities.length;i++)
		{
			buf.writeVarInt(msg.entities[i]);
		}
	}
		
	
	
}
