package futurepack.common.sync;

import org.lwjgl.glfw.GLFW;

import futurepack.common.gui.GuiAllKeys;
import futurepack.common.sync.KeyManager.EnumKeyTypes;
import net.minecraft.client.KeyMapping;
import net.minecraft.client.Minecraft;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.client.settings.KeyConflictContext;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fmlclient.registry.ClientRegistry;

//@ TODO: OnlyIn(Dist.CLIENT)
public class KeyBindAuto extends KeyMapping
{
	private static final String categoryName = "key.categories.futurepack";
	
	
	public static void init()
	{
		new KeyBindAuto(GLFW.GLFW_KEY_X, EnumKeyTypes.ALL_BUTTONS).setKeyConflictContext(KeyConflictContext.IN_GAME);
		new KeyBindAuto(-1, EnumKeyTypes.COMPOSITEARMOR).setKeyConflictContext(KeyConflictContext.IN_GAME);
		new KeyBindAuto(-1, EnumKeyTypes.GLEITER).setKeyConflictContext(KeyConflictContext.IN_GAME);
		new KeyBindAuto(-1, EnumKeyTypes.MODUL_MAGNET).setKeyConflictContext(KeyConflictContext.IN_GAME);
	}
	
//	static
//	{
//		Class cls = KeyBinding.class;
//		Field f_map = cls.getDeclaredFields()[3];
//		f_map.setAccessible(true);
//		
//		try {
//			Map<String,Integer> map = (Map<String, Integer>) f_map.get(null);
//			map.put("key.categories.futurepack", map.size());
//		} 
//		catch (IllegalArgumentException e) {
//			e.printStackTrace();
//		} catch (IllegalAccessException e) {
//			e.printStackTrace();
//		}
//		
//	}
	
	private EnumKeyTypes type;
	
	
	public KeyBindAuto(int keyCode, EnumKeyTypes type) 
	{
		super(type.translationKey, keyCode, categoryName);
		this.type=type;
		ClientRegistry.registerKeyBinding(this);
		MinecraftForge.EVENT_BUS.register(this);
	}
	
	@SubscribeEvent
	public void onInput(InputEvent.KeyInputEvent key)
	{
		if(this.isDown())
		{
			
			if(type == EnumKeyTypes.ALL_BUTTONS)
			{
				Minecraft.getInstance().setScreen(new GuiAllKeys());
			}
			else
			{
				NetworkHandler.sendKeyPressedToServer(type);
			}
		}
	}
}
