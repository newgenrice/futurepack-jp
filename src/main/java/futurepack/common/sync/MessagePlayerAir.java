package futurepack.common.sync;

import java.util.function.Supplier;

import futurepack.api.interfaces.IAirSupply;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fmllegacy.network.NetworkEvent;

/** 
 * Send the modified Air to the Player (for Athmosphereless planets)
 */
public class MessagePlayerAir
{
	private int playerAir;
	
	
	public MessagePlayerAir(int playerAir)
	{
		this.playerAir = playerAir;
	}
	
	public MessagePlayerAir(Player pl)
	{
		this(pl.getCapability(AtmosphereManager.cap_AIR, null).orElseThrow(IllegalArgumentException::new).getAir());
	}
	
	public static MessagePlayerAir decode(FriendlyByteBuf buf) 
	{
		return new MessagePlayerAir(buf.readVarInt());
	}
	
	public static void encode(MessagePlayerAir msg, FriendlyByteBuf buf) 
	{
		buf.writeVarInt(msg.playerAir);
	}
	
	public static void consume(MessagePlayerAir message, Supplier<NetworkEvent.Context> ctx) 
	{
		DistExecutor.runWhenOn(Dist.CLIENT, () ->  
											() -> 
		{
			LazyOptional<IAirSupply> l = Minecraft.getInstance().player.getCapability(AtmosphereManager.cap_AIR, null);
			l.ifPresent(air -> {
				air.addAir(message.playerAir - air.getAir());
			});
			ctx.get().setPacketHandled(true);
		}); 
	}
}
