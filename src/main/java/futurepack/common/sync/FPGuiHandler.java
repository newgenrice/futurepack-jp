package futurepack.common.sync;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import futurepack.api.interfaces.tilentity.ITileRenameable;
import futurepack.api.interfaces.tilentity.ITileScrollableInventory;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.block.inventory.TileEntityAssemblyTable;
import futurepack.common.block.inventory.TileEntityBatteryBox;
import futurepack.common.block.inventory.TileEntityBlockBreaker;
import futurepack.common.block.inventory.TileEntityBoardComputer;
import futurepack.common.block.inventory.TileEntityBrennstoffGenerator;
import futurepack.common.block.inventory.TileEntityDroneStation;
import futurepack.common.block.inventory.TileEntityFermentationBarrel;
import futurepack.common.block.inventory.TileEntityFlashServer;
import futurepack.common.block.inventory.TileEntityForscher;
import futurepack.common.block.inventory.TileEntityFuelCell;
import futurepack.common.block.inventory.TileEntityIndustrialFurnace;
import futurepack.common.block.inventory.TileEntityModulT1;
import futurepack.common.block.inventory.TileEntityModulT2;
import futurepack.common.block.inventory.TileEntityModulT3;
import futurepack.common.block.inventory.TileEntityOptiBenchCraftingModule;
import futurepack.common.block.inventory.TileEntityPartPress;
import futurepack.common.block.inventory.TileEntityScannerBlock;
import futurepack.common.block.inventory.TileEntityTechtable;
import futurepack.common.block.inventory.TileEntityWaterCooler;
import futurepack.common.block.logistic.TileEntitySyncronizer;
import futurepack.common.block.logistic.monorail.TileEntityMonorailStation;
import futurepack.common.block.logistic.plasma.TileEntityPlasmaConverter;
import futurepack.common.block.misc.TileEntityAntenna;
import futurepack.common.block.misc.TileEntityClaime;
import futurepack.common.block.misc.TileEntityRFtoNEConverter;
import futurepack.common.block.misc.TileEntityRsTimer;
import futurepack.common.block.modification.TileEntityFluidPump;
import futurepack.common.block.modification.TileEntityLaserBase;
import futurepack.common.block.modification.TileEntityModificationBase;
import futurepack.common.block.modification.TileEntityModulT1Calculation;
import futurepack.common.block.modification.TileEntityRocketLauncher;
import futurepack.common.block.modification.machines.TileEntityCrusher;
import futurepack.common.block.modification.machines.TileEntityGasTurbine;
import futurepack.common.block.modification.machines.TileEntityImproveComponents;
import futurepack.common.block.modification.machines.TileEntityIndustrialNeonFurnace;
import futurepack.common.block.modification.machines.TileEntityInfusionGenerator;
import futurepack.common.block.modification.machines.TileEntityIonCollector;
import futurepack.common.block.modification.machines.TileEntityLifeSupportSystem;
import futurepack.common.block.modification.machines.TileEntityNeonFurnace;
import futurepack.common.block.modification.machines.TileEntityOptiAssembler;
import futurepack.common.block.modification.machines.TileEntityOptiBench;
import futurepack.common.block.modification.machines.TileEntityRecycler;
import futurepack.common.block.modification.machines.TileEntitySolarPanel;
import futurepack.common.block.modification.machines.TileEntitySorter;
import futurepack.common.block.modification.machines.TileEntityZentrifuge;
import futurepack.common.block.multiblock.TileEntityDeepCoreMinerMain;
import futurepack.common.entity.EntityDrone;
import futurepack.common.entity.monocart.EntityMonocart;
import futurepack.common.gui.GuiMiner;
import futurepack.common.gui.GuiRsTimer;
import futurepack.common.gui.inventory.GuiAirBrush;
import futurepack.common.gui.inventory.GuiAntenne;
import futurepack.common.gui.inventory.GuiAssemblyRezeptCreator;
import futurepack.common.gui.inventory.GuiAssemblyTable;
import futurepack.common.gui.inventory.GuiBatteryBox;
import futurepack.common.gui.inventory.GuiBlockBreaker;
import futurepack.common.gui.inventory.GuiBlockScanner;
import futurepack.common.gui.inventory.GuiBoardComputer;
import futurepack.common.gui.inventory.GuiBrennstoffGenerator;
import futurepack.common.gui.inventory.GuiChipset;
import futurepack.common.gui.inventory.GuiClaime;
import futurepack.common.gui.inventory.GuiCompositeArmor;
import futurepack.common.gui.inventory.GuiCompositeChest;
import futurepack.common.gui.inventory.GuiConstructionTable;
import futurepack.common.gui.inventory.GuiCrusher;
import futurepack.common.gui.inventory.GuiDeepCoreMiner;
import futurepack.common.gui.inventory.GuiDroneStation;
import futurepack.common.gui.inventory.GuiFermentationBarrel;
import futurepack.common.gui.inventory.GuiFlashServer;
import futurepack.common.gui.inventory.GuiFluidPump;
import futurepack.common.gui.inventory.GuiForscher;
import futurepack.common.gui.inventory.GuiFuelZell;
import futurepack.common.gui.inventory.GuiGasTurbine;
import futurepack.common.gui.inventory.GuiImproveComponents;
import futurepack.common.gui.inventory.GuiIndFurnace;
import futurepack.common.gui.inventory.GuiIndNeonFurnace;
import futurepack.common.gui.inventory.GuiInfusionGenerator;
import futurepack.common.gui.inventory.GuiIonCollector;
import futurepack.common.gui.inventory.GuiJSFilterEditor;
import futurepack.common.gui.inventory.GuiJSFilterTester;
import futurepack.common.gui.inventory.GuiLaserEditor;
import futurepack.common.gui.inventory.GuiLifeSupportSystem;
import futurepack.common.gui.inventory.GuiModulT1;
import futurepack.common.gui.inventory.GuiModulT1Calculation;
import futurepack.common.gui.inventory.GuiModulT2;
import futurepack.common.gui.inventory.GuiModulT3;
import futurepack.common.gui.inventory.GuiMonocart;
import futurepack.common.gui.inventory.GuiMonorailStation;
import futurepack.common.gui.inventory.GuiNeonFurnace;
import futurepack.common.gui.inventory.GuiOptiAssembler;
import futurepack.common.gui.inventory.GuiOptiBench;
import futurepack.common.gui.inventory.GuiOptiBenchCraftingModule;
import futurepack.common.gui.inventory.GuiPartPress;
import futurepack.common.gui.inventory.GuiPlasmaConverter;
import futurepack.common.gui.inventory.GuiRFtoNEConverter;
import futurepack.common.gui.inventory.GuiRecycler;
import futurepack.common.gui.inventory.GuiRenameWaypoint;
import futurepack.common.gui.inventory.GuiResearchReward;
import futurepack.common.gui.inventory.GuiRocketLauncher;
import futurepack.common.gui.inventory.GuiScrollableInventory;
import futurepack.common.gui.inventory.GuiSolarPanel;
import futurepack.common.gui.inventory.GuiSorter;
import futurepack.common.gui.inventory.GuiSyncronizer;
import futurepack.common.gui.inventory.GuiTechTable;
import futurepack.common.gui.inventory.GuiWaterCooler;
import futurepack.common.gui.inventory.GuiZentrifuge;
import futurepack.depend.api.interfaces.ITileInventoryProvider;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerContainerEvent;
import net.minecraftforge.fmllegacy.network.NetworkDirection;

public enum FPGuiHandler
{
	TECHTABLE(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i, t) -> new GuiTechTable.ContainerTechTable(i, (TileEntityTechtable) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>()
			{
				@Override
				public Screen apply(Player pl, BlockEntity t) 
				{
					return new GuiTechTable(pl, (TileEntityTechtable) t) ;
				}
			})),
	BLOCK_SCANNER(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i, t) -> new GuiBlockScanner.ContainerScannerBlock(i, (TileEntityScannerBlock) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>()
			{
				@Override
				public Screen apply(Player pl, BlockEntity t) 
				{
					return new GuiBlockScanner(pl, (TileEntityScannerBlock) t) ;
				}
			})),
	FORSCHER(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) -> new GuiForscher.ContainerForscher(i, (TileEntityForscher) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>()
			{
				@Override
				public Screen apply(Player pl, BlockEntity t) 
				{
					return new GuiForscher(pl, (TileEntityForscher) t);
				}
			})),
	AIRBRUSH(EnumSerialisation.NONE, 
			Helper.fromNone(GuiAirBrush.ContainerAirBrush::new), 
			() -> Helper.fromNone(GuiAirBrush::new)),
	COMPOSITE_ARMOR(EnumSerialisation.NONE, 
			Helper.fromNone(GuiCompositeArmor.ContainerCompositeArmor::new), 
			() -> Helper.fromNone(GuiCompositeArmor::new)),//this could crash the server, but currently this is never triggered
	INFUSION_GENERATOR(EnumSerialisation.BLOCK,
			Helper.fromBlockCont((i,t) -> new GuiInfusionGenerator.ContainerInfusionGenerator(i, (TileEntityInfusionGenerator) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>()
			{
				@Override
				public Screen apply(Player pl, BlockEntity t) 
				{
					return new GuiInfusionGenerator(pl, (TileEntityInfusionGenerator) t) ;
				}
			})),
	INDUSTRIAL_NEON_FURNACE(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) -> new GuiIndNeonFurnace.ContainerIndNeonFurnace(i, (TileEntityIndustrialNeonFurnace) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>()
			{
				@Override
				public Screen apply(Player pl, BlockEntity t) 
				{
					return new GuiIndNeonFurnace(pl, (TileEntityIndustrialNeonFurnace) t) ;
				}
			})),
	OPTI_BENCH(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) -> new GuiOptiBench.ContainerOptiBench<TileEntityOptiBench>(i, (TileEntityOptiBench) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>()
			{
				@Override
				public Screen apply(Player pl, BlockEntity t) 
				{
					return new GuiOptiBench(pl, (TileEntityOptiBench) t) ;
				}
			})),
	OPTI_BENCH_CRAFTING_MODULE(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) -> new GuiOptiBench.ContainerOptiBench<TileEntityOptiBenchCraftingModule>(i, (TileEntityOptiBenchCraftingModule) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>()
			{
				@Override
				public Screen apply(Player pl, BlockEntity t) 
				{
					return new GuiOptiBenchCraftingModule(pl, (TileEntityOptiBenchCraftingModule) t) ;
				}
			})),
	CONSTRUCTION_TABLE(EnumSerialisation.BLOCK, 
			(i,w,buf) -> new GuiConstructionTable.ContainerConstructionTable(i.getInventory(), ((BlockPos)buf[0])), 
			() -> new TriFunction<Screen, Player, Level, Object[]>() 
			{
				@Override
				public Screen apply(Player pl, Level j, Object[] buf) 
				{
					return new GuiConstructionTable(pl.getInventory(), ((BlockPos)buf[0]));
				}
			}),
	OPTI_ASSEMBLER(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont( (i,t) -> new GuiOptiAssembler.ContainerOptiAssembler(i, (TileEntityOptiAssembler) t) ),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>()
			{
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiOptiAssembler(pl, (TileEntityOptiAssembler) t) ;}})),
	OPTI_ASSEMBLER_RECIPES(EnumSerialisation.BLOCK, 
			(i,w,buf) -> new GuiAssemblyRezeptCreator.ContainerAssemblyRezeptCreator(i, ((BlockPos)buf[0]) ), 
			() -> new TriFunction<Screen, Player, Level, Object[]>() 
			{
				@Override
				public Screen apply(Player pl, Level j, Object[] buf) 
				{
					return new GuiAssemblyRezeptCreator(pl, ((BlockPos)buf[0]));
				}
			}),
	CRUSHER(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) -> new GuiCrusher.ContainerCrusher(i, (TileEntityCrusher) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>()
			{
				@Override
				public Screen apply(Player pl, BlockEntity t) 
				{
					return new GuiCrusher(pl, (TileEntityCrusher) t) ;
				}
			})),
	ION_COLLECTOR(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) -> new GuiIonCollector.ContainerIonCollector(i, (TileEntityIonCollector)t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiIonCollector(pl, (TileEntityIonCollector) t) ;}})),
	NEON_FURNACE(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) -> new GuiNeonFurnace.ContainerNeonFurnace(i, (TileEntityNeonFurnace) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiNeonFurnace(pl, (TileEntityNeonFurnace) t) ;}})),
	GAS_TURBINE(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) -> new GuiGasTurbine.ContainerGasTurbine(i, (TileEntityGasTurbine) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiGasTurbine(pl, (TileEntityGasTurbine) t) ;}})),
	SOLAR_PANEL(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont( (i,t) -> new GuiSolarPanel.ContainerSolarPanel(i, (TileEntitySolarPanel) t) ),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiSolarPanel(pl, (TileEntitySolarPanel) t)  ;}})),
	RECYCLER(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)-> new GuiRecycler.ContainerRecycler(i, (TileEntityRecycler) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiRecycler(pl, (TileEntityRecycler) t) ;}})), 
	ZENTRIFUGE(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)-> new GuiZentrifuge.ContainerZentrifuge(i, (TileEntityZentrifuge) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiZentrifuge(pl, (TileEntityZentrifuge) t) ;}})),
	FLUID_PUMP(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)-> new GuiFluidPump.ContainerPump(i, (TileEntityFluidPump) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiFluidPump(pl, (TileEntityFluidPump) t) ;}})),
	SORTER(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)-> new GuiSorter.ContainerSorter(i, (TileEntitySorter) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiSorter(pl, (TileEntitySorter) t) ;}})),
	INDUSTRIAL_FURNACE(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)->new GuiIndFurnace.ContainerIndFurnace(i, (TileEntityIndustrialFurnace) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiIndFurnace(pl, (TileEntityIndustrialFurnace) t) ;}})),
	ASSEMBLY_TABLE(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)->new GuiAssemblyTable.ContainerAssemblyTable(i, (TileEntityAssemblyTable) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiAssemblyTable(pl, (TileEntityAssemblyTable) t) ;}})),
	T0_GENERATOR(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) -> new GuiBrennstoffGenerator.ContainerBrennstoffGenerator(i, (TileEntityBrennstoffGenerator) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiBrennstoffGenerator(pl, (TileEntityBrennstoffGenerator) t) ;}})),
	GENERIC_CHEST(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) -> ((ITileInventoryProvider)t).getInventoryContainer(i)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiCompositeChest(pl, (ITileInventoryProvider) t) ;}})),
	FLASH_SERVER(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) -> new GuiFlashServer.ContainerFlashServer(i, (TileEntityFlashServer)t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return  new GuiFlashServer(pl, (TileEntityFlashServer)t) ;}})),
	BATTERY_BOX(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)-> new GuiBatteryBox.ContainerBatteryBox(i, (TileEntityBatteryBox)t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiBatteryBox(pl, (TileEntityBatteryBox)t) ;}})),
	PART_PRESS(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)-> new GuiPartPress.ContainerPartPress(i, (TileEntityPartPress)t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return  new GuiPartPress(pl, (TileEntityPartPress)t) ;}})),
	BLOCK_BREAKER(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)->new GuiBlockBreaker.ContainerBlockBreaker(i, (TileEntityBlockBreaker) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiBlockBreaker(pl, (TileEntityBlockBreaker) t) ;}})),
	FUEL_CELL(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)->new GuiFuelZell.ContainerFuellCell(i, (TileEntityFuelCell) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return  new GuiFuelZell(pl, (TileEntityFuelCell)t) ;}})),
	CHIPSET(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) -> new GuiChipset.ContainerChipset(i, (TileEntityModificationBase) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiChipset(pl, (TileEntityModificationBase) t) ;}})),
	ANTENNA(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)->new GuiAntenne.ContainerAntenne(i, t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiAntenne(pl,  t) ;}})),
	LASER_EDIT(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)->new GuiLaserEditor.ContainerLaserEditor(i, (TileEntityLaserBase<?>) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiLaserEditor(pl, (TileEntityLaserBase<?>) t) ;}})),
	ROCKET_LAUNCHER_EDIT(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)->new GuiLaserEditor.ContainerLaserEditor(i, (TileEntityLaserBase<?>) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiRocketLauncher(pl, (TileEntityRocketLauncher)t) ;}})),
	DRONE_STATION(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)-> new GuiDroneStation.ContainerDroneStation(i, (TileEntityDroneStation) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiDroneStation(pl, (TileEntityDroneStation) t) ;}})),
	RS_TIMER(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)-> new GuiRsTimer.ContainerRsTimer(i, (TileEntityRsTimer) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiRsTimer(pl, (TileEntityRsTimer) t) ;}})),
	CLAIME(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)-> new GuiClaime.ContainerClaime(i, (TileEntityClaime) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiClaime(pl, (TileEntityClaime) t) ;}})),
	DRONE_CLAIME(EnumSerialisation.ENTITY_ID, 
			Helper.fromEntityCont((i,e)-> new GuiMiner.ContainerMiner(i.player, (EntityDrone) e)), 
			() -> Helper.fromEntityGui(new BiFunction<Player, Entity, Screen>(){
				@Override
				public Screen apply(Player pl, Entity e) {return new GuiMiner(pl, (EntityDrone) e) ;}})),
	MODUL_1(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)-> new GuiModulT1.ContainerModulT1(i, (TileEntityModulT1) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiModulT1(pl, (TileEntityModulT1) t) ;}})),
	MODUL_2(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)-> new GuiModulT2.ContainerModulT2(i, (TileEntityModulT2) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiModulT2(pl, (TileEntityModulT2) t) ;}})),
	MODUL_3(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)-> new GuiModulT3.ContainerModulT3(i, (TileEntityModulT3) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiModulT3(pl, (TileEntityModulT3) t) ;}})),
	SYNCRONIZER(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)-> new GuiSyncronizer.ContainerSyncronizer(i, (TileEntitySyncronizer) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiSyncronizer(pl, (TileEntitySyncronizer) t) ;}})),
	RF2NE_CONVERTER(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)-> new GuiRFtoNEConverter.ContainerRFNEConverter((TileEntityRFtoNEConverter) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiRFtoNEConverter(pl, (TileEntityRFtoNEConverter) t) ;}})),
	RENAME_WAYPOINT(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)-> new GuiRenameWaypoint.ContainerRenameWaypoint(i, (ITileRenameable)t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return  new GuiRenameWaypoint(pl, (ITileRenameable)t) ;}})),
	MONOCART(EnumSerialisation.ENTITY_ID, 
			Helper.fromEntityCont((i,e) -> new GuiMonocart.ContainerMonocart(i, (EntityMonocart)e)), 
			() -> Helper.fromEntityGui(new BiFunction<Player, Entity, Screen>(){
				@Override
				public Screen apply(Player pl, Entity e) {return new GuiMonocart(pl, (EntityMonocart)e) ;}})),
	MONORAIL_STATION(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)-> new GuiMonorailStation.ContainerMonorailSation(i, (TileEntityMonorailStation)t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return  new GuiMonorailStation(pl, (TileEntityMonorailStation)t) ;}})),
	BOARD_COMPUTER(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) ->new GuiBoardComputer.ContainerBoardComputer(i, (TileEntityBoardComputer)t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return  new GuiBoardComputer(pl, (TileEntityBoardComputer)t) ;}})),
	BOARD_COMPUTER_BLACK(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) ->new GuiBoardComputer.ContainerBoardComputer(i, (TileEntityBoardComputer)t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiBoardComputer.Black(pl, (TileEntityBoardComputer)t) ;}})),
	BOARD_COMPUTER_LIGHT_GRAY(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) ->new GuiBoardComputer.ContainerBoardComputer(i, (TileEntityBoardComputer)t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiBoardComputer.Gray(pl, (TileEntityBoardComputer)t) ;}})),
	BOARD_COMPUTER_WHITE(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) ->new GuiBoardComputer.ContainerBoardComputer(i, (TileEntityBoardComputer)t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiBoardComputer.White(pl, (TileEntityBoardComputer)t) ;}})),
	DEEPCORE_MINER(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) -> new GuiDeepCoreMiner.ContainerCoreMiner(i, (TileEntityDeepCoreMinerMain)t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiDeepCoreMiner(pl, (TileEntityDeepCoreMinerMain) t) ;}})),
	SCROLLABLE_INVENTORY(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) -> new GuiScrollableInventory.ContainerScollable(i, (ITileScrollableInventory)t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiScrollableInventory(pl, (ITileScrollableInventory) t) ;}
			})),
	JS_FILTER_EDITOR(EnumSerialisation.NONE, 
			(pl, w, obj) -> new GuiJSFilterEditor.ContainerJSFilterEditor(pl.getInventory()), 
			() -> new TriFunction<Screen, Player, Level, Object[]>() 
			{
				@Override
				public Screen apply(Player pl, Level j, Object[] k) 
				{
					return new GuiJSFilterEditor(pl);
				}
			}),
	FERMENTATION_BARREL(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) -> new GuiFermentationBarrel.ContainerFermentationBarrel(i, (TileEntityFermentationBarrel)t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiFermentationBarrel(pl, (TileEntityFermentationBarrel) t) ;}
			})),
	WATER_COOLER(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) -> new GuiWaterCooler.ContainerWaterCooler(i, (TileEntityWaterCooler)t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>()
			{
				@Override
				public Screen apply(Player pl, BlockEntity t) 
				{
					return new GuiWaterCooler(pl, (TileEntityWaterCooler) t);
				}
			})),
	JS_FILTER_TEST(EnumSerialisation.NONE, 
			(pl, w, obj) -> new GuiJSFilterTester.ContainerJSFilterTester(pl.getInventory()), 
			() -> new TriFunction<Screen, Player, Level, Object[]>() 
			{
				@Override
				public Screen apply(Player pl, Level j, Object[] k) 
				{
					return new GuiJSFilterTester(pl);
				}
			}),
	IMPROVE_COMPONENTS(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) -> new GuiImproveComponents.ContainerImproveComponents(i, (TileEntityImproveComponents)t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiImproveComponents(pl, (TileEntityImproveComponents) t) ;}
			})),
	PLASMA_CONVERTER(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) -> new GuiPlasmaConverter.ContainerPlasmaConverter(i, (TileEntityPlasmaConverter)t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiPlasmaConverter(pl, (TileEntityPlasmaConverter) t) ;}
			})),
	RESEARCH_REWARDS(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) -> new GuiResearchReward.ContainerResearchReward(i, ((TileEntityForscher) t).getResearchRewards())),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>()
			{
				@Override
				public Screen apply(Player pl, BlockEntity t) 
				{
					return new GuiResearchReward(pl, ((TileEntityForscher) t).getResearchRewards());
				}
			})),
	MODUL_1_CALC(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t)-> new GuiModulT1Calculation.ContainerModulT1Calculation(i, (TileEntityModulT1Calculation) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiModulT1Calculation(pl, (TileEntityModulT1Calculation) t) ;}})),
	LIFE_SUPPORT_SYSTEM(EnumSerialisation.BLOCK, 
			Helper.fromBlockCont((i,t) -> new GuiLifeSupportSystem.ContainerLifeSupportSystem(i, (TileEntityLifeSupportSystem) t)),
			() -> Helper.fromBlockGui(new BiFunction<Player, BlockEntity, Screen>(){
				@Override
				public Screen apply(Player pl, BlockEntity t) {return new GuiLifeSupportSystem(pl, (TileEntityLifeSupportSystem) t) ;}})),
	;
	

	private static final FPGuiHandler[] LOOKUP = values();

	private final EnumSerialisation type;
	private final TriFunction<AbstractContainerMenu, Player, Level, Object[]> cont;
	private final Supplier<TriFunction<Screen, Player, Level, Object[]>> gui;
	private final int ID;
	
	private FPGuiHandler(EnumSerialisation ser, TriFunction<AbstractContainerMenu, Player, Level, Object[]> containerConstructor, Supplier<TriFunction<Screen, Player, Level, Object[]>> guiConstructor)
	{
		this.cont = containerConstructor;
		this.gui = guiConstructor;
		this.type = ser;
		ID = ordinal();
	}


	public Supplier<AbstractContainerMenu> getContainer(Player pl, Level w, Object[] data)
	{
		return () -> cont.apply(pl, w, data);
	}
	
	public Supplier<Screen> getGui(Player pl, Level w, Object[] data)
	{
		return new Supplier<Screen>()
		{
			@Override
			public Screen get() 
			{
				return gui.get().apply(pl, w, data);
			}
		};
	}
	
	public EnumSerialisation getSerialisationType()
	{
		return type;
	}
	
	public void writeToBuffer(FriendlyByteBuf buf, Object...obj)
	{
		type.serialize(buf, obj);
	}
	
	public Object[] readFromBuffer(FriendlyByteBuf buf)
	{
		return type.deserialize(buf);
	}
	
	public int getID()
	{
		return ID;
	}
	
	public static FPGuiHandler fromId(int id)
	{
		return LOOKUP[id];
	}
	
	
	public void openGui(ServerPlayer pl, Object[] data)
	{
		pl.closeContainer();
		pl.nextContainerCounter();
		
		AbstractContainerMenu c = getContainer(pl, pl.getCommandSenderWorld(), data).get();
		pl.containerMenu = c;
		pl.containerMenu.containerId = pl.containerCounter;
		
		MessageOpenGuiClient msg = new MessageOpenGuiClient(this, pl.containerCounter, data);
		FPPacketHandler.CHANNEL_FUTUREPACK.sendTo(msg, pl.connection.getConnection(), NetworkDirection.PLAY_TO_CLIENT);

		pl.initMenu(pl.containerMenu);
		MinecraftForge.EVENT_BUS.post(new PlayerContainerEvent.Open(pl, c));
	}
	
	public void openGui(Player pl, BlockPos pos)
	{
		if(type != EnumSerialisation.BLOCK)
			throw new RuntimeException("The Container is not BlockPos encoded!");
		
		if(!pl.getCommandSenderWorld().isClientSide)
			openGui((ServerPlayer) pl, new Object[]{pos});
	}
	
	public void openGui(Player pl, Entity entity)
	{
		if(type != EnumSerialisation.ENTITY_ID)
			throw new RuntimeException("The Container is not Entity encoded!");
		
		if(!pl.getCommandSenderWorld().isClientSide)
			openGui((ServerPlayer) pl, new Object[]{entity.getId()});
	}
	
	public static enum EnumSerialisation
	{
		NONE,
		BLOCK
		{
			@Override
			public Object[] deserialize(FriendlyByteBuf buf) 
			{
				return new Object[]{buf.readBlockPos()};
			}
			
			@Override
			public void serialize(FriendlyByteBuf buf, Object[] obj) 
			{
				buf.writeBlockPos((BlockPos) obj[0]);
			}
		},
		ENTITY_ID
		{
			@Override
			public void serialize(FriendlyByteBuf buf, Object[] obj)
			{
				buf.writeVarInt( ((int)obj[0]));
			}
			
			@Override
			public Object[] deserialize(FriendlyByteBuf buf)
			{
				return new Object[]{buf.readVarInt()};
			}
		};
		
		public void serialize(FriendlyByteBuf buf, Object[] obj)
		{
			
		}
		
		public Object[] deserialize(FriendlyByteBuf buf)
		{
			return null;
		}
	}
	
	@FunctionalInterface
	public static interface TriFunction<T, I,J,K>
	{
		public T apply(I i, J j, K k);
	}
	
	public static class Helper
	{
		public static TriFunction<Screen, Player, Level, Object[]> fromBlockGui(BiFunction<Player, BlockEntity, Screen> constructor)
		{
			return new TriFunction<Screen, Player, Level, Object[]>() 
			{
				@Override
				public Screen apply(Player pl, Level w, Object[] buf) 
				{
					return constructor.apply(pl, w.getBlockEntity((BlockPos)buf[0]));
				}
			};
		}
		
		public static TriFunction<AbstractContainerMenu, Player, Level, Object[]> fromBlockCont(BiFunction<Inventory, BlockEntity, AbstractContainerMenu> constructor)
		{
			return (pl,w,buf) -> constructor.apply(pl.getInventory(), w.getBlockEntity((BlockPos)buf[0]));
		}
		
		public static TriFunction<Screen, Player, Level, Object[]> fromEntityGui(BiFunction<Player, Entity, Screen> constructor)
		{
			return new TriFunction<Screen, Player, Level, Object[]>() 
			{
				@Override
				public Screen apply(Player pl, Level w, Object[] buf) 
				{
					return constructor.apply(pl, w.getEntity((int)buf[0]));
				}
			};
		}
		
		public static TriFunction<AbstractContainerMenu, Player, Level, Object[]> fromEntityCont(BiFunction<Inventory, Entity, AbstractContainerMenu> constructor)
		{
			return (pl,w,buf) -> constructor.apply(pl.getInventory(), w.getEntity((int)buf[0]));
		}
		
		public static<T> TriFunction<T, Player, Level, Object[]> fromNone(Function<Player, T> func)
		{
			return (pl,w,data) -> func.apply(pl);
		}
	}
}
