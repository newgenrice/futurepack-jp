package futurepack.common.sync;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import futurepack.common.entity.throwable.EntityHook;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fmllegacy.network.NetworkEvent;

/** Sync the hook with server and client */
public class MessageHookLines
{
	int hookID;
	int throwerID;
	List<Vec3> lineParts;

	public MessageHookLines()
	{
	}
	
		
	public MessageHookLines(int hookID, int throwerID, List<Vec3> lineParts)
	{
		super();
		this.hookID = hookID;
		this.throwerID = throwerID;
		this.lineParts = lineParts;
	}
	
	public void progress()
	{
		DistExecutor.runWhenOn(Dist.CLIENT, () -> new Runnable()
		{
			@Override
			public void run() 
			{
				Level w = Minecraft.getInstance().level;
				EntityHook hook = (EntityHook) w.getEntity(hookID);
				if(hook!=null)
				{
					hook.setThrower(throwerID);
					hook.setList(lineParts);
				}
			}
		});
	}
	
	public static MessageHookLines decode(FriendlyByteBuf buf) 
	{
		MessageHookLines msg = new MessageHookLines();
		msg.hookID = buf.readVarInt();
		msg.throwerID = buf.readVarInt();
		int size = buf.readVarInt();
		msg.lineParts = new ArrayList<>(size);
		for(int i=0;i<size;i++)
		{
			Vec3 vec = new Vec3(buf.readFloat(), buf.readFloat(), buf.readFloat());
			msg.lineParts.add(vec);
		}
		return msg;
	}
		
	public static void encode(MessageHookLines msg, FriendlyByteBuf buf) 
	{
		buf.writeVarInt(msg.hookID);
		buf.writeVarInt(msg.throwerID);
		buf.writeVarInt(msg.lineParts.size());
		for(Vec3 vec: msg.lineParts)
		{
			buf.writeFloat((float) vec.x);
			buf.writeFloat((float) vec.y);
			buf.writeFloat((float) vec.z);
		}
	}
			
	public static void consume(MessageHookLines message, Supplier<NetworkEvent.Context> ctx) 
	{
		message.progress();
		ctx.get().setPacketHandled(true);
	}
	
}
