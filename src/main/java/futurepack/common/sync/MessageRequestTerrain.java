package futurepack.common.sync;

import java.util.function.Supplier;

import futurepack.common.gui.inventory.GuiBoardComputer.ContainerBoardComputer;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.Heightmap.Types;
import net.minecraftforge.fmllegacy.network.NetworkEvent;
import net.minecraftforge.fmllegacy.network.PacketDistributor;

/** Send To the Server to get the Terrain of an Area. Used in the Boardcomputer Gui */
public class MessageRequestTerrain
{
	int x,y,z;
	byte w, h;
	
	public MessageRequestTerrain(BlockPos pos, byte w, byte h)
	{
		x = pos.getX();
		y = pos.getY();
		z = pos.getZ();
		this.w = w;
		this.h = h;
	}
	
	public static MessageRequestTerrain decode(FriendlyByteBuf buf) 
	{
		return new MessageRequestTerrain(new BlockPos(buf.readVarInt(), buf.readVarInt(), buf.readVarInt()), buf.readByte(), buf.readByte());
	}

	public static void encode(MessageRequestTerrain msg, FriendlyByteBuf buf) 
	{
		buf.writeVarInt(msg.x);
		buf.writeVarInt(msg.y);
		buf.writeVarInt(msg.z);
		buf.writeByte(msg.w);
		buf.writeByte(msg.h);
	}
		
		
	public static void consume(MessageRequestTerrain ter, Supplier<NetworkEvent.Context> ctx) 
	{
		Supplier<ServerPlayer> sup = ctx.get()::getSender;
		
		//Server
		
		AbstractContainerMenu con = sup.get().containerMenu;
		if(con!=null)
		{
			if(con instanceof ContainerBoardComputer)
			{
				((ContainerBoardComputer)con).setCoords(ter.x,ter.y,ter.z);
			}
		}

		final ServerLevel serv = sup.get().getLevel();
		Runnable task = new Runnable()
		{	
			@Override
			public void run()
			{
				int x = ter.x;
				int z = ter.z;
				
				BlockState[] states = new BlockState[ter.w*ter.h];
				int[] hights = new int[ter.w*ter.h];
				
				for(int w=0;w<ter.w;w++)
				{
					for(int h=0;h<ter.h;h++)
					{
						BlockPos pos = serv.getHeightmapPos(Types.MOTION_BLOCKING, new BlockPos(x+w,0,z+h));
						while(pos.getY()>0 && serv.isEmptyBlock(pos))
							pos = pos.below();
								
						hights[h*ter.w +w] = pos.getY();
						states[h*ter.w +w] = serv.getBlockState(pos);
					}
				}	
				MessageSendTerrain msg = new MessageSendTerrain(ter.w, ter.h, states, hights);
				FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with(sup),msg);
			}
		};
		ctx.get().enqueueWork(task);
		
		ctx.get().setPacketHandled(true);
	}
}
