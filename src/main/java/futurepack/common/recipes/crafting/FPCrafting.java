package futurepack.common.recipes.crafting;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.function.Consumer;

import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import futurepack.api.Constants;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;

public class FPCrafting
{
	
	private static void shapedRecipe(ItemStack stack, Object... obj)//futurepack:research_shaped
	{
//		addRecipe(new ShapedOreRecipeWithResearch(stack, obj));
		doShaped(fromItem(stack), "futurepack:research_shaped", stack, obj);
	}
	
	private static void shapelessRecipe(ItemStack stack, Object... obj)//futurepack:research_shapeless
	{
//		addRecipe(new ShapelessOreRecipeWithResearch(stack, obj));
		doShapeless(fromItem(stack), "futurepack:research_shapeless", stack, obj);
	}
	
	private static void addRecipe(ItemStack stack, Object... obj)//minecraft:crafting_shaped
	{
		doShaped(fromItem(stack), "minecraft:crafting_shaped", stack, obj);
	}
	
	private static void addRecipeMaschin(ItemStack stack, Object... obj)//futurepack:maschin_shaped
	{
		doShaped(fromItem(stack), "futurepack:maschin_shaped", stack, obj);
	}
	
	private static void addRecipeOre(ResourceLocation res, ItemStack stack, Object... obj)//forge:ore_shaped
	{
		doShaped(fromItem(stack), "forge:ore_shaped", stack, obj);
	}
	
	private static void addShapelessRecipe(ItemStack stack, Object...input)//minecraft:crafting_shapeless
	{
		doShapeless(fromItem(stack), "minecraft:crafting_shapeless", stack, input);
	}
	
	private static ResourceLocation fromItem(ItemStack it)
	{
		ResourceLocation res = it.getItem().getRegistryName();
		return new ResourceLocation(res + "_"+it.getDamageValue());
	}
	
	private static void doShaped(ResourceLocation res, String type, ItemStack out, Object...input)
	{
		JsonObject jo = new JsonObject();
		jo.addProperty("type", type);
		jo.addProperty("group", Constants.MOD_ID);
		
		JsonArray pattern = new JsonArray();
		JsonObject keys = new JsonObject();
		
		for(int i=0;i<input.length;i++)
		{
			if(input[i] instanceof String)
			{
				pattern.add((String)input[i]);
			}
			else if(input[i] instanceof Character)
			{
				keys.add(input[i].toString(), asJson(input[i+1]));
				i++;
			}
		}
		
		jo.add("pattern", pattern);
		jo.add("key", keys);
		jo.add("result", toJson(out));
		
		save(jo, res);
	}
	
	private static void doShapeless(ResourceLocation res, String type, ItemStack out, Object...input)
	{
		JsonObject jo = new JsonObject();
		jo.addProperty("type", type);
		jo.addProperty("group", Constants.MOD_ID);
		
		JsonArray ingredients = new JsonArray();
		
		for(Object o : input)
		{
			ingredients.add(asJson(o));
		}
			
		jo.add("ingredients", ingredients);
		jo.add("result", toJson(out));
		
		save(jo, res);
	}
	
	private static void save(JsonObject jo, ResourceLocation res)
	{
		File f = new File("./"+res.getNamespace() +"/" + res.getPath() +".json");
		f.getParentFile().mkdirs();
		while(f.exists())
		{
			String name = f.getName().substring(0, f.getName().length()-5);
			name+="1";

			f = new File(f.getParentFile(),name+".json");
		}
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		
		try
		{
			Writer w =  Files.newWriter(f, StandardCharsets.UTF_8);
			gson.toJson(jo, gson.newJsonWriter(w));
			w.close();
		}
		catch (JsonIOException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static JsonObject asJson(Object o)
	{
		if(o instanceof String)
		{
			JsonObject jo = new JsonObject();
			jo.addProperty("type", "forge:ore_dict");
			jo.addProperty("ore", (String)o);
			return jo;
		}
		else if(o instanceof Item)
		{
			return toJson(new ItemStack((Item)o));
		}
		else if(o instanceof Block)
		{
			return toJson(new ItemStack((Block)o));
		}
		else if(o instanceof ItemStack)
		{
			return toJson((ItemStack)o);
		}
		else
		{
			throw new RuntimeException("unsupported Object " + o);
		}
	}
	
	private static JsonObject toJson(ItemStack it)
	{
		JsonObject jo = new JsonObject();
//		if(it.getDamage()!=0 || it.getHasSubtypes())
//		{
//			jo.addProperty("data", it.getDamage());
//		}
		if(it.getCount()>1)
		{
			jo.addProperty("count", it.getCount());
		}
		if(it.hasTag())
		{
			jo.addProperty("type", "item_nbt");
			Gson gson = new Gson();
			JsonObject jnbt = gson.fromJson(it.getTag().toString(), JsonObject.class);
			jo.add("nbt", jnbt);
		}
		
		jo.addProperty("item", it.getItem().getRegistryName().toString());
		return jo;
	}
	
	
	
	public static void replaceConstants(File dir) throws JsonSyntaxException, JsonIOException, IOException
	{
		if(!dir.isDirectory())
			throw new IllegalArgumentException("Path must a Directory");
		File _const = new File(dir, "_constants.json");
		if(!_const.exists())
			throw new IllegalArgumentException("no _constants.json found");
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		Gson gsonO = new GsonBuilder().setPrettyPrinting().create();
		JsonObject[] constants = gson.fromJson(new FileReader(_const), JsonObject[].class);
		
		File[] jsons = dir.listFiles((f, s) -> s.endsWith(".json") && !s.startsWith("_"));
		for(File f : jsons)
		{
			Reader r = new FileReader(f);
			JsonObject recipe = gson.fromJson(r, JsonObject.class);
			r.close();
			final boolean[] change = new boolean[]{false};
			checkAllIngredients(recipe, (jo) -> {
				for(JsonObject c : constants)
				{
					String name = c.get("name").getAsString();
					if(name.startsWith("#") || name.contains(":"))
						continue;
					JsonObject ing = c.getAsJsonObject("ingredient");
					if(jo.equals(ing))
					{
						System.out.printf("%s: %s -> {\"item\": #%s}\n", f.getName(), jo.toString(), name);
						jo.entrySet().clear();
						jo.addProperty("item", "#" + name);
						change[0] = true;
						break;
					}
				}
			});
			if(change[0])
			{
				Writer wr = new FileWriter(f);
				gsonO.toJson(recipe, wr);
				wr.close();
			}
			
		}
		
	}
	
	private static void checkAllIngredients(JsonElement elm, Consumer<JsonObject> valid)
	{
		if(elm.isJsonObject())
		{
			JsonObject obj = elm.getAsJsonObject();
			if(obj.has("item"))
				valid.accept(obj);
			else if(obj.has("type") && !(obj.has("result") || obj.has("group")))
			{
				valid.accept(obj);
			}
			else
			{
				for(Entry<String, JsonElement> e : obj.entrySet())
				{
					if(e.getKey().equals("result"))
						continue;
					checkAllIngredients(e.getValue(), valid);
				}
			}
		}
		else if(elm.isJsonArray())
		{
			elm.getAsJsonArray().forEach(j -> checkAllIngredients(j, valid));
		}
		else if(elm.isJsonNull())
			return;
		else if(elm.isJsonPrimitive())
		{
			
		}	
	}
	
	public static void listAllDuplicates(File dir) throws JsonSyntaxException, JsonIOException, FileNotFoundException
	{
		if(!dir.isDirectory())
			throw new IllegalArgumentException("Path must a Directory");
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		File[] jsons = dir.listFiles((f, s) -> { 
			return s.endsWith(".json") && !s.startsWith("_");
			});
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		for(File f : jsons)
		{
			JsonObject recipe = gson.fromJson(new FileReader(f), JsonObject.class);
			checkAllIngredients(recipe, (jo) -> {
				Integer i = map.getOrDefault(jo.toString(), 0);
				i++;
				map.put(jo.toString(), i);
			});
		}
		
		JsonArray arr =  new JsonArray();
		
		map.forEach((s, i) -> {
			if(i > 1)
			{
				JsonObject ing = gson.fromJson(s, JsonObject.class);
				JsonObject obj = new JsonObject();
				String name = "" + System.currentTimeMillis();
				if(ing.has("ore"))
				{
					name = ing.get("ore").getAsString();
				}
				else if(ing.has("item"))
				{
					if(ing.get("item").getAsString().startsWith("#"))
						return;
				}
				obj.addProperty("name", name);
				obj.add("ingredient", ing);
				arr.add(obj);
			}
		});
		System.out.println(gson.toJson(arr));
	}
	
	public static void main(String[] args)
	{
		File dir = new File("./src/main/resources/assets/" + Constants.MOD_ID +"/recipes");
		try {
			replaceConstants(dir);
			listAllDuplicates(dir);
		} 
		catch (JsonSyntaxException | JsonIOException | IOException e) {
			e.printStackTrace();
		}
		
	}
}
