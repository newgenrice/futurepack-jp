package futurepack.common.recipes;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;

public interface ISyncedRecipeManager<T>
{
	public Collection<T> getRecipes();
	
	public void addRecipe(T t);
	
	public ResourceLocation getName();
	
//	public byte[] getRecipeHash();
	
	@SuppressWarnings("unchecked")
	default public T readRecipeFromBuffer(FriendlyByteBuf buf)
	{
		return (T) EnumRecipeSync.readUnknown(buf);
	}
	
	default public void writeRecipeToBuffer(T t, FriendlyByteBuf buf)
	{
		EnumRecipeSync.writeUnknown(t, buf);
	}
	
	default byte[] hashRecipes()
	{
		try 
		{
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			
			ByteBuf bbuf = Unpooled.buffer();
			FriendlyByteBuf buf = new FriendlyByteBuf(bbuf);
			
			byte[] bytes = new byte[1024];
			for(T entrys : getRecipes())
			{
				if(entrys==null)
					throw new NullPointerException(this.getName() + " has null recipe entries!");
				writeRecipeToBuffer(entrys, buf);
				while(bbuf.isReadable())
				{
					int dataLen = Math.min(bytes.length, bbuf.readableBytes());
					bbuf.readBytes(bytes, 0, dataLen);
					digest.update(bytes, 0, dataLen);
				}
				bbuf.discardReadBytes();
			}
			
			return digest.digest();
			
		}
		catch (NoSuchAlgorithmException e) 
		{
			e.printStackTrace();
			throw new IllegalStateException(e);
		}
	}
}
