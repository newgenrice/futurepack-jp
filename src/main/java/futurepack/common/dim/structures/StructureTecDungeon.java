package futurepack.common.dim.structures;

import java.util.Random;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.LevelWriter;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.storage.loot.LootTables;

public class StructureTecDungeon extends StructureBase 
{

	public StructureTecDungeon(StructureBase base) 
	{
		super(base);
	}

	@Override
	public void generateBase(LevelWriter w, BlockPos start) 
	{
		super.generateBase(w, start);
		
		if(w instanceof BlockGetter)
		{
			BlockGetter read = (BlockGetter) w;
			
			for(OpenDoor door : getRawDoors())
			{
				if(door.getDepth()==1 && door.getWeidth()==1 && door.getHeight()==1)
				{
					if(door.getDirection() == Direction.UP)
					{
						int x = door.getPos().getX();
						int y = door.getPos().getY();
						int z = door.getPos().getZ();
						BlockState state = getBlocks()[x][y][z];
						if(state!=null)
						{
							BlockState s1,s2,s3,s4;
							s1 = getBlocks()[x+1][y][z];
							s2 = getBlocks()[x-1][y][z];
							s3 = getBlocks()[x][y][z+1];
							s4 = getBlocks()[x][y][z-1];
							
							BlockPos.MutableBlockPos mut = new BlockPos.MutableBlockPos().set(start);
							mut.move(x, y, z);
							while(mut.getY() < 250)
							{
								mut.move(Direction.UP);
								if(read.getBlockState(mut).isAir())
								{
									break;
								}
								else
								{
									w.setBlock(mut, state, 2);
									w.setBlock(mut.move(1,0,0), s1, 2);
									w.setBlock(mut.move(-2,0,0), s2, 2);
									w.setBlock(mut.move(1,0,1), s3, 2);
									w.setBlock(mut.move(0,0,-2), s4, 2);
									mut.move(0, 0, 1);
								}
							}
						}
					}
				}
			}
		}
	}
	
	@Override
	public void addChestContentBase(ServerLevelAccessor w, BlockPos start, Random rand, CompoundTag extraData, LootTables manager)
	{		
		super.addChestContentBase(w, start, rand, extraData, manager);
	}
}
