package futurepack.common.dim.structures;

import java.util.Random;

import futurepack.api.Constants;
import futurepack.common.FPLog;
import futurepack.common.block.logistic.IInvWrapper;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.Container;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.LootTables;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;
import net.minecraft.world.phys.Vec3;

public class DungeonChest
{
	private BlockPos pos;
	private ResourceLocation loottable;
	private StructureBase base;
	
	public DungeonChest(BlockPos pos, ResourceLocation res, StructureBase base)
	{
		super();
		this.pos = pos;
		loottable = res;
		this.base = base;
	}
	
	public DungeonChest(BlockPos pos, StructureBase base)
	{
		this(pos, new ResourceLocation(Constants.MOD_ID), base);
	}	
	
	public void genLoot(ServerLevel w, Random rand, BlockPos start, CompoundTag nbt)
	{
		genLoot(w, rand, start, nbt, w.getServer().getLootTables());
	}	
	
	public void genLoot(ServerLevelAccessor w, Random rand, BlockPos start, CompoundTag nbt, LootTables manager)
	{
		BlockPos lootpos = start.offset(pos);
		BlockEntity tile =  w.getBlockEntity(lootpos);	    	
		if(tile==null)
		{
			FPLog.logger.error("The TileEntity is null! This mistake is in " + base);
			return;
		}
		Container inv = null;
		if(tile instanceof Container)
		{
			inv = (Container) tile;
		}
		else
		{
			inv = new IInvWrapper(tile);
		}
			
		LootTable loottable;
		int lvl = nbt.getInt("tecLevel");
		ResourceLocation res = new ResourceLocation(this.loottable+"_"+lvl);
		loottable = manager.get(res);
		
		if(loottable==LootTable.EMPTY)
		{
			loottable = manager.get(this.loottable);
			
			if(loottable==LootTable.EMPTY)
			{
				FPLog.logger.error("Missing Loot Table detected: " + res.toString() + " and " + this.loottable.toString());
			}
		}
		
		LootContext.Builder builder = new LootContext.Builder((ServerLevel)w.getLevel());
		builder.withParameter(LootContextParams.ORIGIN, Vec3.atCenterOf(lootpos));
		
		loottable.fill(inv, builder.create(LootContextParamSets.CHEST));
		
		lvl = nbt.getInt("level");
		if(lvl>=3)
		{
			if(rand.nextInt(100) < 5)
			{
				res = new ResourceLocation(Constants.MOD_ID, "loot_tables/chests/blueprints/laserbow");
				loottable = manager.get(this.loottable);
				loottable.fill(inv, builder.create(LootContextParamSets.CHEST));
			}
		}
	}
}
