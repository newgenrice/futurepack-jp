package futurepack.common.item;

import java.util.List;

import futurepack.api.interfaces.IDeepCoreLogic;
import futurepack.api.interfaces.IItemDeepCoreLens;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public abstract class ItemLenseBase extends Item implements IItemDeepCoreLens
{
	private final int energy_value;

	public ItemLenseBase(Properties properties, int energy_needed) 
	{
		super(properties);
		this.energy_value = energy_needed;
	}

	@Override
	public void appendHoverText(ItemStack it, Level w, List<Component> l, TooltipFlag par4) 
	{
		CompoundTag nbt = it.getTagElement("lense");
		if(nbt!=null)
		{
			int damage = nbt.getInt("damage");
			int maxDmage = getMaxDurability(it, null);
			l.add(new TranslatableComponent("tooltip.items.durability", damage, maxDmage));
		}
		super.appendHoverText(it, w, l, par4);
	}

	@Override
	public int getNeededEnergie(ItemStack item, IDeepCoreLogic logic)
	{
		return energy_value;
	}

	@Override
	public abstract boolean isWorking(ItemStack item, IDeepCoreLogic logic);

	@Override
	public boolean isSupportConsumer(ItemStack item, IDeepCoreLogic logic)
	{
		return false;
	}

	/**
	 * Updates the progress of the lense.
	 * @param item
	 * @param logic
	 * @return true if progress is completed, so the lense will be damaged
	 */
	@Override
	public abstract boolean updateProgress(ItemStack item, IDeepCoreLogic logic);

	@Override
	public abstract int getColor(ItemStack item, IDeepCoreLogic logic);
//	{
//		switch(item.getItemDamage())
//		{
//		case LinseL:
//			return 0x9600dd;
//		default:
//			return 0;
//		}
//	}


	@Override
	public abstract int getMaxDurability(ItemStack item, IDeepCoreLogic logic);
//	{
//		switch(item.getItemDamage())
//		{
//		case LinseL:
//			return 10;
//		default:
//			System.out.println("ItemSpaceship.getDestroyChance() --- not implemented");
//			return 0;
//		}
//	}
	
	@Override
	public ParticleOptions randomParticle(ItemStack lenseStack, IDeepCoreLogic logic)
	{
		return null;
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack)
	{
		return getDurabilityForDisplay(stack) > 0d;
	}
	
	@Override
	public double getDurabilityForDisplay(ItemStack it)
	{
		CompoundTag nbt = it.getTagElement("lense");
		if(nbt!=null)
		{
			int damage = nbt.getInt("damage");
			int maxDmage = getMaxDurability(it, null);
			return ((double) damage / (double) maxDmage);
		}
		else
		{
			return 0D;
		}
	}
	
}
