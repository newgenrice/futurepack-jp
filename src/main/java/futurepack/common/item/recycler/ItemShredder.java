package futurepack.common.item.recycler;

import java.util.ArrayList;
import java.util.Random;

import futurepack.api.EnumLogisticIO;
import futurepack.api.interfaces.IRecyclerTool;
import futurepack.common.block.modification.machines.TileEntityRecycler;
import futurepack.common.recipes.recycler.FPRecyclerShredderManager;
import futurepack.common.recipes.recycler.IRecyclerRecipe;
import futurepack.common.recipes.recycler.RecyclerShredderRecipe;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;

public class ItemShredder extends Item implements IRecyclerTool 
{

	public ItemShredder(Item.Properties props)
	{
		super(props);
//		setCreativeTab(FPMain.tab_items);
//		setMaxDamage(1024);
//		setMaxStackSize(1);
	}
	
	@Override
	public boolean craftComplete(TileEntityRecycler tile, IRecyclerRecipe recipe, ItemStack tool, ItemStack in) 
	{
		int damage = tool.getDamageValue();
		
		if(damage + 1 >= tool.getMaxDamage())
		{
			tool.shrink(1);
		}
		else
		{
			tool.hurt(1, tile.getLevel().random, null);
		}
		return true;
	}


	@Override
	public ArrayList<ItemStack> getOutput(TileEntityRecycler tile, IRecyclerRecipe recipe, ItemStack tool, ItemStack in, Random random) 
	{
		return ((RecyclerShredderRecipe)recipe).getWeightedOutput(random);
	}


	@Override
	public IRecyclerRecipe updateRecipe(TileEntityRecycler tile, ItemStack tool, ItemStack in) 
	{		
		//get from toasted
		ItemStack conv = TileEntityRecycler.getUntoastedItemStack(in);

		RecyclerShredderRecipe ar = FPRecyclerShredderManager.instance.getMatchingRecipe(conv);
						
		return ar;
	}

	
	@Override
	public void tick(TileEntityRecycler tile, IRecyclerRecipe recipe, ItemStack tool, ItemStack in) 
	{
	}


	@Override
	public int getMaxProgress(TileEntityRecycler tile, IRecyclerRecipe recipe, ItemStack tool, ItemStack in) 
	{
		return Math.max(2, ((RecyclerShredderRecipe)recipe).getMaxprogress());
	}


	@Override
	public boolean canWork(TileEntityRecycler tileEntityRecycler, IRecyclerRecipe recipe, ItemStack itemStack, ItemStack itemStack2, int ticks) 
	{
		return true;
	}
	
	
	@Override
	public EnumLogisticIO getSupportMode() 
	{
		return EnumLogisticIO.NONE;
	}
	
}
