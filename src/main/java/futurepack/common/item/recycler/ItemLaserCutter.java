package futurepack.common.item.recycler;

import java.util.ArrayList;
import java.util.Random;

import futurepack.api.EnumLogisticIO;
import futurepack.api.interfaces.IRecyclerTool;
import futurepack.common.block.modification.machines.TileEntityRecycler;
import futurepack.common.recipes.recycler.FPRecyclerLaserCutterManager;
import futurepack.common.recipes.recycler.IRecyclerRecipe;
import futurepack.common.recipes.recycler.RecyclerLaserCutterRecipe;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;

public class ItemLaserCutter extends Item implements IRecyclerTool 
{

	public ItemLaserCutter(Item.Properties props)
	{
		super(props);
//		setCreativeTab(FPMain.tab_items);
//		setMaxDamage(2048);
//		setMaxStackSize(1);
	}

	@Override
	public EnumLogisticIO getSupportMode() 
	{
		return EnumLogisticIO.NONE;
	}
	
	@Override
	public boolean craftComplete(TileEntityRecycler tile, IRecyclerRecipe recipe, ItemStack tool, ItemStack in) 
	{
		int damage = tool.getDamageValue();
		
		if(damage + 1 >= tool.getMaxDamage())
		{
			tool.shrink(1);
		}
		else
		{
			tool.hurt(1, tile.getLevel().random, null);
		}
		return true;
	}

	@Override
	public ArrayList<ItemStack> getOutput(TileEntityRecycler tile, IRecyclerRecipe recipe, ItemStack tool,ItemStack in, Random random) 
	{
		return ((RecyclerLaserCutterRecipe)recipe).getWeightedOutput(in, random);
	}

	
	@Override
	public IRecyclerRecipe updateRecipe(TileEntityRecycler tile, ItemStack tool, ItemStack in) 
	{
		//get from toasted
		ItemStack conv = TileEntityRecycler.getUntoastedItemStack(in);

		RecyclerLaserCutterRecipe ar = FPRecyclerLaserCutterManager.instance.getMatchingRecipe(conv);
						
		return ar;
	}

	@Override
	public void tick(TileEntityRecycler tile, IRecyclerRecipe recipe, ItemStack tool, ItemStack in) 
	{
		tile.energy.use((int) (4.0 * tile.getDefaultPowerUsage()));
	}

	@Override
	public int getMaxProgress(TileEntityRecycler tile, IRecyclerRecipe recipe, ItemStack tool, ItemStack in) 
	{
		return Math.max(2, ((RecyclerLaserCutterRecipe)recipe).getMaxprogress());
	}

	@Override
	public boolean canWork(TileEntityRecycler tile, IRecyclerRecipe recipe, ItemStack tool, ItemStack in, int ticks) 
	{
		return tile.energy.get() > ticks * 4.0 * tile.getDefaultPowerUsage();
	}
	
}
