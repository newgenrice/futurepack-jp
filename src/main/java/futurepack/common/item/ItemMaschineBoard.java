package futurepack.common.item;

import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;

public class ItemMaschineBoard extends Item 
{

	public ItemMaschineBoard(Properties properties) 
	{
		super(properties);
	}

	@Override
	public boolean isFoil(ItemStack stack) 
	{
		return stack.hasTag() && stack.getTag().contains("tile");
	}
}
