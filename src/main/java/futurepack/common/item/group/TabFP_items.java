package futurepack.common.item.group;

import futurepack.common.item.CraftingItems;
import net.minecraft.world.item.ItemStack;

public class TabFP_items extends TabFB_Base 
{

	public TabFP_items(String par2Str) 
	{
		super(par2Str);
	}

	@Override
	public ItemStack makeIcon()
	{
		return new ItemStack(CraftingItems.sword_handle);
	}
	
	

}
