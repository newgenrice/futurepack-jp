package futurepack.common.item.tools;

import java.util.List;

import futurepack.api.interfaces.IItemNeon;
import futurepack.common.FPSounds;
import futurepack.common.entity.throwable.EntityLaserProjectile;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class ItemLaserRiffle extends Item implements IItemNeon
{
	public ItemLaserRiffle(Item.Properties props)
	{
		super(props);
//		setMaxStackSize(1);
//		setCreativeTab(FPMain.tab_items);
	}
	
	@Override
	public InteractionResultHolder<ItemStack> use(Level w, Player pl, InteractionHand hand)
	{
		ItemStack it = pl.getItemInHand(hand);
		if(getNeon(it) >= 18)
		{
			pl.getCooldowns().addCooldown(this, 18);
			addNeon(it, -18);	
			
			if(!w.isClientSide)
			{
				EntityLaserProjectile laser = new EntityLaserProjectile(w, pl);
				laser.shootFromRotation(pl, pl.getXRot(), pl.getYRot(), 0F, 3F, 0.5F);
				w.addFreshEntity(laser);
			}
			
			w.playSound(pl, pl.blockPosition(), FPSounds.LOAD, SoundSource.NEUTRAL, 0.5F, 3F);
			return InteractionResultHolder.success(it);
		}
		return InteractionResultHolder.pass(it);
	}
	
	@Override
	public int getRGBDurabilityForDisplay(ItemStack stack)
	{
		return Mth.hsvToRgb(0.52F, 1.0F, (0.5F + (float)getNeon(stack) / (float)getMaxNeon(stack) * 0.5F));
	}
	
	@Override
	public double getDurabilityForDisplay(ItemStack stack)
	{
		return 1- ((double)getNeon(stack) / (double)getMaxNeon(stack));
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack)
	{
		return getNeon(stack) < getMaxNeon(stack);
	}


	@Override
	public int getMaxNeon(ItemStack it)
	{
		return 900;
	}
	
	@Override
	public void appendHoverText(ItemStack stack, Level w, List<Component> tooltip, TooltipFlag advanced)
	{
		tooltip.add(HelperItems.getTooltip(stack, this));
		if(getNeon(stack) < 18)
			tooltip.add(new TranslatableComponent("tooltip.items.noenergy").setStyle(Style.EMPTY.withColor(ChatFormatting.DARK_RED)));
		
		super.appendHoverText(stack, w, tooltip, advanced);
	}
}
