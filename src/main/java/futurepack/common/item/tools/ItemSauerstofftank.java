package futurepack.common.item.tools;

import futurepack.api.interfaces.IAirSupply;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterials;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ItemSauerstofftank extends ArmorItem
{

	public ItemSauerstofftank(Item.Properties props) 
	{
		super(ArmorMaterials.IRON, EquipmentSlot.CHEST, props);
	}
	
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type)
	{
		return "futurepack:textures/models/armor/oxygentanks.png";
	}
	
	public int getMaxOxygen()
	{
		return 2400;	
	}
	
	public int getMaxMetaDamage()
	{
		return 240;	
	}
	
	@Override
	public void setDamage(ItemStack stack, int damage) 
	{
		super.setDamage(stack, 0);
	}
	
	@Override
	public int getDamage(ItemStack stack) 
	{
		return 0;
	}
	
	@Override
	public boolean canBeDepleted() 
	{
		return false;
	}
	
	@Override
	public void onArmorTick(ItemStack stack, Level world, Player player)
	{
		if(player.getAbilities().instabuild)
		{
			super.onArmorTick(stack, world, player);
			return;
		}
		
		if(!stack.hasTag())
		{
			stack.setTag(new CompoundTag());
			stack.getTag().putInt("oxygen", getMaxOxygen());
		}
			
		int ox = stack.getTag().getInt("oxygen");
		
		
		IAirSupply supply = AtmosphereManager.getAirSupplyFromEntity(player);
		
		if(supply.getAir() >= 300)
		{	
			//in air
			if(ox < getMaxOxygen())
			{
				supply.reduceAir(1);
				if(!world.isClientSide)
				{
					stack.getTag().putInt("oxygen", ox + 1);
//					itemStack.setItemDamage((int)(getMaxMetaDamage() - ((float)ox/getMaxOxygen()) * getMaxMetaDamage()));
				}
			}
		}
		else
		{
			if(ox > 1)
			{
				
				if(!world.isClientSide)
				{
					stack.getTag().putInt("oxygen", ox - 1);
//					itemStack.setItemDamage((int)(getMaxMetaDamage() - ((float)ox/getMaxOxygen()) * getMaxMetaDamage()));
				}	
				supply.addAir(1);
//				AtmosphereManager.setAirTanks((float)ox/getMaxOxygen());
				
			}
			if(ox == 1)
			{
				if(!world.isClientSide)
				{
					stack.getTag().putInt("oxygen", 0);
//					itemStack.setItemDamage(getMaxMetaDamage());
				}
//				AtmosphereManager.setAirTanks(0F);
			}
		}
		super.onArmorTick(stack, world, player);
	}
	
	@Override
	public int getRGBDurabilityForDisplay(ItemStack stack)
	{
		return Mth.hsvToRgb(0.62F, 1.0F, (0.5F + getOxygen(stack) / getMaxOxygen() * 0.5F));
	}
	
	private float getOxygen(ItemStack stack)
	{
		if(stack.hasTag())
		{
			return stack.getTag().getInt("oxygen");
		}
		return 0;
	}

	@Override
	public double getDurabilityForDisplay(ItemStack stack)
	{
		return 1- ((double)getOxygen(stack) / (double)getMaxOxygen());
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack)
	{
		return getOxygen(stack) < getMaxOxygen();
	}
	
}
