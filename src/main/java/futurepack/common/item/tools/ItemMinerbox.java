package futurepack.common.item.tools;

import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.common.entity.EntityMiner;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;

public class ItemMinerbox extends Item 
{

	public ItemMinerbox(Properties properties) 
	{
		super(properties);
	}
	
	@Override
	public InteractionResult useOn(UseOnContext context)
	{
		Level w = context.getLevel();
		if(!w.isClientSide)
		{
			BlockPos pos = context.getClickedPos();
			Direction face = context.getClickedFace();
			
			if(w.getBlockState(pos).getBlock() == InventoryBlocks.drone_station)
			{
				EntityMiner m = new EntityMiner(w,pos.relative(face));
				m.setSide(face);
				w.addFreshEntity(m);
				context.getItemInHand().shrink(1);
				return InteractionResult.SUCCESS;
			}
		}
		return super.useOn(context);
	}
}
