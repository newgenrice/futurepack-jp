package futurepack.common.item.tools.compositearmor;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;

public class ItemModulGravity extends ItemModulNeonContainer
{
	public ItemModulGravity(Item.Properties props) 
	{
		super(EquipmentSlot.FEET, 500, props);
	}

	@Override
	public void onArmorTick(Level world, Player player, ItemStack it, CompositeArmorInventory inv)
	{	
        pullEnergy(inv, it);

        CompoundTag nbt = it.getTag();
		if(nbt==null)
		{
			nbt=new CompoundTag();
			it.setTag(nbt);
		}

        if(getNeon(it)>10)
		{
            boolean lastGround = nbt.getBoolean("onground");
            boolean ground = player.isOnGround();
            if(lastGround==true && !ground)
            {
                if(player.getDeltaMovement().y > 0)
                {
                    addNeon(it, -10);
                    player.setDeltaMovement(player.getDeltaMovement().add(0, 1, 0));
                }
            }
            nbt.putBoolean("onground", ground);

            if(getNeon(it)>50)
            {
                if(!ground && player.getDeltaMovement().y < 0 && player.fallDistance >= 3)
                {
                	double range = Math.max(1, (-player.getDeltaMovement().y)+1.5 );
                	
                    BlockPos bll = player.blockPosition().offset(-1, 0, -1);
                    outer:
                    for(int y=1;y<=range;y++)
                    {
	                    for(int x=0;x<3;x++)
	                    {
	                    	for(int z=0;z<3;z++)
	                    	{
	                    		BlockPos bl = bll.offset(x, -y, z);
	                    		BlockState state = world.getBlockState(bl);
	                            if(!state.isAir())
	                            {
	                                player.setDeltaMovement(new Vec3(0D, 0D, 0D));
	                                player.fallDistance = 0F;
	                                addNeon(it, -50);
	                                break outer;
	                            }
	                    	}
	                    }
                    }
                }
            }
        }
	}
	
	@Override
	public boolean isEnergyProvider()
	{
		return false;
	}
	
}
