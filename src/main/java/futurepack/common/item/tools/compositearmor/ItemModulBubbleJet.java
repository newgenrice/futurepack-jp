package futurepack.common.item.tools.compositearmor;

import futurepack.api.interfaces.IAirSupply;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ItemModulBubbleJet extends ItemModulOxygenContainer
{

	public ItemModulBubbleJet(Properties props)
	{
		super(null, 100, props);
	}

	@Override
	public void onArmorTick(Level world, Player player, ItemStack it, CompositeArmorInventory inv)
	{
		IAirSupply supply = AtmosphereManager.getAirSupplyFromEntity(player);
		if(supply.getAir() >= 300)
		{	
			int filled = addOxygen(it, 6);
			
			if(filled > 0)
				supply.reduceAir(filled);
		}
		if(player.isInWater() && player.isSprinting() && this.getOxygen(it)> 0)
		{
			if(player.getDeltaMovement().distanceToSqr(0, 0, 0) < 4.0)
			{
				player.setDeltaMovement(player.getDeltaMovement().scale(1.1));
				addOxygen(it, -1);
			}
		}
		
	}

	@Override
	public boolean isSlotFitting(ItemStack stack, EquipmentSlot type, CompositeArmorPart armorPart)
	{
		return type !=EquipmentSlot.HEAD && type.getType() == EquipmentSlot.Type.ARMOR;
	}
}
