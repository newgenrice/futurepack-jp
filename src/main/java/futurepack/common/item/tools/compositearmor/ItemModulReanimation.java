package futurepack.common.item.tools.compositearmor;

import futurepack.common.FPSounds;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ItemModulReanimation extends ItemModulShield
{

	public ItemModulReanimation(Properties props)
	{
		super(props, 1000);
		initStatus=1000;
	}
	
	@Override
	public boolean canBlockDamage(Level world, Player player, ItemStack it, CompositeArmorPart inv, DamageSource src, float amount)
	{
		return player.getHealth() - amount <= 0F;
	}
	
	@Override
	public void onArmorTick(Level world, Player player, ItemStack it, CompositeArmorInventory armor)
	{
		super.onArmorTick(world, player, it, armor);
		
		CompoundTag nbt = it.getTag();
		if(nbt==null)
		{
			nbt=new CompoundTag();
			it.setTag(nbt);
		}
		if(!world.isClientSide)
		{
			if(nbt.getInt("status") == 38)
			{
				player.setHealth(player.getMaxHealth());
			}
		}
		
			
	}
	
	@Override
	public boolean onWearerDamaged(Level world, Player player, ItemStack it, CompositeArmorPart inv, DamageSource src, float amount)
	{
		CompoundTag nbt = it.getTag();
		if(nbt==null)
		{
			nbt=new CompoundTag();
			it.setTag(nbt);
		}
		boolean block = canBlockDamage(world, player, it, inv, src, amount);
		if(!block)
			return false;
		
		if(nbt.getInt("status")>=getMaxStatus(nbt))
		{
			if(!world.isClientSide)
			{
				nbt.putInt("status", 0);
				player.setHealth(1F);
				world.playSound(null, player.getX(), player.getY(), player.getZ(), FPSounds.DEFIBRILATOR, SoundSource.PLAYERS, 0.8F, 1.0F);
			}
				
			
			return true;
		}
		else if(nbt.getInt("status") < 40)
		{
			return true;
		}
		
		return false;
	}

}
