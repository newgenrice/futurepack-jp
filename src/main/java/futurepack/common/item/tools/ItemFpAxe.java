package futurepack.common.item.tools;

import net.minecraft.world.item.AxeItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Tier;

public class ItemFpAxe extends AxeItem
{
	public ItemFpAxe(Tier tier, float eff, float attackSpeedIn, Item.Properties builder) 
	{
		super(tier, eff, attackSpeedIn, builder);
	}	
}
