package futurepack.common.item.tools;

import futurepack.common.research.CustomPlayerData;
import futurepack.common.research.ScanningManager;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ClipContext.Fluid;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraft.world.level.levelgen.WorldgenRandom;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.HitResult.Type;

public class ItemEScanner extends Item
{
	//IIcon s;
	
	public ItemEScanner(Item.Properties props) 
	{
		super(props);
		
	}
	
//	@Override
//	public void registerIcons(IIconRegister r)
//	{
//		super.registerIcons(r);
//		s = r.registerIcon(getIconString()+"_s");
//	}
//	
	@Override
	public InteractionResultHolder<ItemStack> use(Level w, Player pl, InteractionHand hand)
	{
		if(!w.isClientSide)
		{
			CustomPlayerData.getDataFromPlayer(pl).addAchievement(null);
		}
		ItemStack it = pl.getItemInHand(hand);
		
		final HitResult pos = getPlayerPOVHitResult(w, pl, Fluid.ANY);
		if(pos!=null && pos.getType() == Type.BLOCK)
		{
			ScanningManager.doBlock(w, ((BlockHitResult)pos).getBlockPos(), pl, (BlockHitResult)pos);	
			return InteractionResultHolder.success(it);
		}
		else
		{
			ScanningManager.openStart(pl.level, pl);
			return InteractionResultHolder.success(it);
		}
	}
	
	@Override
	public void inventoryTick(ItemStack it, Level w, Entity e, int itemSlot, boolean isSelected) 
	{
		if(w.isClientSide)
			return;
		
		LevelChunk c = w.getChunkAt(e.blockPosition());
		if(!it.hasTag())
		{
			it.setTag(new CompoundTag());
		}
		
		boolean flag = WorldgenRandom.seedSlimeChunk(c.getPos().x, c.getPos().z, ((ServerLevel)w).getSeed(), 987234911L).nextInt(10) == 0;
		flag &= e.getY() < 40.0D;
		
        it.getTag().putBoolean("s", flag);
		super.inventoryTick(it, w, e, itemSlot, isSelected);
	}
	
	@Override
	public InteractionResult interactLivingEntity(ItemStack it, Player pl, LivingEntity liv, InteractionHand hand)
	{
//		pl.addStat(FPAchievements.theImpalpable, 1);
		ScanningManager.doEntity(pl.level, liv, pl);		
		return InteractionResult.SUCCESS;
	}
	
	
}
