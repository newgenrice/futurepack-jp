package futurepack.common.item.tools;

import futurepack.common.entity.throwable.EntityEgger;
import futurepack.common.item.ItemDispensable;
import net.minecraft.core.BlockSource;
import net.minecraft.core.Direction;
import net.minecraft.core.Position;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ItemEntityEgger extends ItemDispensable 
{

	public ItemEntityEgger(Properties properties) 
	{
		super(properties);
	}
	
	@Override
	public InteractionResultHolder<ItemStack> use(Level w, Player pl, InteractionHand handIn)
	{
		ItemStack it = pl.getItemInHand(handIn);
		if(!w.isClientSide)
		{
			EntityEgger egg = new EntityEgger(w, pl);
			egg.shootFromRotation(pl, pl.getXRot(), pl.getYRot(), 0F, 1.5F, 1F);
			w.addFreshEntity(egg);
		}
		it.shrink(1);
		return InteractionResultHolder.success(it);
	}
	
	@Override
	public ItemStack dispense(BlockSource src, ItemStack it, Position pos, Direction enumfacing)
	{
		it.shrink(1);
		EntityEgger egg = new EntityEgger(src.getLevel(), pos.x(), pos.y(), pos.z());
		double x,y,z;
		x = enumfacing.getStepX();
		y = enumfacing.getStepY();
		z = enumfacing.getStepZ();
		x +=( src.getLevel().random.nextFloat() - src.getLevel().random.nextFloat() )* 0.1;
		y +=( src.getLevel().random.nextFloat() - src.getLevel().random.nextFloat() )* 0.1;
		z +=( src.getLevel().random.nextFloat() - src.getLevel().random.nextFloat() )* 0.1;
		egg.setDeltaMovement(x,y,z);
		src.getLevel().addFreshEntity(egg);
		return it;
	}

}
