package futurepack.common.item.tools;

import java.util.List;
import java.util.UUID;

import futurepack.common.item.ItemDispensable;
import net.minecraft.core.BlockPos;
import net.minecraft.core.BlockSource;
import net.minecraft.core.Direction;
import net.minecraft.core.Position;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;

public class ItemPearlEgger extends ItemDispensable 
{
	
	public ItemPearlEgger(Properties properties) 
	{
		super(properties);
	}
	
	@Override
	public void appendHoverText(ItemStack it, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) 
	{
		super.appendHoverText(it, worldIn, tooltip, flagIn);
		tooltip.add(new TranslatableComponent("tooltip.items.pearlegger", it.getOrCreateTagElement("owner").getInt("charges")));
	}

	@Override
	public InteractionResult useOn(UseOnContext context)
	{
		Level w = context.getLevel();
		if(!w.isClientSide)
		{
			ItemStack it = context.getItemInHand();
			
			if(it.hasTag())
			{
				CompoundTag nbt = it.getTagElement("owner");
				if(nbt!=null)
				{
					int charges = nbt.getInt("charges");
					if(charges > 0)
					{
						UUID uuid = nbt.getUUID("creator");
						Player player = w.getPlayerByUUID(uuid);
						if(player!=null)
						{
							if(player.isSleeping())
							{
								player.stopSleepInBed(true, false); //wakePlayer
							}
							BlockPos pp = context.getClickedPos().relative(context.getClickedFace());
							player.teleportTo(pp.getX()+0.5, pp.getY()+0.2, pp.getZ()+0.5);
							player.fallDistance = 0.0F;
							charges--;
							nbt.putInt("charges", charges);
							return InteractionResult.SUCCESS;
						}
					}
					
				}
			}
		}
		return super.useOn(context);
	}
	
	@Override
	public ItemStack dispense(BlockSource src, ItemStack it, Position pos, Direction enumfacing)
	{
		if(it.hasTag())
		{
			CompoundTag nbt = it.getTagElement("owner");
			if(nbt!=null)
			{
				int charges = nbt.getInt("charges");
				if(charges > 0)
				{
					UUID uuid = nbt.getUUID("creator");
					Player player = src.getLevel().getPlayerByUUID(uuid);
					if(player!=null)
					{
						if(player.isSleeping())
						{
							player.stopSleepInBed(true, false); //wakePlayer
						}
						player.teleportTo(pos.x()+0.5, pos.y()+0.2, pos.z()+0.5);
						player.fallDistance = 0.0F;
						charges--;
						nbt.putInt("charges", charges);
					}
				}
			}
		}
		return it;
	}
}
