package futurepack.common.item;

import futurepack.api.Constants;
import futurepack.common.FuturepackMain;
import futurepack.common.entity.throwable.EntityWakurumIngot;
import net.minecraft.world.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class ResourceItems 
{
	String[] names = new String[]{"ingot_bitripentium"};
	
	public static final Item.Properties resources_64 = new Item.Properties().stacksTo(64).tab(FuturepackMain.tab_resources);
	
	public static final Item ingot_tin = new Item(resources_64).setRegistryName(Constants.MOD_ID, "ingot_tin");
	public static final Item ingot_zinc = new Item(resources_64).setRegistryName(Constants.MOD_ID, "ingot_zinc");
	public static final Item ingot_copper = new Item(resources_64).setRegistryName(Constants.MOD_ID, "ingot_copper");
	public static final Item ingot_magnet = new Item(resources_64).setRegistryName(Constants.MOD_ID, "ingot_magnet");
	public static final Item ingot_aluminium = new Item(resources_64).setRegistryName(Constants.MOD_ID, "ingot_aluminium");
	public static final Item ingot_silicon = new Item(resources_64).setRegistryName(Constants.MOD_ID, "ingot_silicon");
	
	public static final Item ingot_neon = new Item(resources_64).setRegistryName(Constants.MOD_ID, "ingot_neon");
	public static final Item ingot_retium = new Item(resources_64).setRegistryName(Constants.MOD_ID, "ingot_retium");
	public static final Item ingot_glowite = new Item(resources_64).setRegistryName(Constants.MOD_ID, "ingot_glowtite");
	public static final Item ingot_bioterium = new Item(resources_64).setRegistryName(Constants.MOD_ID, "ingot_bioterium");
	public static final Item ingot_wakurium = new ItemThrowable(resources_64, EntityWakurumIngot::new).setRegistryName(Constants.MOD_ID, "ingot_wakurium");
	public static final Item ingot_quantanium = new Item(resources_64).setRegistryName(Constants.MOD_ID, "ingot_quantanium");
	
	public static final Item ingot_rare_earth = new Item(resources_64).setRegistryName(Constants.MOD_ID, "ingot_rare_earth");
	public static final Item ingot_gadolinium = new Item(resources_64).setRegistryName(Constants.MOD_ID, "ingot_gadolinium");
	public static final Item ingot_lithium = new Item(resources_64).setRegistryName(Constants.MOD_ID, "ingot_lithium");
	public static final Item ingot_neodymium = new Item(resources_64).setRegistryName(Constants.MOD_ID, "ingot_neodymium");
	
	public static final Item ingot_bitripentium = new Item(resources_64).setRegistryName(Constants.MOD_ID, "ingot_bitripentium");
	
	public static final Item composite_metal = new Item(resources_64).setRegistryName(Constants.MOD_ID, "composite_metal");
	public static final Item ceramic = new Item(resources_64).setRegistryName(Constants.MOD_ID, "ceramic");
	public static final Item polymer = new Item(resources_64).setRegistryName(Constants.MOD_ID, "polymer");
	
	public static final Item coil_core = new Item(resources_64).setRegistryName(Constants.MOD_ID, "coil_core");	
	public static final Item coil_copper = new Item(resources_64).setRegistryName(Constants.MOD_ID, "coil_copper");
	public static final Item coil_iron = new Item(resources_64).setRegistryName(Constants.MOD_ID, "coil_iron");
	public static final Item coil_neon = new Item(resources_64).setRegistryName(Constants.MOD_ID, "coil_neon");
	public static final Item coil_gold = new Item(resources_64).setRegistryName(Constants.MOD_ID, "coil_gold");
	public static final Item coil_quantanium = new Item(resources_64).setRegistryName(Constants.MOD_ID, "coil_quantanium");
	
	public static final Item parts_iron = new Item(resources_64).setRegistryName(Constants.MOD_ID, "parts_iron");
	public static final Item parts_diamond = new Item(resources_64).setRegistryName(Constants.MOD_ID, "parts_diamond");
	public static final Item parts_quartz = new Item(resources_64).setRegistryName(Constants.MOD_ID, "parts_quartz");
	public static final Item parts_neon = new Item(resources_64).setRegistryName(Constants.MOD_ID, "parts_neon");
	public static final Item parts_copper = new Item(resources_64).setRegistryName(Constants.MOD_ID, "parts_copper");
	public static final Item parts_gold = new Item(resources_64).setRegistryName(Constants.MOD_ID, "parts_gold");
	
	public static void register(RegistryEvent.Register<Item> event)
	{
		IForgeRegistry<Item> r = event.getRegistry();
		
		r.registerAll(ingot_tin, ingot_zinc, ingot_copper, ingot_magnet, ingot_aluminium, ingot_silicon);
		r.registerAll(ingot_neon, ingot_retium, ingot_glowite, ingot_bioterium, ingot_wakurium, ingot_quantanium);
		r.registerAll(ingot_rare_earth, ingot_gadolinium, ingot_lithium, ingot_neodymium);
		r.register(ingot_bitripentium);
		r.registerAll(composite_metal, ceramic, polymer);
		r.registerAll(coil_core, coil_copper, coil_iron, coil_neon, coil_gold, coil_quantanium);
		r.registerAll(parts_iron, parts_diamond, parts_quartz, parts_neon, parts_copper, parts_gold);
		
		DustItems.registerItems(event);
	}
}
