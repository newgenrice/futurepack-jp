package futurepack.common.item.misc;

import java.util.List;

import futurepack.api.interfaces.IItemNeon;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.util.Mth;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class ItemBatterie extends Item implements IItemNeon
{
	private final int defaultMaxNeon;
	
	public ItemBatterie(Item.Properties props, int in) 
	{
		super(props.setNoRepair().stacksTo(1));
		defaultMaxNeon = in;
	}
	
	public static ItemStack getDefaultStack(Item item)
	{
		ItemStack st = new ItemStack(item);
		
		CompoundTag nbt = new CompoundTag();
		nbt.putInt("maxNE", ((ItemBatterie)item).defaultMaxNeon);
		nbt.putInt("ne", 0);
		
		st.addTagElement("neon", nbt);

		return st;
	}
	
	public int getDefaultNeon()
	{
		return defaultMaxNeon;
	}
	
	@Override
	public void appendHoverText(ItemStack it, Level w, List<Component> l, TooltipFlag p_77624_4_) 
	{
		l.add(HelperItems.getTooltip(it, this));
		super.appendHoverText(it, w, l, p_77624_4_);
	}
	
	@Override
	public void fillItemCategory(CreativeModeTab tab, NonNullList<ItemStack> subItems)
	{
		if(allowdedIn(tab))
		{
			ItemStack st = new ItemStack(this);
			addNeon(st, getMaxNeon(st));
			subItems.add(st);
		}
	}
	
	@Override
	public int getRGBDurabilityForDisplay(ItemStack stack)
	{
		return Mth.hsvToRgb(0.52F, 1.0F, (0.5F + (float)getNeon(stack) / (float)getMaxNeon(stack) * 0.5F));
	}
	
	@Override
	public double getDurabilityForDisplay(ItemStack stack)
	{
		return 1- ((double)getNeon(stack) / (double)getMaxNeon(stack));
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack)
	{
		return getNeon(stack) < getMaxNeon(stack);
	}

	@Override
	public int getMaxNeon(ItemStack it)
	{
		CompoundTag nbt = it.getTagElement("neon");
		if(nbt==null)
		{
			nbt = new CompoundTag();
			nbt.putInt("maxNE", defaultMaxNeon);
			it.addTagElement("neon", nbt);
			
			return defaultMaxNeon;
		}
		else if(nbt.contains("maxNE"))
		{
			return nbt.getInt("maxNE");
		}
		else
		{
			return defaultMaxNeon;
		}
		
	}
}
