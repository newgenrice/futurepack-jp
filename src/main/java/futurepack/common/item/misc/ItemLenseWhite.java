package futurepack.common.item.misc;

import futurepack.api.interfaces.IDeepCoreLogic;
import futurepack.common.block.misc.TileEntityBedrockRift;
import futurepack.common.item.ItemLenseBase;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.ItemStack;

public class ItemLenseWhite extends ItemLenseBase 
{

	public ItemLenseWhite(Properties properties)
	{
		super(properties, 10);
	}

	@Override
	public boolean isWorking(ItemStack item, IDeepCoreLogic logic) 
	{
		TileEntityBedrockRift rift = (TileEntityBedrockRift) logic.getRift();
		return rift!=null && !rift.isScanned();
	}

	@Override
	public boolean updateProgress(ItemStack item, IDeepCoreLogic logic) 
	{
		return updateScanning(item, logic);
	}

	@Override
	public int getColor(ItemStack item, IDeepCoreLogic logic) 
	{
		return 0xFFFFFF;
	}

	@Override
	public int getMaxDurability(ItemStack item, IDeepCoreLogic logic) 
	{
		return 1000;
	}

	public boolean updateScanning(ItemStack item, IDeepCoreLogic logic)
	{
		TileEntityBedrockRift rift = (TileEntityBedrockRift) logic.getRift();
		if(rift==null || rift.isScanned())
			return false;
		
		float maxTicks = 1980F;
		CompoundTag nbt = item.getOrCreateTagElement("scanning");
		int progress = nbt.getInt("progress");
		if(++progress > maxTicks)
		{
			progress = 0;
			
			rift.setScanned(true);
			
			nbt.putInt("progress", 0);
			logic.setProgress(0F);
			return true;
		}
		nbt.putInt("progress", progress);
		logic.setProgress(progress / maxTicks);
		return false;
	}
}
