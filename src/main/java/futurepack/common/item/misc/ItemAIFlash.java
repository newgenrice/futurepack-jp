package futurepack.common.item.misc;

import java.util.List;

import futurepack.api.interfaces.IItemSupport;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.util.Mth;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class ItemAIFlash extends Item implements IItemSupport
{
	private final int defaultStorage;
	
	public ItemAIFlash(Item.Properties props, int u)
	{
		super(props.stacksTo(1));
		defaultStorage = u;
	}

	@Override
	public int getMaxSupport(ItemStack it) 
	{
		CompoundTag nbt = it.getTagElement("support");
		if(nbt==null)
		{
			nbt = new CompoundTag();
			nbt.putInt("maxSP", defaultStorage);
			it.addTagElement("support", nbt);
			
			return defaultStorage;
		}
		if(!nbt.contains("maxSP"))
		{
			return defaultStorage;
		}
		return nbt.getInt("maxSP");
	}
	
	@Override
	public void appendHoverText(ItemStack it, Level w, List<Component> l, TooltipFlag p_77624_4_) 
	{
		l.add(HelperItems.getTooltip(it, this));
		super.appendHoverText(it, w, l, p_77624_4_);
	}
	
	@Override
	public void fillItemCategory(CreativeModeTab tab, NonNullList<ItemStack> subItems)
	{
		if(allowdedIn(tab))
		{
			ItemStack st = new ItemStack(this);
			addSupport(st, getMaxSupport(st));
			subItems.add(st);
		}
	}
	
	@Override
	public int getRGBDurabilityForDisplay(ItemStack stack)
	{
		return Mth.hsvToRgb(0.2F, 1.0F, (0.5F + (float)getSupport(stack) / (float)getMaxSupport(stack) * 0.5F));
	}
	
	@Override
	public double getDurabilityForDisplay(ItemStack stack)
	{
		return 1- ((double)getSupport(stack) / (double)getMaxSupport(stack));
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack)
	{
		return getSupport(stack) < getMaxSupport(stack);
	}
}
