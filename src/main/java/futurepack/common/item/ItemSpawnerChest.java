package futurepack.common.item;

import futurepack.common.FPConfig;
import futurepack.depend.api.helper.HelperEntities;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;

public class ItemSpawnerChest extends Item
{

	public ItemSpawnerChest(Properties properties) 
	{
		super(properties);
	}
	
	@Override
	public InteractionResult useOn(UseOnContext context)
	{
		Level w = context.getLevel();
		if(!w.isClientSide)
		{
			ItemStack it = context.getItemInHand();
			BlockPos pos = context.getClickedPos();
			boolean b;
			if(it.getTagElement("spawner")!=null)
			{
				b = placeDownSpawner(w, pos.relative(context.getClickedFace()), it);
				if(b)
				{
					it.removeTagKey("spawner");
				}
			}
			else
			{
				b = pickUpSpawner(w, pos, it);
			}
			return b? InteractionResult.SUCCESS : InteractionResult.PASS;
		}
			
		return super.useOn(context);
	}
	
 

	private boolean pickUpSpawner(Level w, BlockPos pos, ItemStack it)
	{
		if(FPConfig.SERVER.disableSpawnerChest.get())
			return false;
		
		if(w.getBlockState(pos).getBlock() == Blocks.SPAWNER)
		{
			BlockEntity tile = w.getBlockEntity(pos);
			if(tile!=null)
			{
				CompoundTag nbt = it.getOrCreateTagElement("spawner");
				tile.save(nbt);
				HelperEntities.disableItemSpawn();
				w.removeBlock(pos, false);
				HelperEntities.enableItemSpawn();
				return true;
			}
		}
		return false;
	}
	
	private boolean placeDownSpawner(Level w, BlockPos pos, ItemStack it)
	{
		if(w.getBlockState(pos).isAir())
		{
			CompoundTag nbt = it.getTagElement("spawner");
			if(nbt!=null)
			{
				w.setBlockAndUpdate(pos, Blocks.SPAWNER.defaultBlockState());
				BlockEntity tile = BlockEntity.loadStatic(pos, Blocks.SPAWNER.defaultBlockState(), nbt);
				w.setBlockEntity(tile);
				return true;
			}
		}
		return false;
	}
}
