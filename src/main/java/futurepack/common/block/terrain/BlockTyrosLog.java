package futurepack.common.block.terrain;

import java.util.Random;

import futurepack.common.block.plants.PlantBlocks;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.state.BlockState;

public class BlockTyrosLog extends RotatedPillarBlock
{

	public BlockTyrosLog(Properties props) 
	{
		super(props.randomTicks());
	}
	
	@Override
	public void randomTick(BlockState state, ServerLevel w, BlockPos pos, Random random)
	{
		if(random.nextInt(20)==0)
		{
			if(w.isEmptyBlock(pos.below()))
			{
			w.setBlockAndUpdate(pos.below(), PlantBlocks.glowmelo.defaultBlockState());
			}
		}
		super.tick(state, w, pos, random);
	}

}
