package futurepack.common.block.misc;

import java.util.Random;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class BlockBedrockRift extends Block implements EntityBlock
{
	public BlockBedrockRift() 
	{
		super(Properties.copy(Blocks.BEDROCK).sound(SoundType.STONE).lightLevel(state -> 8).noDrops().randomTicks());
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onRemove(BlockState state, Level worldIn, BlockPos pos, BlockState newState, boolean isMoving)
	{
		//Why?!?   Thermal flat bedrock destroy every Block, which is in the bedrock
		//Prevent replacing by bedrock is the easiest and cleanest solution from the pool of dirty hacks :P
        //(Note Bedrockores use generation in EndTick and ChunkLoad)
		 if(worldIn.getBlockState(pos).getBlock() == Blocks.BEDROCK)
			 worldIn.setBlockAndUpdate(pos, defaultBlockState());
		 else
			 super.onRemove(state, worldIn, pos, newState, isMoving); //dont remove TileEntity
	}
	
	@Override
	public void randomTick(BlockState state, ServerLevel w, BlockPos pos, Random r) 
	{
		super.randomTick(state, w, pos, r);
		double regen = (r.nextInt(4) - r.nextInt(10)) / 16384D;
		if(regen > 0)
		{
			TileEntityBedrockRift rift = (TileEntityBedrockRift) w.getBlockEntity(pos);
			if(rift!=null && rift.getFillrate() < 1.0)
			{
				rift.regenerate(regen);;
			}
		}
	}
	
	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new TileEntityBedrockRift(pos, state);
	}
}
