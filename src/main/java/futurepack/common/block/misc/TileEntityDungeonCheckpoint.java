package futurepack.common.block.misc;

import futurepack.api.interfaces.tilentity.ITileRenameable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.dim.structures.NameGenerator;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.server.level.ServerChunkCache;
import net.minecraft.server.level.TicketType;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityDungeonCheckpoint extends FPTileEntityBase implements ITileRenameable
{
	private Component name;

	public TileEntityDungeonCheckpoint(BlockEntityType<TileEntityDungeonCheckpoint> tileEntityTypeIn, BlockPos pos, BlockState state) 
	{
		super(tileEntityTypeIn, pos, state);
	}

	public TileEntityDungeonCheckpoint(BlockPos pos, BlockState state) 
	{
		this(FPTileEntitys.DUNGEON_CHECKPOINT, pos, state);
	}

	@Override
	public Component getName() 
	{
		if(name==null)
		{
			name = new TextComponent(NameGenerator.DUNGEON.getRandomName(level.random));
			setChanged();
		}
		return name;
	}

	@Override
	public void setName(String s) 
	{
		this.name = new TextComponent(s);
	}
	
	@Override
	public CompoundTag save(CompoundTag nbt) 
	{
		if(name!=null)
			nbt.putString("name", Component.Serializer.toJson(name));
		return super.save(nbt);
	}
	
	@Override
	public void load(CompoundTag nbt) 
	{
		super.load(nbt);
		if(nbt.contains("name"))
			name = Component.Serializer.fromJsonLenient(nbt.getString("name"));
	}
	
	public boolean teleportTo(Entity e, BlockPos pos)
	{
		if(level.getChunkSource() instanceof ServerChunkCache)
		{
			ServerChunkCache prov = (ServerChunkCache) level.getChunkSource();
			ChunkPos destination = new ChunkPos(pos);
			prov.addRegionTicket(TicketType.POST_TELEPORT, destination, 0, e.getId());
			
			BlockState state = level.getBlockState(pos);
			if(state.hasProperty(BlockRotateableTile.FACING))
			{
				Direction dir = state.getValue(BlockRotateableTile.FACING);
				BlockPos set = pos.relative(dir);
				if(level.isEmptyBlock(set))
				{
					if(!level.isEmptyBlock(set.above()))
					{
						set = set.below();
					}
					e.teleportToWithTicket(set.getX()+0.5, set.getY()+0.125, set.getZ()+0.5);
					return true;
				}
			}
		}
		return false;
	}
}
