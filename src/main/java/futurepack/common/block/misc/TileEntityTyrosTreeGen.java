package futurepack.common.block.misc;

import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import futurepack.world.dimensions.biomes.FPBiomes;
import futurepack.world.gen.feature.TyrosTreeFeature;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityTyrosTreeGen extends FPTileEntityBase implements ITileServerTickable
{
	private boolean done = false;

	public TileEntityTyrosTreeGen(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.TYROS_TREE_GEN, pos, state);
	}

	@Override
	public void tickServer(Level level, BlockPos pos, BlockState pState)
	{
		if(!done)
		{
			execute((ServerLevel) level, pos);
		}
		if(done)
		{
			level.removeBlock(pos, false);
			level.removeBlockEntity(pos);
		}
			
	}

	private void execute(ServerLevel level, BlockPos pos)
	{
		TyrosTreeFeature f = TyrosTreeFeature.LARGE_TYROS_TREE;
		if(!level.isClientSide) {
			if(!f.place(level, (level).getChunkSource().getGenerator(), level.random, pos.offset(-2, 0, -2), FPBiomes.TYROS_TREE_CONFIG))
			{
				f.place(level, (level).getChunkSource().getGenerator(), level.random, pos, FPBiomes.TYROS_TREE_CONFIG);
			}
		}
		done = true;
	}
	
	@Override
	public CompoundTag save(CompoundTag nbt)
	{
		nbt.putBoolean("done", done);
		return super.save(nbt);
	}
	
	@Override
	public void load(CompoundTag compound)
	{
		done = compound.getBoolean("done");
		super.load(compound);
	}
}
