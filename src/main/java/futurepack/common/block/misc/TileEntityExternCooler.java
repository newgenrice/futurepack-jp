package futurepack.common.block.misc;

import futurepack.api.interfaces.tilentity.ITileClientTickable;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.block.modification.TileEntityModificationBase;
import futurepack.common.modification.thermodynamic.TemperatureManager;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityExternCooler extends FPTileEntityBase implements ITileClientTickable, ITileServerTickable
{
	public TileEntityExternCooler(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.EXTERN_COOLER, pos, state);
	}

	public float f ;

	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState pState)
	{
		tick();
	}

	@Override
	public void tickClient(Level pLevel, BlockPos pPos, BlockState pState)
	{
		tick();
	}
	
	public void tick()
	{
		BlockState state = getBlockState();
		Direction face = state.getValue(BlockRotateableTile.FACING);
		BlockPos xyz = worldPosition.relative(face.getOpposite());
		
		if(level.hasChunkAt(xyz))
		{
			Biome gen = level.getBiome(worldPosition);
			float temp = TemperatureManager.getTempDegrees(gen, worldPosition);
			f = temp>25F ? 1 : temp<20F ? 3 : 2;
			
			BlockEntity t = level.getBlockEntity(xyz);
			if(t instanceof TileEntityModificationBase)
			{
				TileEntityModificationBase m = (TileEntityModificationBase) t;

				m.setHeatCool(f, temp);
			}
		}
	}
	
}
