package futurepack.common.block.misc;

import futurepack.api.interfaces.tilentity.ITileClientTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;

public class TileEntityAirlockDoor extends FPTileEntityBase implements ITileClientTickable
{
	public long switched = 0;
	private int deadTime = 2;
	public boolean extended = false;
	
	public TileEntityAirlockDoor(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.AIRLOCK_DDOR, pos, state);
		switched = System.currentTimeMillis();
	}
	
	@Override
	public AABB getRenderBoundingBox()
	{
		return new AABB(-1,-1,-1,2,2,2).move(worldPosition.getX(), worldPosition.getY(), worldPosition.getZ());
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		if(level.isClientSide)
			extended = getBlockState().getValue(BlockAirlockDoor.EXTENDED);
	}
	
	@Override
	public void tickClient(Level pLevel, BlockPos pPos, BlockState pState)
	{
//		if(level.isClientSide)
//		{
			if(--deadTime <= 0)
			{
				deadTime = 2;
				if(extended != pState.getValue(BlockAirlockDoor.EXTENDED))
				{
					switched = System.currentTimeMillis();
					extended = !extended;
					deadTime = 15;
				}
			}
//		}
	}
}
