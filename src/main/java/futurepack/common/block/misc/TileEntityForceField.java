package futurepack.common.block.misc;


import futurepack.api.interfaces.tilentity.ITileHologramAble;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import futurepack.depend.api.helper.HelperHologram;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.entity.monster.Enemy;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.Projectile;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityForceField extends FPTileEntityBase implements ITileHologramAble
{
	public boolean letProjectilesPass = false;
	public boolean letMobsPass = false;
	public boolean letAnimalsPass = false;
	public boolean letBabysPass = false;
	public boolean letPlayerPass = false;
	/**
	 * The NBT tag of the item will not be checked correctly.
	 */
	public ItemStack neededItem = null;
		
	private BlockState hologram = null;
	
	public TileEntityForceField(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.FORCE_FIELD, pos, state);
	}
	
	public boolean canEntityPass(Entity e)
	{
		if(e instanceof Projectile && letProjectilesPass)
		{
			return true;
		}		
		
		if(e instanceof LivingEntity)
		{
			LivingEntity base = (LivingEntity) e;
			if(base.isBaby() && letBabysPass)
			{
				return true;
			}
		}
		
		if(e instanceof Animal && letAnimalsPass)
		{
			return true;
		}
		
		if(e instanceof Enemy && letMobsPass)
		{
			return true;
		}
		
		if(e instanceof Player && letPlayerPass)
		{
			return true;
		}
			
		if(neededItem!=null && !neededItem.isEmpty())
		{
			if(e instanceof Player)
			{
				Player pl = (Player) e;
				return pl.getInventory().contains(neededItem);	//meta bassed keys!!!
			}
			else if(e instanceof LivingEntity)
			{
				LivingEntity liv = (LivingEntity) e;
				return ItemStack.matches(neededItem, (liv.getItemInHand(InteractionHand.MAIN_HAND))) || ItemStack.matches(neededItem, (liv.getItemInHand(InteractionHand.OFF_HAND)));
			}
			else
			{
				return false;
			}
		}
		
		return false;
	}

	@Override
	public CompoundTag save(CompoundTag nbt)
	{
		super.save(nbt);
		nbt.putBoolean("projectiles", letProjectilesPass);
		nbt.putBoolean("mobs", letMobsPass);
		nbt.putBoolean("animals", letAnimalsPass);
		nbt.putBoolean("babys", letBabysPass);
		nbt.putBoolean("player", letPlayerPass);
		
		if(neededItem!=null)
		{
			nbt.put("item", neededItem.serializeNBT());
		}
		
		if(hologram!=null)
			nbt.put("holo", HelperHologram.toNBT(hologram));
		
		return nbt;
	}
	
	@Override
	public void load(CompoundTag nbt)
	{
		super.load(nbt);
		letProjectilesPass = nbt.getBoolean("projectiles");
		letMobsPass = nbt.getBoolean("mobs");
		letAnimalsPass = nbt.getBoolean("animals");
		letBabysPass = nbt.getBoolean("babys");
		letPlayerPass = nbt.getBoolean("player");
		
		if(nbt.contains("item"))
		{
			neededItem = ItemStack.of(nbt.getCompound("item"));
		}
		if(nbt.contains("holo"))
			hologram = HelperHologram.fromNBT(nbt.getCompound("holo"));
	}
	
//	@Override
//	public boolean hasFastRenderer()
//	{
//		return HelperHologram.hasFastRenderer(this);
//	}

//	@Override
//	public boolean shouldRenderInPass(int pass)
//	{
//		return hasHologram() ? pass==0 : pass == 1;
//	}
	
	@Override
	public BlockState getHologram()
	{
		return hologram;
	}

	@Override
	public boolean hasHologram()
	{
		return hologram!=null;
	}

	@Override
	public void setHologram(BlockState state)
	{
		hologram = state;
	}
}
