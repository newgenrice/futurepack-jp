package futurepack.common.block.misc;

import java.util.ArrayList;
import java.util.List;

import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPLog;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.dim.structures.ManagerDungeonStructures;
import futurepack.common.dim.structures.OpenDoor;
import futurepack.common.dim.structures.StructureBase;
import futurepack.common.dim.structures.StructureToJSON;
import net.minecraft.Util;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.DirectionalBlock;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.entity.SignBlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityStructureFixHelper extends FPTileEntityBase implements ITileServerTickable
{

	private List<LoadedStructure> base = null;
	private List<PlacedStructure> placed = null;
	private int j = 0;
	
	public TileEntityStructureFixHelper(BlockEntityType<? extends TileEntityStructureFixHelper> type, BlockPos pos, BlockState state) 
	{
		super(type, pos, state);
	}
	
	public TileEntityStructureFixHelper(BlockPos pos, BlockState state) 
	{
		this(FPTileEntitys.STRUCTURE_FIX_HELPER, pos, state);
	}

	@Override
	public void tickServer(Level level, BlockPos pPos, BlockState pState)
	{
		if(placed!=null)
		{
			if(j < placed.size())
			{
				PlacedStructure pl = placed.get(j);
				
				if(pl.hasStateChanged())
				{
					if(pl.state == EnumStructureState.FIXED)
					{
						try
						{
							pl.rescanBlocks();
							pl.clearBlocks();
							placed.remove(j);
							Component msg = new TextComponent("Structures Left " + placed.size());
							level.players().forEach(p -> p.sendMessage(msg, Util.NIL_UUID));
							j--;
						}
						catch (Exception e) 
						{
							e.printStackTrace();
							level.setBlockAndUpdate(pl.getPosOfStateBlock(), EnumStructureState.BROKEN.getRepresantantBlock());
						}
					}
					else if(pl.state == EnumStructureState.WORKING)
					{
						pl.clearBlocks();
						placed.remove(j);
						Component msg = new TextComponent("Structures Left " + placed.size());
						level.players().forEach(p -> p.sendMessage(msg, Util.NIL_UUID));
						j--;
					}
				}
				j++;
			}
			else
			{
				j = 0;
			}
		}
		else
		{	
			prepare();
		}
	}
	
	@Override
	public void load(CompoundTag compound) 
	{
		ListTag list = compound.getList("placed", compound.getId());
		placed = new ArrayList<TileEntityStructureFixHelper.PlacedStructure>(list.size());
		for(int i=0;i<list.size();i++)
		{
			placed.add(readNBT(list.getCompound(i)));
		}		
		super.load(compound);
	}
	
	@Override
	public CompoundTag save(CompoundTag compound) 
	{
		ListTag list = new ListTag();
		for(PlacedStructure ps : placed)
		{
			list.add(writeNBT(ps));
		}
		compound.put("placed", list);
		return super.save(compound);
	}
	
	private PlacedStructure readNBT(CompoundTag nbt)
	{
		String name = nbt.getString("name");
		int rot = nbt.getInt("rot");
		int[] start = nbt.getIntArray("start");
		StructureBase struc = ManagerDungeonStructures.get(name, rot);
		return new PlacedStructure(new BlockPos(start[0],start[1],start[2]), name, struc, rot, false);
	}
	
	private CompoundTag writeNBT(PlacedStructure struc)
	{
		CompoundTag nbt = new CompoundTag();
		
		nbt.putString("name", struc.name);
		nbt.putInt("rot", struc.rot);
		nbt.putIntArray("start", new int[]{struc.start.getX()-1,struc.start.getY()-1,struc.start.getZ()-1});
		return nbt;
	}
	
	
	private void prepare()
	{
		if(placed!=null)
			return;
		
		base = new ArrayList<TileEntityStructureFixHelper.LoadedStructure>();
		placed = new ArrayList<TileEntityStructureFixHelper.PlacedStructure>();
		
		//Corridor
		addEntryAuto( 2, "corridor_I");
		addEntryAuto( 4, "corridor_L");
		addEntryAuto( 4, "corridor_T");
		addEntryAuto( 1, "corridor_X_1");
		addEntryAuto( 1, "corridor_X_2");		
		addEntryManual( 4, "corridor_stairs");
		addEntryAuto( 4, "corridor_end_1");
		addEntryAuto( 4, "corridor_end_2");
		addEntryManual( 4, "corridor_ladder_flat");
		addEntryManual( 4, "corridor_ladder");
		addEntryManual( 4, "corridor_laddermulti");
		
		//Deco
		addEntryAuto( 4, "deco_end_1");
		addEntryManual( 4, "deco_end_2");
		
		//Loot
		addEntryManual( 4, "loot_end_1");
		addEntryAuto( 4, "loot_end_crystal_bio");
		addEntryAuto( 4, "loot_end_crystal_neon");
		addEntryManual( 4, "loot_jmpnrun_1");
		addEntryManual( 4, "loot_end_flat");
		addEntryManual( 4, "loot_end_simple");
		addEntryManual( 4, "loot_jmpnrun_2");
		
		//Boss -- TODO more boss rooms @Wugi
		addEntryManual( 4, "special_boss_1");	
		addEntryAuto( 4, "special_teclock_1");
		
		//Entrace -- TODO more entraces ? @Wugi
//		if(spawnEntrace)
			addEntryAuto( 1, "special_entrance_bottom");
//		else
//			base.addEntry(get("special_entrance_bottom", 0));
		
		addEntryManual( 4, "special_endroom_1");
		addEntryAuto( 1, "special_endroom_2");
		
		int size; // the size of each cube 
		size = base.stream().map(LoadedStructure::getSize).mapToInt(v -> Math.max(v.getX(), Math.max(v.getY(), v.getZ()))).max().orElse(0);
		
		int sX, sY; //the amount of structures to place for each side
		
		double root = Math.pow(base.size(), 1.0 / 3.0);
		sX = sY = Math.round((float)root);
		
		if(sY * size + worldPosition.getY() > 240)
		{
			//too large
			sY = (240-worldPosition.getY()) / size;
		}
		
		int x=0,y=0,z=0;
		BlockPos start = worldPosition.offset(2, 2, 2);
		for(int i=0;i<base.size();i++)
		{
			placed.add(base.get(i).placeStructrue(start.offset(x*size,y*size,z*size)));
			x++;
			if(x>=sX)
			{
				x=0;
				y++;
				if(y>=sY)
				{
					y=0;
					z++;
				}
			}
		}
		base = null;
		
	}
	
	public void addEntryAuto(int rotations, String path)
	{
		for(int i=0; i<rotations; i++)
		{
			base.add(new LoadedStructure(path, i));
		}
	}
	
	public void addEntryManual(int rotations, String path)
	{
		for(int i=0; i<rotations; i++)
		{
			base.add(new LoadedStructure(path+"_R"+i, 0));
			
		}	
	}

	
	private class LoadedStructure
	{
		private String name;
		private int rot;
		private StructureBase structure;
		
		public LoadedStructure(String name, int rot) 
		{
			this.name = name;
			this.rot = rot;
			structure = ManagerDungeonStructures.get(name, rot);
		}
		
		public Vec3i getSize()
		{
			return new Vec3i(structure.getWidth(), structure.getHeight(), structure.getDepth());
		}
		
		public PlacedStructure placeStructrue(BlockPos start)
		{
			return new PlacedStructure(start, name, structure, rot, true);
		}
	}
	
	private class PlacedStructure
	{
		private EnumStructureState state;
		private final BlockPos start, end, statePos;
		private final String name;
		private final StructureBase structure;
		private final int rot;
		private boolean shouldSave;
		
		public PlacedStructure(BlockPos start, String name, StructureBase structure, int rot, boolean placeBlocks) 
		{
			super();
			if(placeBlocks)
				level.setBlockAndUpdate(start, Blocks.PINK_GLAZED_TERRACOTTA.defaultBlockState());
			this.start = start.offset(1, 1, 1);
			this.name = name;
			this.structure = structure;
			this.rot = rot;
			
			shouldSave = rot == 0;
			end = this.start.offset(structure.getWidth()-1, structure.getHeight()-1, structure.getDepth()-1);
			statePos = end.offset(1,2,1);
			if(placeBlocks)
			{
				level.setBlockAndUpdate(end.offset(1,1,1), Blocks.PINK_GLAZED_TERRACOTTA.defaultBlockState());
				level.setBlockAndUpdate(statePos, EnumStructureState.UNKNOWN.getRepresantantBlock());
				BlockPos signPos = start.offset(0, structure.getHeight()+1, 0);
				level.setBlockAndUpdate(signPos, Blocks.PINK_GLAZED_TERRACOTTA.defaultBlockState());
				signPos = signPos.above();
				level.setBlockAndUpdate(signPos, Blocks.ACACIA_SIGN.defaultBlockState());
				SignBlockEntity sign = (SignBlockEntity) level.getBlockEntity(signPos);
				sign.setMessage(0, new TextComponent("Name:"));
				sign.setMessage(1, new TextComponent(name));
				sign.setMessage(2, new TextComponent("Rotation:"));
				sign.setMessage(3, new TextComponent("" + rot));
				placeBlocks();
			}
		}
		
		public boolean hasStateChanged()
		{
			EnumStructureState old = state;
			return old != getState();
		}
		
		public EnumStructureState getState()
		{
			return state = EnumStructureState.getStateFromBlock(level.getBlockState(getPosOfStateBlock()).getBlock());
		}
		
		private BlockPos getPosOfStateBlock()
		{
			return statePos;
		}
		
		public void placeBlocks()
		{
			structure.hide = false;
			structure.generate(level, start, new ArrayList<>());
			structure.addChestContentBase((ServerLevelAccessor) level, start, level.random, new CompoundTag(), level.getServer().getLootTables());
			
			BlockState[][][] blocks = structure.getBlocks();
			BlockState bedrock = Blocks.BEDROCK.defaultBlockState();
			for(int x=0;x<blocks.length;x++)
			{
				for(int y=0;y<blocks[x].length;y++)
				{
					for(int z=0;z<blocks[x][y].length;z++)
					{
						if(blocks[x][y][z]==null)
						{
							level.setBlockAndUpdate(start.offset(x,y,z), bedrock);
						}
					}
				}
			}
			
			for(OpenDoor door : structure.getRawDoors())
			{
				BlockPos xyz = start.offset(door.getPos());
				for(int x=0;x<door.getWeidth();x++)
				{
					for(int y=0;y<door.getHeight();y++)
					{
						for(int z=0;z<door.getDepth();z++)
						{
							BlockState state = Blocks.END_ROD.defaultBlockState().setValue(DirectionalBlock.FACING, door.getDirection());
							level.setBlockAndUpdate(xyz.offset(x,y,z), state);
						}
					}
				}
			}
			FPLog.logger.debug("Placed %s, rot %s at %s", name, rot, start);
		}
		
		public void clearBlocks()
		{
			for(BlockPos pos : BlockPos.betweenClosed(start.offset(-1,-1,-1), end.offset(1,2,1)))
			{
				level.setBlockAndUpdate(pos, Blocks.AIR.defaultBlockState());
			}
		}
		
		public void rescanBlocks() throws Exception
		{
			if(shouldSave)
			{
				StructureToJSON gen = new StructureToJSON(level);
				if(name!=null)
				{
					gen.fileName = name;
				}
				gen.generate(start, end);
			}
			else
			{
				throw new UnsupportedOperationException("not saveable");
			}
		}
	}
	
	public static enum EnumStructureState
	{
		UNKNOWN(Blocks.BLACK_WOOL),
		BROKEN(Blocks.RED_WOOL),
		WORKING(Blocks.WHITE_WOOL),
		FIXED(Blocks.LIME_WOOL);
		
		private final Block block;
		
		private EnumStructureState(Block block) 
		{
			this.block = block;
		}
		
		public BlockState getRepresantantBlock()
		{
			return block.defaultBlockState();
		}
		
		public static EnumStructureState getStateFromBlock(Block block)
		{
			for(EnumStructureState s : values())
			{
				if(s.block==block)
					return s;
			}
			return UNKNOWN;
		}
	}

}
