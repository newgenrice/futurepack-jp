package futurepack.common.block.misc;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockHoldingTile;
import futurepack.depend.api.helper.HelperInventory;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.PlantType;

public class BlockSaplingHolder extends BlockHoldingTile implements IBlockServerOnlyTickingEntity<TileEntitySaplingHolder>
{
	public static final BooleanProperty FILLED = BooleanProperty.create("filled");

	private final PlantType planttype;
	
	protected BlockSaplingHolder(Block.Properties props, PlantType planttype)
	{
		super(props);
//		super(Material.IRON);
//		setCreativeTab(FPMain.tab_maschiens);
		
		this.planttype = planttype;
		this.registerDefaultState(this.stateDefinition.any().setValue(FILLED, false));
	}
	
	@Override
	public boolean canSustainPlant(BlockState state, BlockGetter world, BlockPos pos, Direction facing, IPlantable plantable)
	{
		if(world.getBlockState(pos.relative(facing)).isAir())//for prechecks
		{
			return true;
		}
		PlantType plant = plantable.getPlantType(world, pos.relative(facing));
		if(plant == planttype)
			return true;
		return super.canSustainPlant(state, world, pos, facing, plantable);
	}
	
//	@Override
//	public IBlockState getActualState(IBlockState state, IWorldReader w, BlockPos pos)
//	{
//		TileEntitySaplingHolder tile = (TileEntitySaplingHolder) w.getTileEntity(pos);
//		if(tile!=null)
//		{
//			state = state.withProperty(filled, !tile.getFilter().isEmpty());
//			
//		}
//		return state;
//	}
	
//	@Override
//	public void getSubBlocks(ItemGroup itemIn, NonNullList<ItemStack> items)
//	{
//		 items.add(new ItemStack(this,1,0));
//		 items.add(new ItemStack(this,1,1));
//		 items.add(new ItemStack(this,1,2));
//	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		ItemStack it = pl.getItemInHand(hand);
		TileEntitySaplingHolder tile = (TileEntitySaplingHolder) w.getBlockEntity(pos);
		if(it==null || it.isEmpty())
		{
			tile.setFilter(ItemStack.EMPTY);
			w.setBlockAndUpdate(pos, state.setValue(FILLED, false));
			return InteractionResult.SUCCESS;
		}
		else
		{
			if(isSapling(it))
			{
				ItemStack stack = tile.getFilter();
				if(stack!=null)
				{
					if(HelperInventory.areItemsEqualNoSize(stack, it))
					{
						tile.tryPlace(it);
						return InteractionResult.SUCCESS;
					}
				}
				tile.setFilter(it.copy());
				w.setBlockAndUpdate(pos, state.setValue(FILLED, true));
				return InteractionResult.SUCCESS;
			}
		}
		
		return super.use(state, w, pos, pl, hand, hit);
	}
	
	public static boolean isSapling(ItemStack it)
	{
		return ItemTags.SAPLINGS.contains(it.getItem());
	}

    @Override
    protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
    {
    	super.createBlockStateDefinition(builder);
    	builder.add(FILLED);
    }

	@Override
	public BlockEntityType<TileEntitySaplingHolder> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.SAPLING_HOLDER;
	}
}
