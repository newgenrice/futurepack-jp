package futurepack.common.block.misc;

import java.util.function.ToIntFunction;

import futurepack.api.FacingUtil;
import futurepack.common.FuturepackTags;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelWriter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.EntityCollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockQuantanium extends Block
{
	public static final BooleanProperty up = BooleanProperty.create("up");
	public static final BooleanProperty down = BooleanProperty.create("down");
	public static final BooleanProperty north = BooleanProperty.create("north");
	public static final BooleanProperty east = BooleanProperty.create("east");
	public static final BooleanProperty south = BooleanProperty.create("south");
	public static final BooleanProperty west = BooleanProperty.create("west");
	public static final BooleanProperty glowing = BooleanProperty.create("glowing");
	
	public static final BooleanProperty[] faces = new BooleanProperty[]{down,up,north,south,west,east};

	public static final ToIntFunction<BlockState> GET_LIGHT = state -> state.getValue(glowing) ? 12 : 0;
	
	public BlockQuantanium(Block.Properties props)
	{
		super(props);
		this.registerDefaultState(this.stateDefinition.any().setValue(up, false).setValue(down, false).setValue(north, false).setValue(south, false).setValue(west, false).setValue(east, false).setValue(glowing, false));
	}
	
	@Override
	public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, LevelAccessor worldIn, BlockPos currentPos, BlockPos facingPos)
	{
		state = state.setValue(faces[facing.get3DDataValue()], isAccepted(facingState) );
		return state;
	}

	public boolean isAccepted(BlockState state)
	{
		if(state.getBlock() == this)
			return true;
		return state.is(FuturepackTags.quantanium_connecting);
	}
	
	@Override
	public void entityInside(BlockState state, Level world, BlockPos pos, Entity entityIn) {		
		if(!world.isClientSide) {
			if(!state.getValue(glowing) && entityIn instanceof Player) {
//				world.setBlockState(pos, state.with(glowing, true), 3);
			}
		}
	}
	
	@Override
	public VoxelShape getCollisionShape(BlockState state, BlockGetter w, BlockPos pos, CollisionContext context) 
	{
		if(!state.getValue(glowing) && w.getBlockState(pos) == state)
		{
			if(context instanceof EntityCollisionContext && ((EntityCollisionContext)context).getEntity().orElse(null) instanceof Player && w instanceof LevelWriter)
			{
				((LevelWriter)w).setBlock(pos, state.setValue(glowing, true), 3);
			}
		}
		return super.getCollisionShape(state, w, pos, context);
	}

	@Override
	public void onPlace(BlockState state, Level w, BlockPos pos, BlockState oldState, boolean isMoving) 
	{
		super.onPlace(state, w, pos, oldState, isMoving);
		if(!w.isClientSide) {
			if(oldState.getBlock()!=this)
			{
				for(Direction face : FacingUtil.VALUES)
				{
					state = state.setValue(faces[face.get3DDataValue()], isAccepted(w.getBlockState(pos.relative(face))));
				}
				w.setBlock(pos, state, 2);
			}
		}
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(faces);
		builder.add(glowing);
	}
}
