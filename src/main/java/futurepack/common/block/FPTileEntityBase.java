package futurepack.common.block;

import com.google.common.base.Predicate;

import futurepack.common.sync.FPPacketHandler;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.Connection;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;

public abstract class FPTileEntityBase extends BlockEntity
{

	public FPTileEntityBase(BlockEntityType<?> pType, BlockPos pWorldPosition, BlockState pBlockState)
	{
		super(pType, pWorldPosition, pBlockState);
	}

	@Override
	public final CompoundTag getUpdateTag()
	{
		CompoundTag nbt = new CompoundTag();
		writeData(nbt);
		return nbt;
	}
	
	/**
	 * those values will be synced with the client
	 */
	public void writeData(CompoundTag pTag)
	{
		save(pTag);
	}
	
	/**
	 * those values will be synced with the client
	 */
	public void readData(CompoundTag nbt)
	{
		load(nbt);
	}
	
	@Override
	public void onDataPacket(Connection net, ClientboundBlockEntityDataPacket pkt)
	{
		if(level.isClientSide)
		{
			if(pkt.getTag().getBoolean("data"))
			{
				readData(pkt.getTag());
			}
			else
			{
				load(pkt.getTag());
			}
		}
		super.onDataPacket(net, pkt);
	}

	@Override
	public ClientboundBlockEntityDataPacket getUpdatePacket()
	{
		CompoundTag nbt = new CompoundTag();
		save(nbt);
		ClientboundBlockEntityDataPacket pack = new ClientboundBlockEntityDataPacket(worldPosition, 0, nbt);
		return pack;
	}
	
	protected void sendDataUpdatePackage(int range)
	{
		if(!level.isClientSide)
		{		
			CompoundTag nbt = new CompoundTag();
			writeData(nbt);
			nbt.putBoolean("data", true);
			final ClientboundBlockEntityDataPacket pack = new ClientboundBlockEntityDataPacket(worldPosition, 0, nbt);
			level.getEntitiesOfClass(ServerPlayer.class, new AABB(worldPosition.getX()-range, worldPosition.getY()-range, worldPosition.getZ()-range, worldPosition.getX()+range, worldPosition.getY()+range, worldPosition.getZ()+range), new Predicate<ServerPlayer>() //getEntitiesWithinAABBExcludingEntity
			{			
				@Override
				public boolean apply(ServerPlayer var1)
				{
					var1.connection.send(pack);
					return false;
				}
			});
		}
	}
	
	protected void sendFullUpdatePackage(Predicate<ServerPlayer> pred, int range)
	{
		FPPacketHandler.sendTileEntityPacketToAllClients(this, pred, range);
	}
	
}
