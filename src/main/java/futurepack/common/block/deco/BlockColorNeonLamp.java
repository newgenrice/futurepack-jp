package futurepack.common.block.deco;

import java.util.Random;

import javax.annotation.Nullable;

import futurepack.api.helper.HelperTileEntities;
import futurepack.common.FPTileEntitys;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.RedstoneLampBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class BlockColorNeonLamp extends RedstoneLampBlock implements EntityBlock
{

	public BlockColorNeonLamp(Properties properties)
	{
		super(properties);
	}
	
	@Override
	public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level pLevel, BlockState pState, BlockEntityType<T> pBlockEntityType) 
	{
		return pLevel.isClientSide() ? null : HelperTileEntities.createServerTickerHelper(pBlockEntityType, FPTileEntitys.NEON_LAMP);
	}
	
	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new TileEntityNeonLamp(pos, state);
	}
	
	@Override
	@Nullable
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		return this.defaultBlockState().setValue(LIT, false);
	}

	public static boolean isPowered(Level w, BlockPos pos)
	{
		if(w.hasNeighborSignal(pos))
		{
			TileEntityNeonLamp tile = (TileEntityNeonLamp) w.getBlockEntity(pos);
			return tile.isPowered();
		}
		return false;
	}
	
	@Override
	public void neighborChanged(BlockState state, Level worldIn, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving)
	{
		if (!worldIn.isClientSide) 
		{
			boolean flag = state.getValue(LIT);
			if (flag != isPowered(worldIn, pos)) 
			{
				if (flag) 
				{
					worldIn.getBlockTicks().scheduleTick(pos, this, 4);
				}
				else 
				{
					worldIn.setBlock(pos, state.cycle(LIT), 2);
				}
			}

		}
	}
	
	@Override
	public void tick(BlockState state, ServerLevel worldIn, BlockPos pos, Random random)
	{
		if (!worldIn.isClientSide) 
		{
			if (state.getValue(LIT) && !isPowered(worldIn, pos)) 
			{
				worldIn.setBlock(pos, state.cycle(LIT), 2);
			}
		}
	}
}
