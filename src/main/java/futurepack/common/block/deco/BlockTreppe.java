package futurepack.common.block.deco;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.state.BlockState;

public class BlockTreppe extends StairBlock
{
	public BlockTreppe(BlockState state, Block.Properties build)
	{
		super(state, build);
	}
	
	public BlockTreppe(BlockState state)
	{
		this(state, Block.Properties.copy(state.getBlock()).noOcclusion());
	}
	
	public BlockTreppe(Block b)
	{
		this(b.defaultBlockState(), Block.Properties.copy(b).noOcclusion());
	}
}
