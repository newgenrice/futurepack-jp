package futurepack.common.block.modification.machines;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.WorldlyContainer;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public abstract class TileEntityOptiAssemblerBase extends TileEntityMachineSupport implements WorldlyContainer, ITilePropertyStorage
{
	protected int progress = 0;
	
 	public TileEntityOptiAssemblerBase(BlockEntityType<? extends TileEntityOptiAssemblerBase> type, BlockPos pos, BlockState state)
	{
 		super(type, 1024, EnumEnergyMode.USE, pos, state);

	}
	
 	@Override
 	public void configureLogisticStorage(LogisticStorage storage) 
 	{
 		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ITEMS);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ENERGIE);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.SUPPORT);
		storage.removeState(EnumLogisticIO.OUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.OUT, EnumLogisticType.SUPPORT);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.SUPPORT);
		storage.setModeForFace(Direction.DOWN, EnumLogisticIO.OUT, EnumLogisticType.ITEMS);
 	}
 	
	@Override
	protected EnumLogisticType[] getLogisticTypes()
	{
		return new EnumLogisticType[]{EnumLogisticType.ENERGIE, EnumLogisticType.ITEMS, EnumLogisticType.SUPPORT};
	}
	
	@Override
	public void writeData(CompoundTag nbt)
	{
		super.writeData(nbt);
		nbt.putInt("progress", progress);
	}
	
	@Override
	public void readData(CompoundTag nbt)
	{		
		super.readData(nbt);
		progress = nbt.getInt("progress");
	}
	
	@Override
	public EnumEnergyMode getEnergyType()
	{
		return EnumEnergyMode.USE;
	}
	
	@Override
	public abstract void updateTile(int ticks);

	@Override
	public abstract boolean isWorking();
	
	@Override
	protected int getInventorySize()
	{
		return 9 + 3 + 1;
	}

	@Override
	public int[] getSlotsForFace(Direction side)
	{
		return new int[] {0,1,2,3,4,5,6,7,8,9,10,11,12};
	}
	
	@Override
	public abstract boolean canPlaceItem(int var1, ItemStack var2);
	
	@Override
	public boolean canPlaceItemThroughFace(int var1, ItemStack var2, Direction side)
	{
		if(!storage.canInsert(side, EnumLogisticType.ITEMS))
		{
			return false;
		}
		return var1 >= 0 && var1 <= 8 && canPlaceItem(var1, var2);
	}

	@Override
	public boolean canTakeItemThroughFace(int var1, ItemStack var2, Direction side)
	{
		if(!storage.canExtract(side, EnumLogisticType.ITEMS))
		{
			return false;
		}
		return var1 == 12;
	}

	@Override
	public int getProperty(int id)
	{
		switch (id) {
		case 0:
			return energy.get();
		case 1:
			return support.get();
		case 2:
			return progress;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch(id)
		{
		case 0:
			setEnergy(value);
			break;
		case 1:
			support.set(value);
			break;
		case 2:
			progress = value;
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount()
	{
		return 3;
	}
}
