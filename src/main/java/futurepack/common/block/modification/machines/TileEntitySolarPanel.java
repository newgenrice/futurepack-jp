package futurepack.common.block.modification.machines;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.IItemNeon;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.FuturepackTags;
import futurepack.common.modification.EnumChipType;
import futurepack.world.dimensions.Dimensions;
import net.minecraft.core.BlockPos;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.LightLayer;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntitySolarPanel extends TileEntityMachineBase implements ITilePropertyStorage
{
	public int light;
	protected float power = 0;
	
	public TileEntitySolarPanel(BlockPos pos, BlockState state) 
	{
		this(FPTileEntitys.SOLAR_PANEL, pos, state);
	}
	
	public TileEntitySolarPanel(BlockEntityType<? extends TileEntitySolarPanel> type, BlockPos pos, BlockState state) 
	{
		super(type, pos, state);
	}
	
	
	@Override
	public void configureLogisticStorage(LogisticStorage storage) 
	{
		storage.setDefaut(EnumLogisticIO.OUT, EnumLogisticType.ENERGIE);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ITEMS);
		
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.IN, EnumLogisticType.ENERGIE);
	}
	
	@Override
	public EnumEnergyMode getEnergyType() 
	{
		return EnumEnergyMode.PRODUCE;
	}
	
	private float getDimensionalFactor()
	{
		if(level.dimension().location().equals(Dimensions.MENELAUS_ID))
			return 0.5f;
		
		return 1.0f;
	}

	@Override
	public void updateTile(int ticks) 
	{
		light = level.getBrightness(LightLayer.SKY,worldPosition.above()) - level.getSkyDarken();
		light = (light < 0 ? 0 : light);
		
		if(light < 3)
		{
			BlockState state = level.getBlockState(getBlockPos().above());
			if(state.is(FuturepackTags.radioactive_light))
			{
				light = Math.max(light, state.getLightEmission() / 5);
			}
		}
		
		//power = (float) Math.pow(light * getDimensionalFactor() * (1 + getChipPower(EnumChipType.INDUSTRIE)), 1);
		power = light * getDimensionalFactor() * (1 + getChipPower(EnumChipType.INDUSTRIE) * 0.5f);	
		
		if(!level.isClientSide)
		{
			energy.add((int)(ticks* power));
			
			
			if(!items.get(0).isEmpty() && energy.get() > 0)
			{		
				if(items.get(0).getItem()instanceof IItemNeon)
				{
					IItemNeon ch = (IItemNeon) items.get(0).getItem();
					ItemStack it = items.get(0);
					if(ch.isNeonable(it) && ch.getNeon(it) < ch.getMaxNeon(it))
					{
						ch.addNeon(it, energy.use(ticks));
					}					
				}		
			}
		}
	}

	@Override
	public boolean isWorking() 
	{
		return true;
	}

	@Override
	public int getDefaultPowerUsage() 
	{
		return (int) power;
	}

	@Override
	public int getProperty(int id)
	{
		switch(id)
		{
		case 0:
			return energy.get();
		case 1: 
			return light;
		case 2:
			return (int) (power * 2);
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch(id)
		{
		case 0:
			setEnergy(value);
			break;
		case 1:
			light = value;
			break;
		case 2:
			power = 0.5F * value;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount()
	{
		return 3;
	}

	@Override
	protected int getInventorySize()
	{
		return 1;
	}

	@Override
	public int getMaxStackSize() 
	{
		return 1;
	}
	
}
