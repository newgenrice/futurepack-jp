package futurepack.common.block.modification.machines;

import java.util.UUID;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.api.interfaces.tilentity.ITileWithOwner;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.inventory.TileEntityScannerBlock;
import futurepack.common.modification.EnumChipType;
import futurepack.common.recipes.industrialfurnace.FPIndustrialNeonFurnaceManager;
import futurepack.common.recipes.industrialfurnace.IndNeonRecipe;
import futurepack.common.research.CustomPlayerData;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.WorldlyContainer;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityIndustrialNeonFurnace extends TileEntityMachineSupport implements WorldlyContainer, ITilePropertyStorage, ITileWithOwner
{
	public static final int FIELD_PROGRESS = 1;
	
	public boolean[] uses = new boolean[3];
	private int progress =0;
	private int wait=0;

	private UUID owner = new UUID(0,0);
	
	public TileEntityIndustrialNeonFurnace(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.INDUSTRIAL_NEON_FURNACE, 1024, EnumEnergyMode.USE, pos, state);
	}
	
	@Override
	public void configureLogisticStorage(LogisticStorage storage) 
	{
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ITEMS);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ENERGIE);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.SUPPORT);
		
		storage.removeState(EnumLogisticIO.OUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.SUPPORT);
		storage.removeState(EnumLogisticIO.OUT, EnumLogisticType.SUPPORT);
		
		storage.setModeForFace(Direction.DOWN, EnumLogisticIO.OUT, EnumLogisticType.ITEMS);
	}
	
	@Override
	protected EnumLogisticType[] getLogisticTypes()
	{
		return new EnumLogisticType[]{EnumLogisticType.ENERGIE, EnumLogisticType.ITEMS, EnumLogisticType.SUPPORT};
	}
	
	@Override
	public void updateNaturally()
	{
		if(wait>0)
			wait--;
	}
	
	@Override
	public void updateTile(int ticks)
	{	
		if(level.isClientSide)
			return;
		
		BlockState state = level.getBlockState(worldPosition);
		if(state.isAir())
		{
			return; //face palm
		}
		
		if(wait<=0)
		{
			uses = new boolean[3];
			IndNeonRecipe[] recipes = FPIndustrialNeonFurnaceManager.instance.getMatchingRecipes(new ItemStack[]{items.get(0),items.get(1),items.get(2)}, level);
			uses = FPIndustrialNeonFurnaceManager.instance.getAndClearUses();
			boolean up = false;
			boolean reset = false;
			int usedSlots = 0;
			do
			{
				if(recipes==null || recipes.length==0)
				{
					progress=0;
					wait=20;
					break;
				}	
				
				for(IndNeonRecipe recipe : recipes)
				{
					if(recipe==null)
					{
						continue;
					}
					
					if(this.support.get() < recipe.getSupport())
					{
						continue;
					}
					
					ItemStack out = recipe.getOutput();
					
					ServerLevel ws = (ServerLevel) level;
					CustomPlayerData data = CustomPlayerData.getDataFromUUID(owner, ws.getServer());
					if(data!=null && !data.canProduce(out))
					{
						continue;
					}	
					
					ItemStack[] allSlots = new ItemStack[3];
					for(int i=0;i<3;i++)
					{
						allSlots[i] = items.get((usedSlots +i)%3);
					}
					
					if(recipe.match( allSlots, new boolean[3]) && !out.isEmpty())
					{
						int slot = -1;
						for(int i=3;i<6;i++)
						{
							if(items.get(i).isEmpty() || (out.sameItem(items.get(i)) && items.get(i).getCount()+out.getCount() <=64))
							{
								slot = i;
								break;
							}				
						}
						if(slot>=0)
						{
							if(!up)	
							{
								progress+=ticks;
								up=true;
								setChanged();
							}
							if(progress>=11 && !level.isClientSide)
							{
								reset=true;
								progress-=11;
								support.use(recipe.getSupport());
								uses = new boolean[3];
								
								if(items.get(slot).isEmpty())
								{										
									if(recipe.use(allSlots))
									{	
										items.set(slot,  out);
										usedSlots++;
									}
								}
								else if(items.get(slot).getCount()+out.getCount() <=64)
								{								
									if(recipe.use(allSlots))
									{	
										items.get(slot).grow(out.getCount());
										usedSlots++;
									}
								}
								else
								{
									if(recipe.use(allSlots))
									{
										int n = 64 - items.get(slot).getCount();
										items.get(slot).setCount(64);	
										out.setCount(n);
										
										for(int i=3;i<6;i++)
										{
											if(items.get(i).isEmpty())
											{
												items.set(i,  out);
												break;
											}
											else if (out.sameItem(items.get(i)) && items.get(i).getCount()+n <=64)
											{
												items.get(i).grow(out.getCount());
												break;
											}				
										}
										usedSlots++;
									}
								}
							}
							for(int i=0;i<items.size();i++)
							{
								if(!items.get(i).isEmpty() && items.get(i).getCount()<= 0)
									items.set(i, ItemStack.EMPTY);
							}						
						}	
					}
				}
				if(reset)
				{
					setChanged();
				}
				
			}
			while(progress >= 11);
		}
		
		if(getBlockState().getValue(BlockIndustrialNeonFurnace.LIT) != isWorking())
		{
			level.setBlockAndUpdate(worldPosition, getBlockState().setValue(BlockIndustrialNeonFurnace.LIT, isWorking()));
			setChanged();
		}
		
		if(support.get()>0 && this.getChipPower(EnumChipType.SUPPORT)>0)
		{
			HelperEnergyTransfer.sendSupportPoints(this);
		}
	}
	
	@Override
	public boolean isWorking() 
	{
		return progress>0;
	}
	
	@Override
	public CompoundTag save(CompoundTag nbt)
	{
		super.save(nbt);
		storage.write(nbt);
		nbt.putLong("uuid1", owner.getMostSignificantBits());
		nbt.putLong("uuid2", owner.getLeastSignificantBits());
		return nbt;
	}
	
	@Override
	public void load(CompoundTag nbt)
	{
		super.load(nbt);
		storage.read(nbt);
		owner = new UUID(nbt.getLong("uuid1"), nbt.getLong("uuid2"));
	}
	
	@Override
	public void writeData(CompoundTag nbt)
	{
		super.writeData(nbt);
		nbt.putInt("progress", progress);
	}
	
	@Override
	public void readData(CompoundTag nbt)
	{
		super.readData(nbt);
		progress = nbt.getInt("progress");
	}

	@Override
	public boolean canPlaceItem(int var1, ItemStack var2) 
	{
		return true;
	}

	@Override
	public int[] getSlotsForFace(Direction var1)
	{
		return new int[]{0,1,2,3,4,5};
	}

	@Override
	public boolean canPlaceItemThroughFace(int slot, ItemStack item, Direction side)
	{
		if(!storage.canInsert(side, EnumLogisticType.ITEMS))
		{
			return false;
		}
		
		if(slot>=0 && slot <= 2)
		{
			return canPlaceItem(slot, item);
		}
			
		return false;
	}

	@Override
	public boolean canTakeItemThroughFace(int slot, ItemStack it, Direction side)
	{
		if(!storage.canExtract(side, EnumLogisticType.ITEMS))
		{
			return false;
		}
		
		if(slot>=3 && slot <= 5)
		{
			return true;
		}
		return false;
	}
	
	@Override
	public int getProperty(int id)
	{
		switch (id)
		{
		case 0:
			return energy.get();
		case 1:
			return progress;
		case 2:
			return support.get();
		case 3:
			return TileEntityScannerBlock.booleanToInt(uses);
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			setEnergy(value);
			break;
		case 1:
			progress = value;
			break;
		case 2:
			support.set(value);
			break;
		case 3:
			uses = TileEntityScannerBlock.intToBool(uses, value);
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 4;
	}

	@Override
	public void clearContent() { }
	
	
	
	@Override
	public int getDefaultPowerUsage() 
	{	
		return super.getDefaultPowerUsage() + (progress>0 ? 10 : 0);
	}
	
	public boolean isBurning()
	{
		return progress > 0;
	}
	
	public float getProgress()
	{
		return progress / 11F;
	}
	
	
	@Override
	public EnumEnergyMode getEnergyType() 
	{
		return EnumEnergyMode.USE;
	}	

	@Override
	protected int getInventorySize()
	{
		return 6;
	}

	@Override
	public void setOwner(Player pl)
	{
		owner = pl.getGameProfile().getId();
	}

	@Override
	public boolean isOwner(Player pl)
	{
		if(owner==null)
			return true;
		return isOwner(pl.getGameProfile().getId());
	}
	
	@Override
	public boolean isOwner(UUID pl)
	{
		if(owner==null)
			return true;
		return this.owner.equals(pl);
	}
}
