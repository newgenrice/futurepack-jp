package futurepack.common.block.modification.machines;

import java.util.Comparator;
import java.util.TreeMap;
import java.util.function.Function;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.IItemSupport;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.modification.EnumChipType;
import futurepack.common.recipes.SingletonInventory;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.helper.HelperOreDict;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.WorldlyContainer;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.item.crafting.SmeltingRecipe;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityNeonFurnace extends TileEntityMachineSupport implements WorldlyContainer, ITilePropertyStorage
{
	public static final int FIELD_PROGRESS = 1;
	
	/*
	 * 0 = in
	 * 1 = out
	 * 3 = support slot
	 */
	private int progress =0;

	public TileEntityNeonFurnace(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.NEON_FURNACE, 1024, EnumEnergyMode.PRODUCE, pos, state);
	}
	
	@Override
	public void configureLogisticStorage(LogisticStorage storage) 
	{
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ITEMS);
		storage.setModeForFace(Direction.DOWN, EnumLogisticIO.OUT, EnumLogisticType.ITEMS);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ENERGIE);
		storage.setDefaut(EnumLogisticIO.OUT, EnumLogisticType.SUPPORT);
		
		storage.removeState(EnumLogisticIO.OUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.SUPPORT);
		storage.removeState(EnumLogisticIO.IN, EnumLogisticType.SUPPORT);
	}
	
	@Override
	protected EnumLogisticType[] getLogisticTypes()
	{
		return new EnumLogisticType[]{EnumLogisticType.ENERGIE, EnumLogisticType.ITEMS, EnumLogisticType.SUPPORT};
	}
	
	private SmeltingRecipe recipe = null;
	
	@Override
	public void updateTile(int ticks) 
	{
		if(!level.isClientSide)
		{
			boolean done = false;
			//burning = false;
			if(!items.get(0).isEmpty())
			{
				if(recipe == null || !recipe.matches(this, level))
				{
					recipe = level.getRecipeManager().getRecipeFor(RecipeType.SMELTING, this, level).orElse(null);
				}
					
				if(recipe!=null && recipe.matches(this, level))
				{		
					ItemStack it;
					it = recipe.assemble(getInventory()).copy();
					it = HelperOreDict.FuturepackConveter.getChangedItemSizeSensitiv(it);
					
					if(!getBlockState().getValue(BlockNeonFurnace.LIT))
					{
						level.setBlock(worldPosition, getBlockState().setValue(BlockNeonFurnace.LIT, true), 3);
						setChanged();
					}
					
					do
					{
						if(items.get(1).isEmpty() || (it.sameItem(items.get(1)) && items.get(1).getCount()<64))
						{				
							if(progress>=11)
							{
								progress -= 11;
								if(support.get()<this.support.getMax())
									support.add(1);
									
								if(items.get(1).isEmpty())
								{
									items.set(1, it);
									items.get(0).shrink(1);
								}
								else
								{
									items.get(1).grow(it.getCount());
									items.get(0).shrink(1);
								}
								
							}
							progress+= ticks;
							done = true;
						}
					}
					while(progress >= 11);
				}
				else
				{
					recipe = null;
					if(getBlockState().getValue(BlockNeonFurnace.LIT))
					{
						level.setBlock(worldPosition, getBlockState().setValue(BlockNeonFurnace.LIT, false), 3);
						setChanged();
					}
				}
			}
			if(!items.get(2).isEmpty() && support.get()>0)
			{
				if(items.get(2).getItem() instanceof IItemSupport)
				{
					IItemSupport support = (IItemSupport) items.get(2).getItem();
					int space = support.getMaxSupport(items.get(2))-support.getSupport(items.get(2));
					if(space > 0)
					{
						support.addSupport(items.get(2), this.support.use(Math.min(space, ticks)));
					}
				}
			}
			
			if(!done)
			{
				progress=0;
			}
			
			if(support.get()>0 && this.getChipPower(EnumChipType.SUPPORT )>0)
			{
				HelperEnergyTransfer.sendSupportPoints(this);
			}
		}
	}
	

	
	@Override
	public void writeData(CompoundTag nbt)
	{
		super.writeData(nbt);
		nbt.putInt("progress", progress);
	}
	
	@Override
	public void readData(CompoundTag nbt)
	{
		super.readData(nbt);
		progress = nbt.getInt("progress");
	}
	
	@Override
	public boolean isWorking() 
	{
		return progress>0;
	}
	
	@Override
	public EnumEnergyMode getEnergyType() 
	{
		return EnumEnergyMode.USE;
	}

	private TreeMap<Item, Boolean> smeltable = new TreeMap<Item, Boolean>(new Comparator<Item>()
	{
		@Override
		public int compare(Item o1, Item o2) 
		{
			return Item.getId(o1) - Item.getId(o2);
		}
	});
	
	@Override
	public boolean canPlaceItem(int slot, ItemStack it) 
	{
		if(slot == 1)//output
			return false;
		
		if(slot==0)
		{
			return smeltable.computeIfAbsent(it.getItem(), new Function<Item, Boolean>()
			{
				@Override
				public Boolean apply(Item t) 
				{
					ItemStack result = level.getRecipeManager().getRecipeFor(RecipeType.SMELTING, new SingletonInventory(it), level).map(r -> r.getResultItem()).orElse(ItemStack.EMPTY);
					return !result.isEmpty();
				}
			});
		}
		return (slot == 2 && it.getItem() instanceof IItemSupport);
	}

	@Override
	public int[] getSlotsForFace(Direction var1)
	{
		return new int[]{0,1};
	}

	@Override
	public boolean canTakeItemThroughFace(int slot, ItemStack var2, Direction side)
	{		
		return slot==1 && super.canTakeItemThroughFace(slot, var2, side);
	}
	
	@Override
	public int getProperty(int id)
	{
		switch (id)
		{
		case 0:
			return energy.get();
		case 1:
			return progress;
		case 2:
			return support.get();
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			setEnergy(value);
			break;
		case 1:
			progress = value;
			break;
		case 2:
			support.set(value);
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 3;
	}

	@Override
	public void clearContent() { }
	
	
	@Override
	public int getDefaultPowerUsage() 
	{	
		return super.getDefaultPowerUsage() + (progress>0 ? 10 : 0);
	}

	public boolean isBurning()
	{
		return progress > 0;
	}

	@Override
	protected int getInventorySize()
	{
		return 3;
	}
}
