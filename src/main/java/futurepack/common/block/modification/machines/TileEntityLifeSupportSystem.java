package futurepack.common.block.modification.machines;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.IChunkAtmosphere;
import futurepack.api.interfaces.IFluidTankInfo.FluidTankInfo;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.logistic.LogisticFluidWrapper;
import futurepack.common.block.logistic.MultitankFluidHandler;
import futurepack.common.fluids.FPFluids;
import futurepack.common.modification.EnumChipType;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.helper.HelperFluid;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraft.world.level.material.Fluids;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler.FluidAction;
import net.minecraftforge.fluids.capability.IFluidHandlerItem;
import net.minecraftforge.fluids.capability.templates.FluidTank;


//TODO: rename this to electrolyseur and add a recipe system so it can be used to break 1 fluid into 2 different ones
// A -> B + C
public class TileEntityLifeSupportSystem  extends TileEntityMachineBase implements ITilePropertyStorage
{
	
	private FluidTank waterTank = new FluidTank(HelperFluid.MAX_OXYGEN_PER_BLOCK, fluid -> fluid!=null && (fluid.getFluid() == Fluids.WATER));
	private FluidTank oxygenTank = new FluidTank(HelperFluid.MAX_OXYGEN_PER_BLOCK, fluid -> fluid!=null && (fluid.getFluid() == FPFluids.oxygenGasStill));
	private FluidTank hydrogenTank = new FluidTank(2*HelperFluid.MAX_OXYGEN_PER_BLOCK, fluid -> fluid!=null && (fluid.getFluid() == FPFluids.hydrogenGasStill));
	
	private MultitankFluidHandler multiTankWrapper;
	
	private int progress = 0;
	
	private LazyOptional<IFluidHandler>[] fluidTankOpt;
	
	private LazyOptional<IChunkAtmosphere> opt;
	
	public TileEntityLifeSupportSystem(BlockEntityType<? extends TileEntityLifeSupportSystem> type, BlockPos pos, BlockState state) 
	{
		super(type, pos, state);
		multiTankWrapper = new MultitankFluidHandler(waterTank, oxygenTank, hydrogenTank);
		multiTankWrapper.setDrainEnabled(0, false);
		multiTankWrapper.setDrainEnabled(1, true);
		multiTankWrapper.setDrainEnabled(2, true);
		multiTankWrapper.setFillEnabled(0, true);
		multiTankWrapper.setFillEnabled(1, false);
		multiTankWrapper.setFillEnabled(2, false);
	}
	
	public TileEntityLifeSupportSystem(BlockPos pos, BlockState state) 
	{
		this(FPTileEntitys.LIFE_SUPPORT_SYSTEM, pos, state);
	}
	
	@Override
	protected int getInventorySize() 
	{
		return 2;
	}
	
	@Override
	public void configureLogisticStorage(LogisticStorage storage) 
	{
		fluidTankOpt = new LazyOptional[6];
		
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ITEMS);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ENERGIE);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.FLUIDS);
		
		storage.removeState(EnumLogisticIO.OUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.ENERGIE);
		
		storage.setModeForFace(Direction.DOWN, EnumLogisticIO.OUT, EnumLogisticType.ITEMS);
		storage.setModeForFace(Direction.UP, EnumLogisticIO.OUT, EnumLogisticType.FLUIDS);
	}
	
	@Override
	protected EnumLogisticType[] getLogisticTypes()
	{
		return new EnumLogisticType[]{EnumLogisticType.ENERGIE, EnumLogisticType.ITEMS, EnumLogisticType.FLUIDS};
	}
	
	@Override
	protected void onLogisticChange(Direction face, EnumLogisticType type)
	{
		if(type == EnumLogisticType.FLUIDS)
		{
			if(fluidTankOpt[face.get3DDataValue()]!=null)
			{
				fluidTankOpt[face.get3DDataValue()].invalidate();
				fluidTankOpt[face.get3DDataValue()] = null;
			}
		}
	}
	
	
	@Override
	public EnumEnergyMode getEnergyType() 
	{
		return EnumEnergyMode.USE;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY)
		{
			if(fluidTankOpt[facing.get3DDataValue()]!=null)
			{
				return (LazyOptional<T>) fluidTankOpt[facing.get3DDataValue()];
			}
			else
			{
				if(getLogisticStorage().getModeForFace(facing, EnumLogisticType.FLUIDS) == EnumLogisticIO.NONE)
				{
					return LazyOptional.empty();
				}
				else
				{
					fluidTankOpt[facing.get3DDataValue()] = LazyOptional.of(() -> new LogisticFluidWrapper(getLogisticStorage().getInterfaceforSide(facing), multiTankWrapper));
					fluidTankOpt[facing.get3DDataValue()].addListener(p -> fluidTankOpt[facing.get3DDataValue()]=null);
					return (LazyOptional<T>) fluidTankOpt[facing.get3DDataValue()];
				}
			}
		}
		
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(fluidTankOpt);
		super.setRemoved();
	}
	
	
	@Override
	public void load(CompoundTag nbt) 
	{
		super.load(nbt);
		waterTank.readFromNBT(nbt.getCompound("h2o"));
		oxygenTank.readFromNBT(nbt.getCompound("o2"));
		hydrogenTank.readFromNBT(nbt.getCompound("h2"));
		progress = nbt.getInt("progress");
	}
	
	@Override
	public CompoundTag save(CompoundTag nbt) 
	{
		nbt.put("h2o", waterTank.writeToNBT(new CompoundTag()));
		nbt.put("o2", oxygenTank.writeToNBT(new CompoundTag()));
		nbt.put("h2", hydrogenTank.writeToNBT(new CompoundTag()));
		nbt.putInt("progress", progress);
		return super.save(nbt);
	}
	
	
	
	@Override
	public void updateTile(int tickCount) 
	{
		//2 water -> 1 oxygen + 2 hydrogen
		
		if(!level.isClientSide)
		{
			
			int scale = 20 * (int) (1 + getChipPower(EnumChipType.INDUSTRIE)* 5);
			
			if(waterTank.getFluidAmount() >= 2*scale && oxygenTank.getCapacity()-oxygenTank.getFluidAmount() >= 1*scale && hydrogenTank.getCapacity()-hydrogenTank.getFluidAmount() >= 2*scale)
			{
				progress+=tickCount;
				while(progress>=13)
				{
					if(waterTank.getFluidAmount() >= 2*scale && oxygenTank.getCapacity()-oxygenTank.getFluidAmount() >= 1*scale && hydrogenTank.getCapacity()-hydrogenTank.getFluidAmount() >= 2*scale)
					{
						progress-= 13;
						waterTank.drain(2*scale,FluidAction.EXECUTE);
						oxygenTank.fill(new FluidStack(FPFluids.oxygenGasStill, 1*scale), FluidAction.EXECUTE);
						hydrogenTank.fill(new FluidStack(FPFluids.hydrogenGasStill, 2*scale), FluidAction.EXECUTE);
						this.setChanged();
					}
				}
			}
			
			if(opt == null)
			{
				LevelChunk c = level.getChunkAt(worldPosition);
				multiTankWrapper.setDrainEnabled(1, true);
				opt = c.getCapability(AtmosphereManager.cap_ATMOSPHERE, null);
				opt.addListener( p -> opt = null);
			}
			if(opt!=null && level.isEmptyBlock(worldPosition.above()))
			{
				multiTankWrapper.setDrainEnabled(1, false);
				opt.ifPresent(atm -> {
					
					int space = atm.getMaxAir() - atm.getAirAt(worldPosition.getX()&15, worldPosition.getY(), worldPosition.getZ()&15);
					space /= HelperFluid.OXYGEN_PER_MILIBUCKET;
					int there = oxygenTank.getFluidAmount() * HelperFluid.OXYGEN_PER_MILIBUCKET;
					int transfer = Math.min(space, there);
					transfer /= HelperFluid.OXYGEN_PER_MILIBUCKET;
					if(transfer > 0)
					{
						oxygenTank.drain( atm.addAir(worldPosition.getX()&15, worldPosition.getY()+1, worldPosition.getZ()&15, transfer * HelperFluid.OXYGEN_PER_MILIBUCKET) / HelperFluid.OXYGEN_PER_MILIBUCKET, FluidAction.EXECUTE);
						this.setChanged();
					}
					
				});
			}
			
			HelperFluid.putFluidFromTankInBucket(this::getItem, this::setItem, 0, 1, hydrogenTank, hydrogenTank.getFluidAmount());
			
			
			int milibuckets = (int) (500 * getChipPower(EnumChipType.TRANSPORT) * tickCount);
			if(milibuckets>0)
			{
				HelperFluid.doFluidExtractAndImport(this, milibuckets);			
			}
		}
	}

	@Override
	public boolean isWorking() 
	{
		return progress > 0;
	}
	
	
	/**
	 *  1NE = 1,20967119 MJ , {@see TileEntityBrennstoffGenerator} for details.
	 *  //1g water needs 15.87kJ to break = 1ml
	 *  // 1 m^3 = 1000milibuckets = 1000 liter
	 *  // 1mb water = 1000ml = 1000g => 15.87MJ to break -> 13,11
	 *  
	 *  -> but sicne its per tick only 1 and we need 13 ticks
	 *  
	 *  but 1mb = 36 oxygen, so 36 for one player of life
	 */
	@Override
	public int getDefaultPowerUsage() 
	{
		return super.getDefaultPowerUsage() + 20;
	}

	@Override
	public int getProperty(int id) 
	{
		switch(id)
		{
		case 0:
			return energy.get();
		case 1:
			return (int)(waterTank.getFluidAmount()/((double)HelperFluid.MAX_OXYGEN_PER_BLOCK)*32767.0);
		case 2:
			return (int)(oxygenTank.getFluidAmount()/((double)HelperFluid.MAX_OXYGEN_PER_BLOCK)*32767.0);
		case 3:
			return (int)(hydrogenTank.getFluidAmount()/(2.0*HelperFluid.MAX_OXYGEN_PER_BLOCK)*32767.0);
		case 4:
			return progress;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value) 
	{
		switch (id)
		{
		case 0:
			setEnergy(value);
			break;
		case 1:
			waterTank.setFluid(new FluidStack(Fluids.WATER, (int) (value/32767.0 * HelperFluid.MAX_OXYGEN_PER_BLOCK)));
			break;
		case 2:
			oxygenTank.setFluid(new FluidStack(FPFluids.oxygenGasStill, (int) (value/32767.0 * HelperFluid.MAX_OXYGEN_PER_BLOCK)));
			break;
		case 3:
			hydrogenTank.setFluid(new FluidStack(FPFluids.hydrogenGasStill, (int) (value/32767.0 * 2*HelperFluid.MAX_OXYGEN_PER_BLOCK)));
			break;
		case 4:
			progress = value;
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 5;
	}

	public FluidTankInfo getFluid(int i) 
	{
		return new FluidTankInfo(this.multiTankWrapper, i);
	}

	public float getProgress() 
	{
		return progress / 13F;
	}

	@Override
	public boolean canPlaceItem(int slot, ItemStack stack) 
	{
		if(slot==0)
		{
			LazyOptional<IFluidHandlerItem> fh = FluidUtil.getFluidHandler(stack);	
			FluidStack fs = fh.map(p->p.getFluidInTank(0)).orElse(null);
					
			if(fs != null && fs.equals(FluidStack.EMPTY))
				return true;		
		}
		else if(slot == 1)
		{
			return false;
		}
		return super.canPlaceItem(slot, stack);
	}

}
