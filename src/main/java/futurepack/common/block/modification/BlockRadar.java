package futurepack.common.block.modification;

import futurepack.api.interfaces.IBlockBothSidesTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockRadar extends BlockRotateableTile implements IBlockBothSidesTickingEntity<TileEntityRadar>
{
	protected static final VoxelShape BOTTOM = Shapes.join(
			Shapes.join(
				Block.box(0, 0, 0, 16, 6, 16),
				Block.box(4, 6, 4, 12, 7, 12),
				BooleanOp.OR),
			Block.box(5, 7, 5, 11, 10, 11),
			BooleanOp.OR);

	protected static final VoxelShape TOP = Shapes.join(
			Shapes.join(
					Block.box(0, 10, 0, 16, 16, 16),
					Block.box(4, 9, 4, 12, 10, 12),
					BooleanOp.OR),
			Block.box(5, 6, 5, 11, 9, 11),
			BooleanOp.OR);

	protected static final VoxelShape NORTH = Shapes.join(
			Shapes.join(
					Block.box(0,0,10, 16, 16,16),
					Block.box(4, 4, 9, 12, 12, 10),
					BooleanOp.OR),
			Block.box(5, 5, 6, 11, 11, 9),
			BooleanOp.OR);

	protected static final VoxelShape EAST = Shapes.join(
			Shapes.join(
					Block.box(0,0,0, 6, 16,16),
					Block.box(6, 4, 4, 7, 12, 12),
					BooleanOp.OR),
			Block.box(7, 5, 5, 10, 11, 11),
			BooleanOp.OR);

	protected static final VoxelShape SOUTH = Shapes.join(
			Shapes.join(
					Block.box(0,0,0, 16, 16,6),
					Block.box(4, 4, 6, 12, 12, 7),
					BooleanOp.OR),
			Block.box(5, 5, 7, 11, 11, 10),
			BooleanOp.OR);

	protected static final VoxelShape WEST = Shapes.join(
			Shapes.join(
					Block.box(10,0,0, 16, 16,16),
					Block.box(9, 4, 4, 10, 12, 12),
					BooleanOp.OR),
			Block.box(6, 5, 5, 9, 11, 11),
			BooleanOp.OR);

	public BlockRadar(Properties props)
	{
		super(props);
	}

	@Override
	public VoxelShape getShape(BlockState blockState, BlockGetter blockReader, BlockPos pos, CollisionContext selectionContext) {
		switch (blockState.getValue(BlockRotateableTile.FACING)) {
			case UP: return BOTTOM;
			case DOWN: return TOP;
			case NORTH: return NORTH;
			case EAST: return EAST;
			case SOUTH: return SOUTH;
			case WEST: return WEST;
			default: return BOTTOM;
		}
	}

	@Override
	public RenderShape getRenderShape(BlockState p_149645_1_) {
		return RenderShape.ENTITYBLOCK_ANIMATED;
	}

	@Override
	public BlockEntityType<TileEntityRadar> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.RADAR;
	}
	

}
