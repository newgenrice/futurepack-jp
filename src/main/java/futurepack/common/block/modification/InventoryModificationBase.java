package futurepack.common.block.modification;

import java.util.Arrays;
import java.util.stream.Stream;

import com.google.common.base.Predicates;

import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.item.ItemChip;
import futurepack.common.item.ItemCore;
import futurepack.common.item.ItemRam;
import futurepack.common.modification.EnumChipType;
import futurepack.common.modification.IModificationPart;
import futurepack.common.modification.IModificationPart.EnumCorePowerType;
import futurepack.common.modification.JoinedParts;
import futurepack.common.modification.PartsManager;
import futurepack.common.modification.thermodynamic.TemperatureWrapper;
import net.minecraft.core.Direction;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.world.WorldlyContainer;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;

public abstract class InventoryModificationBase implements WorldlyContainer, ITilePropertyStorage
{
	
	public NonNullList<ItemStack> chipset;
	public NonNullList<ItemStack> ram;
	public NonNullList<ItemStack> core;
	
	public boolean guiOpen = false;
	
//	private Integer needCoreCash, corePowerCash, canWorkCash;
//	private Float pureRamCash;
//	private Map<EnumChipType, Float> chipPowerCash = new EnumMap<EnumChipType, Float>(EnumChipType.class);
	
	private JoinedParts joined_parts;
	
	private float lastTemp = -1F;
	
	/**
	 * 
	 */
	public TileEntityModificationBase sync;

	/**
	 * @param tileEntityModificationBase
	 */
	public InventoryModificationBase()
	{
		clearContent();
	}	 
	
	public CompoundTag write(CompoundTag nbt)
	{		
		ListTag list;
		
		list = new ListTag();
		for(int i=0;i<chipset.size();i++)
		{
			if(!chipset.get(i).isEmpty())
			{
				CompoundTag tag = new CompoundTag();
				tag.putInt("Slot", i);
				chipset.get(i).save(tag);
				list.add(tag);
			}
		}
		nbt.put("chipset", list);
		
		list = new ListTag();
		for(int i=0;i<ram.size();i++)
		{
			if(!ram.get(i).isEmpty())
			{
				CompoundTag tag = new CompoundTag();
				tag.putInt("Slot", i);
				ram.get(i).save(tag);
				list.add(tag);
			}
		}
		nbt.put("ram", list);
		
		list = new ListTag();
		for(int i=0;i<core.size();i++)
		{
			if(!core.get(i).isEmpty())
			{
				CompoundTag tag = new CompoundTag();
				tag.putInt("Slot", i);
				core.get(i).save(tag);
				list.add(tag);
				
			}
		}
		nbt.put("core", list);
		return nbt;
	}

	public void read(CompoundTag nbt)
	{
		clearContent();		
		
		ListTag l;
		l = nbt.getList("chipset", 10);
		for(int i=0;i<l.size();i++)
		{
			CompoundTag tag = l.getCompound(i);
			chipset.set(tag.getInt("Slot"), ItemStack.of(tag));
		}
		
		l = nbt.getList("ram", 10);
		for(int i=0;i<l.size();i++)
		{
			CompoundTag tag = l.getCompound(i);
			ram.set(tag.getInt("Slot"), ItemStack.of(tag));
		}
		
		if(nbt.contains("core"))
		{
			if(nbt.getTagType("core")==9)
			{
				l = nbt.getList("core", 10);
				for(int i=0;i<l.size();i++)
				{
					CompoundTag tag = l.getCompound(i);
					core.set(tag.getInt("Slot"), ItemStack.of(tag));
				}
			}
			else				//Old system were core is an NBT of the itemStack
			{
				CompoundTag tag = nbt.getCompound("core");
				core.set(0, ItemStack.of(tag));
			}
			
		}		
	}
	
	private JoinedParts getJoinParts()
	{
		if(joined_parts==null)
		{
			Stream<IModificationPart> chips = chipset.stream().filter(Predicates.not(ItemStack::isEmpty)).map(PartsManager::getPartFromItem).filter(Predicates.notNull());
			Stream<IModificationPart> rams = ram.stream().filter(Predicates.not(ItemStack::isEmpty)).map(PartsManager::getPartFromItem).filter(Predicates.notNull());
			Stream<IModificationPart> cores = core.stream().filter(Predicates.not(ItemStack::isEmpty)).map(PartsManager::getPartFromItem).filter(Predicates.notNull());
			
			IModificationPart[] parts = Arrays.asList(chips, rams, cores).stream().flatMap(f -> f).map(p -> heatWrappper(p)).toArray(IModificationPart[]::new);
			joined_parts = PartsManager.joinUpdateable(parts);
			return joined_parts;
		}
		else
		{
			return joined_parts;
		}
	}
	
	private TemperatureWrapper heatWrappper(final IModificationPart part)
	{
		return new TemperatureWrapper(part, sync.getTemperatureControloer());
	}
	
	private void checkForTempUpdate()
	{
		float dt = Math.abs(sync.getHeat() - lastTemp);
		if(dt >= 10F)
		{
			if(joined_parts != null)
			{
				joined_parts.reseatCache();
			}
			else
			{
				getJoinParts();
			}
			lastTemp = sync.getHeat();
		}
	}
	
	public float getPureRam()
	{
//		if(pureRamCash!=null)
//			return pureRamCash;
//		
//		pureRamCash = 0F;
//		for(ItemStack it : this.ram)
//		{
//			pureRamCash += tempCon.getHotRam(it);
//		}
//		return pureRamCash;
		
		
		return getJoinParts().getRamSpeed();
	}
	
	public int getNeededCore()
	{
		return getJoinParts().getCorePower(EnumCorePowerType.NEEDED);
	}
	
	public int getCorePower()
	{
		return getJoinParts().getCorePower(EnumCorePowerType.PROVIDED);
	}
	
	public boolean canWork()
	{
		if(getCorePower()<=0)
		{
			return false;
		}
		
		boolean chip = getChipPower(EnumChipType.LOGIC) > 0;
		if(!chip)
			return false;
		
		return getPureRam() > 0;
	}
	
	public float getChipPower(EnumChipType type)
	{
//		float dt = sync.getHeat() - lastTemp;
//		if(dt > 10 || dt < -10)
//		{	
//			chipPowerCash.clear();
//			lastTemp = sync.getHeat();
//		}	
//		else
//		{
//			Float f = chipPowerCash.get(i);
//			if(f!=null)
//				return f;
//		}
//		
//		
//		float f = 0;
//		for(ItemStack it : chipset)
//		{
//			if(!it.isEmpty() && it.getItemDamage() == i)
//			{
//				f += tempCon.getHotChip(it);//ItemChip.getChipPower(it);
//			}
//		}
//		chipPowerCash.put(i, f);
//		return f;
		
		checkForTempUpdate();
		return getJoinParts().getChipPower(type);
	}
	
	@Override
	public int getContainerSize() 
	{
		return chipset.size() + ram.size() + core.size();
	}
	
	@Override
	public ItemStack getItem(int var1) 
	{
		ItemStack it = var1 < chipset.size() ? chipset.get(var1) : (var1-chipset.size() < core.size() ? core.get(var1-chipset.size()) :ram.get(var1-chipset.size()-core.size()));
		return it;
	}
	
	@Override
	public ItemStack removeItem(int slot, int size) 
	{
		if (getItem(slot) != null)
        {
            ItemStack itemstack;

            if (getItem(slot).getCount() <= size)
            {
                itemstack = getItem(slot);
                setItem(slot, ItemStack.EMPTY);
                this.setChanged();
                return itemstack;
            }
            else
            {
                itemstack = getItem(slot).split(size);

                if (getItem(slot).getCount() == 0)
                {
                	setItem(slot, ItemStack.EMPTY);
                }

                this.setChanged();
                return itemstack;
            }
        }
        else
        {
            return ItemStack.EMPTY;
        }
	}
	
	@Override
	public ItemStack removeItemNoUpdate(int var1) 
	{
		ItemStack it =  getItem(var1);
		setItem(var1, ItemStack.EMPTY);
		resetCash();
		return it;
	}
	
	@Override
	public void setItem(int var1, ItemStack var2) 
	{
		if(var2==null)
		{
			var2 = ItemStack.EMPTY;
		}
		
		if(var1 < chipset.size())
		{
			chipset.set(var1, var2);
		}
		else if(var1-chipset.size() < core.size())
		{
			core.set(var1-chipset.size(), var2);
		}	
		else
		{
			ram.set(var1-chipset.size()-core.size(), var2);
		}
		resetCash();
	}
	
	@Override
	public int getMaxStackSize() 
	{
		return 1;
	}
	
	@Override
	public boolean stillValid(Player var1)
	{
		return true;
	}
	
	@Override
	public void startOpen(Player pl)
	{
		guiOpen = true;
	}
	
	@Override
	public void stopOpen(Player pl)
	{
		guiOpen = false;
	}
	
	@Override
	public boolean canPlaceItem(int var1, ItemStack var2) 
	{		
		if(var1 < chipset.size())
		{
			return var2.getItem() instanceof ItemChip;
		}
		else if(var1-chipset.size() < core.size())
		{
			return var2.getItem() instanceof ItemCore;
		}
		else if(var1-chipset.size()-core.size() < ram.size())
		{
			return var2.getItem() instanceof ItemRam;
		}
		return false;
	}

	
	@Override
	public int[] getSlotsForFace(Direction var1)
	{
		return new int[]{};
	}

	@Override
	public boolean canPlaceItemThroughFace(int var1, ItemStack var2, Direction var3)
	{
		return false;
	}

	@Override
	public boolean canTakeItemThroughFace(int var1, ItemStack var2, Direction var3)
	{
		return false;
	}

	@Override
	public void setChanged() 
	{
		if(sync instanceof TileEntityModificationBase)
		{
			if(sync.getLevel().isClientSide)
				return;
			
			sync.edit = true;
			sync.setChanged();
		}
		resetCash();
	}


	@Override
	public int getProperty(int id)
	{
		switch (id)
		{
		case 0:
			return sync.energy.get();
		case 1:
			return (int) (sync.heat * 10);
		case 2:
			return sync.edit ? 1 : 0;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			sync.setEnergy(value);
			break;
		case 1:
			sync.heat = value/10F;
			break;
		case 2:
			sync.edit = value == 1;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 3;
	}
	
	@Override
	public void clearContent()
	{
		chipset = NonNullList.withSize(getChipSlots(), ItemStack.EMPTY);
		ram = NonNullList.withSize(getRamSlots(), ItemStack.EMPTY);
		core = NonNullList.withSize(getCoreSlots(), ItemStack.EMPTY);
		resetCash();
	}




	@Override
	public boolean isEmpty()
	{
		return false;
	}

	
	public abstract int getRamSlots();
	
	public abstract int getCoreSlots();
	
	public abstract int getChipSlots();
	
	public void resetCash()
	{
//		pureRamCash = null;
//		needCoreCash = null;
//		corePowerCash = null;
//		canWorkCash = null;
//		chipPowerCash.clear();
		joined_parts = null; 
		lastTemp = -1F;
	}
}
