package futurepack.common.block.modification;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import javax.annotation.Nullable;

import futurepack.api.Constants;
import futurepack.common.FPTileEntitys;
import futurepack.common.entity.throwable.EntityRocket;
import futurepack.common.item.misc.ItemRocket;
import futurepack.common.modification.EnumChipType;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemStackHandler;

public class TileEntityRocketLauncher extends TileEntityLaserBase<LivingEntity>
{
	
	ArrayList<WeakReference<EntityRocket>> list;

	private final RocketInventory inv = new RocketInventory();
	
	public TileEntityRocketLauncher(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.ROCKET_LAUNCHER, LivingEntity.class, pos , state);
		list = new ArrayList<WeakReference<EntityRocket>>();
	}

	@Override
	public CompoundTag save(CompoundTag nbt)
	{
		nbt.put("items", inv.serializeNBT());
		return super.save(nbt);
	}
	
	@Override
	public void load(CompoundTag nbt)
	{
		inv.deserializeNBT(nbt.getCompound("items"));
		super.load(nbt);
	}
	
	private LazyOptional<IItemHandler> opt;
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(opt!=null)
				return (LazyOptional<T>) opt;
			else
			{
				opt = LazyOptional.of(() -> inv);
				opt.addListener(p -> opt = null);
				return (LazyOptional<T>) opt;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(opt);
		super.setRemoved();
	}
	
	@Override
	public boolean isEntityValid(LivingEntity entity)
	{	
		if(!matchConfig(entity))
			return false;
		
		clearDead();
		if(list.size()>10)
			return false;

		if(getConfiguration("kill.not"))//In this context "only glowing"
		{
			return entity.hasEffect(MobEffects.GLOWING);
		}
		
		return entity.isAlive();
	}

	private void clearDead()
	{
		list.removeIf(ref -> ref.get()==null || ref.get().isAlive()==false);
	}
	
	@Override
	public void progressEntity(LivingEntity entity)
	{
		if(level.isClientSide)
			return;
		
		EntityRocket rocket = createRocket(entity);
		if(rocket == null)
			return;
		
		rocket.doPlayerDamage = this.getChipPower(EnumChipType.ULTIMATE)>0;
		if(this.getChipPower(EnumChipType.TRANSPORT)>0)
		{
			rocket.doTransport = Vec3.atLowerCornerOf(worldPosition);
		}
		rocket.setPos(blockPos.x, blockPos.y, blockPos.z);
		Vec3 mov = blockPos.vectorTo(entity.position()).normalize();
		mov = mov.add((level.random.nextDouble()-0.5) * 0.7, (level.random.nextDouble()-0.5) * 0.7, (level.random.nextDouble()-0.5) * 0.7);
		rocket.setDeltaMovement(mov);
		list.add(new WeakReference<EntityRocket>(rocket));
		level.addFreshEntity(rocket);
	}
	
	@Nullable
	public ItemStack getFirstRocket()
	{
		for(int i=0;i<inv.getSlots();i++)
		{
			ItemStack st = inv.getStackInSlot(i);
			if(!st.isEmpty())
			{
				return st;
			}
		}
		return ItemStack.EMPTY;
	}
	
	public EntityRocket createRocket(LivingEntity target)
	{
		ItemStack st = getFirstRocket();
		if(!st.isEmpty())
		{
			ItemRocket rocket = (ItemRocket) st.getItem();
			EntityRocket ent = rocket.createRocket(level, target, this, st);
			return ent;
		}
		inventoryCooldown = 0x100 | 30;//longer cooldown
		return null;
	}
	
	private int inventoryCooldown = 0;
	@Override
	public boolean shouldWork()
	{
		if((inventoryCooldown & 0xFF) <= 0)
		{
			if(getFirstRocket().isEmpty())
			{
				inventoryCooldown = 0x100 | 20;
			}
			else
			{
				inventoryCooldown = 0xFF;
			}
		}
		else
			inventoryCooldown--;
		
		if((inventoryCooldown & 0x100) == 0x100)
			return false;
		
		return energy.get() > 0;
	}

	@Override
	public ResourceLocation getTexture()
	{
		return new ResourceLocation(Constants.MOD_ID, "textures/model/lancher.png");
	}

	@Override
	public int getLaserColor()
	{
		return 0;
	}

	@Override
	public ResourceLocation getLaser()
	{
		return null;
	}

	@Override
	public float getRange()
	{
		return super.getRange() * 1.5F;
	}
	
	public static boolean isRocketForLauncher(ItemStack is)
	{
		return is.getItem() instanceof ItemRocket;
	}
	
	public IItemHandlerModifiable getGui()
	{
		return new IItemHandlerModifiable()
		{	
			@Override
			public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
			{
				return inv.insertItem(slot, stack, simulate);
			}
			
			@Override
			public ItemStack getStackInSlot(int slot)
			{
				return inv.getStackInSlot(slot);
			}
			
			@Override
			public int getSlots()
			{
				return inv.getSlots();
			}
			
			@Override
			public int getSlotLimit(int slot)
			{
				return inv.getSlotLimit(slot);
			}
			
			@Override
			public ItemStack extractItem(int slot, int amount, boolean simulate)
			{
				return inv.extractItem(slot, amount, simulate, true);
			}
			
			@Override
			public void setStackInSlot(int slot, ItemStack stack)
			{
				inv.setStackInSlot(slot, stack);
			}

			@Override
			public boolean isItemValid(int slot, ItemStack stack) 
			{
				return inv.isItemValid(slot, stack);
			}
		};
	}
	
	public static class RocketInventory extends ItemStackHandler
	{
		public RocketInventory()
		{
			super(9);
		}
		
		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
		{
			if(isItemValid(slot, stack))
				return super.insertItem(slot, stack, simulate);
			else
				return stack;
		}
		
		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate)
		{
			return extractItem(slot, amount, simulate, false);
		}
		
		
		public ItemStack extractItem(int slot, int amount, boolean simulate, boolean gui)
		{
			if(gui)
				return super.extractItem(slot, amount, simulate);
			else
				return ItemStack.EMPTY;
		}
		
		@Override
		public boolean isItemValid(int slot, ItemStack stack)
		{
			return isRocketForLauncher(stack);
		}
	}
}
