package futurepack.common.block.modification;

import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityExternalCore extends TileEntityModificationBase
{
	private boolean working = false;

	private BlockEntityTicker ticker;
	
	public TileEntityExternalCore(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.EXTERNAL_CORE, pos, state);
	}

	@Override
	public EnumEnergyMode getEnergyType()
	{
		return EnumEnergyMode.USE;
	}

	@Override
	public void updateTile(int ticks)
	{
		working = false;
		
		BlockEntity tile = level.getBlockEntity(worldPosition.relative(getDirection()));
		if(tile!=null)
		{
			ticker = ((EntityBlock)tile.getBlockState().getBlock()).getTicker(getLevel(), tile.getBlockState(), tile.getType());
		}
		if(ticker != null)
		{
			if(tile.hasLevel())
			{
				for(int i=0;i<ticks;i++)
					ticker.tick(level, worldPosition.relative(getDirection()), tile.getBlockState(), tile);
				working = true;
			}
		}
	}

	@Override
	public boolean isWorking()
	{
		return working;
	}

	private Direction getDirection()
	{
		BlockState state = level.getBlockState(worldPosition);
		if(state.isAir())
		{
			return Direction.UP;
		}
		return state.getValue(BlockRotateableTile.FACING).getOpposite();
	}
}
