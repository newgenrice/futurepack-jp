package futurepack.common.block.inventory;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.FacingUtil;
import futurepack.api.LogisticStorage;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.depend.api.helper.HelperInventory;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.items.CapabilityItemHandler;

public class TileEntityLogisticChest extends TileEntityInventoryLogistics implements ITileServerTickable
{

	private int buffer;
	
	public TileEntityLogisticChest(BlockEntityType<? extends TileEntityLogisticChest> t, BlockPos pos, BlockState state) 
	{
		super(t, pos, state);
	}

	public TileEntityLogisticChest(BlockPos pos, BlockState state) 
	{
		this(FPTileEntitys.LOGISTIC_CHEST, pos, state);
	}
	
	@Override
	protected int getInventorySize()
	{
		return 4 * 9;
	}


	@Override
	public void configureLogisticStorage(LogisticStorage storage) 
	{
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ITEMS);
	}

	@Override
	public void tickServer(Level level, BlockPos pos, BlockState state) 
	{
		buffer++;
		if( (buffer + worldPosition.getX() + worldPosition.getY() + worldPosition.getZ()) % 6 == 0)
		{
			for(Direction face : FacingUtil.VALUES)
			{	
				this.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, face).ifPresent(inv -> {
					HelperInventory.tryExtractIntoInv(getLevel(), getBlockPos(), face, inv, 0F, false, 2);
				});
			}
			if((buffer + worldPosition.getX() + worldPosition.getY() + worldPosition.getZ()) % 24 == 0)
			{
				checkEmpty();
			}
		}
	}
	
	@Override
	public void stopOpen(Player pl) 
	{
		super.stopOpen(pl);
		checkEmpty();
	}
	
	protected void checkEmpty()
	{
		boolean notEmpty = items.stream().anyMatch(p -> !p.isEmpty());
		if(getBlockState().getValue(BlockLogisticChest.FILLED) != notEmpty)
		{
			level.setBlockAndUpdate(worldPosition, getBlockState().setValue(BlockLogisticChest.FILLED, notEmpty));
		}
	}
	
	@Override
	public String getGUITitle() {
		return "block.futurepack.logistic_chest";
	}
}
