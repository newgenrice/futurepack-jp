package futurepack.common.block.inventory;

import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPConfig;
import futurepack.common.FPSounds;
import futurepack.common.FPTileEntitys;
import futurepack.common.FuturepackTags;
import futurepack.common.item.ResourceItems;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.WorldlyContainer;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.FurnaceBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.ForgeHooks;

public class TileEntityPartPress extends TileEntityInventoryBase implements WorldlyContainer, ITilePropertyStorage, ITileServerTickable
{
//	ItemStack[] items = new ItemStack[5];
	
	public int pressure = 0;
	public int burn = 0;
	public int maxburn = 0;
	
	public TileEntityPartPress(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.PART_PRESS, pos, state);
	}
	
	@Override
	public void tickServer(Level level, BlockPos worldPosition, BlockState pState)
	{	
		if(burn==0 && FurnaceBlockEntity.isFuel(items.get(0)))
		{
			ItemStack its = ItemStack.EMPTY;
			if(!items.get(1).isEmpty())
			{
				its = matchItem(items.get(1).getItem());
			}
			if(!items.get(2).isEmpty() && its.isEmpty())
			{
				its = matchItem(items.get(2).getItem());
			}
			if(!its.isEmpty())
			{
				maxburn = ForgeHooks.getBurnTime(items.get(0), RecipeType.SMELTING);
				burn = maxburn;
				ItemStack it = items.get(0).getItem().getContainerItem(items.get(0));
				if(!it.isEmpty())
				{
					items.set(0, it.copy());
				}
				else
				{
					items.get(0).shrink(1);
				}
			}
		}
		if(burn == 0)
		{
			maxburn = 0;
			pressure = 0;
		}
		
		if(burn>0)
		{
			if(pressure==0)
			{
				level.playSound(null, worldPosition, FPSounds.PART_PRESS, SoundSource.BLOCKS, (float) (0.7 + level.random.nextFloat() * 0.3 * FPConfig.CLIENT.volume_partpress.get()), 0.8F + 0.2F*level.random.nextFloat());			
			}
			
			pressure++;
			burn--;
		}
		
		if(pressure>=100)
		{
			pressure = 0;
			if(!items.get(1).isEmpty())
			{
				ItemStack result = matchItem(items.get(1).getItem());
				if(!result.isEmpty())
				{
					if(items.get(3).isEmpty())
					{
						items.set(3, result);
						items.get(1).shrink(1);
					}
					else if(result.sameItem(items.get(3)) && items.get(3).getCount()<64)
					{
						items.get(3).grow(result.getCount());
						items.get(1).shrink(1);
					}
				}
			}
			if(!items.get(2).isEmpty())
			{
				ItemStack result = matchItem(items.get(2).getItem());
				if(!result.isEmpty())
				{
					if(items.get(4).isEmpty())
					{
						items.set(4, result);
						items.get(2).shrink(1);
					}
					else if(result.sameItem(items.get(4)) && items.get(4).getCount()<64)
					{
						items.get(4).grow(result.getCount());
						items.get(2).shrink(1);
					}
				}
			}
		}
	}
	
	private ItemStack matchItem(Item item)
	{
		if(item!=null)
		{	
			if(FuturepackTags.gemDiamond.contains(item))
			{
				return new ItemStack(ResourceItems.parts_diamond,4);
			}
			if(FuturepackTags.ingotIron.contains(item))
			{
				return new ItemStack(ResourceItems.parts_iron,4);
			}
			if(FuturepackTags.ingotNeon.contains(item))
			{
				return new ItemStack(ResourceItems.parts_neon,4);
			}
			if(FuturepackTags.ingotCopper.contains(item))
			{
				return new ItemStack(ResourceItems.parts_copper,4);
			}
			if(FuturepackTags.gemQuartz.contains(item))
			{
				return new ItemStack(ResourceItems.parts_quartz,4);
			}
			if(FuturepackTags.ingotGold.contains(item))
			{
				return new ItemStack(ResourceItems.parts_gold,4);
			}
		}
		return ItemStack.EMPTY;
	}
	
	@Override
	public CompoundTag save(CompoundTag nbt)
	{
		super.save(nbt);
		nbt.putInt("pressure", pressure);
		nbt.putInt("burn", burn);
		nbt.putInt("maxburn", maxburn);
		return nbt;
	}
	
	@Override
	public void load(CompoundTag nbt)
	{
		super.load(nbt);
		pressure = nbt.getInt("pressure");
		burn = nbt.getInt("burn");
		maxburn = nbt.getInt("maxburn");
	}

	@Override
	public int[] getSlotsForFace(Direction var1)
	{
		return new int[]{0,1,2,3,4};
	}

	@Override
	public boolean canPlaceItemThroughFace(int slot, ItemStack var2, Direction side)
	{
		if(slot==0||slot==1||slot==2)
			return canPlaceItem(slot, var2);
		
		return false;
	}

	@Override
	public boolean canTakeItemThroughFace(int slot, ItemStack var2, Direction side)
	{
		if(slot==3||slot==4)
			return true;
		
		return false;
	}
	
	@Override
	public boolean canPlaceItem(int slot, ItemStack var2)
	{
		if(slot==0)
		{
			return FurnaceBlockEntity.isFuel(var2);
		}
		else
		{
			return true;
		}
	}

	public float getPressure() 
	{
		return pressure;
	}

	public float getBurn() 
	{
		return (float)burn / (float)maxburn;
	}
	
	public boolean isBruning()
	{
		return maxburn >0;
	}
	
	
	@Override
	public int getProperty(int id) 
	{
		switch (id)
		{
		case 0:
			return this.pressure;
		case 1:
			return this.burn;
		case 2:
			return this.maxburn;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			this.pressure = value;
			break;
		case 1:
			this.burn = value;
			break;
		case 2:
			this.maxburn = value;
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 3;
	}


	@Override
	protected int getInventorySize()
	{
		return 5;
	}
	
	@Override
	public String getGUITitle() {
		return "gui.futurepack.partpress.title";
	}
	
}
