package futurepack.common.block.logistic;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import futurepack.depend.api.StableConstants;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.helper.HelperInventory;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemHandlerHelper;
import net.minecraftforge.items.ItemStackHandler;

public class TileEntitySyncronizer extends FPTileEntityBase implements ITileServerTickable
{
	private ArrayList<ItemStack> filter = new ArrayList<ItemStack>(8);
	private DynamicInventory stacks = new DynamicInventory();
	
	private int last = 0;
	
	public TileEntitySyncronizer(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.SYNCRONIZER, pos, state);
	}
	
	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState pState)
	{
		if(last-- <= 0)
		{
			last = 21;
			tryEject();
		}
	}
	
	@Override
	public CompoundTag save(CompoundTag nbt)
	{
		super.save(nbt);
		
		ListTag list = new ListTag();
		for(ItemStack it : filter)
		{
			if(it!=null && !it.isEmpty())
			{
				list.add(it.serializeNBT());
			}
		}
		nbt.put("filter", list);
		nbt.put("items", stacks.serializeNBT());
		return nbt;
	}
	
	@Override
	public void load(CompoundTag nbt)
	{
		ListTag list = nbt.getList("filter", StableConstants.NBT.TAG_COMPOUND);
		filter = new ArrayList<ItemStack>(list.size());
		for(Tag tag : list)
		{
			filter.add(ItemStack.of((CompoundTag)tag));
		}
		stacks.deserializeNBT(nbt.getCompound("items"));
		super.load(nbt);
	}
	
	public void storeFilterInItem(ItemStack item)
	{
		if(item.isEmpty())
			return;
		
		System.out.println("TileEntitySyncronizer.storeFilterInItem()");
		//TODO: storeFilterInItem
	}
	
	public void loadFilterFromItem(ItemStack item)
	{
		if(item.isEmpty())
			return;
		
		System.out.println("TileEntitySyncronizer.loadFilterFromItem()");
		//TODO: loadFilterFromItem
	}
	
	private LazyOptional<IItemHandler> outOpt, normalOpt;
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(getOutput() == facing)
			{
				if(outOpt!=null)
				{
					return (LazyOptional<T>) outOpt;
				}
				else
				{
					outOpt = LazyOptional.of(() -> new RestrictedItemHandlerWrapper(false, true, stacks));
					outOpt.addListener(p -> outOpt = null);
					return (LazyOptional<T>) outOpt;
				}
			}
			else
			{
				if(normalOpt!=null)
				{
					return (LazyOptional<T>) normalOpt;
				}
				else
				{
					normalOpt = LazyOptional.of(()->stacks);
					normalOpt.addListener(p -> normalOpt = null);
					return (LazyOptional<T>) normalOpt;
				}
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(outOpt, normalOpt);
		super.setRemoved();
	}
	
	public void dropItem(ItemStack item)
	{
		if(item.isEmpty())
			return;
		
		ItemEntity drop = new ItemEntity(level, worldPosition.getX()+0.5, worldPosition.getY()+1.1, worldPosition.getZ()+0.5, item);
		drop.setPickUpDelay(20);
		
		if(!level.isClientSide)
			level.addFreshEntity(drop);
	}
	
	public boolean isRecipeReady()
	{
		for(int i = 0;i < filter.size();i++)
		{
			if(filter.get(i).isEmpty())
				return false;
			
			int expected = filter.get(i).getCount();
			if(stacks.getStackInSlot(i).isEmpty())
				return false;
			
			int real = stacks.getStackInSlot(i).getCount();
			if(expected > real)
				return false;
		}
		return true;
	}
	
	public boolean tryEject()
	{
		if(isRecipeReady())
		{
			last = 21;
			BlockPos output = worldPosition.relative(getOutput());
			BlockEntity tile = level.getBlockEntity(output);
			if(tile!=null)
			{
				IItemHandler handler = HelperInventory.getHandler(tile, getOutput().getOpposite());
				if(handler==null)
					return false;
					
				for(int i=0;i<stacks.getSlots();i++)
				{
					ItemStack stack = stacks.getStackInSlot(i);
					ItemStack remain = ItemHandlerHelper.insertItem(handler, stack, true);
					if(!remain.isEmpty())
						return false;
				}
				
				for(int i=0;i<stacks.getSlots();i++)
				{
					ItemStack stack = stacks.getStackInSlot(i);
					ItemStack remain = ItemHandlerHelper.insertItem(handler, stack, false);
					dropItem(remain);
					stacks.setStackInSlot(i, ItemStack.EMPTY);
				}
				return true;
			}
		}
		return false;
	}
	
	private Direction getOutput()
	{
		if(getBlockState().hasProperty(BlockSyncronizer.FACING))
			return getBlockState().getValue(BlockSyncronizer.FACING);
		else
			return Direction.UP;
	}
	
	public class DynamicInventory extends ItemStackHandler
	{
		public void resize(int newSize)
		{
			List<ItemStack> old = super.stacks;
			setSize(newSize);
			for(ItemStack insert : old)
			{
				ItemStack left = ItemHandlerHelper.insertItem(this, insert, false);
				dropItem(left);
			}
		}
		
		@Override
		protected void validateSlotIndex(int slot)
		{
			while(slot >= super.stacks.size())
				resize(slot+1);
			super.validateSlotIndex(slot);
		}
		
		@Override
		public int getSlots()
		{
			int filterSize = filter.size();
			int size = super.getSlots();
			if(size!=filterSize)
			{
				resize(filterSize);
			}
			return filterSize;
		}
		
		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
		{
			if(isItemValid(slot, stack))
			{
				return super.insertItem(slot, stack, simulate);
			}
			else
			{
				return stack;
			}
		}
		
		@Override
		public boolean isItemValid(int slot, ItemStack stack)
		{
			if(slot < filter.size())
			{
				ItemStack st = filter.get(slot);
				if(st.isEmpty())
					return false;
				
				return ItemHandlerHelper.canItemStacksStack(st, stack);
			}
			return false;
		}
		
		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate)
		{
			return ItemStack.EMPTY;
		}
		
		@Override
		public int getSlotLimit(int slot)
		{
			if(slot < filter.size())
			{
				ItemStack st = filter.get(slot);
				if(!st.isEmpty())
					return st.getCount();
			}
			return 0;
		}
		
		@Override
		protected void onContentsChanged(int slot)
		{
			super.onContentsChanged(slot);
			tryEject();
			setChanged();
		}
		
		@Override
		public ItemStack getStackInSlot(int slot)
		{
			if(slot < super.stacks.size())
				return super.getStackInSlot(slot);
			else
				return ItemStack.EMPTY;
		}
	}
	
	public IItemHandlerModifiable getInventory()
	{
		return new IItemHandlerModifiable()
		{
			
			@Override
			@Nonnull
		    public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate)
		    {
		        if (stack.isEmpty())
		            return ItemStack.EMPTY;

//		        validateSlotIndex(slot);
		        resize(slot);

		        ItemStack existing = filter.get(slot);

		        int limit = getStackLimit(slot, stack);

		        if (!existing.isEmpty())
		        {
		            if (!ItemHandlerHelper.canItemStacksStack(stack, existing))
		                return stack;

		            limit -= existing.getCount();
		        }

		        if (limit <= 0)
		            return stack;

		        boolean reachedLimit = stack.getCount() > limit;

		        if (!simulate)
		        {
		            if (existing.isEmpty())
		            {
		            	filter.set(slot, reachedLimit ? ItemHandlerHelper.copyStackWithSize(stack, limit) : stack);
		            }
		            else
		            {
		                existing.grow(reachedLimit ? limit : stack.getCount());
		            }
//		            onContentsChanged(slot);
		        }

		        return reachedLimit ? ItemHandlerHelper.copyStackWithSize(stack, stack.getCount()- limit) : ItemStack.EMPTY;
		    }

		    @Override
		    @Nonnull
		    public ItemStack extractItem(int slot, int amount, boolean simulate)
		    {
		        if (amount == 0)
		            return ItemStack.EMPTY;

//		        validateSlotIndex(slot);
		        resize(slot);

		        ItemStack existing = filter.get(slot);

		        if (existing.isEmpty())
		            return ItemStack.EMPTY;

		        int toExtract = Math.min(amount, existing.getMaxStackSize());

		        if (existing.getCount() <= toExtract)
		        {
		            if (!simulate)
		            {
		                filter.set(slot, ItemStack.EMPTY);
//		                onContentsChanged(slot);
		            }
		            return existing;
		        }
		        else
		        {
		            if (!simulate)
		            {
		            	filter.set(slot, ItemHandlerHelper.copyStackWithSize(existing, existing.getCount() - toExtract));
//		                onContentsChanged(slot);
		            }

		            return ItemHandlerHelper.copyStackWithSize(existing, toExtract);
		        }
		    }
			
		    protected int getStackLimit(int slot, @Nonnull ItemStack stack)
		    {
		        return Math.min(getSlotLimit(slot), stack.getMaxStackSize());
		    }
		    
			@Override
			public ItemStack getStackInSlot(int slot)
			{
				resize(slot);
				return filter.get(slot);
			}
			
			@Override
			public int getSlots()
			{
				return filter.size();
			}
			
			@Override
			public int getSlotLimit(int slot)
			{
				return 64;
			}
			
			@Override
			public void setStackInSlot(int slot, ItemStack stack)
			{
				resize(slot);
				filter.set(slot, stack);
			}
			
			private void resize(int size)
			{
				while(filter.size() <= size)
				{
					filter.add(ItemStack.EMPTY);
				}
			}

			@Override
			public boolean isItemValid(int slot, ItemStack stack) 
			{
				ItemStack existing = filter.get(slot);

		        if (!existing.isEmpty())
		        {
		            return ItemHandlerHelper.canItemStacksStack(stack, existing);
		        }
		        else
		        {
		        	return false;
		        }
				
			}
		};
		
		
	}
	
}
