package futurepack.common.block.logistic;

import java.util.ArrayList;
import java.util.List;

import futurepack.api.FacingUtil;
import futurepack.api.interfaces.IFluidTankInfo.FluidTankInfo;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.block.inventory.TileEntityScannerBlock;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler.FluidAction;
import net.minecraftforge.fluids.capability.templates.FluidTank;

public class TileEntityFluidTank extends FPTileEntityBase implements ITileServerTickable
{
	protected FluidTank tank;
	private int ticksdown=0;
	
	private int lastFail = 0;
	
	private int[] fluidInput;
	
	public int pixelsFilled;//0 to 32 
	private LazyOptional<IFluidHandler>[] fluidOpt;
	
	private boolean wasEmpty = true;
	
	protected boolean canTransferHorizontal = true, canTransferVertical = true;
	
	@SuppressWarnings("unchecked")
	public TileEntityFluidTank(BlockEntityType<? extends TileEntityFluidTank> type, int capacity, BlockPos pos, BlockState state)
	{
		super(type, pos, state);
		tank = new FluidTankSided(capacity);
//		tank.setTileEntity(this);
		fluidInput = new int[FacingUtil.VALUES.length];
		fluidOpt = new LazyOptional[6];
		
	}
	
	public TileEntityFluidTank(BlockPos pos, BlockState state) 
	{
		this(8000, pos, state);
	}
	
	public TileEntityFluidTank(int capacity, BlockPos pos, BlockState state) 
	{
		this(FPTileEntitys.FLUID_TANK, capacity, pos, state);
	}
	
	
	protected void tranaferFluidVertical()
	{
		LazyOptional<IFluidHandler> optdown = null;
		if(getTank().getFluid().getFluid().getAttributes().isGaseous(getTank().getFluid()))
		{
			optdown = FluidUtil.getFluidHandler(level, worldPosition.above(), Direction.DOWN);
		}
		else
		{
			optdown = FluidUtil.getFluidHandler(level, worldPosition.below(), Direction.UP);
		}
		
		optdown.ifPresent(down -> 
		{
			int now = getTank().getFluidAmount();
			FluidStack stack = FluidUtil.tryFluidTransfer(down, getTank(), 1000, true);
			if(stack==null || stack.isEmpty())
			{
				lastFail = 10;
			}
			else if(now == getTank().getFluidAmount() && stack.getAmount()>0)
			{
				lastFail = 10;
			}
		});
	}
	
	protected void transferFluidHorizontal()
	{
		List<TileEntityFluidTank> list = new ArrayList<TileEntityFluidTank>();
		for(Direction face : FacingUtil.HORIZONTAL)
		{
			BlockEntity tile = level.getBlockEntity(worldPosition.relative(face));
			if(tile!=null)
			{
				if(tile instanceof TileEntityFluidTank)
				{
					TileEntityFluidTank tank = (TileEntityFluidTank)tile;
					FluidStack fs1 = tank.getTank().getFluid();
					FluidStack fs2 = this.getTank().getFluid();
					
					if(tank.getTank().getCapacity() == this.lastTank.getCapacity())
					{
						if(fs2.equals(fs1) || fs1.isEmpty())
						{
							list.add(tank);
						}
					}
				}
			}
		}
		
		if(!list.isEmpty() && this.getTank().getFluid()!=null)
		{
			int total = this.getTank().getFluidAmount();
			for(TileEntityFluidTank tank : list)
			{
				total += tank.getTank().getFluidAmount();
			}
			float each = total / (1F + list.size());
			
			if(this.getTank().getFluidAmount() > (int)each)
			{
				int fill = (int)each;
				if(fill<=5 && this.getTank().getFluid()!=null)
				{		
					int next = level.random.nextInt(list.size());
					TileEntityFluidTank tank = list.get(next);
					
					if(tank.getTank().getFluidAmount()>0)
						tank.getTank().getFluid().grow(1);
					else
						tank.getTank().setFluid(new FluidStack(this.getTank().getFluid(), 1));
					
					tank.setChanged();
					
					this.getTank().drain(1, FluidAction.EXECUTE);
					setChanged();
				}
				else
				{
					this.getTank().getFluid().setAmount(total -  list.size()*fill);
					for(TileEntityFluidTank tank : list)
					{
						if(tank.getTank().getFluidAmount()>0)
							tank.getTank().getFluid().setAmount(fill);
						else
							tank.getTank().setFluid(new FluidStack(this.getTank().getFluid(), fill));
						tank.setChanged();
					}
					setChanged();
				}
				
			}
			
		}
	}
	
	@Override
	public void tickServer(Level level, BlockPos worldPosition, BlockState pState)
	{
		ticksdown++;
		if(ticksdown>4)
		{
			lastTank = new FluidTankInfo(getTank(), 0);
			
			for(int i=0;i<fluidInput.length;i++)
			{
				if(fluidInput[i]>0)
					fluidInput[i]-=4;
				
				if(fluidInput[i]<0)
					fluidInput[i]=0;
			}
			
			if(canTransferVertical && this.getTank().getFluidAmount()>0 && --lastFail<=0)
			{
				tranaferFluidVertical();
			}
			if(canTransferHorizontal && this.getTank().getFluidAmount()>0 && !level.isClientSide)
			{
				transferFluidHorizontal();
			}
			
			ticksdown=0;
		}	
		
		if(wasEmpty != getTank().isEmpty())
		{
			wasEmpty = getTank().isEmpty();
			level.getLightEngine().checkBlock(worldPosition);
		}
	}
	
	@Override
	public void setChanged()
	{
		int now = getTank().getFluidAmount() / 250 +1;
		if(getTank().getFluidAmount()==0 && now>0)
		{
			now = 0;
		}
		
		if(now!=pixelsFilled)
		{
			pixelsFilled = now;
		
			FPPacketHandler.sendTileEntityPacketToAllClients(this);
		}
		super.setChanged();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(facing == null)
			return LazyOptional.empty();
			
		if(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY==capability)
		{
			if(fluidOpt[facing.get3DDataValue()]!=null)
			{
				return (LazyOptional<T>) fluidOpt[facing.get3DDataValue()];
			}
			else
			{
				fluidOpt[facing.get3DDataValue()] = LazyOptional.of(() -> new FluidHandlerTank(getTank(), facing));
				fluidOpt[facing.get3DDataValue()].addListener(p -> fluidOpt[facing.get3DDataValue()] = null);
				return (LazyOptional<T>) fluidOpt[facing.get3DDataValue()];
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(fluidOpt);
		super.setRemoved();
	}
	
	public int getLastFailTime()
	{
		return this.lastFail;
	}
	
	@Override
	public CompoundTag save(CompoundTag nbt)
	{
		CompoundTag tank = new CompoundTag();
		this.getTank().writeToNBT(tank);
		tank.putInt("capacity", this.getTank().getCapacity());
		
		nbt.put("tank", tank);
		nbt.putInt("fillDir", TileEntityScannerBlock.booleanToInt(new boolean[]{fluidInput[0]>0, fluidInput[1]>0, fluidInput[2]>0, fluidInput[3]>0, fluidInput[4]>0, fluidInput[5]>0}));
		return super.save(nbt);
	}
	
	@Override
	public void load(CompoundTag nbt)
	{
		CompoundTag tank = nbt.getCompound("tank");
		this.getTank().readFromNBT(tank);
		if(tank.contains("capacity"))
		{
			this.getTank().setCapacity(tank.getInt("capacity"));
		}
		
		boolean[] bools = TileEntityScannerBlock.intToBool(new boolean[6], nbt.getInt("fillDir"));
		for(int i=0;i<bools.length;i++)
		{
			fluidInput[i] = bools[i] ? 10 : Math.max(0, fluidInput[i]);
		}
		super.load(nbt);
	}
	

	


	
	public void addFilling(Direction side)
	{
		if(!level.isClientSide)
		{	
			if(side!=null)
			{
				fluidInput[side.ordinal()] = 10;
				setChanged();
			}
		}
	}
	
	public boolean isInputAktiv(Direction face)
	{
		return fluidInput[face.ordinal()] > 0;
	}
	
	private class FluidTankSided extends FluidTank
	{
		public FluidTankSided(int capacity)
		{
			super(capacity);
		}
		
		@Override
		protected void onContentsChanged()
		{
			setChanged();
		}
		
		@Override
		public int fill(FluidStack resource, FluidAction doFill)
		{
			if(getFluidAmount() >= getCapacity())
			{
				if(resource!=null && resource.getFluid()!=null)
				{
					boolean gas = resource.getFluid().getAttributes().isGaseous(resource);
					BlockEntity tile = null;
					if(gas)
					{
						tile = level.getBlockEntity(worldPosition.below());
					}
					else
					{
						tile = level.getBlockEntity(worldPosition.above());
					}
					if(tile!=null && tile instanceof TileEntityFluidTank)
					{
						Direction side = gas? Direction.UP : Direction.DOWN;
						tile.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, side).ifPresent(h -> {
							h.fill(resource, doFill);
						});
					}
				}
			}
			return super.fill(resource, doFill);
		}
	}
	
	private class FluidHandlerTank implements IFluidHandler
	{
		final FluidTank delegate;
		final Direction side;
		
		public FluidHandlerTank(FluidTank delegate, Direction side) 
		{
			super();
			this.delegate = delegate;
			this.side = side;
		}
		
		@Override
		public int getTanks() 
		{
			return delegate.getTanks();
		}
		@Override
		public FluidStack getFluidInTank(int tank) 
		{
			return delegate.getFluidInTank(tank);
		}
		@Override
		public int getTankCapacity(int tank) 
		{
			return delegate.getTankCapacity(tank);
		}
		@Override
		public boolean isFluidValid(int tank, FluidStack stack) 
		{
			return delegate.isFluidValid(tank, stack);
		}
		@Override
		public int fill(FluidStack resource, FluidAction action) 
		{
			int lvlA = delegate.getFluidAmount();
			int f = delegate.fill(resource, action);
			if(f>0 && delegate.getFluidAmount() > lvlA)//can not use only f because tank can fill to connected tanks
			{
				addFilling(side);
			}
			return f;
		}
		
		@Override
		public FluidStack drain(FluidStack resource, FluidAction action) 
		{
			return delegate.drain(resource, action);
		}
		@Override
		public FluidStack drain(int maxDrain, FluidAction action) 
		{
			return delegate.drain(maxDrain, action);
		}
	}
	
	
	private FluidTankInfo lastTank;

	public FluidTankInfo getTankInfo()
	{
		return new FluidTankInfo(getTank(), 0);
	}
	
	public FluidTankInfo getLastTankInfo()
	{
		if(lastTank==null)
		{
			return getTankInfo();
		}
			
		if(lastTank.getFluidStack().isEmpty() && !getTank().getFluid().isEmpty())
		{
			lastTank = getTankInfo();
		}
//		else if(!lastTank.getFluidStack().isEmpty() && lastTank.getFluidStack().getAmount()==0)
//		{
//			lastTank = new FluidTankInfo(new FluidStack(lastTank.fluid, 1), 1);
//		}
		return lastTank;
	}

	public FluidTank getTank()
	{
		return tank;
	}

//	@Override
//	public boolean shouldRenderInPass(int pass)
//	{
//		return pass==1;
//	}
}
