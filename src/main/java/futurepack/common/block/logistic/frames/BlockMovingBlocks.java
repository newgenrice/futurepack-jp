package futurepack.common.block.logistic.frames;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import net.minecraft.world.level.block.BarrierBlock;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class BlockMovingBlocks extends BarrierBlock implements IBlockServerOnlyTickingEntity<TileEntityMovingBlocks>
{

	public BlockMovingBlocks(Properties p_i48447_1_) 
	{
		super(p_i48447_1_);
	}

	@Override
	public BlockEntityType<TileEntityMovingBlocks> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.MOVING_BLOCKS;
	}
	
}
