package futurepack.common.block.logistic.frames;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.spaceships.moving.MovingBlocktUtil;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.MiniWorld;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Vec3i;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.TickNextTickData;
import net.minecraft.world.level.TickPriority;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.registries.ForgeRegistries;

public class TileEntityMovingBlocks extends TileEntityWithMiniWorldBase implements ITileServerTickable
{

	public TileEntityMovingBlocks(BlockEntityType<TileEntityMovingBlocks> type, BlockPos pos, BlockState state)
	{
		super(type, pos, state);
	}
	
	public TileEntityMovingBlocks(BlockPos pos, BlockState state)
	{
		this(FPTileEntitys.MOVING_BLOCKS, pos, state);
	}

	private Vec3i direction;
	private List<TickNextTickData<Block>> pendingTicks = null;
	private ListTag pending = null;

	@Override
	public void tickServer(Level level, BlockPos worldPosition, BlockState pState)
	{
//		if(level.isClientSide)
//		{
//			this.level.addParticle(ParticleTypes.POOF, true, this.worldPosition.getX() + 0.5,  this.worldPosition.getY() + 0.5,  this.worldPosition.getZ() + 0.5, 0D, 0D, 0D);
//			
//			
//		}
		
		if(!level.isClientSide() && ticks +(maxticks/10) == maxticks)
		{
			FPPacketHandler.sendTileEntityPacketToAllClients(this);
			System.out.println("resending");
		}
		
		ticks--;
//		ticks = 15;
		if(!level.isClientSide)
		{
	//		BlockPos start = w.start.add(w.rotationpoint.xCoord, w.rotationpoint.yCoord, w.rotationpoint.zCoord);
			if(w==null || ticks<=0)
			{			
				level.removeBlock(worldPosition, false);
				
				MovingBlocktUtil.endMoveBlocks(getLevel(), getMiniWorld(), direction, pendingTicks);
			}
		}
		
	}	
	
	@Override
	public CompoundTag save(CompoundTag nbt)
	{
		nbt.putByteArray("direction", new byte[] {(byte) direction.getX(), (byte) direction.getY(), (byte) direction.getZ()});
		
		ListTag pending = saveTickList(ForgeRegistries.BLOCKS::getKey, pendingTicks, this.getLevel().getGameTime());
		nbt.put("pendingBlockTicks", pending);
		
		return super.save(nbt);
	}
	
	@Override
	public void load(CompoundTag nbt)
	{
		byte[] d = nbt.getByteArray("direction");
		direction = new Vec3i(d[0], d[1], d[2]);
		
		ListTag listnbt = nbt.getList("pendingBlockTicks", nbt.getId());
		if(this.level!=null)
			setPendingTicks(loadTickList(ForgeRegistries.BLOCKS::getValue, listnbt, this.level.getGameTime()));
		else
			pending = listnbt;
		
		super.load(nbt);
	}
	
	public static <T> ListTag saveTickList(Function<T, ResourceLocation> p_219502_0_, Iterable<TickNextTickData<T>> p_219502_1_, long gameTime) 
	{
		ListTag listnbt = new ListTag();
		
		for(TickNextTickData<T> nextticklistentry : p_219502_1_) 
		{
			CompoundTag compoundnbt = new CompoundTag();
			compoundnbt.putString("i", p_219502_0_.apply(nextticklistentry.getType()).toString());
			compoundnbt.putInt("x", nextticklistentry.pos.getX());
			compoundnbt.putInt("y", nextticklistentry.pos.getY());
			compoundnbt.putInt("z", nextticklistentry.pos.getZ());
			compoundnbt.putInt("t", (int)(nextticklistentry.triggerTick - gameTime));
			compoundnbt.putInt("p", nextticklistentry.priority.getValue());
			listnbt.add(compoundnbt);
		}

		return listnbt;
	}
	
	public static <T> List<TickNextTickData<T>> loadTickList(Function<ResourceLocation, T> p_219502_0_, ListTag listnbt, long gameTime) 
	{
		return listnbt.stream().map(n -> (CompoundTag)n).map(nbt -> {
			BlockPos pos = new BlockPos(nbt.getInt("x"), nbt.getInt("y"), nbt.getInt("z"));
			TickNextTickData<T> e = new TickNextTickData<>(pos, p_219502_0_.apply(new ResourceLocation(nbt.getString("i"))), nbt.getInt("t") + gameTime, TickPriority.byValue(nbt.getInt("p")));
			return e;
		}).collect(Collectors.toList());

	}
	
	@Override
	public void setLevel(Level worldIn) 
	{
		super.setLevel(worldIn);
		if(pending!=null)
		{
			setPendingTicks(loadTickList(ForgeRegistries.BLOCKS::getValue, pending, worldIn.getGameTime()));
		}
	}
	
	@Override
	public void writeData(CompoundTag nbt)
	{
		nbt.putByteArray("direction", new byte[] {(byte) direction.getX(), (byte) direction.getY(), (byte) direction.getZ()});
		super.writeData(nbt);
	}
	
	@Override
	public void setMiniWorld(MiniWorld w)
	{
		this.w = w;
		w.rotationpoint = Vec3.atLowerCornerOf(this.getBlockPos()).subtract(Vec3.atLowerCornerOf(w.start)).add(0.5, 0, 0.5);
		if(w.face==null)
			w.face = Direction.UP;
	
		maxticks = ticks = Math.max(10, w.depth * w.height * w.width / 500);
	}
	
	public void setDirection(Vec3i dir)
	{
		this.direction =  dir;
		maxticks = ticks = maxticks * ( Math.abs(dir.getX()) + Math.abs(dir.getY()) + Math.abs(dir.getZ()) ) ;
	}
	
	public Vec3i getDirection()
	{
		return direction;
	}

	public void setPendingTicks(List<TickNextTickData<Block>> pendingTicks) 
	{
		this.pendingTicks = pendingTicks;
	}
}
