package futurepack.common.block.logistic;

import futurepack.common.block.BlockRotateableTile;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockLaserTransmitter extends BlockRotateableTile
{
	public final VoxelShape DOWN = Block.box(0, 8, 0, 16, 16, 16);
	public final VoxelShape UP = Block.box(0, 0, 0, 16, 8, 16);
	public final VoxelShape NORTH = Block.box(0, 0, 8, 16, 16, 16);
	public final VoxelShape SOUTH = Block.box(0, 0, 0, 16, 16, 8);
	public final VoxelShape WEST = Block.box(8, 0, 0, 16, 16, 16);
	public final VoxelShape EAST = Block.box(0, 0, 0, 8, 16, 16);
	
	
	protected BlockLaserTransmitter(Block.Properties props)
	{
		super(props);
//		super(Material.IRON);
//		setCreativeTab(FPMain.tab_maschiens);
		//setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.5F, 1.0F);
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext sel)
	{
		Direction face = state.getValue(BlockRotateableTile.FACING);
		switch (face)
		{
		case NORTH:
			return NORTH;
		case SOUTH:
			return SOUTH;
		case WEST:	
			return WEST;
		case EAST:
			return EAST;
		case DOWN:
			return DOWN;
		case UP:
		default:
			return UP;
		}
	}
	
//	@Override
//	public boolean isOpaqueCube(IBlockState state)
//	{
//		return false;
//	}
	
//	@Override
//	public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer, EnumHand hand)
//	{
//		return super.getStateForPlacement(worldIn, pos, facing, hitX, hitY, hitZ, facing.getIndex(), placer, hand);
//	}

	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new TileEntityLaserTransmitter(pos, state);
	}

}
