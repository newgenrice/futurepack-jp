package futurepack.common.block.logistic;

import java.util.BitSet;

import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.IFluidTank;
import net.minecraftforge.fluids.capability.IFluidHandler;

public class MultitankFluidHandler implements IFluidHandler 
{

	public IFluidTank[] tanks;
	private BitSet modes;
	
	public MultitankFluidHandler(IFluidTank...tanks) 
	{
		this.tanks = tanks;
		modes = new BitSet(tanks.length * 2); 
		modes.set(0, tanks.length * 2 -1, true);
	}
	
	public void setDrainEnabled(int tank, boolean enabled)
	{
		modes.set(tank, enabled);
	}
	
	public void setFillEnabled(int tank, boolean enabled)
	{
		modes.set(tanks.length + tank, enabled);
	}
	
	public boolean isDrainEnabled(int tank)
	{
		return modes.get(tank);
	}
	
	public boolean isFillEnabled(int tank)
	{
		return modes.get(tanks.length + tank);
	}
	
	@Override
	public int getTanks() 
	{
		return tanks.length;
	}

	@Override
	public FluidStack getFluidInTank(int tank) 
	{
		return tanks[tank].getFluid();
	}

	@Override
	public int getTankCapacity(int tank) 
	{
		return tanks[tank].getCapacity();
	}

	@Override
	public boolean isFluidValid(int tank, FluidStack stack) 
	{
		return tanks[tank].isFluidValid(stack);
	}

	@Override
	public int fill(FluidStack resource, FluidAction action) 
	{
		for(int i=0;i<tanks.length;i++)
		{
			if(isFillEnabled(i))
			{
				if(isFluidValid(i, resource) && getFluidInTank(i).getAmount() < getTankCapacity(i))
				{
					return tanks[i].fill(resource, action);
				}
			}
		}
		return 0;
	}

	@Override
	public FluidStack drain(FluidStack resource, FluidAction action) 
	{
		for(int i=0;i<tanks.length;i++)
		{
			if(isDrainEnabled(i))
			{
				if(!getFluidInTank(i).isEmpty() && getFluidInTank(i).isFluidEqual(resource))
				{
					return tanks[i].drain(resource, action);
				}
			}
		}
		return FluidStack.EMPTY;
	}

	@Override
	public FluidStack drain(int maxDrain, FluidAction action) 
	{
		for(int i=0;i<tanks.length;i++)
		{
			if(isDrainEnabled(i))
			{
				if(!getFluidInTank(i).isEmpty())
				{
					return tanks[i].drain(maxDrain, action);
				}
			}
		}
		return FluidStack.EMPTY;
	}

}
