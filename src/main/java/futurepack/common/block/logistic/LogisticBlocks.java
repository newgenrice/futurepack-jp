package futurepack.common.block.logistic;

import futurepack.api.Constants;
import futurepack.common.FPTileEntitys;
import futurepack.common.FuturepackMain;
import futurepack.common.block.logistic.frames.BlockMovingBlocks;
import futurepack.common.block.logistic.monorail.BlockMonorailBasic;
import futurepack.common.block.logistic.monorail.BlockMonorailBooster;
import futurepack.common.block.logistic.monorail.BlockMonorailCharger;
import futurepack.common.block.logistic.monorail.BlockMonorailDetector;
import futurepack.common.block.logistic.monorail.BlockMonorailLift;
import futurepack.common.block.logistic.monorail.BlockMonorailOneway;
import futurepack.common.block.logistic.monorail.BlockMonorailStation;
import futurepack.common.block.logistic.monorail.BlockMonorailWaypoint;
import futurepack.common.block.logistic.plasma.BlockPlasma2NeonT0;
import futurepack.common.block.logistic.plasma.BlockPlasmaConverter;
import futurepack.common.block.logistic.plasma.BlockPlasmaJar;
import futurepack.common.block.logistic.plasma.BlockPlasmaTransferPipe;
import futurepack.common.block.logistic.plasma.TileEntityPlasma2NeonT0;
import futurepack.common.block.logistic.plasma.TileEntityPlasmaConverter;
import futurepack.common.block.logistic.plasma.TileEntityPlasmaTransporter;
import futurepack.common.block.misc.ItemWIP;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockBehaviour.Properties;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.material.MaterialColor;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class LogisticBlocks 
{
	public static final Block pipe_normal = new BlockPipeNormal(Block.Properties.of(Material.METAL, DyeColor.WHITE).noOcclusion().strength(3F, 5F).sound(SoundType.METAL)).setRegistryName(Constants.MOD_ID, "pipe_normal");
	public static final Block pipe_neon = new BlockPipeNeon(Block.Properties.of(Material.METAL, DyeColor.CYAN).noOcclusion().strength(3F, 5F).sound(SoundType.METAL)).setRegistryName(Constants.MOD_ID, "pipe_neon");
	public static final Block pipe_support = new BlockPipeSupport(Block.Properties.of(Material.METAL, DyeColor.YELLOW).noOcclusion().strength(3F, 5F).sound(SoundType.METAL)).setRegistryName(Constants.MOD_ID, "pipe_support");
	public static final Block pipe_redstone = new BlockPipeRedstone(Block.Properties.of(Material.METAL, DyeColor.RED).noOcclusion().strength(3F, 5F).sound(SoundType.METAL)).setRegistryName(Constants.MOD_ID, "pipe_redstone");
	
	public static final Block wire_normal = new BlockWireNormal(Block.Properties.of(Material.METAL, DyeColor.WHITE).strength(3F, 5F).sound(SoundType.METAL).noOcclusion()).setRegistryName(Constants.MOD_ID, "wire_normal");
	public static final Block wire_support = new BlockWireSupport(Block.Properties.of(Material.METAL, DyeColor.YELLOW).strength(3F, 5F).sound(SoundType.METAL).noOcclusion()).setRegistryName(Constants.MOD_ID, "wire_support");
	public static final Block wire_network = new BlockWireNetwork(Block.Properties.of(Material.METAL, DyeColor.PURPLE).strength(3F, 5F).sound(SoundType.METAL).noOcclusion()).setRegistryName(Constants.MOD_ID, "wire_network");
	public static final Block wire_super = new BlockWireSuper(Block.Properties.of(Material.METAL, MaterialColor.COLOR_LIGHT_BLUE).strength(3F, 5F).sound(SoundType.METAL).noOcclusion()).setRegistryName(Constants.MOD_ID, "wire_super");
	public static final Block wire_redstone = new BlockWireRedstone(Block.Properties.of(Material.METAL, MaterialColor.COLOR_RED).strength(3F, 5F).sound(SoundType.METAL).noOcclusion()).setRegistryName(Constants.MOD_ID, "wire_redstone");
	
	public static final Block insert_node = new BlockInsertNode(Block.Properties.of(Material.METAL, DyeColor.WHITE).strength(3F, 5F).sound(SoundType.METAL).noOcclusion()).setRegistryName(Constants.MOD_ID, "insert_node");
	public static final Block laser_transmitter = new BlockLaserTransmitter(Block.Properties.of(Material.METAL, DyeColor.WHITE).strength(3F, 5F).sound(SoundType.METAL).noOcclusion()).setRegistryName(Constants.MOD_ID, "laser_transmitter");
	public static final Block syncronizer = new BlockSyncronizer(Block.Properties.of(Material.METAL, DyeColor.LIGHT_GRAY).strength(3F, 5F).sound(SoundType.METAL)).setRegistryName(Constants.MOD_ID, "syncronizer");
	
	public static final Block.Properties p_monorail = Block.Properties.of(Material.METAL, MaterialColor.COLOR_YELLOW).strength(3F, 5F);
	
	public static final Block monorail = new BlockMonorailBasic(p_monorail).setRegistryName(Constants.MOD_ID, "monorail");
	public static final Block monorail_station = new BlockMonorailStation(p_monorail).setRegistryName(Constants.MOD_ID, "monorail_station");
	public static final Block monorail_waypoint = new BlockMonorailWaypoint(p_monorail).setRegistryName(Constants.MOD_ID, "monorail_waypoint");
	public static final Block monorail_booster = new BlockMonorailBooster(p_monorail).setRegistryName(Constants.MOD_ID, "monorail_booster");
	public static final Block monorail_charger = new BlockMonorailCharger(p_monorail).setRegistryName(Constants.MOD_ID, "monorail_charger");
	public static final Block monorail_detector = new BlockMonorailDetector(p_monorail).setRegistryName(Constants.MOD_ID, "monorail_detector");
	public static final Block monorail_oneway = new BlockMonorailOneway(p_monorail).setRegistryName(Constants.MOD_ID, "monorail_oneway");
	public static final Block monorail_lift = new BlockMonorailLift(p_monorail).setRegistryName(Constants.MOD_ID, "monorail_lift");
	
//	public static final Block plasma_core_t1 = new BlockPlasmaCore(Block.Properties.of(Material.METAL).strength(100000F, 100000F)).setRegistryName(Constants.MOD_ID, "plasma_core_t1");
	public static final Block plasma_jar = new BlockPlasmaJar<TileEntityPlasmaTransporter>(Block.Properties.of(Material.METAL, DyeColor.WHITE).strength(3F, 5F).sound(SoundType.METAL).lightLevel(state -> state.getValue(BlockPlasmaJar.LIT) ? 10 : 0), () -> FPTileEntitys.PLASMA_PIPE_T0).setRegistryName(Constants.MOD_ID, "plasma_jar");
	public static final Block plasma_pipe_t1 = new BlockPlasmaTransferPipe<TileEntityPlasmaTransporter>(Block.Properties.of(Material.METAL, DyeColor.WHITE).strength(3F, 5F).sound(SoundType.METAL).noOcclusion(), () -> FPTileEntitys.PLASMA_PIPE_T1).setRegistryName(Constants.MOD_ID, "plasma_pipe_t1");	
	public static final Block plasma_converter_t1 = new BlockPlasmaConverter<TileEntityPlasmaConverter>(Block.Properties.of(Material.METAL, DyeColor.WHITE).strength(3F, 5F).sound(SoundType.METAL), () -> FPTileEntitys.PLASMA_CONVRTER_T1).setRegistryName(Constants.MOD_ID, "plasma_converter_t1");
	public static final Block plasma_converter_t0 = new BlockPlasmaConverter<TileEntityPlasmaConverter>(Block.Properties.of(Material.METAL, DyeColor.GRAY).strength(3F, 5F).sound(SoundType.METAL).noOcclusion(), () -> FPTileEntitys.PLASMA_CONVRTER_T0).setRegistryName(Constants.MOD_ID, "plasma_converter_t0");
	public static final Block plasma_2_neon_t0 = new BlockPlasma2NeonT0<TileEntityPlasma2NeonT0>(Block.Properties.of(Material.METAL, DyeColor.GRAY).strength(3F, 5F).sound(SoundType.METAL).noOcclusion().lightLevel(state -> state.getValue(BlockPlasmaJar.LIT) ? 10 : 0), () -> FPTileEntitys.PLASMA_2_NEON_T0).setRegistryName(Constants.MOD_ID, "plasma_2_neon_converter_t0");
	
	public static final Block fluid_tube = new BlockFluidTube(Block.Properties.of(Material.METAL, DyeColor.ORANGE).strength(3F, 5F).sound(SoundType.METAL)).setRegistryName(Constants.MOD_ID, "fluid_tube");
	public static final Block fluid_tank = new BlockFluidTank(Block.Properties.of(Material.METAL, DyeColor.GRAY).strength(3F, 5F).sound(SoundType.METAL).noOcclusion(), 8000).setRegistryName(Constants.MOD_ID, "fluid_tank");
	public static final Block fluid_intake = new BlockFluidIntake(Block.Properties.of(Material.METAL, DyeColor.LIGHT_GRAY).strength(3F, 5F).sound(SoundType.METAL)).setRegistryName(Constants.MOD_ID, "fluid_intake");
	public static final Block fluid_tank_mk2 = new BlockFluidTank(Block.Properties.of(Material.METAL, DyeColor.GRAY).strength(3F, 5F).sound(SoundType.METAL).noOcclusion(), 16000).setRegistryName(Constants.MOD_ID, "fluid_tank_mk2");
	public static final Block fluid_tank_mk3 = new BlockFluidTank(Block.Properties.of(Material.METAL, DyeColor.GRAY).strength(3F, 5F).sound(SoundType.METAL).noOcclusion(), 64000).setRegistryName(Constants.MOD_ID, "fluid_tank_mk3");
	
	public static final Block moving_blocks = new BlockMovingBlocks(Properties.copy(Blocks.BARRIER).noDrops()).setRegistryName(Constants.MOD_ID, "moving_blocks");
	public static final Block space_link = new BlockSpaceLink(Block.Properties.of(Material.METAL, DyeColor.PURPLE).strength(3F, 5F).sound(SoundType.METAL).noOcclusion()).setRegistryName(Constants.MOD_ID, "space_link");
	
	
	public static void registerBlocks(RegistryEvent.Register<Block> event)
	{
		IForgeRegistry<Block> r = event.getRegistry();
		
		r.registerAll(pipe_normal, pipe_neon, pipe_support, pipe_redstone);
		r.registerAll(wire_normal, wire_support, wire_network, wire_super, wire_redstone);
		r.registerAll(insert_node, laser_transmitter, syncronizer);
		r.registerAll(monorail, monorail_station, monorail_waypoint, monorail_booster, monorail_charger, monorail_detector, monorail_oneway, monorail_lift);
		r.registerAll(plasma_jar, plasma_pipe_t1, plasma_converter_t1, plasma_converter_t0);
//		r.registerAll(plasma_core_t1, , , );
		r.registerAll(fluid_tube, fluid_tank, fluid_intake, fluid_tank_mk2, fluid_tank_mk3, plasma_2_neon_t0);
		r.registerAll(moving_blocks, space_link);
	}
	
	public static void registerItems(RegistryEvent.Register<Item> event)
	{
		IForgeRegistry<Item> r = event.getRegistry();
		
		r.registerAll(item(pipe_normal), item(pipe_neon), item(pipe_support), item(pipe_redstone));
		r.registerAll(item(wire_normal), item(wire_support), item(wire_network), item(wire_super), item(wire_redstone));
		r.registerAll(item(insert_node), item(laser_transmitter), item(syncronizer));
		r.registerAll(item(monorail), item(monorail_station), item(monorail_waypoint), item(monorail_booster), item(monorail_charger), item(monorail_detector), item(monorail_oneway), item(monorail_lift));
		r.registerAll(itemWIP(plasma_pipe_t1), itemWIP(plasma_jar), itemWIP(plasma_converter_t1), itemWIP(plasma_converter_t0));
//		r.registerAll(, itemWIP(plasma_core_t1), );
		r.registerAll(item(fluid_tube), item(fluid_tank), item(fluid_intake), item(fluid_tank_mk2), item(fluid_tank_mk3), itemWIP(plasma_2_neon_t0));
		r.registerAll(item(space_link));
	}
	
	private static final Item item(Block bl)
	{
		return new BlockItem(bl, (new Item.Properties()).tab(FuturepackMain.tab_maschiens)).setRegistryName(bl.getRegistryName());
	}
	
	private static final Item itemWIP(Block bl)
	{
		return new ItemWIP(bl, (new Item.Properties()).tab(FuturepackMain.tab_maschiens)).setRegistryName(bl.getRegistryName());
	}
}
