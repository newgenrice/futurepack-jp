package futurepack.common.block.logistic;

import futurepack.common.block.BlockHologram;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.SimpleWaterloggedBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluids;

public abstract class BlockWaterLoggedHologram extends BlockHologram implements SimpleWaterloggedBlock
{

	public static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;
	  
	public BlockWaterLoggedHologram(Block.Properties builder) 
	{
		super(builder);
	}
	
	public BlockWaterLoggedHologram(Block.Properties builder, boolean hasNBTCustomDrops) 
	{
		super(builder, hasNBTCustomDrops);
	}
	
	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(WATERLOGGED);
	}
	
	@Override
	public FluidState getFluidState(BlockState state)
	{
		return state.getValue(WATERLOGGED) ? Fluids.WATER.getSource(false) : super.getFluidState(state);
	}
}
