package futurepack.common.block.logistic.plasma;

import java.util.Random;
import java.util.function.Supplier;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.block.BlockRotateableTile;
import futurepack.depend.api.helper.HelperBoundingBoxes;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockPlasmaTransferPipe<T extends BlockEntity & ITileServerTickable> extends BlockRotateableTile implements IBlockServerOnlyTickingEntity<T>
{
	private VoxelShape up    = HelperBoundingBoxes.createBlockShape(new double[][] {{2,2,2,14,16,14}}, 0, 0);
	private VoxelShape down  = HelperBoundingBoxes.createBlockShape(new double[][] {{2,2,2,14,16,14}}, 180, 0);
	private VoxelShape north = HelperBoundingBoxes.createBlockShape(new double[][] {{2,2,2,14,16,14}}, 90, 0);
	private VoxelShape south = HelperBoundingBoxes.createBlockShape(new double[][] {{2,2,2,14,16,14}}, 90, 180);
	private VoxelShape west  = HelperBoundingBoxes.createBlockShape(new double[][] {{2,2,2,14,16,14}}, 90, 90);
	private VoxelShape east  = HelperBoundingBoxes.createBlockShape(new double[][] {{2,2,2,14,16,14}}, 90, 270);
	
	private final Supplier<BlockEntityType<T>> sup;
	
	public BlockPlasmaTransferPipe(Properties properties, Supplier<BlockEntityType<T>> sup) 
	{
		super(properties);
		this.sup = sup;
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) 
	{
		switch (state.getValue(FACING))
		{
		case UP:return up;
		case DOWN:return down;
		case NORTH:return north;
		case SOUTH: return south;
		case WEST:return west;
		case EAST: return east;
		default:
			return super.getShape(state, worldIn, pos, context);
		}
	}
	
	@Override
	public void tick(BlockState state, ServerLevel worldIn, BlockPos pos, Random rand)
	{
		//called when plasma state changes between empty and filled
		super.tick(state, worldIn, pos, rand);
	}

	@Override
	public BlockEntityType<T> getTileEntityType(BlockState pState)
	{
		return sup.get();
	}
}
