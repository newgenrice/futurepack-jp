package futurepack.common.block.logistic.plasma;

import java.util.Random;
import java.util.function.Supplier;

import com.mojang.math.Vector3f;

import futurepack.depend.api.helper.HelperBoundingBoxes;
import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.DustParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RedstoneLampBlock;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockPlasmaJar<T extends TileEntityPlasmaTransporter> extends BlockPlasmaTransferPipe<T>
{
	public static final BooleanProperty LIT = RedstoneLampBlock.LIT;
	
	private VoxelShape up    = HelperBoundingBoxes.createBlockShape(new double[][] {{1,1,1,15,16,15}}, 0, 0);
	private VoxelShape down  = HelperBoundingBoxes.createBlockShape(new double[][] {{1,1,1,15,16,15}}, 180, 0);
	private VoxelShape north = HelperBoundingBoxes.createBlockShape(new double[][] {{1,1,1,15,16,15}}, 90, 0);
	private VoxelShape south = HelperBoundingBoxes.createBlockShape(new double[][] {{1,1,1,15,16,15}}, 90, 180);
	private VoxelShape west  = HelperBoundingBoxes.createBlockShape(new double[][] {{1,1,1,15,16,15}}, 90, 90);
	private VoxelShape east  = HelperBoundingBoxes.createBlockShape(new double[][] {{1,1,1,15,16,15}}, 90, 270);
	
	
	public BlockPlasmaJar(Properties properties, Supplier<BlockEntityType<T>> sup)
	{
		super(properties, sup);
	}

	@Override
	public void tick(BlockState state, ServerLevel w, BlockPos pos, Random rand)
	{
		super.tick(state, w, pos, rand);
		w.setBlockAndUpdate(pos, state.setValue(LIT, ((TileEntityPlasmaTransporter)w.getBlockEntity(pos)).getTank().getFluidAmount()>0));
	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) 
	{
		switch (state.getValue(FACING))
		{
		case UP:return up;
		case DOWN:return down;
		case NORTH:return north;
		case SOUTH: return south;
		case WEST:return west;
		case EAST: return east;
		default:
			return super.getShape(state, worldIn, pos, context);
		}
	}
	
	@Override
	public void animateTick(BlockState state, Level w, BlockPos pos, Random rand)
	{
		super.animateTick(state, w, pos, rand);
		if(state.getValue(LIT))
		{
			int r = rand.nextInt(100);
			if(r < 5)
				w.addAlwaysVisibleParticle(ParticleTypes.FLASH, pos.getX()+0.5F, pos.getY()+0.5F, pos.getZ()+0.5F, 0, 0, 0);
			else if (r < 20)
				w.addAlwaysVisibleParticle(new DustParticleOptions(new Vector3f(1.0F, 0.5F, 0.0F), 1.0F), pos.getX()+0.5F, pos.getY()+0.5F, pos.getZ()+0.5F, 0.0, 0.0, 0.0);
		}
	}
	
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) 
	{
		super.createBlockStateDefinition(builder);
		builder.add(LIT);
	}
}
