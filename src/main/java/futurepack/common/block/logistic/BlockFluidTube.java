package futurepack.common.block.logistic;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.common.item.tools.ToolItems;
import futurepack.depend.api.helper.HelperChunks;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;

public class BlockFluidTube extends Block implements EntityBlock
{
	private static EnumProperty<EnumSideState> upS = EnumProperty.create("s_up", EnumSideState.class),
	downS = EnumProperty.create("s_down", EnumSideState.class),
	northS = EnumProperty.create("s_north", EnumSideState.class),
	eastS = EnumProperty.create("s_east", EnumSideState.class),
	southS = EnumProperty.create("s_south", EnumSideState.class),
	westS = EnumProperty.create("s_west", EnumSideState.class);
//	private static PropertyEnum<EnumLogisticIO> upC = PropertyEnum.create("c_up", EnumLogisticIO.class),
//	downC = PropertyEnum.create("c_down", EnumLogisticIO.class),
//	northC = PropertyEnum.create("c_north", EnumLogisticIO.class),
//	eastC = PropertyEnum.create("c_east", EnumLogisticIO.class),
//	southC = PropertyEnum.create("c_south", EnumLogisticIO.class),
//	westC = PropertyEnum.create("c_west", EnumLogisticIO.class);
	
	public static VoxelShape shape = Block.box(4, 4, 4, 12, 12, 12);
	
	public BlockFluidTube(Block.Properties props)
	{
		super(props.noOcclusion());
		
//		super(Material.IRON);
//		setCreativeTab(FPMain.tab_maschiens);
		registerDefaultState(this.stateDefinition.any().setValue(upS, EnumSideState.NOT).setValue(downS, EnumSideState.NOT).setValue(northS, EnumSideState.NOT).setValue(eastS, EnumSideState.NOT).setValue(southS, EnumSideState.NOT).setValue(westS, EnumSideState.NOT));
	}
	
	@Override
	public RenderShape getRenderShape(BlockState state)
    {
        return RenderShape.MODEL;
    }
	
	@Override
	public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, LevelAccessor world, BlockPos currentPos, BlockPos facingPos) 
	{
		state = super.updateShape(state, facing, facingState, world, currentPos, facingPos);
		EnumProperty<EnumSideState> prop = null;
		switch (facing)
		{
		case UP:    prop = upS; break;
		case DOWN:  prop = downS; break;
		case NORTH: prop = northS; break;
		case EAST:  prop = eastS; break;
		case SOUTH: prop = southS; break;
		case WEST:  prop = westS; break;
		default: break;
		}
		state = state.setValue(prop, getSideState(world, currentPos, facing));
		return state;
	}
	
	@Override
	public void onPlace(BlockState state, Level w, BlockPos pos, BlockState oldState, boolean isMoving) 
	{
		super.onPlace(state, w, pos, oldState, isMoving);
		if(oldState.getBlock()!=this)
		{
			state = state.setValue(upS, getSideState(w, pos, Direction.UP));
			state = state.setValue(downS, getSideState(w, pos, Direction.DOWN));
			state = state.setValue(northS, getSideState(w, pos, Direction.NORTH));
			state = state.setValue(eastS, getSideState(w, pos, Direction.EAST));
			state = state.setValue(southS, getSideState(w, pos, Direction.SOUTH));
			state = state.setValue(westS, getSideState(w, pos, Direction.WEST));
			
			w.setBlockAndUpdate(pos, state);
		}
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) 
	{
		return shape;
	}
	
	private EnumSideState getSideState(LevelReader w, BlockPos pos, Direction face)
	{
		BlockPos tank = pos.relative(face);
		TileEntityFluidTube own = (TileEntityFluidTube) w.getBlockEntity(pos);
		if(own.getModeForFace(face, EnumLogisticType.FLUIDS)==EnumLogisticIO.NONE)
		{
			return EnumSideState.NOT;
		}
		BlockEntity tile = w.getBlockEntity(tank);
		
		if(tile!=null)
		{
			if(tile.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, face.getOpposite()).isPresent())
			{
				if(tile instanceof TileEntityFluidTube)
				{
					if(own.getModeForFace(face, EnumLogisticType.FLUIDS) != EnumLogisticIO.INOUT)
					{
						return EnumSideState.MACHINE;
					}
					else
					{
						return EnumSideState.NORMAL;
					}
				}
				else
				{
					return EnumSideState.MACHINE;
				}
			}
		}
		return EnumSideState.NOT;
	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit) 
	{
		ItemStack held  = pl.getItemInHand(hand);
		Direction facing = hit.getDirection();
		if(!held.isEmpty())
		{
			if(held.getItem() == ToolItems.scrench)
			{
				TileEntityFluidTube tube = (TileEntityFluidTube) w.getBlockEntity(pos);
				BlockEntity other = w.getBlockEntity(pos.relative(facing));
				if(tube.getModeForFace(facing, EnumLogisticType.FLUIDS)!=EnumLogisticIO.NONE)
				{
					tube.setModeForFace(facing, EnumLogisticIO.NONE, EnumLogisticType.FLUIDS);
					if(other instanceof TileEntityFluidTube)
					{
						if(((TileEntityFluidTube) other).setModeForFace(facing.getOpposite(), EnumLogisticIO.NONE, EnumLogisticType.FLUIDS))
						{
							other.setChanged();
							HelperChunks.renderUpdate(w, pos);
						}
					}
				}
				else
				{
					tube.setModeForFace(facing, EnumLogisticIO.INOUT, EnumLogisticType.FLUIDS);
					if(other instanceof TileEntityFluidTube)
					{
						if(((TileEntityFluidTube) other).setModeForFace(facing.getOpposite(), EnumLogisticIO.INOUT, EnumLogisticType.FLUIDS))
						{
							other.setChanged();
							HelperChunks.renderUpdate(w, pos);
						}
					}
				}
				w.sendBlockUpdated(pos, Blocks.AIR.defaultBlockState(), w.getBlockState(pos), 2);
				return InteractionResult.SUCCESS;
			}
		}
		return super.use(state, w, pos, pl, hand, hit);
	}
	
//	private EnumLogisticIO getConnectionState(IWorldReader w, BlockPos pos, EnumFacing face)
//	{
//		TileEntityFluidTube tube = (TileEntityFluidTube) w.getTileEntity(pos);
//		if(tube!=null)
//		{
//			return tube.getModeForFace(face, EnumLogisticType.FLUIDS);
//		}
//		return EnumLogisticIO.NONE;
//	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder) 
	{
		super.createBlockStateDefinition(builder);
		builder.add(upS, downS, northS, eastS, southS, westS);
	}
	
	public static enum EnumSideState implements StringRepresentable
	{
		NOT, //not connected to anything
		NORMAL, //normal conection to another tube
		MACHINE; //connection to a fluid conainer (or special between tubes) 

		@Override
		public String getSerializedName()
		{
			return name().toLowerCase();
		}
	}

	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state) 
	{
		return new TileEntityFluidTube(pos, state);
	}
}
