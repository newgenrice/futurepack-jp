package futurepack.common.block.logistic.monorail;

import java.util.Arrays;

import futurepack.api.interfaces.tilentity.ITileRenameable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityMonorailWaypoint extends FPTileEntityBase implements ITileRenameable
{
	private String name = null;
	
	
	public TileEntityMonorailWaypoint(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.MONORAIL_WAYPOINT, pos, state);
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		if(name==null)
			name = "Monorail Waypoint " +Arrays.toString(new int[]{worldPosition.getX(),worldPosition.getY(),worldPosition.getZ()});
	}
	
	@Override
	public CompoundTag save(CompoundTag nbt)
	{
		if(name!=null)
		nbt.putString("name", name);
		return super.save(nbt);
	}

	@Override
	public void load(CompoundTag nbt)
	{
		super.load(nbt);
		if(nbt.contains("name"))
			name = nbt.getString("name");
	}
	
	@Override
	public boolean hasCustomName()
	{
		return name!=null;
	}
	
	@Override
	public void setName(String name)
	{
		this.name = name;
		this.setChanged();
	}

	@Override
	public Component getName() 
	{
		return new TextComponent(name);
	}

	@Override
	public Component getCustomName() 
	{
		return getName();
	}
	
//	@Override
//	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newSate)
//	{
//		return oldState.getBlock() != newSate.getBlock();
//	}
}
