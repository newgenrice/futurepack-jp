package futurepack.common.block.logistic.monorail;

import java.util.ArrayList;
import java.util.Arrays;

import futurepack.api.FacingUtil;
import futurepack.common.entity.monocart.EntityMonocartBase;
import futurepack.depend.api.helper.HelperBoundingBoxes;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.SimpleWaterloggedBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.level.material.PushReaction;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockMonorailBasic extends Block implements SimpleWaterloggedBlock
{
	
	public static final EnumProperty<EnumMonorailStates> State = EnumProperty.create("shape", EnumMonorailStates.class, EnumMonorailStates.base);
	public static final EnumProperty<EnumMonorailStates> State_bendless = EnumProperty.create("shape", EnumMonorailStates.class, EnumMonorailStates.straight);
	public static final EnumProperty<EnumMonorailStates> State_lift = EnumProperty.create("shape", EnumMonorailStates.class, EnumMonorailStates.lift);
	
	public static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;
	
	public static void placeMonorail(Level w, BlockPos pos, BlockState state)
	{		
		EnumMonorailStates enumm = getEnum(w, pos, state);
		BlockMonorailBasic bl = (BlockMonorailBasic) w.getBlockState(pos).getBlock();
		w.setBlockAndUpdate(pos, state.setValue(getPropertie(bl), enumm));
	}
	
	public static EnumMonorailStates getEnum(Level w, BlockPos pos, BlockState state)
	{
		boolean isBendable = ((BlockMonorailBasic)w.getBlockState(pos).getBlock()).isBendable();
		boolean isLift = ((BlockMonorailBasic)w.getBlockState(pos).getBlock()).isLift();
		
		BlockState north = w.getBlockState(pos.north());
		BlockState east = w.getBlockState(pos.east());
		BlockState south = w.getBlockState(pos.south());
		BlockState west = w.getBlockState(pos.west());
		
		if(isLift)
		{
			BlockState down = w.getBlockState(pos.below());
			BlockState up = w.getBlockState(pos.above());
			// 4 Fach
			if((isMonorail(north)) || isMonorail(east) || isMonorail(south) || isMonorail(west) )
			{
				if(isMonorail(down) && !isMonorail(up))
				{
					return EnumMonorailStates.NORTH_EAST_SOUTH_WEST_DOWN;
				}
				else if(!isMonorail(down))
				{
					return EnumMonorailStates.NORTH_EAST_SOUTH_WEST_UP;
				}
				else
				{
					return EnumMonorailStates.UP_DOWN;
				}
			}
			else
			{
				return EnumMonorailStates.UP_DOWN;
			}
		}
		
		if(w.isEmptyBlock(pos.north()))
		{
			north = w.getBlockState(pos.north().below());
		}
		if(w.isEmptyBlock(pos.east()))
		{
			east = w.getBlockState(pos.east().below());
		}
		if(w.isEmptyBlock(pos.south()))
		{
			south = w.getBlockState(pos.south().below());
		}
		if(w.isEmptyBlock(pos.west()))
		{
			west = w.getBlockState(pos.west().below());
		}
		
		BlockState north_up = w.getBlockState(pos.above().north());
		BlockState east_up = w.getBlockState(pos.above().east());
		BlockState south_up = w.getBlockState(pos.above().south());
		BlockState west_up = w.getBlockState(pos.above().west());
		
		if(isBendable)
		{
			// 4 Fach
			if((isMonorail(north)||isMonorail(north_up)) && (isMonorail(east)||isMonorail(east_up)) && (isMonorail(south)||isMonorail(south_up)) && (isMonorail(west)||isMonorail(west_up)))
			{
				return EnumMonorailStates.NORTH_EAST_SOUTH_WEST;
			}
			
			//3 Fach
			if((isMonorail(north)||isMonorail(north_up)) && (isMonorail(east)||isMonorail(east_up)) && (isMonorail(south)||isMonorail(south_up)))
			{
				return EnumMonorailStates.NORTH_EAST_SOUTH;
			}
			if((isMonorail(north)||isMonorail(north_up)) && (isMonorail(east)||isMonorail(east_up)) && (isMonorail(west)||isMonorail(west_up)))
			{
				return EnumMonorailStates.NORTH_EAST_WEST;
			}
			if((isMonorail(north)||isMonorail(north_up)) && (isMonorail(south)||isMonorail(south_up)) && (isMonorail(west)||isMonorail(west_up)))
			{
				return EnumMonorailStates.NORTH_SOUTH_WEST;
			}
			if((isMonorail(east)||isMonorail(east_up)) && (isMonorail(south)||isMonorail(south_up)) && (isMonorail(west)||isMonorail(west_up)))
			{
				return EnumMonorailStates.EAST_SOUTH_WEST;
			}
			
			//2 Fach
			if((isMonorail(north)||isMonorail(north_up)) && (isMonorail(east)||isMonorail(east_up)))
			{
				return EnumMonorailStates.NORTH_EAST;
			}
			if((isMonorail(north)||isMonorail(north_up)) && (isMonorail(west)||isMonorail(west_up)))
			{
				return EnumMonorailStates.NORTH_WEST;
			}
			if((isMonorail(south)||isMonorail(south_up)) && (isMonorail(west)||isMonorail(west_up)))
			{
				return EnumMonorailStates.SOUTH_WEST;
			}
			if((isMonorail(east)||isMonorail(east_up)) && (isMonorail(south)||isMonorail(south_up)))
			{
				return EnumMonorailStates.EAST_SOUTH;
			}
		}
		
		//1 Fach
		if(isMonorail(north) || isMonorail(south))
		{
			if(isMonorail(north_up) && !isMonorail(north))
			{
				return EnumMonorailStates.ASCENDING_NORTH;
			}
			if(isMonorail(south_up) && !isMonorail(south))
			{
				return EnumMonorailStates.ASCENDING_SOUTH;
			}
			
			return EnumMonorailStates.NORTH_SOUTH;
		}
		if(isMonorail(east) || isMonorail(west))
		{
			if(isMonorail(east_up) && !isMonorail(east))
			{
				return EnumMonorailStates.ASCENDING_EAST;
			}
			if(isMonorail(west_up) && !isMonorail(west))
			{
				return EnumMonorailStates.ASCENDING_WEST;
			}
			
			return EnumMonorailStates.EAST_WEST;
		}
		
		//1 Fach nach oben
		if(w.isEmptyBlock(pos.above()))
		{
			if(isMonorail(north_up))
			{
				return EnumMonorailStates.ASCENDING_NORTH;
			}
			if(isMonorail(east_up))
			{
				return EnumMonorailStates.ASCENDING_EAST;
			}
			if(isMonorail(south_up))
			{
				return EnumMonorailStates.ASCENDING_SOUTH;
			}
			if(isMonorail(west_up))
			{
				return EnumMonorailStates.ASCENDING_WEST;
			}
		}
		
		return EnumMonorailStates.NORTH_SOUTH;
	}

	public static boolean isMonorail(Block b)
	{
		return b instanceof BlockMonorailBasic;
	}
	
	public static boolean isMonorail(BlockState b)
	{
		return isMonorail(b.getBlock());
	}
	
	public BlockMonorailBasic(Block.Properties props)
	{
		super(props.noCollission().noOcclusion());
//		super(Material.CIRCUITS);
//		setCreativeTab(FPMain.tab_items);
		this.registerDefaultState(this.stateDefinition.any().setValue(WATERLOGGED, false));
	}

	
	/**
	 * Whether or not can make Curves
	 */
	public boolean isBendable()
	{
		return true;
	}

	public boolean isLift()
	{
		return false;
	}
	
	
	public void onMonocartPasses(Level w, BlockPos pos, BlockState state, EntityMonocartBase cart)
	{
		
	}
	
	public boolean canMonocartPass(Level w, BlockPos pos, BlockState state, BlockPos from, BlockState fromState)
	{
		if(pos.getY() == from.getY())
		{
			Direction face = FacingUtil.getSide(pos, from);
			boolean blocked = state.getValue(getPropertie(this)).isBlocked(face);
			if(blocked)
				return false;
		}
		else
		{
			Direction face = FacingUtil.getSide(pos, from);
			boolean blocked = state.getValue(getPropertie(this)).isBlocked(face);
			if(blocked)
				return false;
		}
		return true;
	}
	
//	@Override
//	public boolean isOpaqueCube(IBlockState state)
//	{
//		return false;
//	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext sel)
	{
		EnumMonorailStates mono = state.getValue(getPropertie(this));
		return mono.getShape();
		
	}
	
	@Override
	public PushReaction getPistonPushReaction(BlockState state)
    {
        return PushReaction.NORMAL;
    }
	
	@SuppressWarnings("deprecation")
	@Override
	public void onPlace(BlockState state, Level worldIn, BlockPos pos, BlockState oldState, boolean isMoving)
	{
		if(oldState.getBlock()!=this)
		{
			placeMonorail(worldIn, pos, state);
			worldIn.updateNeighborsAtExceptFromFacing(pos.above(), this, Direction.DOWN);
			worldIn.updateNeighborsAtExceptFromFacing(pos.below(), this, Direction.UP);
		}
		super.onPlace(state, worldIn, pos, oldState, isMoving);
	}
	
	@Override
	public void neighborChanged(BlockState state, Level w, BlockPos pos, Block neighborBlock, BlockPos npos, boolean isMoving)
	{
		if(!state.canSurvive(w, pos))
		{
			w.destroyBlock(pos, true);
    		w.updateNeighborsAtExceptFromFacing(pos.above(), this, Direction.DOWN);
    		w.updateNeighborsAtExceptFromFacing(pos.below(), this, Direction.UP);
		}
		else
		{
			placeMonorail(w, pos, state);
		}
	}
	
	
	
	
//	@Override
//	public IBlockState getStateFromMeta(int meta)
//    {
//        return this.getDefaultState().with(getPropertie(this), EnumMonorailStates.getFromMeta(meta));
//    }
//
//	@Override
//    public int getMetaFromState(IBlockState state)
//    {
//        return state.get(getPropertie(this)).getMeta();
//    }
//	

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(getPropertie(this), BlockStateProperties.WATERLOGGED);
		super.createBlockStateDefinition(builder);
	}
	
	@Override
	public FluidState getFluidState(BlockState state)
	{
		return state.getValue(WATERLOGGED) ? Fluids.WATER.getSource(false) : super.getFluidState(state);
	}
	
	public static EnumProperty<EnumMonorailStates> getPropertie(BlockMonorailBasic b)
	{
		return b.isLift() ? State_lift : (b.isBendable()?State:State_bendless);
	}
	
	
	
	public static enum EnumMonorailStates implements StringRepresentable
	{
		//Gerade
		NORTH_SOUTH(true,true,false), // 180, 0 / 0
	    EAST_WEST(false,true,true),   // 	-90, 90 / 0
	    //Aufsteigend
	    ASCENDING_EAST(false,false,true),  // -90 / -45 , 45
	    ASCENDING_WEST(false,false,true),  // 90 / -45, 45
	    ASCENDING_NORTH(true,false,false), // 180 / -45, 45
	    ASCENDING_SOUTH(true,false,false), // 0 / -45, 45
	    //Kurven
	    EAST_SOUTH(false,true,false), // -90, 0   / 0
	    SOUTH_WEST(false,true,false), //   0, 90  / 0
	    NORTH_WEST(false,true,false), // 180, 90  / 0
	    NORTH_EAST(false,true,false), // 180, -90 / 0
	    //3 fach verbindung
	    NORTH_EAST_SOUTH(false,true,false), // 180, 0, -90 / 0
	    NORTH_SOUTH_WEST(false,true,false), // 180, 0, 90 / 0
	    NORTH_EAST_WEST(false,true,false),  // 180, 90, -90 / 0
	    EAST_SOUTH_WEST(false,true,false),  // 0, 90, -90 / 0
	    //Kreuzung
	    NORTH_EAST_SOUTH_WEST(false,true,false),  // 0, 90, 180, -90 / 0
	    
	    UP_DOWN(true, false, true),
	    NORTH_EAST_SOUTH_WEST_UP(false, false, false),
	    NORTH_EAST_SOUTH_WEST_DOWN(false, false, false)
	    ;
		
		private boolean lockedX, lockedY, lockedZ;
		private Direction[] notPossible;
		
		public static EnumMonorailStates[] straight = new EnumMonorailStates[]{NORTH_SOUTH, EAST_WEST, ASCENDING_EAST, ASCENDING_WEST, ASCENDING_NORTH, ASCENDING_SOUTH};
		public static EnumMonorailStates[] base =  new EnumMonorailStates[]{
				NORTH_SOUTH, EAST_WEST,
				ASCENDING_EAST, ASCENDING_WEST, ASCENDING_NORTH, ASCENDING_SOUTH,
				EAST_SOUTH, SOUTH_WEST, NORTH_WEST, NORTH_EAST,
				NORTH_EAST_SOUTH, NORTH_SOUTH_WEST, NORTH_EAST_WEST, EAST_SOUTH_WEST,
				NORTH_EAST_SOUTH_WEST
		};
		public static EnumMonorailStates[] lift =  new EnumMonorailStates[]{UP_DOWN, NORTH_EAST_SOUTH_WEST_UP, NORTH_EAST_SOUTH_WEST_DOWN};
		
		public VoxelShape part = null;
		
		public static double start_xz = 4.0/16D;
		public static double end_xz = 1D - 4.0/16D;
		
		public static VoxelShape part_main = Shapes.box(start_xz, 0, start_xz, end_xz, 0.0625F, end_xz);
		public static VoxelShape part_west = Shapes.box(0, 0, start_xz, start_xz, 0.0625F, end_xz);
		public static VoxelShape part_east = Shapes.box(end_xz, 0, start_xz, 1, 0.0625F, end_xz);
		public static VoxelShape part_south = Shapes.box(start_xz, 0, end_xz, end_xz, 0.0625F, 1);
		public static VoxelShape part_north = Shapes.box(start_xz, 0, 0, end_xz, 0.0625F, start_xz);
		
		
		
		private EnumMonorailStates(boolean x, boolean y, boolean z)
		{
			lockedX = x;
			lockedY = y;
			lockedZ = z;
			
			calc(this);
			notPossible = blockedDirections(this);
		}

		public VoxelShape getShape() 
		{
			if(part!=null)
				return part;
			
			if(isYLoocked())
			{
				part = part_main;
				if(isAvailable(Direction.NORTH))
					part = Shapes.or(part, part_north);
				if(isAvailable(Direction.SOUTH))
					part = Shapes.or(part, part_south);
				if(isAvailable(Direction.EAST))
					part = Shapes.or(part, part_east);
				if(isAvailable(Direction.WEST))
					part = Shapes.or(part, part_west);
				
				return part;
			}
			else
			{
				if(this==ASCENDING_EAST || this==ASCENDING_WEST)
				{
					return part = Shapes.box(0, 0, start_xz, 1, 1, end_xz);
				}
				else if(this==ASCENDING_NORTH || this==ASCENDING_SOUTH)
				{
					return part = Shapes.box(start_xz, 0, 0, end_xz, 1, 1);
				}
				else if(this == NORTH_EAST_SOUTH_WEST_UP || this == NORTH_EAST_SOUTH_WEST_DOWN)
				{
					return part = Shapes.or(UP_DOWN.getShape(), NORTH_EAST_SOUTH_WEST.getShape());	
				}
				else if(this == UP_DOWN)
				{
					return part = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,0,2,16,2}, {14,0,0,16,16,2}, {0,0,14,2,16,16}, {14,0,14,16,16,16}}, 0, 0);
				}
			}
			return null;
		}

		@Override
		public String getSerializedName()
		{
			return this.name().toLowerCase();
		}
		
		public int getMeta()
		{
			return this.ordinal();
		}
		
		public static EnumMonorailStates getFromMeta(int meta)
		{
			return EnumMonorailStates.values()[meta];
		}
		
		public boolean isXLoocked()
		{
			return lockedX;
		}
		
		public boolean isYLoocked()
		{
			return lockedY;
		}
		
		public boolean isZLoocked()
		{
			return lockedZ;
		}
		
		public int[] getYaws()
		{
			return yaws;
		}
		
		public int[] getPitch()
		{
			return pitch;
		}
		
		public boolean isBlocked(Direction face)
		{
			return Arrays.binarySearch(notPossible, face) >= 0;
		}
		
		public boolean isAvailable(Direction face)
		{
			return !isBlocked(face);
		}
		
		
		private int[] yaws;
		private int[] pitch;
		
		private void calc(EnumMonorailStates state)
		{
			if(state.name().contains("ASCENDING"))
			{
				pitch = new int[]{45,-45};
			}
			else
			{
				pitch = new int[]{0};
			}
			
			ArrayList<Integer> list = new ArrayList<Integer>(5);
			if(state.name().contains("NORTH"))
			{
				list.add(180);
				list.add(-180);
			}
			if(state.name().contains("EAST"))
			{
				list.add(-90);
			}
			if(state.name().contains("SOUTH"))
			{
				list.add(0);
			}
			if(state.name().contains("WEST"))
			{
				list.add(90);
			}
			
			yaws = new int[list.size()];
			for(int i=0;i<list.size();i++)
			{
				yaws[i] = list.get(i);
			}			
			
		}
	
		private static Direction[] blockedDirections(final EnumMonorailStates states)
		{
			ArrayList<Direction> list = new ArrayList<Direction>(Arrays.asList(FacingUtil.HORIZONTAL));
			
			list.removeIf(e -> {		
				String s = states.name().toLowerCase();
				boolean remove =  s.contains(e.getSerializedName()); 
				if(!remove)
				{
					if(s.contains("ascending"))
					{
						 remove = s.contains(e.getOpposite().getSerializedName()); 
					}
				}
				return remove;
			});
			
			Direction[] faces = list.toArray(new Direction[list.size()]);
			Arrays.sort(faces);
			return faces;
		}
	}
}
