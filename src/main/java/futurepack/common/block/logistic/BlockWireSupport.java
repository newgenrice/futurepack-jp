package futurepack.common.block.logistic;

import java.util.List;

import futurepack.api.capabilities.CapabilitySupport;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.ISupportStorage;
import futurepack.common.FPTileEntitys;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraftforge.common.util.LazyOptional;

public class BlockWireSupport extends BlockWireBase<TileEntityWireSupport> 
{
	public BlockWireSupport(Properties props) 
	{
		super(props);
	}

	public static final BooleanProperty POWERED = BlockStateProperties.POWERED;

	@Override
	public void appendHoverText(ItemStack stack, BlockGetter worldIn, List<Component> tooltip, TooltipFlag flagIn)
	{
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslatableComponent("tooltip.futurepack.block.conduct.support")); //"Transports: "+TextFormatting.YELLOW + "Support");
	}
	
	@Override
	public boolean canConnect(LevelAccessor w, BlockPos pos, BlockPos xyz, BlockState state, BlockState facingState, TileEntityWireBase wire, BlockEntity tile, Direction face)
	{
		if(super.canConnect(w, pos, xyz, state, facingState, wire, tile, face))
			return true;
		
		if(canConnect(w, pos, xyz, facingState, tile, face))
		{
			LazyOptional<ISupportStorage> wireC = wire.getCapability(CapabilitySupport.cap_SUPPORT, face);
			if(wireC.isPresent())
			{
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public boolean canConnect(LevelAccessor w, BlockPos pos, BlockPos xyz, BlockState facingState, BlockEntity tile, Direction face)
	{
		if(super.canConnect(w, pos, xyz, facingState, tile, face))
			return true;
		
		if(tile!=null)
		{
			LazyOptional<ISupportStorage> supO = tile.getCapability(CapabilitySupport.cap_SUPPORT, face.getOpposite());
			if(supO.isPresent())
			{
				ISupportStorage store = supO.orElseThrow(NullPointerException::new);
				if(store.getType()!=EnumEnergyMode.NONE)
				{	
					return true;
				}
			}
		}
		return false;
	}

	@Override
	protected int getMaxNeon() 
	{
		return 500;
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(POWERED);
	}

	@Override
	public BlockEntityType<TileEntityWireSupport> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.WIRE_SUPPORT;
	}
}
