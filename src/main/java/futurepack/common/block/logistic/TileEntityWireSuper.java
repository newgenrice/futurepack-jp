package futurepack.common.block.logistic;

import futurepack.common.FPTileEntitys;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityWireSuper extends TileEntityWireBase 
{

	public TileEntityWireSuper(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.WIRE_SUPER, pos, state);
	}

	@Override
	public int getMaxEnergy() 
	{
		return 1000;
	}

}
