package futurepack.common.block.logistic;

import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.INetworkUser;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.block.misc.MiscBlocks;
import futurepack.common.network.EventWirelessFunk;
import futurepack.common.network.NetworkManager;
import it.unimi.dsi.fastutil.objects.Object2ObjectAVLTreeMap;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.DirectionalBlock;
import net.minecraft.world.level.block.entity.BeaconBlockEntity;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntitySpaceLink extends FPTileEntityBase implements ITileNetwork, INetworkUser
{
	private final Consumer<EventWirelessFunk> method; 
	private static final int FREQUENZ = -42;
	private int frequenz_shift;
	
	private static Map<BlockEntityType<BlockEntity>, ToIntFunction<BlockEntity>> beaconValidatorMap;
	
	static
	{
		beaconValidatorMap = new Object2ObjectAVLTreeMap<>((a,b) -> a.hashCode() - b.hashCode());
		registerBeaconValidator(BlockEntityType.BEACON, TileEntitySpaceLink::getColorFromBeacon);
		registerBeaconValidator(FPTileEntitys.TELEPORTER_DUNGEON, t -> t.getBlockState().getBlock() == MiscBlocks.beam ? 0xFFFFFF : -1);
	}
	
	public static <T extends BlockEntity> void registerBeaconValidator(BlockEntityType<T> type, ToIntFunction<T> validator)
	{
		beaconValidatorMap.put((BlockEntityType<BlockEntity>)type, (ToIntFunction<BlockEntity>)validator);
	}
	
	public static int getColorFromBeacon(BeaconBlockEntity tile)
	{
		if(tile.getBeamSections().isEmpty())
			return -1;
		else
		{
			BeaconBlockEntity.BeaconBeamSection topSection = tile.getBeamSections().get(tile.getBeamSections().size()-1);
			int r = (int) (topSection.getColor()[0] * 255);
			int g = (int) (topSection.getColor()[1] * 255);
			int b = (int) (topSection.getColor()[2] * 255);
			
			return (r<<16) | (g<<8) | b;
		}
	}
	
	public TileEntitySpaceLink(BlockEntityType<? extends TileEntitySpaceLink> type, BlockPos pos, BlockState state) 
	{
		super(type, pos, state);
		method = this::onFunkEvent;
	}
	
	public TileEntitySpaceLink(BlockPos pos, BlockState state) 
	{
		this(FPTileEntitys.SPACE_LINK, pos, state);
	}

	@Override
	public void onLoad()
	{
		super.onLoad();
		if(getOverLevel() != null)
		{
			NetworkManager.registerWirelessTile(method, getOverLevel());
		}
	}
	
	private ServerLevel getOverLevel()
	{
		if(hasLevel() && getLevel().getServer() != null)
			return getLevel().getServer().getLevel(Level.OVERWORLD);
		else
			return null;
	}
	
	@Override
	public void onChunkUnloaded()
	{
		if(getOverLevel() != null)
		{
			NetworkManager.unregisterWirelessTile(method, getOverLevel());
		}
		super.onChunkUnloaded();
	}
	
	public void onFunkEvent(EventWirelessFunk event)
	{
		if(hasBeacon())
		{
			if(!level.isClientSide)
			{	
				if(this.isRemoved())
				{
					if(getOverLevel() != null)
					{
						NetworkManager.unregisterWirelessTile(method, getOverLevel());
					}
					return;
				}
				if(!level.hasChunkAt(worldPosition))
				{
					if(getOverLevel() != null)
					{
						NetworkManager.unregisterWirelessTile(method, getOverLevel());
					}
					return;
				}
				
				if(event.canRecive(this))
				{
					if(event.frequenz == getFrequenz())
					{
						if(event.range == -1)
						{
							NetworkManager.sendPacketUnthreaded(this, event.objPassed);
						}
					}		
				}
			}
			else if(getOverLevel() != null)
			{
				NetworkManager.unregisterWirelessTile(method, getOverLevel());
			}
		}
	}

	@Override
	public boolean isNetworkAble()
	{
		return true;
	}

	@Override
	public boolean isWire()
	{
		return false;
	}

	@Override
	public void onFunkPacket(PacketBase pkt)
	{
		if(hasBeacon())
			NetworkManager.sendEvent(new EventWirelessFunk(getSenderPosition(), getFrequenz(), -1, pkt), getOverLevel());
	}

	@Override
	public void postPacketSend(PacketBase pkt)
	{
		
	}

	@Override
	public Level getSenderWorld()
	{
		return this.level;
	}
	
	@Override
	public BlockPos getSenderPosition()
	{
		return this.worldPosition;
	}	
	
	
	private int getFrequenz()
	{
		return FREQUENZ - frequenz_shift;
	}
	
	private boolean hasBeacon()
	{
		if(getLevel().isClientSide())
			return false;
		else
		{
			Direction dir = getBlockState().getValue(DirectionalBlock.FACING);
			BlockEntity beacon;
			try 
			{
				beacon = getLevel().getServer().submit((Supplier<BlockEntity>) () -> getLevel().getBlockEntity(this.getBlockPos().relative(dir))).get();
				if(beacon!=null)
				{
					int color = beaconValidatorMap.getOrDefault(beacon.getType(), t -> -1).applyAsInt(beacon);
					if(color >= 0)
					{
						frequenz_shift = color; 
						return true;
					}
					return false;
				}
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
			
			
			return false;
		}
	}
}
