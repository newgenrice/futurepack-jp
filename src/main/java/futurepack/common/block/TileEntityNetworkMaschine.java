package futurepack.common.block;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import futurepack.common.network.FunkPacketPing;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityNetworkMaschine extends FPTileEntityBase implements ITileNetwork
{
	public TileEntityNetworkMaschine(BlockEntityType<? extends TileEntityNetworkMaschine> tileEntityTypeIn, BlockPos pos, BlockState state) 
	{
		super(tileEntityTypeIn, pos, state);
	}

	@Override
	public boolean isNetworkAble()
	{
		return true;
	}

	@Override
	public boolean isWire()
	{
		return false;
	}

	@Override
	public void onFunkPacket(PacketBase pkt)
	{
		if(pkt instanceof FunkPacketPing)
		{
			((FunkPacketPing) pkt).pong(this);
		}
	}
}
