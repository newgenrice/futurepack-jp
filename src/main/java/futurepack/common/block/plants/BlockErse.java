package futurepack.common.block.plants;

import futurepack.common.item.FoodItems;
import net.minecraft.core.BlockPos;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockErse extends CropBlock
{
	private static final VoxelShape box = Shapes.box(0.25F, 0, 0.25F, 0.75F, 0.4375F, 0.75F);
	
	public BlockErse(Block.Properties b) 
	{
		super(b);		
	}
	
	@Override
	public void onRemove(BlockState state, Level w, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if(newState.isAir())
		{
			if(isMaxAge(state))
			{
				w.setBlock(pos, getStateForAge(3), 2);
				Block.dropResources(state, w, pos);
			}
		}
		super.onRemove(state, w, pos, newState, isMoving);
	}
	
	@Override
	protected Item getBaseSeedId()
    {
        return FoodItems.erse;
    }

//    @Override
//	protected Item getCropsItem()
//    {
//        return FoodItems.erse;
//    }
//    
//    @Override
//    public int quantityDropped(BlockState state, Random random)
//    {
//    	return 1;
//    }
    
//    @Override
//    public boolean doesSideBlockRendering(IBlockState state, IWorldReader world, BlockPos pos, EnumFacing face)
//    {
//    	return false;
//    }
  
    
    @Override
    public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) 
    {
    	return box;
    }
}

