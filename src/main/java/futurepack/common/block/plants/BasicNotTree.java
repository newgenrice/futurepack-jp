package futurepack.common.block.plants;

import java.util.Random;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.grower.AbstractTreeGrower;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.TreeConfiguration;

public abstract class BasicNotTree<F extends FeatureConfiguration> extends AbstractTreeGrower 
{

	@Override
	protected ConfiguredFeature<TreeConfiguration, ?> getConfiguredFeature(Random randomIn, boolean largeHive) 
	{
		return null;
	}
	
	protected abstract ConfiguredFeature<F, ?> getTree(Random randomIn, boolean largeHive);
	
	public boolean growTree(ServerLevel world, ChunkGenerator chunkGenerator, BlockPos pos, BlockState state, Random rand)
	{
		ConfiguredFeature<F, ?> tree = getTree(rand, false);
		
		world.setBlock(pos, Blocks.AIR.defaultBlockState(), 4);
//		tree.config.forcePlacement();
		if (tree.place(world, chunkGenerator, rand, pos)) {
			return true;
		} else {
			world.setBlock(pos, state, 4);
			return false;
		}
	}

}
