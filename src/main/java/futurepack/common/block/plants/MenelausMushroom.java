package futurepack.common.block.plants;

import java.util.Random;

import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.world.dimensions.biomes.FPBiomes;
import futurepack.world.gen.feature.BigMushroomFeatureConfig;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;

public class MenelausMushroom extends BasicNotTree<BigMushroomFeatureConfig>
{

	BigMushroomFeatureConfig config = new BigMushroomFeatureConfig(TerrainBlocks.log_mushroom.defaultBlockState(), TerrainBlocks.leaves_mushroom.defaultBlockState(), 5, 10);
	ConfiguredFeature<BigMushroomFeatureConfig, ?> tree = FPBiomes.MUSHROOM_TREE.configured(config);
	
	@Override
	protected ConfiguredFeature<BigMushroomFeatureConfig, ?> getTree(Random random, boolean par2) 
	{
		return tree;
	}

}
