package futurepack.common.block.plants;

import java.util.Random;

import futurepack.common.item.FoodItems;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.Mth;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.common.PlantType;

public class BlockTopinambur extends CropBlock
{
	 public static final BooleanProperty TOP = BooleanProperty.create("top");
	 private static final VoxelShape[] CROPS_AABB_1 = new VoxelShape[] {Shapes.box(0.0D, 0.0D, 0.0D, 1.0D, 0.375D, 1.0D), Shapes.box(0.0D, 0.0D, 0.0D, 1.0D, 0.5625D, 1.0D), Shapes.box(0.0D, 0.0D, 0.0D, 1.0D, 0.75D, 1.0D), Shapes.box(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), Shapes.box(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), Shapes.box(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), Shapes.box(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), Shapes.box(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D)};
	 private static final VoxelShape[] CROPS_AABB_2 = new VoxelShape[] {Shapes.box(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), Shapes.box(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), Shapes.box(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), Shapes.box(0.0D, 0.0D, 0.0D, 1.0D, 0.1875D, 1.0D), Shapes.box(0.0D, 0.0D, 0.0D, 1.0D, 0.5625D, 1.0D), Shapes.box(0.0D, 0.0D, 0.0D, 1.0D, 0.9375D, 1.0D), Shapes.box(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), Shapes.box(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D)};
	 
	public BlockTopinambur(Block.Properties	props) 
	{
		super(props);	
		this.registerDefaultState(this.stateDefinition.any().setValue(this.getAgeProperty(), Integer.valueOf(0)).setValue(TOP, false));
	}
	
	@Override
    public VoxelShape getShape(BlockState state, BlockGetter source, BlockPos pos, CollisionContext sel)
    {
		int age = getAge(state);
		
		if(state.getValue(TOP))
			return CROPS_AABB_2[age];
		else
			return CROPS_AABB_1[age];
    }
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(TOP);
	}
    
    private boolean growBothBlocks(Level worldIn, BlockPos pos, BlockState state, int age)
    {
    	if(age > getMaxAge())
    		return false;
    	
    	if(age > 2)
    	{
    		BlockState tbs = worldIn.getBlockState(pos.above());
    		
    		if(tbs.getBlock() == this)
    		{
    			worldIn.setBlock(pos.above(), tbs.setValue(AGE, age), 2);
    		}
    		else if(worldIn.isEmptyBlock(pos.above()))
    		{
    			worldIn.setBlock(pos.above(), this.getStateForAge(age).setValue(TOP, true), 3);
    		}
    		else
    		{
    			return false;
    		}
    		
    	}
    	
    	worldIn.setBlock(pos, state.setValue(AGE, age), 2);
    	
    	return true;
    }
    
    @Override
    public void tick(BlockState state, ServerLevel worldIn, BlockPos pos, Random rand)
    {
        if(state.getValue(TOP))
        	return;

        if (!worldIn.isAreaLoaded(pos, 1)) return; // Forge: prevent loading unloaded chunks when checking neighbor's light
        if (worldIn.getRawBrightness(pos.above(), 0) >= 9)
        {
            int i = this.getAge(state);

            if (i < this.getMaxAge())
            {
                float f = getGrowthSpeed(this, worldIn, pos);

                if(net.minecraftforge.common.ForgeHooks.onCropsGrowPre(worldIn, pos, state, rand.nextInt((int)(25.0F / f) + 1) == 0))
                {
                	if(growBothBlocks(worldIn, pos, state, i + 1))
                	{         	
                		net.minecraftforge.common.ForgeHooks.onCropsGrowPost(worldIn, pos, state);
                	}
                }
            }
        }
    }
    
    @Override
    public void growCrops(Level worldIn, BlockPos pos, BlockState state)
    {
        if(state.getValue(TOP))
        {
        	BlockState ibs = worldIn.getBlockState(pos.below());
        	if(ibs.getBlock() == this)
        	{
        		growCrops(worldIn, pos.below(), worldIn.getBlockState(pos.below()));
        	}
        	
        	return;
        }
        	
        int i = this.getAge(state) + this.getBonemealAgeIncrease(worldIn);
        int j = this.getMaxAge();

        if (i > j)
        {
            i = j;
        }

        growBothBlocks(worldIn, pos, state, i);
    }
    
    @Override
    protected int getBonemealAgeIncrease(Level worldIn)
    {
        return Mth.nextInt(worldIn.random, 1, 3);
    }
    
    @Override
    public boolean canSurvive(BlockState state, LevelReader worldIn, BlockPos pos)
    {
        if (state.getBlock() == this) //Forge: This function is called during world gen and placement, before this block is set, so if we are not 'here' then assume it's the pre-check.
        {
        	if(state.getValue(TOP))
        	{
        		BlockState ibs = worldIn.getBlockState(pos.below());
        		return ibs.getBlock() == this  && !ibs.getValue(TOP);
        	}
        	       	
            BlockState soil = worldIn.getBlockState(pos.below());
            return soil.getBlock().canSustainPlant(soil, worldIn, pos.below(), Direction.UP, this);
            
        }
        
        return super.canSurvive(state, worldIn, pos);
    }
        
    @Override
    public PlantType getPlantType(BlockGetter world, BlockPos pos) 
    {
    	return PlantType.PLAINS;
    }
    
    @Override
	protected ItemLike getBaseSeedId() 
    {
        return FoodItems.topinambur_potato;
     }


    
}
