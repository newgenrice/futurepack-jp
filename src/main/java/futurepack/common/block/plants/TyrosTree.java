package futurepack.common.block.plants;

import java.util.Random;

import futurepack.world.dimensions.biomes.FPBiomes;
import futurepack.world.gen.feature.TyrosTreeFeature;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.TreeConfiguration;

public class TyrosTree extends LargeTree
{
	public TyrosTree() 
	{
		super(5);
	}

	@Override
	protected ConfiguredFeature<TreeConfiguration, ?> getConfiguredFeature(Random random, boolean par2) 
	{
		return Feature.TREE.configured(FPBiomes.TYROS_TREE_CONFIG);
	}

	@Override
	protected ConfiguredFeature<TreeConfiguration, ?> getLargeTreeFeature(Random random) 
	{
		return TyrosTreeFeature.LARGE_TYROS_TREE.configured(FPBiomes.TYROS_TREE_CONFIG);
	}

}
