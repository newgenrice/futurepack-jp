package futurepack.common.block;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import net.minecraft.world.level.block.AirBlock;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class BlockLinkedLight extends AirBlock implements IBlockServerOnlyTickingEntity<TileEntityLinkedLight>
{

	public BlockLinkedLight(Properties properties) 
	{
		super(properties);
	}

	@Override
	public BlockEntityType<TileEntityLinkedLight> getTileEntityType(BlockState pState) 
	{
		return FPTileEntitys.LINKED_LIGHT;
	}
}
