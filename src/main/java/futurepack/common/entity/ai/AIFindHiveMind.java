package futurepack.common.entity.ai;

import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Predicate;

import it.unimi.dsi.fastutil.longs.Long2ByteAVLTreeMap;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.util.LandRandomPos;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraft.world.phys.Vec3;

public class AIFindHiveMind extends WaterAvoidingRandomStrollGoal 
{
	protected Long2ByteAVLTreeMap checkedChunks;
	protected final Predicate<BlockEntity> isHive;
	private List<BlockPos> found;
	
	
	public AIFindHiveMind(PathfinderMob creature, Predicate<BlockEntity> predicate, double speedIn, float probabilityIn) 
	{
		super(creature, speedIn, probabilityIn);
		setFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.JUMP));
		checkedChunks = new Long2ByteAVLTreeMap();
		isHive = predicate;
		found = new LinkedList<BlockPos>();
	}
	
	@Override
	public boolean canUse() 
	{
		if(mob.getRestrictCenter() != BlockPos.ZERO)
			return false;
		else
			return super.canUse();
	}
	
	@Override
	public boolean canContinueToUse() 
	{
		return super.canContinueToUse();
	}
	
	@Override
	public void start() 
	{
		if(found.isEmpty())
		{
			ChunkPos currentChunk = new ChunkPos(mob.blockPosition());
			byte b = getChunkStatus(currentChunk);
			if((b & 0b11) == 0b01)
			{
				//chunk is empty
				super.start();
			}
		}
		else
		{
			double dis = found.get(0).distSqr(mob.position(), true);
			if( dis < 4)
			{
				mob.restrictTo(found.get(0), 16 * 4);
				found.clear();
				checkedChunks.clear();
				return;
			}
			
			wantedX = found.get(0).getX();
			wantedY = found.get(0).getY();
			wantedZ = found.get(0).getZ();
			
			super.start();
		}
		
	}
	
	@Override
	protected Vec3 getPosition() 
	{
		if(!found.isEmpty())
		{
			BlockPos p = found.get(0);
			return new Vec3(p.getX()+0.5, p.getY()+0.5, p.getZ()+0.5);
		}
		Vec3 rndPos = super.getPosition();
		ChunkPos pos;
		if(rndPos!=null)
		{
			pos = new ChunkPos(new BlockPos(rndPos));
			if(checkedChunks.getOrDefault(pos.toLong(), (byte)0) == 0)
			{
				return rndPos;
			}
		}
		for(int i=0;i<10;i++)
		{
			rndPos = LandRandomPos.getPos(this.mob, 10 +i*3, 7 + i);
			if(rndPos!=null)
			{
				pos = new ChunkPos(new BlockPos(rndPos));
				if(checkedChunks.getOrDefault(pos.toLong(), (byte)0) == 0)
				{
					return rndPos;
				}
			}
		}
		
		return super.getPosition();
	}
	
	protected byte getChunkStatus(ChunkPos pos)
	{
		return checkedChunks.compute(pos.toLong(), this::calcChunkStatus);
	}
	
	protected byte calcChunkStatus(long chunkPos, Byte currentStatus)
	{
		if(currentStatus == null)
			currentStatus = 0;
		else if( currentStatus != 0)
				return currentStatus;
		
		LevelChunk c = mob.getCommandSenderWorld().getChunk(ChunkPos.getX(chunkPos), ChunkPos.getZ(chunkPos));
		for(Entry<BlockPos, BlockEntity> e : c.getBlockEntities().entrySet())
		{
			if(isHive.test(e.getValue()))
			{
				found.add(e.getKey());
				currentStatus = (byte) (currentStatus.byteValue() | 0b10);
			}
		}
		currentStatus = (byte) (currentStatus.byteValue() |0b1);
		
		return currentStatus;
	}

}
