package futurepack.common.entity.ai;

import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;

import com.google.common.base.Predicate;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.AABB;

public class EntityAIHide extends Goal implements Predicate<BlockState>
{
	private PathfinderMob entity;
	private final int time;
	public Predicate<BlockState> checker;
	
	private int currentTime;
	
	public EntityAIHide(IHideableCreature creatureIn, int maxTime)
    {
        this.entity = (PathfinderMob) creatureIn;
        this.time = maxTime;
        this.setFlags(EnumSet.of(Goal.Flag.MOVE));
        checker = this;
        currentTime = 0;
    }
	
	@Override
	public boolean canUse()
	{ 	
		if(!((IHideableCreature)entity).canHide())
		{
			return false;
		}
	
		AABB bb = entity.getBoundingBox();
		BlockPos pos1 = new BlockPos(bb.minX,entity.getY()-1,bb.minZ);
		BlockPos pos2 = new BlockPos(bb.maxX,entity.getY()-1,bb.maxZ);
		
		return checkRange(pos1, pos2) && (entity.getTarget()==null || this.entity.getTarget().isAlive()==false);
	}

	@Override
	public boolean canContinueToUse()
	{
		return currentTime > 0;
	}
	
	@Override
	public void start()
	{
		super.start();
		currentTime = time;
		this.entity.setTarget(null);
		this.entity.getNavigation().stop();
		this.entity.setYRot(0);
		this.entity.setXRot(0);
		((IHideableCreature)this.entity).setHiding(true);
	}
	
	public void finishExecuting()
	{
		currentTime=0;
		((IHideableCreature)this.entity).setHiding(false);
	}
	
	@Override
	public void tick()
	{
		super.tick();
		currentTime--;
		if(currentTime>0)
		{
			if(this.entity.getTarget()!=null || this.entity.getKillCredit()!=null)
			{
				finishExecuting();
				return;
			}
				
			List<LivingEntity> list = entity.level.getEntitiesOfClass(LivingEntity.class, entity.getBoundingBox());
			Iterator<LivingEntity> iter = list.iterator();
			while(iter.hasNext())
			{
				LivingEntity base = iter.next();
				if(base==this.entity)
					continue;
				
				if(!base.isAlive()==false)
				{
					((IHideableCreature)entity).onHiddenAttack(base);
					finishExecuting();
					break;
				}
			}
		}
		else
		{
			finishExecuting();
		}
	}
	
	@Override
	public void stop()
	{
		super.stop();
		finishExecuting();
	}
	
	private boolean checkRange(BlockPos begin, BlockPos end)
	{
		for(int x=begin.getX();x<=end.getX();x++)
		{
			for(int y=begin.getY();y<=end.getY();y++)
			{
				for(int z=begin.getZ();z<=end.getZ();z++)
				{
					BlockPos xyz = new BlockPos(x,y,z);
					BlockState state = entity.level.getBlockState(xyz);
					if(!checker.apply(state))
					{
						return false;
					}
				}
			}
		}
		return true;
	}

	@Override
	public boolean apply(BlockState input)
	{
		return input.getMaterial() == Material.SAND;
	}
}
