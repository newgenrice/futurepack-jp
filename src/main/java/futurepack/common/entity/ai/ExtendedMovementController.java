package futurepack.common.entity.ai;

import futurepack.common.DirtyHacks;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.control.MoveControl;

public class ExtendedMovementController extends MoveControl 
{
	public ExtendedMovementController(Mob mob) 
	{
		super(mob);
		DirtyHacks.replaceData(this, mob.moveControl, MoveControl.class);
	}

	public boolean isWaiting()
	{
		return this.operation == Operation.WAIT;
	}
	
}
