package futurepack.common.entity.ai.hivemind;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import futurepack.common.entity.living.EntityDungeonSpider;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.INBTSerializable;

public class AssignedTask implements INBTSerializable<CompoundTag>
{
	private HVTask task;
	private List<WeakReference<EntityDungeonSpider>> workingSpiders;

	public AssignedTask(HVTask task) 
	{
		super();
		this.task = task;
		workingSpiders = new ArrayList<WeakReference<EntityDungeonSpider>>();
	}

	public AssignedTask(CompoundTag nbt)
	{
		deserializeNBT(nbt);
	}
	
	@Override
	public CompoundTag serializeNBT() 
	{
		CompoundTag nbt = new CompoundTag();
		nbt.put("task", HVTask.save(task));
		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundTag nbt) 
	{
		task = HVTask.load(nbt.getCompound("task"));
	}
	
	public boolean isFinished(Level w)
	{
		return task.isFinished(w);
	}
	
	public HVTask getTask()
	{
		return task;
	}
	
	
	public int getAssignedSpidersCount()
	{
		workingSpiders.removeIf(ref -> (ref.get()==null || !ref.get().isAlive()));
		return workingSpiders.size();
	}
	
	public Stream<EntityDungeonSpider> getAssignedSpiders()
	{
		return workingSpiders.stream().map(WeakReference::get).filter(e -> e!=null && e.isAlive());
	}
	
	public boolean canExecute(EntityDungeonSpider spider)
	{
		return task.canExecute(spider, this);
	}

	public HVTask assign(EntityDungeonSpider spider) 
	{
		workingSpiders.add(new WeakReference<EntityDungeonSpider>(spider));
		return task;
	}
}