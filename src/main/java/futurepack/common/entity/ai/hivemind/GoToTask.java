package futurepack.common.entity.ai.hivemind;

import futurepack.common.entity.living.EntityDungeonSpider;
import futurepack.depend.api.helper.HelperSerialisation;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;

public class GoToTask extends HVTask 
{
	private BlockPos destination;
	
	public GoToTask(BlockPos destination) 
	{
		this.destination = destination;
	}
	
	public GoToTask(CompoundTag nbt) 
	{
		super(nbt);
	}
	
	@Override
	public CompoundTag serializeNBT() 
	{
		CompoundTag nbt = new CompoundTag();
		HelperSerialisation.putBlockPos(nbt, "pos", destination);
		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundTag nbt) 
	{
		destination = HelperSerialisation.getBlockPos(nbt, "pos");
	}

	@Override
	public BlockPos getSpiderPosition() 
	{
		return destination;
	}

	@Override
	public boolean execute(EntityDungeonSpider spider) 
	{
		return isDone = destination.distSqr(spider.position(), false) < 1;
	}

	@Override
	public boolean canExecute(EntityDungeonSpider spider, AssignedTask assignedTask) 
	{
		return assignedTask.getAssignedSpidersCount() < 1;
	}
}
