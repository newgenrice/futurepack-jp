package futurepack.common.entity;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.ForgeMod;

public class NavigatorWallCrawler extends Navigator 
{

	public NavigatorWallCrawler(LivingEntity entity) 
	{
		super(entity);
	}

	@Override
	protected LazyAStar initPathSearch(Level w, BlockPos start, BlockPos target) 
	{
		return new WallCrawlerPathFinder(w, start, target);
	}
	
	@Override
	public boolean calcRotaion(Vec3 vec) 
	{
		super.calcRotaion(vec);
		return true;
	}
	
	@Override
	protected Vec3 getMotionToBlock() 
	{
		Vec3 entity = this.entity.position();
		Vec3 mot = Vec3.atLowerCornerOf(getTarget()).add(0.5, this.entity.getEyeHeight(), 0.5).subtract(entity);
		if(mot.lengthSqr()>0.1)
		{
			mot = mot.normalize();
		}
		double gravity = ((LivingEntity)this.entity).getAttribute(ForgeMod.ENTITY_GRAVITY.get()).getValue();
		mot = mot.add(0, -gravity, 0);
		return mot;
	}
	
	public boolean isFinished()
	{
		if(target.isEmpty())
			return true;
		
		BlockPos target = this.getTarget();
		AABB bb  =new AABB(0.4, 0.0, 0.4, 0.6, 0.2, 0.6).move(target).move(0.0, this.entity.getEyeHeight(), 0.0);		
		if(bb.contains(entity.position()))
		{
			nextTarget();
			return this.target.isEmpty();
		}
		return false;
	}
}
