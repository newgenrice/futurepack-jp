package futurepack.common.entity;

import futurepack.common.block.misc.TileEntityClaime;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.levelgen.structure.BoundingBox;

public class ClaimeData
{
	private final int x,y,z;
	final BlockPos c;
	
	private ClaimeData(int x, int y, int z, BlockPos c)
	{
		this.x=x;
		this.y=y;
		this.z=z;
		this.c = c;
	}
	
	public int getMaxY()
	{
		return y;
	}
	
	public int getMaxX()
	{
		return x;
	}
	
	public int getMaxZ()
	{
		return z;
	}
	
	public BlockPos getCurentMiddle()
	{
		return c;
	}
	
	public BoundingBox getClaimedArea()
	{
		return BoundingBox.fromCorners(c.offset(-x,-y,-z), c.offset(x,y,z));
	}
	
	public static ClaimeData getCurrentData(EntityDrone m)
	{
		TileEntityClaime clm = getCurrentTile(m);
		if(clm != null)
		{
			BlockPos c = clm.getBlockPos().offset(clm.mx,clm.my,clm.mz);				
			return new ClaimeData(clm.x,clm.y,clm.z, c);
		}
		BlockPos c = m.getInventoryPos();
		return new ClaimeData(10, 10, 10, c);
		
	}
	
	public static TileEntityClaime getCurrentTile(EntityDrone m)
	{
		String s = m.getClaime();
		if(s==null || s.equals(""))
		{
			if(m.currentDone<m.todo.size())
			{
				m.setClaime(m.todo.get(m.currentDone));
				return getCurrentTile(m);
			}
			return null;
		}
		
		TileEntityClaime.claims.removeIf(r -> r==null ||r.get()==null);
		for(int i=0;i<TileEntityClaime.claims.size();i++)
		{
			TileEntityClaime clm = TileEntityClaime.claims.get(i).get();
			if(clm != null && !clm.isRemoved() && clm.getLevel() == m.level)
			{
				if(s.equals(clm.name))
				{
					return clm;
				}
			}
		}
		if(m.currentDone<m.todo.size())
		{
			String s2 = m.todo.get(m.currentDone);
			if(!s.equals(s2))
			{
				m.setClaime(s2);
			}
			else
			{
				m.currentDone++;
			}
			return getCurrentTile(m);
		}
		else
		{
			m.setWorking(false);
			BlockPos c = m.getInventoryPos();
			m.setPos(c.getX()+0.5, c.getY()+0.2, c.getZ()+0.5);
		}
		return null;
	}
}
