package futurepack.common.entity;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import futurepack.api.ParentCoords;
import futurepack.common.entity.LazyAStar.EnumPathSearch;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Vec3i;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.MoverType;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

/**
 * A navigator for Entity
 * Only used by ForstMaster
 */
public class Navigator
{
	protected final Entity entity;
	
	protected LinkedList<BlockPos> target = new LinkedList<BlockPos>(); 
	
	protected LazyAStar searching;
	
	public Navigator(Entity entity)
	{
		this.entity = entity;
		searching = null;
	}
	
	public void addTarget(BlockPos pos)
	{
		target.addLast(pos);
	}
	
	public void move(float speed)
	{
		entity.level.getProfiler().push("move");
		
		if(isFinished())
			return; 
		
		BlockPos that = entity.blockPosition();
		BlockPos him = getTarget();
		
		if(entity.getCommandSenderWorld() instanceof ServerLevel)
			((ServerLevel)entity.getCommandSenderWorld()).sendParticles(ParticleTypes.BUBBLE_POP, him.getX()+0.5, him.getY()+0.5, him.getZ()+0.5, 0, 0, 0, 0, 0);
		
		Vec3 motion = getMotionToBlock().scale(speed);
//		if(motion.lengthSquared()<1.0e-10D)
//		{
//			isFinished();
//			motion = getMotionToBlock().scale(speed);
////			move(speed);
////			return;
//		}
		if(calcRotaion(motion))
		{
			entity.setDeltaMovement(motion);
			entity.move(MoverType.SELF, motion);
		}
		
		boolean movedHorizontal =Math.max( Math.abs(motion.x) ,  Math.abs(motion.z)) >= 0.0625*0.0625;
		boolean movedVerticaly = Math.abs(motion.y) >= 0.0625;
		
		if(movedHorizontal && entity.horizontalCollision || movedVerticaly && entity.verticalCollision)
		{
			entity.level.getProfiler().push("findPath");
			lazyAStar(motion);
			entity.level.getProfiler().pop();
		}
		entity.level.getProfiler().pop();
	}
	
	protected LazyAStar initPathSearch(Level world, BlockPos start, BlockPos target)
	{
		return new LazyAStar(world, start, target);
	}
	
	protected void lazyAStar(Vec3 motion)
	{
		if(searching == null)
		{
			searching = initPathSearch(entity.level, entity.blockPosition(), getTarget());
			searching.hitBox = entity.getBoundingBox().move(-entity.getX() +0.5, -entity.getY() +0.5-this.entity.getEyeHeight()/2, -entity.getZ() +0.5);
		}
		EnumPathSearch path = searching.searchPathWithTimeout(10);
		
		if(path == EnumPathSearch.TIMEOUT )
		{
			return;
		}
		else if(path == EnumPathSearch.START_BLOCKED)
		{
			entity.move(MoverType.SELF, motion.scale(-1));
			BlockPos pos = entity.blockPosition();
			//For Debug
			entity.getCommandSenderWorld().addParticle(ParticleTypes.BARRIER, pos.getX()+0.5F, pos.getY()+0.5F, pos.getZ()+0.5F, 0, 0, 0);
			
			
			BlockPos end = pos.relative(Direction.getNearest((float)-motion.x, (float)-motion.y, (float)-motion.z));
			searching.clear();
			searching = null;
			nextTarget();
			target.addFirst(end);
			target.addFirst(pos);
			return;
		}
		else if(path == EnumPathSearch.TARGET_BLOCKED)
		{
			//For Debug
			BlockPos p = getTarget();
			entity.getCommandSenderWorld().addParticle(ParticleTypes.BARRIER, p.getX()+0.5F, p.getY()+0.5F, p.getZ()+0.5F, 0, 0, 0);
			
			searching.clear();
			searching = null;
			nextTarget();
			return;
		}
		
		
		ParentCoords coords = searching.getFinish();
		searching.clear();
		searching = null;
		if(coords==null)
		{
			nextTarget();
		}
		BlockPos offset = new BlockPos(-10,-10,-10); 
		ParentCoords last = coords;
		while(coords!=null)
		{
			BlockPos newoff = coords.subtract(last);
			if(newoff.equals(offset))
			{												
				this.target.set(0, new BlockPos(coords));
			}
			else
			{																			
				this.target.addFirst(new BlockPos(coords));
				
				offset = newoff;
			}
			
			last = coords;
			coords = coords.getParent();
		}
		cleanPath();
	}

	
	
	
	protected Vec3 getMotionToBlock()
	{
		Vec3 entity = this.entity.position();
		Vec3 mot = Vec3.atLowerCornerOf(getTarget()).add(0.5, 0.5 -this.entity.getEyeHeight()/2, 0.5).subtract(entity);
		if(mot.lengthSqr()>0.1)
		{
			mot = mot.normalize();
		}
		return mot;
	}
	
	public boolean calcRotaion(Vec3 vec)
	{		
		final float delta = 90F / 5F;
		
		float pitch = (float) Math.toDegrees(-Math.asin(vec.y)); //neigung
		float yaw = (float) Math.toDegrees(-Math.atan2(vec.x, vec.z)); //drehwinkel
		entity.xRotO = entity.getXRot();
		entity.yRotO = entity.getYRot();
		
//		System.out.println(entity.rotationPitch + ", " + entity.rotationYaw);
		
		float dpitch = pitch - entity.getXRot();
		float dyaw = yaw - entity.getYRot();
		
		if(dyaw > 180)
			dyaw -= 360;
		if(dyaw < -180)
			dyaw += 360;
		
		if(dpitch > delta)
			dpitch = delta;
		if(dpitch < -delta)
			dpitch = -delta;
		
		if(dyaw > delta)
			dyaw = delta;
		if(dyaw < -delta)
			dyaw = -delta;
		
		entity.setXRot(entity.getXRot() + dpitch);
		entity.setYRot(entity.getYRot() + dyaw);
		
		if(entity.getYRot() > 180)
			entity.setYRot(entity.getYRot() - 360);
		if(entity.getYRot() < -180)
			entity.setYRot(entity.getYRot() + 360);
		
//		System.out.println(entity.rotationPitch + ", " + entity.rotationYaw);
		
		return dyaw <= 0.1 && dpitch <= 0;
		
	}
	
	public boolean calcRotaion(Vec3i vec)
	{
		return calcRotaion(Vec3.atLowerCornerOf(vec).add(0.5, 0.5-this.entity.getEyeHeight()/2, 0.5));
	}
	
	public boolean isFinished()
	{
		if(target.isEmpty())
			return true;
		
		BlockPos target = this.getTarget();
		AABB bb  =new AABB(0.4, 0.0, 0.4, 0.6, 0.2, 0.6).move(target).move(0.0, 0.4 -this.entity.getEyeHeight()/2, 0.0);		
		if(bb.contains(entity.position()))
		{
			nextTarget();
			return this.target.isEmpty();
		}
		return false;
	}

	public BlockPos getTarget()
	{
		if(target.isEmpty())
			return null;
		return target.getFirst();
	}
	
	public void nextTarget()
	{
		if(!target.isEmpty())
		{
			target.removeFirst();
		}
	}
	
	public void clear()
	{
		target.clear();
	}

	public void setPath(Collection<BlockPos> path)
	{
		target = new LinkedList<BlockPos>(path);
	}
	
	public BlockPos getFinish()
	{
		if(target.isEmpty())
			return null;
		return target.getLast();
	}
	
	/**Removed double entryis*/
	protected void cleanPath()
	{
		Iterator<BlockPos> iter = target.iterator();
		BlockPos last = null;
		while(iter.hasNext())
		{
			BlockPos pos = iter.next();
			if(pos.equals(last))
			{
				iter.remove();
			}
			else
			{
				last = pos;
			}
		}
	}
}
