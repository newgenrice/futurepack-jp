package futurepack.common.entity.throwable;

import futurepack.common.FPEntitys;
import futurepack.common.item.ResourceItems;
import net.minecraft.network.protocol.Packet;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.projectile.ItemSupplier;
import net.minecraft.world.entity.projectile.ThrowableProjectile;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.HitResult.Type;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fmllegacy.network.NetworkHooks;
@OnlyIn(
	value = Dist.CLIENT,
	_interface = ItemSupplier.class
)
public class EntityWakurumIngot extends ThrowableProjectile implements ItemSupplier
{
	public EntityWakurumIngot(EntityType<EntityWakurumIngot> type, Level w)
	{
		super(type, w);
	}

	public EntityWakurumIngot(Level w, LivingEntity throwerIn)
	{
		super(FPEntitys.WAKURIUM_THROWN, throwerIn, w);
	}

	@Override
	protected void onHit(HitResult result)
	{
		if(result.getType() ==Type.BLOCK)
		{
			BlockHitResult block = (BlockHitResult) result;
			BlockState state = level.getBlockState(block.getBlockPos());
			if(!state.getCollisionShape(level, block.getBlockPos()).isEmpty())
			{
				Vec3 mot = this.getDeltaMovement();
				switch(block.getDirection().getAxis())
				{
				case X:
					setDeltaMovement(mot.multiply(-0.95, 1, 1));
					break;
				case Y:
					setDeltaMovement(mot.multiply(1, -0.95, 1));
					break;
				case Z:
					setDeltaMovement(mot.multiply(1, 1, -0.95));
					break;
				default:
					break;		
				}
				setPos(block.getLocation().x, block.getLocation().y, block.getLocation().z);
			}		
		}
		
		if(result.getType() == Type.ENTITY)
		{
			((EntityHitResult)result).getEntity().hurt(DamageSource.mobAttack((LivingEntity) getOwner()), 1.8F);
		}
		else if(!level.isClientSide)
		{
			double dis = this.getDeltaMovement().lengthSqr();
			
			if(dis<0.1)
			{
				discard();
				ItemEntity item = new ItemEntity(level, getX(), getY(), getZ(), new ItemStack(ResourceItems.ingot_wakurium));
				item.setDeltaMovement(this.getDeltaMovement());
				level.addFreshEntity(item);
			}
		}
	}

	@Override
	protected void defineSynchedData() 
	{
		
	}

	@Override
	public ItemStack getItem() 
	{
		return new ItemStack(ResourceItems.ingot_wakurium);
	}

	@Override
	public Packet<?> getAddEntityPacket() 
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
}
