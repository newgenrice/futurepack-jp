package futurepack.common.entity.throwable;

import com.mojang.math.Vector3f;

import futurepack.common.FPEntitys;
import net.minecraft.core.particles.DustParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.network.protocol.Packet;
import net.minecraft.util.Mth;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.projectile.ThrowableProjectile;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.HitResult.Type;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.fmllegacy.network.NetworkHooks;

public class EntityLaserProjectile extends ThrowableProjectile
{

	public EntityLaserProjectile(Level worldIn, LivingEntity th)
	{
		super(FPEntitys.LASER_PROJECTILE, th, worldIn);
	}
	
	public EntityLaserProjectile(EntityType<EntityLaserProjectile> type, Level w)
	{
		super(type, w);
	}

	@Override
	protected void onHit(HitResult pos)
	{
		if(pos.getType() == Type.BLOCK)
		{
			BlockHitResult bl = (BlockHitResult) pos;
			if(level.isClientSide)
			{
				float[] vel = new float[3]; 
				
				vel[0] = 0.15F * bl.getDirection().getStepX() * random.nextFloat();
				vel[1] = 0.15F * bl.getDirection().getStepY() * random.nextFloat();		
				vel[2] = 0.15F * bl.getDirection().getStepZ() * random.nextFloat();

				for(int i=0;i<vel.length;i++)
				{
					if(vel[i]==0)
					{
						vel[i] = random.nextFloat() * 0.05F;
					}
				}
				
				level.addParticle(ParticleTypes.FLAME, bl.getLocation().x, bl.getLocation().y, bl.getLocation().z, vel[0], vel[1], vel[2]);
//				worldObj.addParticle(ParticleTypes.FLAME, pos.hitVec.xCoord, pos.hitVec.yCoord, pos.hitVec.zCoord, vel[3], vel[4], vel[5]);
			}
		}
		else if(pos.getType() == Type.ENTITY)
		{
			EntityHitResult ent = (EntityHitResult) pos;
			if(ent.getEntity() == getOwner())
				return ;
			
			ent.getEntity().hurt(DamageSource.mobAttack((LivingEntity) getOwner()), 18);
		}
		if(!level.isClientSide)
			this.discard();
	}

	@Override
	public void tick()
	{		
		Vec3 m = this.getDeltaMovement();
		double mx = m.x;
		double my = m.y;
		double mz = m.z;
		
		level.addParticle(new DustParticleOptions(new Vector3f(1F, 0F, 0F), 1F), getX(), getY(), getZ(), 0,0,0);
		
		super.tick();
		
		float f1 = (float) Math.sqrt(mx * mx + mz * mz);
		this.setYRot(this.yRotO =  (float)(Mth.atan2(mx, mz) * 180.0D / Math.PI));
		this.setXRot(this.xRotO =  (float)(Mth.atan2(my, f1) * 180.0D / Math.PI));		
        
		this.setDeltaMovement(m);
	}
	
	@Override
	public float getBrightness()
	{
		return level.getRawBrightness(blockPosition(), 15);
	}

	@Override
	protected void defineSynchedData() 
	{
		
	}
	
	@Override
	public Packet<?> getAddEntityPacket() 
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
	
}
