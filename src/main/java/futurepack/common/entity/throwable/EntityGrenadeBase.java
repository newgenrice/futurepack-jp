package futurepack.common.entity.throwable;

import java.util.List;
import java.util.Optional;

import futurepack.common.FPEntitys;
import futurepack.common.item.misc.ItemKompost;
import futurepack.common.item.misc.MiscItems;
import futurepack.common.item.tools.ToolItems;
import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtUtils;
import net.minecraft.network.protocol.Packet;
import net.minecraft.util.Mth;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.ItemSupplier;
import net.minecraft.world.entity.projectile.ThrowableProjectile;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Explosion.BlockInteraction;
import net.minecraft.world.level.GameRules;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.HitResult.Type;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fmllegacy.network.NetworkHooks;

public abstract class EntityGrenadeBase extends ThrowableProjectile
{
	private int radius = 1;

	protected EntityGrenadeBase(EntityType<? extends EntityGrenadeBase> type, Level worldIn, LivingEntity th, int size)
	{
		super(type, th, worldIn);
		this.radius = size;
	}
	
	protected EntityGrenadeBase(EntityType<? extends EntityGrenadeBase> type, Level worldIn)
	{
		super(type, worldIn);
	}

	public void setSize(int size)
	{
		if(level.isClientSide)
			throw new IllegalStateException("This shouldbe called only on Server side");
		
		this.radius = size;
	}
	
	public void setThrower(LivingEntity th)
	{
		if(level.isClientSide)
			throw new IllegalStateException("This shouldbe called only on Server side");
		
		CompoundTag nbt = new CompoundTag();
		addAdditionalSaveData(nbt);
		nbt.put("owner", NbtUtils.createUUID(th.getUUID()));
		readAdditionalSaveData(nbt);
//		this.owner = th;
		//FIXME: maxbe the missing owner is important
		this.setPos(th.getX(), th.getY() + th.getEyeHeight() - 0.1F, th.getZ());
	}
	
	@Override
	protected abstract void onHit(HitResult mov);
	
	@Override
	protected void defineSynchedData() 
	{

	}
	
	@Override
	public Packet<?> getAddEntityPacket() 
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
	
	@OnlyIn(
		value = Dist.CLIENT,
		_interface = ItemSupplier.class
	)
	public static class Plasma extends EntityGrenadeBase implements ItemSupplier
	{
		public Plasma(Level worldIn, LivingEntity throwerIn, int size)
		{
			super(FPEntitys.GRENADE_PLASMA, worldIn, throwerIn, size);
		}
		
		public Plasma(EntityType<Plasma> type, Level worldIn)
		{
			super(type, worldIn);
		}

		@Override
		protected void onHit(HitResult mov)
		{
			if (!this.level.isClientSide)
	        {
	            this.level.explode(this, this.getX(), this.getY(), this.getZ(), 6F * super.radius, BlockInteraction.NONE);
	            this.discard();
	        }
		}

		@Override
		public ItemStack getItem() 
		{
			return new ItemStack(MiscItems.icon_plasma);
		}
	}
	
	public static class Normal extends EntityGrenadeBase
	{
		public Normal(Level worldIn, LivingEntity throwerIn, int size)
		{
			super(FPEntitys.GRENADE_NORMAL, worldIn, throwerIn, size);
		}
		
		public Normal(EntityType<Normal> type, Level worldIn)
		{
			super(type, worldIn);
		}

		@Override
		protected void onHit(HitResult mov)
		{
			if (!this.level.isClientSide)
	        {
	            boolean flag = this.level.getGameRules().getBoolean(GameRules.RULE_MOBGRIEFING);
	            this.level.explode(this, this.getX(), this.getY(), this.getZ(), 3F * super.radius, flag ? BlockInteraction.BREAK : BlockInteraction.NONE);
	            this.discard();
	        }
		}
		
		@Override
		public void tick()
		{		
			double f1 = this.getDeltaMovement().length();
			double mx = this.getDeltaMovement().x;
			double mz = this.getDeltaMovement().z;
			double my = this.getDeltaMovement().y;
			this.setYRot(this.yRotO = (float)(Mth.atan2(mx, mz) * 180.0D / Math.PI));
			this.setXRot(this.xRotO = (float)(Mth.atan2(my, f1) * 180.0D / Math.PI));		
	        
			super.tick();
		}
	}
	
	@OnlyIn(
		value = Dist.CLIENT,
		_interface = ItemSupplier.class
	)
	public static class Blaze extends EntityGrenadeBase implements ItemSupplier
	{
		public Blaze(Level worldIn, LivingEntity throwerIn, int size)
		{
			super(FPEntitys.GRENADE_BLAZE, worldIn, throwerIn, size);
		}
		
		public Blaze(EntityType<Blaze> type, Level worldIn)
		{
			super(type, worldIn);
		}

		@Override
		protected void onHit(HitResult mov)
		{
			if (!this.level.isClientSide)
	        {
				int r=3 * super.radius;
	            this.level.explode(this, this.getX(), this.getY(), this.getZ(), 2F * super.radius, true, BlockInteraction.NONE);
	            for(int x=-r;x<r;x++)
	            {
	            	for(int y=-r;y<r;y++)
	 	            {
	            		for(int z=-r;z<r;z++)
	     	            {
	            			BlockPos pos = new BlockPos(mov.getLocation()).offset(x, y, z);
	            			if(level.isEmptyBlock(pos))
	            			{
	            				level.setBlockAndUpdate(pos, Blocks.FIRE.defaultBlockState());
	            			}
	     	            }	
	 	            }	
	            }
	            AABB bb =   new AABB(mov.getLocation().add(-r, -r, -r), mov.getLocation().add(r, r, r));
	            List<LivingEntity> list = level.getEntitiesOfClass(LivingEntity.class, bb);
	            for(LivingEntity liv : list)
	            {
	            	liv.setSecondsOnFire(100);	
	            }
	            this.discard();
	        }
		}

		@Override
		public ItemStack getItem() 
		{
			return new ItemStack(MiscItems.icon_blaze);
		}
	}
	
	@OnlyIn(
		value = Dist.CLIENT,
		_interface = ItemSupplier.class
	)
	public static class Slime extends EntityGrenadeBase implements ItemSupplier
	{
		public Slime(Level worldIn, LivingEntity throwerIn, int size)
		{
			super(FPEntitys.GRENADE_SLIME, worldIn, throwerIn, size);
		}
		
		public Slime(EntityType<Slime> type, Level worldIn)
		{
			super(type, worldIn);
		}

		@Override
		protected void onHit(HitResult mov)
		{
			if(tickCount<20 && mov.getType()==Type.ENTITY && level.isClientSide && ((EntityHitResult)mov).getEntity() instanceof Player)
				return;
				
			int r = 3 * super.radius;
			if (!this.level.isClientSide)
	        {
				AABB bb =   new AABB(mov.getLocation().add(-r, -r, -r), mov.getLocation().add(r, r, r));
	            List<LivingEntity> list = level.getEntitiesOfClass(LivingEntity.class, bb);
	            for(LivingEntity liv : list)
	            {
	            	slowDown(liv);	
	            }

	            
	        }
			else 
			{	
				for(int x=-r;x<r;x++)
	            {
	            	for(int y=-r;y<r;y++)
	 	            {
	            		for(int z=-r;z<r;z++)
	     	            {
//	            			try {
	            				level.addParticle(ParticleTypes.ITEM_SLIME, true, getX()+x+0.5, getY()+y+0.5, getZ()+z+0.5, 0, 0, 0);
//	            			} catch(NoSuchMethodError e) {
//	            				e.printStackTrace();
//	            			}
	     	            }	
	 	            }	
	            }
            }
			this.discard();
		}
		
//		@Override
//		public void discard()
//		{
//			int r = 3;
//			if (this.level.isClientSide)
//			{	
//				for(int x=-r;x<r;x++)
//	            {
//	            	for(int y=-r;y<r;y++)
//	 	            {
//	            		for(int z=-r;z<r;z++)
//	     	            {
//	            			try {
//	            				level.addParticle(ParticleTypes.ITEM_SLIME, true, getX()+x+0.5, getY()+y+0.5, getZ()+z+0.5, 0, 0, 0);
//	            			} catch(NoSuchMethodError e) {
//	            				e.printStackTrace();
//	            			}
//	     	            }	
//	 	            }	
//	            }
//            }
//			super.discard();
//		}
		
		private void slowDown(LivingEntity base)
		{
			MobEffectInstance eff = new MobEffectInstance(MobEffects.MOVEMENT_SLOWDOWN, 20*30, 4);
			base.addEffect(eff);
		}

		@Override
		public ItemStack getItem() 
		{
			return new ItemStack(MiscItems.icon_slime);
		}
	}

	public static class Kompost extends EntityGrenadeBase
	{
		public Kompost(Level w, LivingEntity thrower, int size)
		{
			super(FPEntitys.GRENADE_KOMPOST, w, thrower, size);
		}
		
		public Kompost(EntityType<Kompost> type, Level worldIn)
		{
			super(type, worldIn);
		}

		@Override
		protected void onHit(HitResult mov)
		{
			if(getOwner() instanceof Player)
			{
				Player pl = (Player) getOwner();
			
				if(mov.getType() == HitResult.Type.BLOCK)
				{
					int r = super.radius;
					for(int x=-r;x<r;x++)
					{
						for(int z=-r;z<r;z++)
						{
							for(int y=-r;y<r;y++)
							{
								if(r*r >= x*x + z*z +y*y)
								{
									ItemKompost.applyCompost(new ItemStack(MiscItems.kompost, 2), level, ((BlockHitResult)mov).getBlockPos().offset(x,y,z), pl);
								}
							}
							
						}
					}
					this.discard();
				}
			}
			
		}
	}

	public static class Futter extends EntityGrenadeBase
	{
		public Futter(Level worldIn, LivingEntity throwerIn, int size)
		{
			super(FPEntitys.GRENADE_FUTTER, worldIn, throwerIn, size);
		}
		
		public Futter(EntityType<Futter> type, Level worldIn)
		{
			super(type, worldIn);
		}

		@Override
		protected void onHit(HitResult mov)
		{
			if(!level.isClientSide)			
			{
				int r = 3 * super.radius;
				int animalsNum = 9 * super.radius;
				AABB bb =   new AABB(mov.getLocation().add(-r, -r, -r), mov.getLocation().add(r, r, r));
				List<Animal> list = this.level.getEntitiesOfClass(Animal.class, bb);
				for (Animal anim : list)
				{
					if(animalsNum>0)
					{
						if(anim.getAge()==0 && !anim.isInLove())
						{
							animalsNum--;
							if(getOwner() instanceof Player)
								anim.setInLove((Player) getOwner());
							else 
								anim.setInLove(null);
						}
					}
					else
					{
						break;
					}
				}	
				discard();
			}	
		}
	}

	public static class Saat extends EntityGrenadeBase
	{
		public Saat(Level worldIn, LivingEntity throwerIn, int size)
		{
			super(FPEntitys.GRENADE_SAAT, worldIn, throwerIn, size);
		}
		
		public Saat(EntityType<Saat> type, Level worldIn)
		{
			super(type, worldIn);
		}

		@Override
		protected void onHit(HitResult mov)
		{
			if(!level.isClientSide && mov.getType() == HitResult.Type.BLOCK)
			{
				int r = 1 * super.radius;
				for(int x=-r;x<=r;x++)
				{
					for(int z=-r;z<=r;z++)
					{
						for(int y=-1;y<=r;y++)
						{
							BlockState state = Blocks.WHEAT.defaultBlockState();
							BlockPos pos = ((BlockHitResult)mov).getBlockPos().offset(x,y,z);
							if(state.canSurvive(level, pos))
							{
								level.setBlockAndUpdate(pos, state);
								break;
							}		
						}
					}
				}
				discard();
			}
		}		
	}	
	
	@OnlyIn(
		value = Dist.CLIENT,
		_interface = ItemSupplier.class
	)
	public static class EnityEggerFullGranade extends EntityGrenadeBase implements ItemSupplier
	{
		
		private CompoundTag enity_tag;
		
		public EnityEggerFullGranade(Level worldIn, LivingEntity throwerIn, CompoundTag nbt)
		{
			super(FPEntitys.GRENADE_ENTITY_EGGER, worldIn, throwerIn, 1);
			enity_tag = nbt;
		}
		
		public EnityEggerFullGranade(EntityType<EnityEggerFullGranade> type, Level worldIn)
		{
			super(type, worldIn);
		}

		@Override
		protected void onHit(HitResult mov)
		{
			if(enity_tag == null)
			{
				discard();
				return;
			}
			
			if(mov.getType() == HitResult.Type.BLOCK || mov.getType() == HitResult.Type.ENTITY)
			{
				Optional<Entity> o = EntityType.create(enity_tag, this.level);
				o.ifPresent(e -> 
				{
					if(e instanceof Mob)
					{
						Mob liv = (Mob) e;
						e.setPos(mov.getLocation().x, mov.getLocation().y + liv.getMyRidingOffset(), mov.getLocation().z);
						e.setUUID(Mth.createInsecureUUID(this.random));
						level.addFreshEntity(e);
					}
				});
				discard();
			}
		}
		
		@Override
		public ItemStack getItem() 
		{
			return new ItemStack(ToolItems.entity_egger_full);
		}
	}
}
