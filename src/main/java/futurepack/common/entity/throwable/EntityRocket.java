package futurepack.common.entity.throwable;

import java.util.Random;
import java.util.UUID;

import javax.annotation.Nullable;

import futurepack.common.FPEntitys;
import futurepack.common.player.FakePlayerFactory;
import futurepack.common.player.FuturepackPlayer;
import futurepack.common.potions.FPPotions;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.Mth;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.AreaEffectCloud;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.projectile.ThrowableProjectile;
import net.minecraft.world.item.alchemy.Potion;
import net.minecraft.world.level.Explosion;
import net.minecraft.world.level.GameRules;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.HitResult.Type;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.fmllegacy.network.NetworkHooks;

public class EntityRocket extends ThrowableProjectile
{
	private Entity cache=null;
	protected static final EntityDataAccessor<Integer> TARGET = SynchedEntityData.defineId(EntityRocket.class, EntityDataSerializers.INT);
	
	public boolean doPlayerDamage = false;
	public Vec3 doTransport = null;

	private double startDis = -1;
	private Vec3 startVec = null;
	
	public EntityRocket(EntityType<? extends EntityRocket> type, Level worldIn)
	{
		super(type, worldIn);
	}
	
	public EntityRocket(EntityType<? extends EntityRocket> type, Level w, LivingEntity base)
	{
		super(type, base, w);
		setTarget(base);
	}

	@Override
	protected void defineSynchedData()
	{
		this.entityData.define(TARGET, -1);
	}
	
	public void setTarget(Entity e)
	{
		cache=e;
		this.entityData.set(TARGET, e.getId());
	}

	
	public Entity getTarget()
	{
		if(cache==null)
		{
			int id = this.entityData.get(TARGET);
			this.cache = level.getEntity(id);
		}
		return cache;
	}
	
	@Override
	public void readAdditionalSaveData(CompoundTag nbt)
	{
		if(nbt.contains("target"))
		{
			UUID uuid = nbt.getUUID("target");
			ServerLevel ws = (ServerLevel) level;
			setTarget(ws.getEntity(uuid));
		}
		
	}

	@Override
	public void addAdditionalSaveData(CompoundTag nbt)
	{
		if(getTarget()!=null)
		{
			Entity e = getTarget();
			nbt.putUUID("target", e.getUUID());
		}
		
	}

	@Override
	public void tick()
	{
		//Kill Rockets that are to long alive, to make the bug that rockets "dance" and not despawn less visible
		if(this.tickCount > 250) {
			this.discard();
		}
		//if(this.ticksExisted>5)
		//	return;
		
		Entity target = getTarget();
		if(target != null)
		{
			if(!target.isAlive())
				this.discard();
			
			if(startDis < 0)
			{
				startDis = this.distanceToSqr(target);
				if(startDis < 100)
					startDis = 100;
				Random r = new Random(this.getUUID().getMostSignificantBits());
				startVec = (new Vec3(r.nextDouble()-0.5, r.nextDouble()-0.5, r.nextDouble()-0.5)).normalize().scale(5);
			}
			Vec3 Vector3d = new Vec3(target.getX(),target.getY()+target.getEyeHeight(),target.getZ());
			
			if(startVec!=null && this.distanceToSqr(target) > startDis / 4)
			{
				Vector3d = Vector3d.add(startVec);
			}
			
			Vec3 vec = Vector3d.subtract(getX(), getY(), getZ());
			if(vec.lengthSqr() > 1)
				vec = vec.normalize();
			
			Vec3 mot = this.getDeltaMovement();
			mot = mot.scale(2).add(vec).scale(1/3F);
			this.setDeltaMovement(mot);
			
			AABB box = target.getBoundingBox();
			if(box.contains(this.position()))
			{
				onHit(new EntityHitResult(target, this.position()));
			}
		}
		else {
			this.discard();
		}
		super.tick();
		
		
		
		Vec3 mot = this.getDeltaMovement();
		double mx = mot.x;
		double my = mot.y;
		double mz = mot.z;	
		if(level.isClientSide)
		{
			level.addParticle(ParticleTypes.CLOUD, getX(), getY(), getZ(), -mx*0.2, -my*0.2, -mz*0.2);
			level.addParticle(ParticleTypes.CLOUD, getX(), getY(), getZ(), 0, 0, 0);
			level.addParticle(ParticleTypes.CLOUD, getX(), getY(), getZ(), -mx*0.1, -my*0.1, -mz*0.1);
		}
		double f1 = Math.sqrt(mx * mx + mz * mz);
		this.setYRot(this.yRotO = (float)(Mth.atan2(mx, mz) * 180.0D / Math.PI));
		this.setXRot(this.xRotO = (float)(Mth.atan2(my, f1) * 180.0D / Math.PI));	
		
	}

	@Override
	protected void onHit(HitResult mov)
	{
		if(!level.isClientSide)
		{
			Entity e = this;
			if(doPlayerDamage)
			{
				FuturepackPlayer player = FakePlayerFactory.INSTANCE.getPlayer((ServerLevel) level);
				player.setPos(this.getX(), this.getY(), this.getZ());
				e = player;
			}
			attackTarget(e, mov);
			if(mov.getType() == Type.ENTITY)
			{
				((EntityHitResult)mov).getEntity().setDeltaMovement(this.getDeltaMovement());
				
				if(doTransport!=null)
				{
					HelperItems.moveItemTo(level, doTransport, ((EntityHitResult)mov).getEntity().position());
				}
			}
			
			this.discard();
		}
	}
	
	public void attackTarget(Entity damagingEntity, HitResult mov)
	{
		Vec3 hitVec = mov.getLocation();
		level.explode(damagingEntity, hitVec.x, hitVec.y, hitVec.z, 1.0F, level.getGameRules().getBoolean(GameRules.RULE_MOBGRIEFING) ? Explosion.BlockInteraction.BREAK : Explosion.BlockInteraction.NONE);
	}
	
	public static class EntityPlasmaRocket extends EntityRocket
	{

		public EntityPlasmaRocket(Level w, LivingEntity base)
		{
			super(FPEntitys.ROCKET_PLASMA, w, base);
		}
		
		public EntityPlasmaRocket(EntityType<EntityPlasmaRocket> type, Level worldIn)
		{
			super(type, worldIn);
		}
		
		@Override
		public void attackTarget(@Nullable Entity damagingEntity, HitResult mov)
		{
			Vec3 hitVec = mov.getLocation();
			level.explode(damagingEntity, hitVec.x, hitVec.y, hitVec.z, 2.0F, Explosion.BlockInteraction.NONE);
		}
	}
	
	public static class EntityBlazeRocket extends EntityRocket
	{

		public EntityBlazeRocket(Level w, LivingEntity base)
		{
			super(FPEntitys.ROCKET_BLAZE, w, base);
		}
		
		public EntityBlazeRocket(EntityType<EntityBlazeRocket> type, Level worldIn)
		{
			super(type, worldIn);
		}
		
		@Override
		public void attackTarget(@Nullable Entity damagingEntity, HitResult mov)
		{
			Vec3 hitVec = mov.getLocation();
			level.explode(damagingEntity, hitVec.x, hitVec.y, hitVec.z, 2.0F, true, Explosion.BlockInteraction.NONE);
		}
	}
	
	public static class EntityBioteriumRocket extends EntityRocket
	{

		public EntityBioteriumRocket(Level w, LivingEntity base)
		{
			super(FPEntitys.ROCKET_BIOTERIUM, w, base);
		}
		
		public EntityBioteriumRocket(EntityType<EntityBioteriumRocket> type, Level worldIn)
		{
			super(type, worldIn);
		}
		
		@Override
		public void attackTarget(Entity damagingEntity, HitResult mov)
		{
			if(mov.getType() == Type.ENTITY && ((EntityHitResult)mov).getEntity() instanceof LivingEntity)
			{
				LivingEntity liv = (LivingEntity) ((EntityHitResult)mov).getEntity();
				liv.addEffect(new MobEffectInstance(FPPotions.PARALYZED, 20 * 10, 2));
			}
			AreaEffectCloud cloud = new AreaEffectCloud(level, mov.getLocation().x, mov.getLocation().y, mov.getLocation().z);
			cloud.setOwner((LivingEntity) this.getOwner());
			cloud.setDuration(20  * 45);
			cloud.setRadius(6.0F);
			cloud.setRadiusOnUse(-0.4F);
			cloud.setWaitTime(10);
	        cloud.setRadiusPerTick((-cloud.getRadius() / cloud.getDuration()) * 0.5F);
	        cloud.setPotion(new Potion("Death", new MobEffectInstance(MobEffects.POISON, 20 * 20, 3)));
	        level.addFreshEntity(cloud);
	        Vec3 hitVec = mov.getLocation();
			level.explode(damagingEntity, hitVec.x, hitVec.y, hitVec.z, 2.0F, false, Explosion.BlockInteraction.NONE);
		}
	}
	
	public static class EntityNormalRocket extends EntityRocket
	{

		public EntityNormalRocket(EntityType<EntityNormalRocket> type, Level worldIn)
		{
			super(type, worldIn);
		}
		
		public EntityNormalRocket(Level w, LivingEntity base)
		{
			super(FPEntitys.ROCKET_NORMAL, w, base);
		}
	}
	
	@Override
	public Packet<?> getAddEntityPacket() 
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
}
