package futurepack.common.entity.throwable;


import java.util.ArrayList;
import java.util.List;

import futurepack.common.FPEntitys;
import futurepack.common.item.tools.ItemGrablingHook;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageHookLines;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.projectile.ItemSupplier;
import net.minecraft.world.entity.projectile.ThrowableProjectile;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.HitResult.Type;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fmllegacy.network.NetworkHooks;
import net.minecraftforge.fmllegacy.network.PacketDistributor;
@OnlyIn(
	value = Dist.CLIENT,
	_interface = ItemSupplier.class
)
public class EntityHook extends ThrowableProjectile implements ItemSupplier
{
	private static final EntityDataAccessor<Integer> ACTIVE = SynchedEntityData.defineId(EntityHook.class, EntityDataSerializers.INT);
//	private static final DataParameter<Integer> THROWER = EntityDataManager.createKey(EntityHook.class, DataSerializers.VARINT);
//	
	boolean collided = false; // <-- needs to be synced
	
	private LivingEntity clientThrower;
	private List<Vec3> list = null;
	
	public EntityHook(Level w, LivingEntity lb)
	{
		super(FPEntitys.HOOK, lb, w);
		
		this.setDeltaMovement(this.getDeltaMovement().add(lb.getDeltaMovement()));

		if(!level.isClientSide)
			list = new ArrayList<>();
	}

	public EntityHook(EntityType<EntityHook> type, Level p_i1776_1_) 
	{
		super(type, p_i1776_1_);
		if(!level.isClientSide)
			list = new ArrayList<>();
	}

	@Override
	protected void defineSynchedData()
	{
		this.entityData.define(ACTIVE, -1);
//		super.registerData();
	}
	
	
	
	@Override
	public void shoot(double d1, double d2, double d3, float p_70186_7_, float p_70186_8_)
    {
		double f2 = Math.sqrt(d1 * d1 + d2 * d2 + d3 * d3);
		 d1 /= f2;
		 d2 /= f2;
		 d3 /= f2;
//		 d1 += this.rand.nextGaussian() * 0.007499999832361937D * (double)p_70186_8_;
//		 d2 += this.rand.nextGaussian() * 0.007499999832361937D * (double)p_70186_8_;
//		 d3 += this.rand.nextGaussian() * 0.007499999832361937D * (double)p_70186_8_;
		 d1 *= p_70186_7_;
		 d2 *= p_70186_7_;
		 d3 *= p_70186_7_;
		 this.setDeltaMovement(new Vec3(d1*2.5, d2*2.5, d3*2.5));
		 double f3 = Math.sqrt(d1 * d1 + d3 * d3);
		 this.setYRot(this.yRotO = (float)(Math.atan2(d1, d3) * 180.0D / Math.PI));
		 this.setXRot(this.xRotO = (float)(Math.atan2(d2, f3) * 180.0D / Math.PI));
		 
    }
	
	
	
	@Override
	protected void onHit(HitResult mov) 
	{
		if(level.isClientSide && mov.getType() == Type.BLOCK)
		{
			this.setDeltaMovement(Vec3.ZERO);
		}
		if(getOwner()==null)
		{
			return;
		}
		if(mov.getType() == Type.BLOCK)
		{
			this.setDeltaMovement(Vec3.ZERO);
			
			Vec3 hitVec = ((BlockHitResult)mov).getLocation();
			Vec3 vec = hitVec.add(-this.getX(), -this.getY(), -this.getZ());
			double l = vec.length();
			
			this.setPos(hitVec.x - (vec.x/l * 0.0625), hitVec.y - (vec.y/l * 0.0625), hitVec.z - (vec.z/l * 0.0625));

			collided = true;
			sync();
		}

	}
	
	private void sync()
	{
		if(!level.isClientSide)
		{
			ServerPlayer mp = (ServerPlayer) getOwner();
			MessageHookLines mes = new MessageHookLines(this.getId(), mp.getId(), list);
			FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with(() -> mp), mes);
		}
	}
	
	@Override
	public void tick() 
	{
		super.tick(); 

		if(collided)
		{
			this.setDeltaMovement(Vec3.ZERO);

			Entity thrower = getOwner();
			if(thrower == null)
			{
				//this happens when e.g. you are atached and the player si teleportewt away.
				discard();
			}
			thrower.fallDistance = 0F;
//			List<EntityPlayer> players = worldObj.getEntitiesWithinAABB(EntityPlayer.class, AxisAlignedBB.fromBounds(this.getPosX()-0.5, this.getPosY()-0.5, this.getPosZ()-0.5, this.getPosX()+0.5, this.getPosY()+0.5, this.getPosZ()+0.5));
//			if(!players.contains(throrer))
			{
				
				Vec3 pos = getFirstRopePart();
				
				level.addParticle(ParticleTypes.CRIT, pos.x, pos.y, pos.z, 0F, 0F, 0F);
				
				
				
				double d = Math.sqrt(pos.distanceToSqr(thrower.getX(), thrower.getY(), thrower.getZ()));
				
				if(!level.isClientSide && d <= thrower.getBbWidth() && getRopeParts() > 0)
				{
					removeFirstRopePart();
				}
					
				d = Math.sqrt(pos.distanceToSqr(thrower.getX(), thrower.getY()-thrower.getEyeHeight(), thrower.getZ()));
				d = d < 1 ? 1 : d;
				double x = pos.x - thrower.getX();
				double y = pos.y - (thrower.getY());//-throrer.getStandingEyeHeight());
				double z = pos.z - thrower.getZ();

				thrower.setDeltaMovement(new Vec3(x/d, y/d, z/d));
			}
			
			if(thrower.isAlive()==false)
				discard();
		}
		else if(!level.isClientSide)
		{
			if(getOwner()==null)
			{
				discard();
				return;
			}
			subdivideLine();
		}
		if(!level.isClientSide)
		{
			EntityHook other = ((ItemGrablingHook )ToolItems.grappling_hook).map(level).get(getOwner());
			if(other != this)
			{
				this.discard();
			}
		}
	}	
	
	private void removeFirstRopePart()
	{
		if(!level.isClientSide)
		{
			list.remove(0);
			int active = entityData.get(ACTIVE);
			active++;
			entityData.set(ACTIVE,active);
		}
	}

	private void subdivideLine()
	{
		Vec3 a = this.position();
		
		Vec3 b = null;
		if(getRopeParts() == 0)
		{
			b = getOwner().position().add(0, getOwner().getEyeHeight(), 0);
		}
		else
		{
			b = list.get(list.size()-1);
		}
		
		BlockHitResult ray = level.clip(new ClipContext(b, a, ClipContext.Block.COLLIDER, ClipContext.Fluid.NONE, getOwner()));
		if(ray!=null && ray.getType()==Type.BLOCK)
		{
			Vec3 hit = ray.getLocation();
			
//			Vector3d ah = hit.subtract(a);
//			Vector3d bh = hit.subtract(b);
//			
//			Vector3d away = ah.add(bh).normalize();
			
			Vec3 point = hit;
			point = point.add(Vec3.atLowerCornerOf(ray.getDirection().getNormal()));
			
			addRopePart(point);
		}
	}
	
	private int getRopeParts()
	{
		if(list==null)
			return -1;
		
		return list.size();
	}
	
	private void addRopePart(Vec3 vec)
	{
		if(level.isClientSide)
			return;
		
		list.add(vec);
	}
	
	private Vec3 getFirstRopePart()
	{
		if(level.isClientSide)
		{
			int active = this.entityData.get(ACTIVE);
			if(list!=null && active >= 0 && active < list.size())
			{
				return list.get(active);
			}
			else
			{
				list = null;
			}
			return position();
		}
		else
		{
			if(list.isEmpty())
			{	
				return position();
			}
			else
			{
				return list.get(0);
			}
		}
	}
	
	
	
	@Override
	public Entity getOwner()
	{
		if(level.isClientSide)
			return getClientThrower();
		return super.getOwner();
	}
	
	public LivingEntity getClientThrower()
	{
		return clientThrower;
	}
	
	public void setThrower(int id) //client only
	{
		if(level.isClientSide)
		{
			this.clientThrower = (LivingEntity) level.getEntity(id);
		}
	}
	
	public void setList(List<Vec3> list) //client Only
	{
		this.list = list;
		for(Vec3 vec : list)
		{
			level.addParticle(ParticleTypes.BARRIER, vec.x, vec.y, vec.z, 0F, 0F, 0F);
		}
		collided = true;
	}

	@Override
	public ItemStack getItem() 
	{
		return new ItemStack(Items.SLIME_BALL);
	}
	
	@Override
	public Packet<?> getAddEntityPacket() 
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
}
