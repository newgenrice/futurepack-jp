package futurepack.common.filter;

import javax.script.Bindings;

import futurepack.api.interfaces.filter.IItem;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraftforge.common.ForgeHooks;

public class ItemImpl implements IItem 
{

	private ItemStack item;
	
	public ItemImpl(ItemStack stack) 
	{
		this.item = stack;
	}
	
	@Override
	public int getStackSize() 
	{
		return item.getCount();
	}

	@Override
	public String getRegisteredName() 
	{
		return (item==null || item.getItem() == null) ? "null" : item.getItem().getRegistryName().toString();
	}

	@Override
	public Bindings getNBT() 
	{
		return getNBTWrapper(item.getTag());
	}

	@Override
	public boolean hasNBT() 
	{
		return item.hasTag();
	}

	@Override
	public float getSmeltedItems() 
	{
		return ForgeHooks.getBurnTime(item, RecipeType.SMELTING) / 200F;
	}

	@Override
	public boolean isEmpty() 
	{
		return item.isEmpty();
	}

	@Override
	public int getMaxStacksize() 
	{
		return item.getMaxStackSize();
	}

	@Override
	public int getDurability() 
	{
		return item.getDamageValue();
	}

	@Override
	public int getMaxDurability() 
	{		
		return item.getMaxDamage();
	}

	
	public static Bindings getNBTWrapper(CompoundTag nbt)
	{
		if(nbt==null)
			return null;
		else
			return new NBTWrapper(nbt);
	}
}
