package futurepack.common.gui;

public class TextArea
{
	public int beginLine,endLine,beginPos,endPos;

	public TextArea(int beginLine, int endLine, int beginPos, int endPos) 
	{
		super();
		this.beginLine = beginLine;
		this.endLine = endLine;
		this.beginPos = beginPos;
		this.endPos = endPos;
	}
	
	public boolean isOpen()
	{
		return endLine ==-1 && endPos ==-1;
	}
	
	public boolean isInArea(int line, int pos)
	{
		if( beginLine < line && line < endLine)
		{
			return true;
		}
		else if(line == beginLine)
		{
			if(line == endLine)
			{
				return beginPos <= pos && pos < endPos;
			}
			else
			{
				return beginPos >= pos;
			}
		}
		else if(line == endPos)
		{
			return pos < endPos;
		}
		else
		{
			if(endLine ==-1 && endPos ==-1)
			{
				return (beginLine < line) || (beginLine==line && pos>= beginPos); 
			}
			
			return false;
		}
	}

	public void fixBounds() 
	{
		if(beginLine<0)
			beginLine=0;
		if(endLine<0)
			endLine = 0;
		
		if(endLine < beginLine)
		{
			int line = beginLine;
			int pos = beginPos;
			beginLine = endLine;
			beginPos = endPos;
			endLine = line;
			endPos = pos;
		}
		else if(endPos < beginPos && beginLine == endLine)
		{
			int pos = beginPos;
			beginPos = endPos;
			endPos = pos;
		}
	}
}
