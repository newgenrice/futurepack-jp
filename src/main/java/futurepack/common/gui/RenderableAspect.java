package futurepack.common.gui;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.interfaces.IGuiRenderable;
import futurepack.depend.api.EnumAspects;

public class RenderableAspect implements IGuiRenderable
{
	public final EnumAspects icon;
	
	public RenderableAspect(EnumAspects asp)
	{
		icon = asp;
	}

	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, int x, int y, int zLevel)
	{
		PartRenderer.drawQuadWithTexture(matrixStack, icon.getResourceLocation(), x, y, 0.0f, 0.0f, 16, 16, 16, 16, 16, 16, zLevel);
	}

	@Override
	public String toString() 
	{
		return "Rendering{Aspect "+icon+"}";
	}
}
