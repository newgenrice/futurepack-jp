package futurepack.common.gui.inventory;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.common.block.inventory.TileEntityModulT3;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.PartRenderer;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperRendering;
import futurepack.depend.api.helper.HelperResearch;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class GuiModulT3 extends ActuallyUseableContainerScreen
{

	//private TileEntityBaterieBox tile;
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID,"textures/gui/modul_t3.png");
				
	public GuiModulT3(Player pl, TileEntityModulT3 tile)
	{
		super(new ContainerModulT3(pl.getInventory(), tile), pl.getInventory(), "gui.modul.t3");
		//this.tile = tile;
	}
		
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderBg(PoseStack matrixStack, float var1, int mx, int my) 
	{
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		RenderSystem.setShaderTexture(0, res);
		this.blit(matrixStack, leftPos, topPos, 0, 0, this.imageWidth, this.imageHeight);
		
		PartRenderer.renderExp(matrixStack, leftPos+79, topPos+11, tile(), mx, my);	
	}
	
	@Override
	public boolean mouseReleased(double mx, double my, int but) 
	{
		if(but == 0)
		{
			if(HelperComponent.isInBox(mx-leftPos, my-topPos, 43, 26, 43+18, 26+18))
			{
				((ContainerModulT3)getMenu()).up = true;
				FPPacketHandler.syncWithServer((ContainerModulT3)this.getMenu());
			}
			if(HelperComponent.isInBox(mx-leftPos, my-topPos, 43, 44, 43+18, 44+18))
			{
				((ContainerModulT3)getMenu()).up = false;
				FPPacketHandler.syncWithServer((ContainerModulT3)this.getMenu());
			}
		}
		return super.mouseReleased(mx, my, but);
	}
	
	@Override
	protected void renderLabels(PoseStack matrixStack, int p_146979_1_, int p_146979_2_)
	{
		//this.font.drawString(matrixStack, I18n.format("container.modulT3", new Object[0]), 6, 4, 4210752);
		//this.font.drawString(matrixStack, I18n.format("container.inventory", new Object[0]), 6, this.ySize - 96 + 4, 4210752);
	}
				
	private TileEntityModulT3 tile()
	{
			return ((ContainerModulT3)getMenu()).tile;
	}
		
	public static class ContainerModulT3 extends ContainerSyncBase implements IGuiSyncronisedContainer
	{
		TileEntityModulT3 tile;
		Player pl;
//		int lp;
		boolean up;	
		
		public ContainerModulT3(Inventory inv, TileEntityModulT3 tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			pl = inv.player;
			
			int l;
			int i1;
										
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
			}
		}
				
		@Override
		public ItemStack quickMoveStack(Player pl, int par2)
		{
			return ItemStack.EMPTY;
		}
			
		@Override
		public boolean stillValid(Player var1)
		{
				return HelperResearch.isUseable(var1, tile);
		}
						
//		@Override
//		public void addCraftingToCrafters(ICrafting c)
//		{
//			super.addCraftingToCrafters(c);
//			c.sendProgressBarUpdate(this, 0, (int)this.tile.getXp());
//		}
//					
//		@Override
//		public void detectAndSendChanges() 
//		{
//			super.detectAndSendChanges();
//			if(this.lp != (int)this.tile.getXp())
//			{
//				for (int i = 0; i < this.listeners.size(); ++i)
//				{
//					ICrafting c = (ICrafting)this.listeners.get(i);
//					c.sendProgressBarUpdate(this, 0, (int)this.tile.getXp());
//				}
//			}
//			this.lp = (int)this.tile.getXp();
//		}
//			
//		@Override
//		public void updateProgressBar(int id, int val)
//		{
//			super.updateProgressBar(id, val);
//			if(id==0)
//			{
//				this.tile.setXp(val);
//			}
//		}

		@Override
		public void writeToBuffer(FriendlyByteBuf nbt)
		{
			nbt.writeBoolean(up);
		}

		@Override
		public void readFromBuffer(FriendlyByteBuf nbt)
		{
//			float total = pl.experience;
			up = nbt.readBoolean();
			pl.level.getServer().submitAsync(() -> 
			{
				if(up)
				{
					if(pl.experienceLevel > 0 && tile.getXp() < tile.getMaxXp())
					{
						int xp = Math.min(tile.getMaxXp()-tile.getXp(), (int)pl.experienceProgress);
						if(xp==0)
						{
							pl.giveExperienceLevels(-1);
							int removed = pl.getXpNeededForNextLevel();
							xp = Math.min(tile.getMaxXp()-tile.getXp(), removed);
							tile.setXp(tile.getXp() + xp);
							if(removed > xp)
							{
								pl.giveExperiencePoints(removed - xp);
							}
						}
						else
						{
							pl.experienceProgress = 0;
							tile.setXp(tile.getXp() + xp);
						}		
					}				
				}
				else
				{
					if(tile.getXp() > 0)
					{
						int xp = (int) Math.min(pl.getXpNeededForNextLevel() - pl.experienceProgress, tile.getXp());		
						pl.giveExperiencePoints(xp);
						tile.setXp(tile.getXp() - xp);
					}
				}
				if(!pl.level.isClientSide)
				{
					ServerLevel serv = (ServerLevel) pl.level;
					serv.playSound(null, tile.getBlockPos(), SoundEvents.UI_BUTTON_CLICK, SoundSource.PLAYERS, 0.75F, 1.0F);
				}
			});
		}
	}

}
