package futurepack.common.gui.inventory;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.block.logistic.plasma.TileEntityPlasmaConverter;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.PartRenderer;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.helper.HelperRendering;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;

public class GuiPlasmaConverter extends ActuallyUseableContainerScreen<GuiPlasmaConverter.ContainerPlasmaConverter> 
{
	
	protected ResourceLocation res;
	int nx=7, ny=7;
	
	
	public GuiPlasmaConverter(Player pl, TileEntityPlasmaConverter tile) 
	{
		super(new ContainerPlasmaConverter(pl.getInventory(), tile), pl.getInventory(), "gui.plasma.converter");
	}
	
	@Override
	protected void renderLabels(PoseStack matrixStack, int mouseX, int mouseY)
	{
		super.renderLabels(matrixStack, mouseX, mouseY);
		
		if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos , -20, 0, -20+18, 0+18))
		{
			PartRenderer.renderHoverText(matrixStack, mouseX-leftPos +12, mouseY-topPos+4, getBlitOffset(), "This machine is modifiable with a Scrench.");
		}
		PartRenderer.renderNeonTooltip(matrixStack, leftPos, topPos, nx, ny, menu.tile.getNeon(), mouseX, mouseY);
	}

	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY) 
	{
//		this.minecraft.getTextureManager().bindTexture(res);
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
//		this.blit(matrixStack, guiLeft, guiTop, 0, 0, this.xSize, this.ySize);
		fill(matrixStack, leftPos, topPos, leftPos + this.imageWidth, topPos + this.imageHeight, 0xFF777777);
		
		PartRenderer.renderNeon(matrixStack, leftPos+nx, topPos+ny, menu.tile.getNeon(), mouseX, mouseY);
		HelperComponent.renderSymbol(matrixStack, leftPos-20, topPos, getBlitOffset(), 29);

		PartRenderer.renderRedstoneFlux(matrixStack, leftPos+nx + 30, topPos+ny, menu.tile.getRf(), menu.tile.getMaxRf(), mouseX, mouseY);
		
		PartRenderer.renderText(matrixStack, leftPos+nx + 70, topPos+ny + 20, "Plasma:" + menu.tile.getPlasma());
		
//		this.minecraft.getTextureManager().bindTexture(res);
	}
	
	public static class ContainerPlasmaConverter extends ContainerSyncBase
	{
		TileEntityPlasmaConverter tile;
		
		public ContainerPlasmaConverter(Inventory inventory, TileEntityPlasmaConverter tile) 
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			
			HelperContainerSync.addInventorySlots(8, 84, inventory, this::addSlot);
		}

		@Override
		public boolean stillValid(Player playerIn) 
		{
			return true;
		}
		
	}
}
