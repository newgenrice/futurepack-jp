package futurepack.common.gui.inventory;

import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.AbstractContainerMenu;

public abstract class ActuallyUseableContainerScreen<T extends AbstractContainerMenu> extends AbstractContainerScreen<T> 
{

	public ActuallyUseableContainerScreen(T screenContainer, Inventory inv, String toTranslate) 
	{
		super(screenContainer, inv, new TranslatableComponent(toTranslate.contains("futurepack") ? toTranslate : "futurepack." + toTranslate));
	}

	@Deprecated
	public ActuallyUseableContainerScreen(T screenContainer, Inventory inv) 
	{
		this(screenContainer, inv, "TODO");
	}
	
	@Override
	protected void renderLabels(PoseStack matrixStack, int x, int y)
	{
		//Removed to remove default text (inventory and gui name) super.drawGuiContainerForegroundLayer(matrixStack, x, y);
	}
}
