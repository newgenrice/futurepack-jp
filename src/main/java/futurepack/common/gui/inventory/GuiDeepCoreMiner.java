package futurepack.common.gui.inventory;

import java.util.ArrayList;
import java.util.List;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.block.multiblock.DeepCoreLogic;
import futurepack.common.block.multiblock.TileEntityDeepCoreMinerMain;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.PartRenderer;
import futurepack.common.gui.SlotScrollable;
import futurepack.common.modification.EnumChipType;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.helper.HelperRendering;
import futurepack.depend.api.helper.HelperResearch;
import futurepack.depend.api.interfaces.IContainerScrollable;
import net.minecraft.ChatFormatting;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.Container;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ClickType;
import net.minecraft.world.inventory.ResultContainer;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class GuiDeepCoreMiner extends GuiModificationBase<TileEntityDeepCoreMinerMain>
{

	public GuiDeepCoreMiner(Player pl, TileEntityDeepCoreMinerMain tile)
	{
		super(new ContainerCoreMiner(pl.getInventory() , tile) ,"core_miner.png", pl.getInventory());
		this.imageHeight = 184;
	}

	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(matrixStack, partialTicks, mouseX, mouseY);
		if(tile().getChipPower(EnumChipType.NAVIGATION)<=0)
		{
			blit(matrixStack, this.leftPos+94, this.topPos+16, 222, 0, 18, 54);
		}
		
		int mx = 199; //change this lense based -- but we should make this gray and just color it
		
		float r=1F,g=1F,b=1F;
		DeepCoreLogic logic = tile().getLogic();
		if(logic!=null)
		{
			if(logic.getLense()!=null)
			{
				int color = logic.getLense().getColor(logic.getLenseStack(), logic);
				b = ((color>>0) & 0xFF) /255F;
				g = ((color>>8) & 0xFF) /255F;
				r = ((color>>16) & 0xFF) /255F;
				HelperRendering.glColor4f(r, g, b, 1);
			}
			if(logic.needSupport())
			{
				int sp = tile().support.get();
				if(sp<50)
				{
					
				}
			}
		}
		
		
		PartRenderer.drawTexturedModalRect(leftPos+76, topPos+47, mx, 1, 3F, 19F * tile().getLogic().getProgress(), this.getBlitOffset());
		RenderSystem.setShaderColor(1, 1, 1, 1);
		//progress for item damage
		float s = 16F * tile().getLogic().getDurability();
		PartRenderer.drawTexturedModalRect(leftPos+63, topPos+13+ (16-s), 179, 2 + (16-s), 2, s, this.getBlitOffset());
		
		ItemStack active = tile().getInternInvetory().getStackInSlot(4);
		if(!active.isEmpty())
		{
			HelperRendering.glColor4f(r, g, b, 1);	
			blit(matrixStack, leftPos+68, topPos+34, 179, 25, 19, 11);
			HelperRendering.glColor4f(1F, 1F, 1F, 1);
		}
		
		PartRenderer.renderSupport(matrixStack, leftPos+158, topPos+7, tile().support, mouseX, mouseY);
		
		if(logic!=null)
		{
			if(logic.needSupport())
			{
				int sp = tile().support.get();
				if(sp<50)
				{
					
					HelperComponent.renderSymbol(matrixStack, leftPos+17+158, topPos+7, getBlitOffset(), 18);
					HelperComponent.renderSupport(matrixStack, leftPos+17+158, topPos+7, getBlitOffset());
				}
			}
		}
		
		ContainerCoreMiner cont = (ContainerCoreMiner)this.getMenu();
		cont.visible = HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos, 125, 13, 125+18, 79);
		if(cont.visible)
		{
			int c = cont.slots.size();
			if(c>0)
			{
				int h = Math.min(6, c)*18;
				int w = (c/6);
				if(w*6<c)w++;
				w*=20;
				int x = leftPos -10 -w;
				int y = topPos+13+33 - h/2;
				
				HelperRendering.disableLighting();
				RenderSystem.setShaderColor(1, 1, 1, 1);	
				GuiComponent.fill(matrixStack, x-2, y-2, x + w +2, y + h+2, 0xff5c89c1);
				
				GuiComponent.fill(matrixStack, x-2, y-1, x -1, y + h+1, 0xff394c66);
				GuiComponent.fill(matrixStack, x-2, y-2, x + w +1, y -1, 0xff394c66);
				
				GuiComponent.fill(matrixStack, x +w +1, y-1, x + w +2, y + h+1, 0xff83b0e7);
				GuiComponent.fill(matrixStack, x -1, y+h+1, x + w +2, y + h+2, 0xff83b0e7);
			}
		}
		
		
	}
	
	@Override
	protected void renderLabels(PoseStack matrixStack, int p_146979_1_, int p_146979_2_)
	{
		super.renderLabels(matrixStack, p_146979_1_, p_146979_2_);
		PartRenderer.renderSupportTooltip(matrixStack, leftPos, topPos, 158, 7, tile().support, p_146979_1_, p_146979_2_);
	}

	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int state)
	{
		if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos, 68, 34, 68+19, 34+11) && state == 0)
		{
			if(getMenu().getCarried().isEmpty())
			{
				this.slotClicked(((ContainerCoreMiner)getMenu()).slot, ((ContainerCoreMiner)getMenu()).slot.index, state, ClickType.PICKUP);
				return true;
			}
		}
		return super.mouseReleased(mouseX, mouseY, state);
	}
	
	@Override
	public List<Component> getTooltipFromItem(ItemStack stack)
	{
		List<Component> base = super.getTooltipFromItem(stack);
		CompoundTag nbt = stack.getTagElement("scanresult");
		if(nbt != null)
		{
			float fill = nbt.getFloat("fill") * 100F;
			ChatFormatting f = fill>75 ? ChatFormatting.GREEN : fill>50 ? ChatFormatting.YELLOW : fill>25 ? ChatFormatting.GOLD : ChatFormatting.RED;
			
			base.add(new TextComponent(String.format("Fillrate: %s%.4f%s%%", f, fill, ChatFormatting.RESET)));
			base.add(new TextComponent(""));
			
			ListTag list = nbt.getList("list", 10);
			for(Tag tag : list)
			{
				CompoundTag c = (CompoundTag) tag;
				base.add(new TextComponent(String.format("%s %.1f%%", c.getString("k"), (c.getInt("v")*0.1))));
			}
		}
		return base;
	}
	
	@Override
	public TileEntityDeepCoreMinerMain tile()
	{
		return ((ContainerCoreMiner)getMenu()).tile;
	}

	public static class ContainerCoreMiner extends ContainerSyncBase implements IContainerScrollable
	{
		TileEntityDeepCoreMinerMain tile;
		private List<Slot> slots;
		private Container inv;
		private int size;
		private boolean visible = false;
		
		protected Slot slot;
		
		public ContainerCoreMiner(Inventory inv, TileEntityDeepCoreMinerMain tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			this.inv = tile.getCompressedOres();
			slots = new ArrayList<Slot>();
			
			this.addSlot(new SlotItemHandler(tile.getInternInvetory(), 0, 69, 13)); //Linse
					
			for(int i=0;i<3;i++)
				this.addSlot(new SlotItemHandler(tile.getInternInvetory(), 1+i, 95, 17 + 18*i)); //Filter
			
			slot = this.addSlot(new SlotScrollable(this, tile.getInternInvetory(), 4, 69, 31)); //Active linse
			
			IItemHandler handler = tile.getFakeSlots();		
			this.addSlot(new SlotItemHandler(handler, 0, 69, 72)); //69, 72 rift
			for(int i=0;i<3;i++)
				this.addSlot(new SlotItemHandler(handler, i+1, 126, 13 + 22*i));//126, 13, 22px abstand --- kisten
				
			HelperContainerSync.addInventorySlots(8, 102, inv, this::addSlot);//lamdas \o/  -- because its protected
			
		}

		@Override
		public boolean stillValid(Player playerIn)
		{
			return HelperResearch.isUseable(playerIn, tile);
		}
		
		@Override
		public ItemStack quickMoveStack(Player playerIn, int index)
		{
			ItemStack itemstack = ItemStack.EMPTY;
	        Slot slot = this.slots.get(index);

	        if (slot != null && slot.hasItem())
	        {
	            ItemStack itemstack1 = slot.getItem();
	            itemstack = itemstack1.copy();

	            if (index < 4)
	            {
	                if (!this.moveItemStackTo(itemstack1, 9, 9+4*9, true))
	                {
	                    return ItemStack.EMPTY;
	                }
	            }
	            else if (index >=9 && !this.moveItemStackTo(itemstack1, 0, 4, false))
	            {
	                return ItemStack.EMPTY;
	            }

	            if (itemstack1.isEmpty())
	            {
	                slot.set(ItemStack.EMPTY);
	            }
	            else
	            {
	                slot.setChanged();
	            }
	        }

	        return itemstack;
		}
		
		
		
		@Override
		public Slot getSlot(int slotId)
		{
			if(tile.getLevel().isClientSide && slotId >= slots.size())
			{
				addSlot(new SlotDeepCore()); 
			}
			return super.getSlot(slotId);
		}
		
		@Override
		public void broadcastChanges()
		{
			if(!tile.getLevel().isClientSide)
			{
				while(size<inv.getContainerSize())
				{
					addSlot(new Slot(inv, size++, 0, 0));
				}
			}
			super.broadcastChanges();
		}
		
		
		@Override
		public int getScollIndex() { return 0; }

		@Override
		public int getRowWidth() { return 0; }

		@Override
		public int getRowCount() { return 0; }
		
		@Override
		public boolean isEnabled(SlotScrollable s)
		{
			return !tile.getLevel().isClientSide;
		}
		
		private class SlotDeepCore extends Slot
		{
			int lokal;
			
			public SlotDeepCore()
			{
				super(new ResultContainer(), 0, -16, 0);
				lokal = slots.size();
				slots.add(this);
			}
			
			@Override
			public boolean mayPickup(Player playerIn)
			{
				return false;
			}
			
			@Override
			public boolean mayPlace(ItemStack stack)
			{
				return false;
			}

			@Override
			public boolean isActive()
			{
				int my = (slots.size()>6?6:slots.size()) * 9;
				y = 46 -my +(lokal%6)*18;
				int w = slots.size()/6;
				if(w*6 < slots.size())w++;
				x = -10 + -20*w + (lokal/6)*20; 
				return visible;
			}
			
			@Override
			public ItemStack getItem()
			{
				ItemStack st = super.getItem();
				if(st.hasTag())
				{
					CompoundTag nbt = st.getTag();
					if(nbt.contains("c"))
						st.setCount(nbt.getInt("c"));
				}
				return st;
			}
		}

	}
}
