package futurepack.common.gui.inventory;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.api.interfaces.IFluidTankInfo;
import futurepack.common.block.modification.machines.TileEntityGasTurbine;
import futurepack.common.block.modification.machines.TileEntityIndustrialNeonFurnace;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.PartRenderer;
import futurepack.common.gui.inventory.GuiIndNeonFurnace.ContainerIndNeonFurnace;
import futurepack.depend.api.helper.HelperRendering;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class GuiGasTurbine extends GuiModificationBase<TileEntityGasTurbine>
{
	private TileEntityGasTurbine tile;
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID,"textures/gui/gas_turbine.png");
	private IFluidTankInfo gastank;
	public GuiGasTurbine(Player pl, TileEntityGasTurbine tile)
	{
		super(new ContainerGasTurbine(pl.getInventory(), tile), "gas_turbine.png", pl.getInventory());
		this.tile = tile;
		imageHeight+=20;
		gastank = tile.getGas();
	}
	
	

	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
        
		if(!gastank.isEmpty())
		{
			int k = (this.width - this.imageWidth) / 2;
			int l = (this.height - this.imageHeight) / 2;
			PartRenderer.renderFluidTankTooltip(matrixStack, k+26, l+31, 16, 52, tile.getGas(), mouseX, mouseY);
		}
    }
	
	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY) 
	{
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		RenderSystem.setShaderTexture(0, res);
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);
		
		if(tile.getSpeed() > 0)
		{
			float width = Math.round((18.0f / 10.0f) * tile.getSpeed());
			this.blit(matrixStack, leftPos+128, topPos+41, 176, 0, (int)width, 25);
		}
		
		if(!gastank.isEmpty())
		{
			PartRenderer.renderFluidTank(k+26, l+31, 16, 52, gastank, mouseX, mouseY, 10);
		}
		
		PartRenderer.renderNeon(matrixStack, k+7, l+14, tile.energy, mouseX, mouseY);
	}
	

	@Override
	public TileEntityGasTurbine tile()
	{
		return ((ContainerGasTurbine)this.getMenu()).tile;
	}
	
	
	public static class ContainerGasTurbine extends ContainerSyncBase
	{
		@SuppressWarnings("unused")
		private TileEntityGasTurbine tile;
		private IItemHandler handler;
		
		public ContainerGasTurbine(Inventory pl, TileEntityGasTurbine tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			
			handler = tile.getGui();
			
			this.addSlot(new SlotItemHandler(handler, 0, 26, 9)); 
			
			for (int l = 0; l < 3; ++l)
			{
				for (int i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(pl, i1 + l * 9 + 9, 8 + i1 * 18, 104 + l * 18));
				}
			}
			
			for (int l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(pl, l, 8 + l * 18, 162));
			}
		}
		
		@Override
		public ItemStack quickMoveStack(Player playerIn, int index)
		{
			Slot slot = this.getSlot(index);
			if(!playerIn.level.isClientSide && slot.hasItem())
			{
				if(slot.container == playerIn.getInventory())
				{
					this.moveItemStackTo(slot.getItem(), 0, 1, false);					
				}
				else
				{
					this.moveItemStackTo(slot.getItem(), 1, this.slots.size(), false);				
				}
				
				if(slot.getItem().getCount()<=0)
				{
					slot.set(ItemStack.EMPTY);
				}
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}
		
		@Override
		public boolean stillValid(Player playerIn)
		{
			return true;
		}
	}

}
