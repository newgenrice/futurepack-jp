package futurepack.common.gui.inventory;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;

import org.lwjgl.glfw.GLFW;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.FPLog;
import futurepack.common.filter.ScriptItemFilterFactory;
import futurepack.common.gui.FormatedTextEditor;
import futurepack.common.gui.TextEditorGui;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.ISyncable;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperRendering;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.client.gui.components.Button;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.util.FormattedCharSequence;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;

public class GuiJSFilterEditor extends ActuallyUseableContainerScreen<GuiJSFilterEditor.ContainerJSFilterEditor> 
{
	private TextEditorGui editor;
	private boolean isAdded;
	private boolean defaultFont = false;
	
	private static final int minWidth = 266, minHeight = 256;
	public static int userWidth = minWidth, userHeight = minHeight;
	
	private Button save;
	
	public GuiJSFilterEditor(Player pl) 
	{
		super(new ContainerJSFilterEditor(pl.getInventory()), pl.getInventory(), "gui.js_item_filter");
		this.imageWidth = userWidth;
		this.imageHeight = userHeight;
		getMenu().completeText = () -> GuiJSFilterEditor.this.editor.getEditor().getFullText();
	}
	
	
	@Override
	public void init() 
	{
		if(imageWidth > width || imageHeight > height)
		{
			imageWidth = userWidth = minWidth;
			imageHeight = userHeight = minHeight;
		}
		
		this.minecraft.keyboardHandler.setSendRepeatsToGui(true);
		
		if(editor==null)
		{
			FormatedTextEditor te =  new FormatedTextEditor(new FormatedTextEditor.JavaScriptFormatter());
			editor = new TextEditorGui(imageWidth-10, imageHeight-56, te);
			editor.setPosition(leftPos+5, topPos+5);
			isAdded = false;
		}
		else
		{
			FormatedTextEditor te =  editor.getEditor();
			editor = new TextEditorGui(imageWidth-10, imageHeight-56, te);
			editor.setPosition(leftPos+5, topPos+5);
			isAdded = true;
			addWidget(editor);
		}
		
		if(!getMenu().syncedWithServer)
		{
			getMenu().type_client = RequestType.REQUEST_TEXT;
			FPPacketHandler.syncWithServer(getMenu());
		}
		int w = 0;
		int x = leftPos+3;
		int y = topPos + (imageHeight-56);
		
		addRenderableWidget(save = new Button(x, y +10, w = 18+this.font.width("Save"), 20, new TextComponent("Save"), b ->  {
			getMenu().compileMsg = "";
			getMenu().type_client = RequestType.SAVE_TEXT;
			FPPacketHandler.syncWithServer(getMenu());
		}));
		x+=w+5;
		addRenderableWidget(new Button(x, y +10, w = 18+this.font.width("Reload"), 20, new TextComponent("Reload"), b ->  {
			isAdded = false;
			removeWidget(editor);
			getMenu().syncedWithServer = false;
			getMenu().type_client = RequestType.REQUEST_TEXT;
			FPPacketHandler.syncWithServer(getMenu());
		}));
		x+=w+5;
		addRenderableWidget(new Button(x, y +10, w = 18+this.font.width("Compile"), 20, new TextComponent("Compile"), b ->  {
			getMenu().type_client = RequestType.COMPILE;
			FPPacketHandler.syncWithServer(getMenu());
		}));
		x+=w+5;
		addRenderableWidget(new Button(x, y+10, w= 18+this.font.width("Change Fontsize"), 20, new TextComponent("Change Font Size"), b -> {
			if(defaultFont)
			{
				editor.setFont(HelperComponent.getUnicodeFont());
				defaultFont = false;
			}
			else
			{
				editor.setFont(Style.DEFAULT_FONT);
				defaultFont = true;
			}
			
		}));
	}
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
		if(getMenu().syncedWithServer && !isAdded)
		{
			if(getMenu().lines!=null)
			{
				isAdded = true;
				addWidget(editor);
				editor.getEditor().clear();
				editor.getEditor().addAll(getMenu().lines);
				getMenu().lines = null;
			}
			else
			{
				isAdded = false;
				removeWidget(editor);
				getMenu().syncedWithServer = false;
				getMenu().type_client = RequestType.REQUEST_TEXT;
				FPPacketHandler.syncWithServer(getMenu());
			}
		}
		
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	private boolean resize = false;
	
	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) 
	{
		resize = false;
		if(button==0)
		{
			if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos, imageWidth-10, imageHeight-10, imageWidth, imageHeight))
			{
				resize = true;
			}
		}
		return super.mouseClicked(mouseX, mouseY, button);
	}
	
	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int button) 
	{
		if(resize)
		{
			int newWidth,newHeight;
			
			newWidth = (int) ((mouseX - width/2D) * 2D);
			newHeight = (int) ((mouseY - height/2D) * 2D);
			
			newWidth = Math.max(minWidth, newWidth);
			newHeight = Math.max(minHeight, newHeight);
			newWidth = Math.min(this.width, newWidth);
			newHeight = Math.min(this.height, newHeight);
			
			if(newHeight != imageHeight || newWidth != imageWidth)
			{
				userWidth = imageWidth = newWidth;
				userHeight = imageHeight = newHeight;
				init(getMinecraft(), width, height);
			}
			
			resize = false;
			return true;
		}
		return super.mouseReleased(mouseX, mouseY, button);
	}
	
	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY) 
	{
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
//		this.minecraft.getTextureManager().bindTexture(res);
//		this.blit(matrixStack, k, l, 0, 0, this.xSize, this.ySize);
		fill(matrixStack, leftPos, topPos, leftPos+imageWidth, topPos+imageHeight, 0xd0d0d0d0);
		fill(matrixStack, leftPos+imageWidth-10, topPos+imageHeight-10, leftPos+imageWidth, topPos+imageHeight, 0xff001090);
		fill(matrixStack, leftPos+imageWidth-10, topPos+imageHeight-10, leftPos+imageWidth-1, topPos+imageHeight-1, 0xff0030c0);
		
		editor.render(matrixStack, mouseX, mouseY, partialTicks);
		
		List<FormattedCharSequence> widthWrapped = this.font.split(new TextComponent("Compiler: " + getMenu().compileMsg), imageWidth-10);
		int yOffset = 0;
		for(FormattedCharSequence rp : widthWrapped)
		{
			this.font.drawShadow(matrixStack, rp, leftPos+5, topPos + (imageHeight-56)+20+15+yOffset, 0xFF11FF11);
			yOffset+= this.font.lineHeight;
		}
		
		if(resize)
		{
			int newWidth,newHeight;
			newWidth = (int) ((mouseX - width/2D) * 2D);
			newHeight = (int) ((mouseY - height/2D) * 2D);
			int k = (width-newWidth)/2;
			int l = (height -newHeight)/2;
			
			int green = 0xFF00FF00;
			fill(matrixStack, k, l, k+newWidth, l+2, green);
			fill(matrixStack, k, l+newHeight-2, k+newWidth, l+newHeight, green);
			fill(matrixStack, k, l, k+2, l+newHeight, green);
			fill(matrixStack, k+newWidth-2, l, k+newWidth, l+newHeight, green);
		}
	}
	
	private int autosave;
	
	@Override
	public void containerTick() 
	{
		autosave++;
		if(autosave > 20 * 60 *10)//auto-save every 10 minutes
		{
			getMenu().type_client = RequestType.SAVE_TEXT;
			FPPacketHandler.syncWithServer(getMenu());
			autosave = 0;
		}
		super.containerTick();
	}
	
	@Override
	public void removed() 
	{
		getMenu().type_client = RequestType.SAVE_TEXT;
		FPPacketHandler.syncWithServer(getMenu());
		this.minecraft.keyboardHandler.setSendRepeatsToGui(false);
		super.removed();
	}
	
	@Override
	public void mouseMoved(double mouseX, double mouseY) 
	{
		super.mouseMoved(mouseX, mouseY);
		editor.mouseMoved(mouseX, mouseY);
	}
	
	@Override
	public boolean keyPressed(int keyCode, int scanCode, int modifiers) 
	{
		if(hasControlDown() && keyCode == GLFW.GLFW_KEY_S)
		{
			save.mouseClicked(save.x+1, save.y+1, 0);//triggering the button with all actiaveted checks
			return true;
		}
		return super.keyPressed(keyCode, scanCode, modifiers);
	}

	public static class ContainerJSFilterEditor extends ActuallyUseableContainer implements ISyncable, IGuiSyncronisedContainer
	{
		private boolean syncedWithServer = false;
		private Player pl;
		
		private String scriptName = null;
		private List<String> lines;
		
		private Supplier<String> completeText;
		
		private String compileMsg = "";
		
		public ContainerJSFilterEditor(Inventory inv)
		{
			this.pl = inv.player;
		}
		
		@Override
		public boolean stillValid(Player playerIn) 
		{
			return true;
		}

		@Override
		public void writeAdditional(DataOutputStream buffer) throws IOException 
		{
			buffer.writeByte(type_server.ordinal());
			
			//server
			
			switch (type_server)
			{
			case ANSWER_TEXT:
				scriptName = ScriptItemFilterFactory.getOrCreateFilterName(pl.getItemInHand(pl.getUsedItemHand()));
				buffer.writeUTF(scriptName);
				Reader r = ScriptItemFilterFactory.getFilterScript(scriptName);
				BufferedReader read = new BufferedReader(r);
				
				while(read.ready())
				{
					buffer.writeUTF(read.readLine());
				}
				break;
			case ANSWER_COMPILE:
				buffer.writeUTF(compileMsg);
				break;
			default:
				break;
			}
			
		}

		@Override
		public void readAdditional(DataInputStream buffer) throws IOException 
		{
			type_server = RequestType.values()[buffer.readByte()];
			
			//client
			
			switch (type_server)
			{
			case ANSWER_TEXT:
				syncedWithServer = true;
				
				scriptName = buffer.readUTF();
				lines = new LinkedList<String>();
				while(buffer.available()>0)
				{
					lines.add(buffer.readUTF());
				}
				break;
			case ANSWER_COMPILE:
				compileMsg = buffer.readUTF();
				break;
			default:
				break;
			}
			
		}

		private RequestType type_client, type_server;

		@Override
		public void writeToBuffer(FriendlyByteBuf buf) 
		{
			//client
			buf.writeByte(type_client.ordinal());
			
			switch (type_client)
			{
			case SAVE_TEXT:
			case COMPILE:
				buf.writeUtf(scriptName);
				buf.writeUtf(completeText.get());
				break;
			default:
				break;
			}
		}



		@Override
		public void readFromBuffer(FriendlyByteBuf buf) 
		{
			//server
			type_client = RequestType.values()[buf.readByte()];
			
			switch (type_client)
			{
			case REQUEST_TEXT:
				type_server = RequestType.ANSWER_TEXT;
				FPPacketHandler.sendToClient(this, pl);
				break;
			case SAVE_TEXT:
			case COMPILE:
				String name = buf.readUtf(32767);
				String text = buf.readUtf(32767);
				if(name.equals(scriptName))
				{
					FPLog.logger.debug("Saved item filter script %s", name);
					Writer w;
					try 
					{
						w = ScriptItemFilterFactory.getFilterScriptWriter(scriptName);
						w.write(text);
						w.close();
					}
					catch (FileNotFoundException e)  {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					if(type_client==RequestType.COMPILE)
					{
						Exception e = ScriptItemFilterFactory.compileAndTest(scriptName);
						if(e==null)
							compileMsg = "Compiled Succesfully!";
						else
							compileMsg = e.getMessage();
						
						type_server = RequestType.ANSWER_COMPILE;
						FPPacketHandler.sendToClient(this, pl);
					}
				}
				else
				{
					FPLog.logger.warn("Mismatched Script named between client(%s) and server(%s)", name, scriptName);
				}
				break;
			default:
				break;
			}
		}
		
	}
	
	private enum RequestType
	{
		REQUEST_TEXT,
		SAVE_TEXT,
		COMPILE,
		ANSWER_TEXT,
		ANSWER_COMPILE;
	}

	
}
