package futurepack.common.gui.inventory;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.block.modification.machines.TileEntityIonCollector;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.SlotBaseXPOutput;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class GuiIonCollector extends GuiModificationBase<TileEntityIonCollector>
{

	public GuiIonCollector(Player player, TileEntityIonCollector tile)
	{
		super(new ContainerIonCollector(player.getInventory(), tile), "teilchen_collector.png", player.getInventory());		
	}
	
	

	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY) 
	{
		super.renderBg(matrixStack, partialTicks, mouseX, mouseY);
		
		float f = tile().power / 10;
		f= f>1?1:(f<0?0:f);
		
		this.blit(matrixStack, leftPos + 71, topPos + 10 + (int)(42 * (1-f)), 176, (int)(42 * (1-f)), 33, (int)(42 * f));
		
	}
	
	@Override
	public TileEntityIonCollector tile()
	{
		return ((ContainerIonCollector)this.getMenu()).tile;
	}
	
	public static class ContainerIonCollector extends ContainerSyncBase
	{
		protected final TileEntityIonCollector tile;
		
		public ContainerIonCollector(Inventory inv, TileEntityIonCollector tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			this.addSlot(new SlotBaseXPOutput(inv.player, tile, 0,80, 54));
			
			 
			for (int l = 0; l < 3; ++l)
			{
				for (int i1 = 0; i1 < 9; ++i1)
	            {
	                this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
	            }
	        }
			
	        for (int l = 0; l < 9; ++l)
	        {
	            this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
	        }
	      
		}
		
		@Override
		public boolean stillValid(Player var1)
		{
			return tile.stillValid(var1);
		}
		
		@Override
		public ItemStack quickMoveStack(Player par1EntityPlayer, int par2)
		{
			 Slot slot = this.slots.get(par2);
			 if(slot !=null && slot.hasItem())
			 {
				 if(par2==0)
				 {
					 this.moveItemStackTo(slot.getItem(), 1, this.slots.size(), false);
				 }
				 else
				 {
					 this.moveItemStackTo(slot.getItem(), 0, 1, false);
				 }
				 
				 if(slot.getItem().getCount()<=0)
				 {
					 slot.set(ItemStack.EMPTY);
				 }
				 this.broadcastChanges();
			 }
			return ItemStack.EMPTY;
		}
		
//		public Slot getFirstFreeSlot()
//		{
//			for(int i=0;i<this.inventorySlots.size();i++)
//			{
//				Slot sl = (Slot) getContainer().get(i);
//				if(sl.getStack()==null)
//				{
//					return sl;
//				}
//			}
//			return null;
//		}
	}
}
