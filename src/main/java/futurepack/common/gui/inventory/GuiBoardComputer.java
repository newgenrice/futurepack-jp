package futurepack.common.gui.inventory;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.blaze3d.vertex.VertexFormat;
import com.mojang.math.Matrix4f;
import com.sun.jna.platform.unix.X11.Screen;

import futurepack.api.Constants;
import futurepack.api.EnumStateSpaceship;
import futurepack.api.interfaces.IPlanet;
import futurepack.client.research.LocalPlayerResearchHelper;
import futurepack.common.FPConfig;
import futurepack.common.block.inventory.TileEntityBoardComputer;
import futurepack.common.entity.CapabilityPlayerData;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.PartRenderer;
import futurepack.common.gui.SlotUses;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.spaceships.FPPlanetRegistry;
import futurepack.common.spaceships.FPSpaceShipSelector;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.ISyncable;
import futurepack.common.sync.MessageRequestTerrain;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperRendering;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.Util;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Tuple;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ClickType;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;

public class GuiBoardComputer extends ActuallyUseableContainerScreen<GuiBoardComputer.ContainerBoardComputer> implements Runnable
{
	private boolean canRender = false;
	
	private CustomPlayerData local = null;
	
	private ResourceLocation res;
	private int[][] pos;
	protected int color;

	private boolean stats = false;
	private int X, Y, Z;
	private float rotX, rotY, rotZ;
	private int selectedDrehKnopf = -1;
//	private int dis = 100;
	private int[][] blocks = null;

	static class MiniMapSize
	{
		public static int width = 48;
		public static int height = 48;
	}

	public GuiBoardComputer(Player pl, TileEntityBoardComputer tile)
	{
		super(new ContainerBoardComputer(pl.getInventory(), tile), pl.getInventory(), "gui.boardcomputer");
		container().tile.currentUser = pl;
		imageWidth=176;
		imageHeight=222;
		res = new ResourceLocation(Constants.MOD_ID, "textures/gui/boardcomputer.png");
		pos = new int[][]{{28,9,126,9},{22,33,132,33},{28,57,126,57}};
		color = 0xffe1fdff;

		blocks = null;
	}
	
	@Override
	public void init()
	{
		super.init();
		LocalPlayerResearchHelper.requestServerData(this);
		container().tile.searchForShip();
	}
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderLabels(PoseStack matrixStack, int p_146979_1_, int p_146979_2_)
	{
//		if(container().tile.chat!=null)
//		{
//			this.font.drawString(matrixStack, container().tile.chat.getString(), 20, 6, 4210752);
//		}
		//this.font.drawString(matrixStack, I18n.format("container.inventory", new Object[0]), 48, this.ySize - 96 + 2, 4210752);
		
		if(stats)
		{
			if (container().tile.client_jump_progress > 0)
			{
				int p = (int) (container().tile.client_jump_progress * 100);
				drawCenteredString(matrixStack, font, p + "%", imageWidth / 2, 45, color & 0xCCCCCC);
				return;
			}
			BlockPos blockPos = this.container().tile.getBlockPos();
			if (container().tile.blockCount < 0)
				this.container().tile.searchForShip();
			this.font.draw(matrixStack, "Blocks: " + container().tile.blockCount, 22, 11, ~color);
			this.font.draw(matrixStack, "Thrusters: " + container().tile.thrusterCount, 22, 21, ~color);
			this.font.draw(matrixStack, "Fuel: " + container().tile.fuelCount, 22, 31, ~color);

//			String s = "D: " + dis;
//			this.font.drawString(matrixStack, s, 155 - this.font.getStringWidth(s), 11, 0xffffff ^color);

			//Dial labels

			int labelColor = contrastBlackOrWhite(color);
			this.font.draw(matrixStack, "Y", 56, 67, labelColor);
			this.font.draw(matrixStack, "X", 62, 67, labelColor);
			this.font.draw(matrixStack, "Z", 68, 67, labelColor);

			//Values

			int dX = (int) (rotX / 9F);
			int dY = (int) (rotY / 9F);
			int dZ = (int) (rotZ / 9F);
			//Print deltas
			this.font.draw(matrixStack, String.format("%s %d", "dX", dX), 99, 11, ~color);
			this.font.draw(matrixStack, String.format("%s %d", "dY", dY), 99, 21, ~color);
			this.font.draw(matrixStack, String.format("%s %d", "dZ", dZ), 99, 31, ~color);
			X = blockPos.getX() + dX;
			Y = blockPos.getY() + dY;
			Z = blockPos.getZ() + dZ;

			//Print target if the computer is an advanced one (add something about how it was inconvenient before to advanced computer description?)

			if (this.container().tile.isAdvancedBoardComputer()) {
				this.font.draw(matrixStack, String.format("%c %d", 'X', X), 99, 51, ~color);
				this.font.draw(matrixStack, String.format("%c %d", 'Y', Y), 99, 61, ~color);
				this.font.draw(matrixStack, String.format("%c %d", 'Z', Z), 99, 71, ~color);
			}
			if (blocks == null)//Calculate button text
				this.font.draw(matrixStack, "Calculate", 50 + 30 + 14 + 2, 90, ~color);
		}
	}

	/**
	 * Selects the most visually contrast to the given color from black and white
	 * @param color The color for which the contrast color is needed
	 * @return Black or white color, whichever is more contrast to the argument
	 */

	private static int contrastBlackOrWhite(int color) {
		return contrastBlackOrWhite(color, 0);
	}

	/**
	 * Selects the most visually contrast to the given color from black and white, softened by specified amount
	 * @param color The color for which the contrast color is needed
	 * @param margin The amounts to offset from pure black or white, 0xRRGGBB
	 * @return Softened black or white color, whichever is more contrast to the color argument
	 */

	private static int contrastBlackOrWhite(int color, int margin) {
		margin &= 0x00ffffff;
		return ((color & 0x0000ff) + ((color & 0x00ff00) >> 8) + ((color & 0xff0000) >> 16)) / 3 > 0x7f ?
				(color & 0xff000000) + margin : (color | 0x00ffffff) - margin;
	}

	/**
	 * Selects the most visually contrast to the given array of color values from black and white, transparency data are discarded
	 * @param array The array of color values for which the contrast color is needed
	 * @return Black or white color, whichever is more contrast to median brightness of the array
	 */

	private static int contrastBlackOrWhiteForArray(int[] array) {
		return contrastBlackOrWhiteForArray(array, 0);
	}

	/**
	 * Selects the most visually contrast to the given array of color values from black and white, softened by specified amount, transparency data are discarded
	 * @param array The array of color values for which the contrast color is needed
	 * @param margin The amounts to offset from pure black or white, 0xRRGGBB
	 * @return Softened black or white color, whichever is more contrast to median brightness of the array
	 */

	private static int contrastBlackOrWhiteForArray(int[] array, int margin) {
		margin &= 0x00ffffff;
		int result = 0;
		for (int color : array)
		{
			result += ((color & 0x0000ff) + ((color & 0x00ff00) >> 8) + ((color & 0xff0000) >> 16)) / 3;
		}
		return result / array.length > 0x7f ? 0xff000000 + margin : 0xffffffff - margin;
	}

//	@Override
//	protected void actionPerformed(GuiButton button) throws IOException
//	{
//		super.actionPerformed(button);
//		
//		if(button.id==0)
//		{
//			container().seleced++;	
//			if(container().seleced>=container().planet.length)
//			{
//				container().seleced = container().planet.length-1;
//			}
//		}
//		else if(button.id==1)
//		{
//			container().seleced--;
//			if(container().seleced<0)
//			{
//				container().seleced = 0;
//			}
//		}
//		
//	}
	
	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		RenderSystem.setShaderTexture(0, res);
		
		if(container().tile.client_jump_progress>0)
			stats = true;
		
		if(stats)
		{
			this.blit(matrixStack, leftPos, topPos, 0, 0, imageWidth, imageHeight);
			fill(matrixStack, leftPos+21, topPos+8, leftPos+21+134, topPos+8+92, color);
			
			if(container().tile.client_jump_progress>0)
			{
				drawColoredCircle(this.width / 2, topPos+50, 40, ~color);
				
				if(container().tile.client_jump_progress>=1F)
				{			
					boolean hover = HelperComponent.drawRoundButton(matrixStack, mouseX, mouseY , this.width / 2 -9*2, topPos+55, 18*2, 20);
					HelperComponent.renderSymbol(matrixStack, this.width / 2 -9, topPos+56, getBlitOffset(), 19);
				}
				return;
			}
			
			
			
			int x=leftPos+50, y=topPos+70;		
			drawCircle(matrixStack, x, y, 10, 0xFFFF30^color);
			drawCircle(matrixStack, x, y, 15, 0xFFFF30^color);
			drawCircle(matrixStack, x, y, 20, 0xFFFF30^color);
			drawCircle(matrixStack, x, y, 25, 0xFFFF30^color);
			
			
			
			HelperRendering.glColor4f(0F, 0F, 0F, 1F);
			
			
			if(selectedDrehKnopf==1)
				HelperRendering.glColor4f(1F, 0F, 0F, 1F);
			drawRotatedButton(matrixStack, x, y, 12, 180-rotY);
			if(selectedDrehKnopf==1)
				HelperRendering.glColor4f(0F, 0F, 0F, 1F);
			
			if(selectedDrehKnopf==2)
				HelperRendering.glColor4f(1F, 0F, 0F, 1F);
			drawRotatedButton(matrixStack, x, y, 17, 180-rotX);
			if(selectedDrehKnopf==2)
				HelperRendering.glColor4f(0F, 0F, 0F, 1F);
			
			if(selectedDrehKnopf==3)
				HelperRendering.glColor4f(1F, 0F, 0F, 1F);
			drawRotatedButton(matrixStack, x, y, 22, 180-rotZ);

			
			HelperRendering.glColor4f(1F, 1F, 1F, 1F);	
			
			x+=30;
			y-=25;
			
//			fill(matrixStack, x, y, x+1, y+50, 0xFF000000);
//			
//			int h = 50 -dis/100;
//			fill(matrixStack, x-2, y+h-1, x+3, y+h+1, 0xFF000000);
//			fill(matrixStack, x-1, y+h-1, x+2, y+h+1, 0xFF777777);
//			
//			if(HelperComponent.isInBox(mouseX, mouseY, x-1, y-1, x+2, y+50) && Mouse.isButtonDown(0))
//			{
//				int dy = mouseY - y;
//				dy = 50 - dy;
//				dis = dy * 100;
//				blocks = null;
//			}
			x+=14;
			//y-=5;
			
			if(blocks!=null)
			{
				
				
				HelperComponent.renderItemStackNormal(matrixStack, new ItemStack(Items.FILLED_MAP), x+44, y, getBlitOffset(), false);
				if(HelperComponent.isInBox(mouseX, mouseY, x+44, y, x+44+16, y+16))
				{
					matrixStack.translate(0, 0, 200);
					fill(matrixStack, mouseX-1, mouseY-1, mouseX+1+96, mouseY+1+96, 0xFF000000);
					drawTerrain(matrixStack, mouseX, mouseY);
				}
				float cx = rotX/9F;
				float cy = rotY/9F;
				float cz = rotZ/9F;
				double dis = Math.sqrt(cx*cx + cy*cy + cz*cz);
				int ff = FPSpaceShipSelector.getFuel(dis) * container().tile.thrusterCount;
				
				y += 45;
				this.font.draw(matrixStack, "F: " + ff, x + 5, y, ~color);
			}
			else
			{
				x -= 1;
				y += 43;//Calculate button
				HelperComponent.drawRoundButton(matrixStack, mouseX, mouseY, x, y, 51, 11);		
			}
			
			drawCheckmarks(matrixStack);
		}
		else
		{
			
			this.blit(matrixStack, leftPos, topPos, 0, 0, imageWidth, imageHeight);
			//fill(matrixStack, guiLeft+85, guiTop+20, guiLeft+85+16, guiTop+20+16, 0xFF5b5b5b);	
			
			if(canRender)
			{
				renderButtons(matrixStack, mouseX, mouseY);					
				renderPlanet(matrixStack, mouseX, mouseY, leftPos+56, topPos+12);	
				drawCheckmarks(matrixStack);
			}
		}
		
		HelperComponent.drawRoundButton(matrixStack, mouseX, mouseY, leftPos-8, topPos+41,18, 18);
	}
		
	@Override
	public void run()
	{	
		local = LocalPlayerResearchHelper.getLocalPlayerData();
		canRender = true;
	}
	
	private void drawRotatedButton(PoseStack matrixStack, int x, int y, int r, float rot)
	{
		RenderSystem.disableTexture();
		
		double d1 = Math.sin(Math.toRadians(rot))*r;
		double d2 = Math.cos(Math.toRadians(rot))*r;		
		
		blit(matrixStack, x+(int)d1-1, y+(int)d2-1, 0, 0, 3, 3);
		RenderSystem.enableTexture();
	}
	
	private void renderPlanet(PoseStack matrixStack, int mouseX, int mouseY, int x, int y)
	{
		int w = 64;
		int h = 64;
		IPlanet pa = container().planet[container().seleced];
		if(pa!=null)
		{
			boolean reachable = FPPlanetRegistry.instance.canJump(container().tile, container().planet[1], pa);
			if(reachable)
				HelperRendering.glColor4f(1F,1F,1F,1F);
			else
				HelperRendering.glColor4f(0F,0F,0F,1F);
			
			RenderSystem.setShaderTexture(0, pa.getTexture());
			blit(matrixStack, x, y, w, h, 0, 0, 1, 1, 1, 1);
			
			if(!reachable)
			{
				int xq = x + (64-18)/2;
				int yq= y + (66-18)/2;
				HelperComponent.renderQuestionmark(matrixStack, xq, yq, getBlitOffset());
				if(HelperComponent.isInBox(mouseX, mouseY, x, y, x+w, y+h))
				{
					TextComponent wrapper = new TextComponent("");
					Arrays.stream(container().tile.getMissingUpgrades())
						.map(u -> "spaceship.upgrade." + u.getTranslationKey().toLowerCase())
						.map(TranslatableComponent::new)
						.forEach(wrapper::append);
					TranslatableComponent message = new TranslatableComponent("chat.spaceship.no_upgrade", wrapper);
					message.append(new TextComponent(".\n"));
					Arrays.stream(container().tile.getMissingUpgrades())
						.map(e -> new Tuple<>(e, e.getErrorMessage()))
						.filter(e -> e.getB()!=null)
						.map(e -> new TranslatableComponent("spaceship.upgrade." + e.getA().getTranslationKey().toLowerCase()).append(e.getB()))
						.forEach(message::append);
					
					PartRenderer.renderHoverText(matrixStack, mouseX, mouseY, 450, Collections.singletonList(message), this.width -mouseX - 10);
				}
			}
		}
		
//		if(GuiForscher.drawRoundButton(mouseX, mouseY, x, y, w+4, h+4))
//		{
//			if(!Mouse.isButtonDown(0) && last)
//			{
//				FPPacketHandler.syncWithServer(container());
//			}
//		}
//		
//		
//		
//		
//		
//		if(GuiForscher.drawRoundButton(mouseX, mouseY, guiLeft+200, guiTop+100, 18, 18))
//		{
//			if(!Mouse.isButtonDown(0) && last)
//			{
//				container().home=true;
//				FPPacketHandler.syncWithServer(container());
//			}
//		}
//		this.minecraft.getTextureManager().bindTexture(res);
//		this.blit(matrixStack, guiLeft+201, guiTop+101, 0, 170, 16, 16);
	}
	
	private void renderButtons(PoseStack matrixStack, int mouseX, int mouseY)
	{
//		EnumStateSpaceship state = container().tile.getState();
//		if(state==null)
//		{
//			canRender = false;
//			return;
//		}
		//Tabs
//		int[][] pos = new int[][]{{28,9,126,9},{22,33,132,33},{28,57,126,57}};
		for(int i=0;i<pos.length;i++)
		{
			int hovering = 0;
			if(i==container().seleced)
			{
				hovering=2;
			}
			else if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos, pos[i][0], pos[i][1], pos[i][0]+22, pos[i][1]+22) || HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos, pos[i][2], pos[i][3], pos[i][2]+22, pos[i][3]+22))
			{
				hovering = 1;	
			}
			
			drawRoundButton(matrixStack, leftPos+pos[i][0], topPos+pos[i][1], hovering);
			drawRoundButton(matrixStack, leftPos+pos[i][2], topPos+pos[i][3], hovering);
			
		}		
		boolean hover;		
		hover = HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos, 53, 82, 71, 100);
		if(hover)
		{
			blit(matrixStack, leftPos+53, topPos+82, 178, 49, 18, 18);
		}
		
		hover = HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos, 74, 82, 123, 99);
		if(hover)
		{
			blit(matrixStack, leftPos+74, topPos+82, 199, 49, 49, 18);
		}
		if(!container().tile.getState(EnumStateSpaceship.GO))
		{
			HelperComponent.renderSymbol(matrixStack, leftPos+88, topPos+82, getBlitOffset(), 18);
			if(hover && container().tile.chat!=null)	
			{
				matrixStack.translate(0, 0, 100);
				PartRenderer.renderHoverText(matrixStack, mouseX+10, mouseY, 250+getBlitOffset(), Arrays.asList(container().tile.chat),  (int) (this.width * 0.4));
				matrixStack.translate(0, 0, -100);
			}
		}
		else
		{
			HelperComponent.renderSymbol(matrixStack, leftPos+88, topPos+82, getBlitOffset(), 19);
		}
		
		HelperComponent.renderSymbol(matrixStack, leftPos+53, topPos+82, getBlitOffset(), 20);
				
		for(int i=0;i<pos.length;i++)
		{
			HelperComponent.renderSymbol(matrixStack, leftPos+pos[i][0]+2, topPos+pos[i][1]+2, getBlitOffset(), 24+i);
			HelperComponent.renderSymbol(matrixStack, leftPos+pos[i][2]+2, topPos+pos[i][3]+2, getBlitOffset(), 24+i);
		}
		
	}

	/**
	 * The states to draw as a pre-flight checklist for this version of GUI
	 */

	private static final EnumStateSpaceship[] checklistStates = new EnumStateSpaceship[]
			{
					EnumStateSpaceship.lowEnergie,
					EnumStateSpaceship.open,
					EnumStateSpaceship.missingThruster,
					EnumStateSpaceship.missingEngine,
					EnumStateSpaceship.missingBeam,
					EnumStateSpaceship.missingFuel
			};
	
	private void drawCheckmarks(PoseStack matrixStack)
	{
		int i = 0;
		for(EnumStateSpaceship en : checklistStates)
		{
			HelperComponent.renderSymbol(matrixStack, leftPos+42 + 18*i++, topPos+108, getBlitOffset(), container().tile.getState(en) ? 21 : 19);
		}
	}
	
	private void drawRoundButton(PoseStack matrixStack, int x, int y, int state)
	{
		if(state==0)// not selected
		{
			return;
		}
		else if(state==1)// hover
		{
			this.blit(matrixStack, x, y, 178, 25, 22, 22);
		}
		else if(state==2)//selected
		{
			this.blit(matrixStack, x, y, 178, 1, 22, 22);
		}
	}
	
	@Override
	protected void slotClicked(Slot slotIn, int slotId, int clickedButton, ClickType clickType) 
	{
		super.slotClicked(slotIn, slotId, clickedButton, clickType);
		
		if(stats)
		{
			
		}
		else
		{	
			if(!container().tile.getItem(0).isEmpty())
			{
				container().planet[2] = FPPlanetRegistry.instance.generatePlanetByItem(container().tile.getItem(0));
			}
			else
			{
				container().planet[2] = null;
			}
		}
	}
	
	@Override
	public boolean mouseDragged(double mx, double my, int button, double p_mouseDragged_6_, double p_mouseDragged_8_) 
	{
		if(stats)
		{
			int x=leftPos+50, y=topPos+70;		
			if(HelperComponent.isInBox(mx, my, x-30, y-30, x+30, y+30))
			{
				drawDrehKnopf(x, y, (int)mx, (int)my);					
				blocks = null;
			}
		}
		return super.mouseDragged(mx, my, button, p_mouseDragged_6_, p_mouseDragged_8_);
	}
	
	@Override
	public boolean mouseReleased(double mx, double my, int button) 
	{
		if(stats)
		{
			if(container().tile.client_jump_progress>0)
			{
				if(container().tile.client_jump_progress>=1F)
				{			
					boolean hover = HelperComponent.isInBox(mx, my, this.width / 2 -9*2 , topPos+55, this.width / 2 -9*2 + 18*2, topPos+55+20);
					if(hover)
					{					
						FPPacketHandler.syncWithServer(container());
					}

				}
				return true;
			}
			
			int x=leftPos+50, y=topPos+70;		

			x+=30;
			y-=25;
			x+=14;
			
			if(blocks!=null)
			{
				y+=5;
			}
			else
			{
				x-=1;
				y += 43;//Calculate button click
				if(HelperComponent.isInBox(mx, my, x, y, x+51, y+11))
				{
					BlockPos pos = new BlockPos(X - MiniMapSize.width / 2, Y, Z - MiniMapSize.height / 2);//Minimap origin, target pos minus half map size
					FPPacketHandler.CHANNEL_FUTUREPACK.sendToServer(new MessageRequestTerrain(pos, (byte)MiniMapSize.width, (byte)MiniMapSize.height));//Minimap definition
					container().seleced = 1;
				}			
			}
			
		}
		
		if(stats)
		{
			selectedDrehKnopf = -1;
		}
		else if(canRender)
		{
			for(int i=0;i<pos.length;i++)
			{
				if(HelperComponent.isInBox(mx-leftPos, my-topPos, pos[i][0], pos[i][1], pos[i][0]+22, pos[i][1]+22) || HelperComponent.isInBox(mx-leftPos, my-topPos, pos[i][2], pos[i][3], pos[i][2]+22, pos[i][3]+22))
				{
					container().seleced = i;
				}
			}	
			
			if(HelperComponent.isInBox(mx-leftPos, my-topPos, 53, 82, 71, 100))
			{
				container().home=true;
				FPPacketHandler.syncWithServer(container());
			}
			if(container().tile.getState(EnumStateSpaceship.GO))
			{
				if(HelperComponent.isInBox(mx-leftPos, my-topPos, 74, 82, 123, 99))
				{
					FPPacketHandler.syncWithServer(container());
				}
			}
		}
		
		if(button == 0)
		{
			if(HelperComponent.isInBox(mx, my, leftPos-8, topPos+41, leftPos-8+18, topPos+41+18))
			{
				stats=!stats;
			}
		}
		
		return super.mouseReleased(mx, my, button);
	}
	
	public ContainerBoardComputer container()
	{
		return this.getMenu();
	}
	
	private void drawCircle(PoseStack matrixStack, int x, int y, int r, int color)
	{
		RenderSystem.disableTexture();
		RenderSystem.lineWidth(1F);

		RenderSystem.setShaderColor(1F, 1F, 1F, 1F);
		
		RenderSystem.setShader(GameRenderer::getPositionColorShader);
		RenderSystem.enableBlend();
		RenderSystem.defaultBlendFunc();
		Tesselator tesselator = Tesselator.getInstance();
		BufferBuilder bufferbuilder = tesselator.getBuilder();
		bufferbuilder.begin(VertexFormat.Mode.DEBUG_LINE_STRIP, DefaultVertexFormat.POSITION_COLOR);
		Matrix4f mat = matrixStack.last().pose();
		float red = (((color>>16) & 255) /255F);
		float green = (((color>>8)& 255) /255F);
		float blue = (((color>>0) & 255) /255F);
		
		for(int i=0;i<360;i+=10)
		{
			double d1 = Math.sin(Math.toRadians(i))*r;
			double d2 = Math.cos(Math.toRadians(i))*r;
			bufferbuilder.vertex(mat, (float)d1+x, (float)d2+y, (float)getBlitOffset()).color(red, green, blue, 1F).endVertex();
		}
		double d1 = Math.sin(Math.toRadians(0))*r;
		double d2 = Math.cos(Math.toRadians(0))*r;
		bufferbuilder.vertex(mat, (float)d1+x, (float)d2+y, (float)getBlitOffset()).color(red, green, blue, 1F).endVertex();
		
		tesselator.end();
		RenderSystem.lineWidth(2F);
		RenderSystem.enableTexture();
		RenderSystem.disableBlend();
	}
	
	private void drawColoredCircle(int x, int y, int r, int color)
	{
		GlStateManager._disableTexture();
		GlStateManager._enableBlend();
		HelperRendering.disableAlphaTest();
		GlStateManager._blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA.value, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA.value, GlStateManager.SourceFactor.ONE.value, GlStateManager.DestFactor.ZERO.value);
		RenderSystem.setShader(GameRenderer::getPositionColorShader);
		GL11.glLineWidth(4F);
		
		GL11.glBegin(GL11.GL_LINE_LOOP);
		
		float red = (((color>>16) & 255) /255F);
		float green = (((color>>8)& 255) /255F);
		float blue = (((color>>0) & 255) /255F);
		
		HelperRendering.glColor4f(red, green, blue, 1F);
		
		int j =36- (int) ((System.currentTimeMillis() % 1000)/1000F * 36);
		
		for(int i=0;i<360;i+=10)
		{
			double d1 = Math.sin(Math.toRadians(i))*r;
			double d2 = Math.cos(Math.toRadians(i))*r;
			
			if(j==i/10)
				HelperRendering.glColor4f(1F, 1F, 1F, 1F);
			GL11.glVertex3d(d1+x, d2+y, 10F);
			if(j==i/10)
				HelperRendering.glColor4f(red, green, blue, 1F);
		}
		
		GL11.glEnd();
		
		GL11.glLineWidth(2F);
		RenderSystem.setShader(GameRenderer::getPositionColorShader);
		GlStateManager._disableBlend();
		HelperRendering.enableAlphaTest();
		GlStateManager._enableTexture();
	}
	
	public void drawTerrain(PoseStack matrixStack , int gx, int gy)
	{
//		double scale = 0.6;
		
		
//		gx/=scale;
//		gy/=scale;
		for(int x=0;x<blocks.length;x++)
		{
			for(int y=0;y<blocks[x].length;y++)
			{
				fill(matrixStack, (gx+x), (gy+y), (gx+x)+1, (gy+y)+1, 0xFF000000 | blocks[x][y]);
			}
		}
//		GL11.glScaled(1/scale,1/scale,1/scale);
	}
	
	public void setData(int w, int h, BlockState[] states, byte[] highMap)
	{
		BlockPos pos = new BlockPos(X - w / 2, Y, Z - h / 2);
		Level world = container().tile.getLevel();
		blocks = new int[w*2][h*2];
		int crossBeamSize = 2;
		int crossSize = crossBeamSize * 2 + 1;
		int crossOriginX = (pos.getX() + (w - crossSize) / 2) - pos.getX();
		int crossOriginZ = (pos.getZ() + (h - crossSize) / 2) - pos.getZ();
		int crossSpanX = crossOriginX + crossSize;
		int crossSpanZ = crossOriginZ + crossSize;
		int[] crossField = new int[crossSize * crossSize];
		for (int z = crossOriginZ; z < crossSpanZ; z++)
		{
			for (int x = crossOriginX; x < crossSpanX; x++)
			{
				BlockPos offset = pos.offset(x,0, z);
				int col = states[z * w + x].getMapColor(world, offset).col | 0x111111;
				crossField[(crossSize - (crossSpanZ - z)) * crossSize + crossSize - (crossSpanX - x)] = col;
			}
		}
		int crossColor = contrastBlackOrWhiteForArray(crossField, 0x111111);//The margin is a workaround for addBrighter() flipping marginal colors
		for(int x=0;x<w;x++)
		{
			for(int y=0;y<h;y++)
			{
				BlockPos offset = pos.offset(x,0, y);
				int col = states[y*w +x].getMapColor(world, offset).col | 0x111111;
				if (offset.getX() == X && (Math.abs(offset.getZ() - Z) <= crossBeamSize)) {
					col = crossColor;
				} else if (Math.abs(offset.getX() - X) <= crossBeamSize && (offset.getZ() == Z)) {
					col = crossColor;
				}
				blocks[x*2][y*2] = col;
				blocks[x*2+1][y*2] = col;
				blocks[x*2][y*2+1] = col;
				blocks[x*2+1][y*2+1] = col;
			}
		}
		
		for(int x=1;x<w-1;x++)
		{
			for(int y=1;y<h-1;y++)
			{
				int diff = 10;
				byte now = highMap[y*w+x];
				
				boolean b = true;
				
				if(highMap[y*w+x-1]>now)
				{
					addBrighter(x*2,y*2, -diff);
					addBrighter(x*2,y*2+1, -diff);
					b=false;
				}
				if(highMap[y*w+x -w]>now)
				{
					if(b)
						addBrighter(x*2,y*2, -diff);
					addBrighter(x*2+1,y*2, -diff);
				}
				
				b=true;
				if(highMap[y*w+x+1]>now)
				{
					addBrighter(x*2+1,y*2, diff);
					addBrighter(x*2+1,y*2+1, diff);
					b=false;
				}
				if(highMap[y*w+x+w]>now)
				{
					addBrighter(x*2,y*2+1, diff);
					if(b)
					addBrighter(x*2+1,y*2+1, diff);
				}

			}
		}
	}
	
	private void addBrighter(int x, int y, int diff)
	{
		diff = diff>10?10:diff;
		
		int color = blocks[x][y];
		
		int r = (color>>16) & 255;
		int g = (color>>8) & 255;
		int b = (color) & 255;
		
		r+= diff;
		g+= diff;
		b+= diff;
		
		r = Math.min(255, r);
		g = Math.min(255, g);
		b = Math.min(255, b);
		
		r <<= 16;
		g <<= 8;
		
		blocks[x][y] = 0xFF000000|r|g|b;
	}
	
	private void drawDrehKnopf(int x, int y, int mouseX, int mouseY)
	{
		double dx = x-mouseX;
		double dy = y-mouseY;
		
		double dis = dx*dx + dy*dy;
		
		float rotation = (float) -Math.toDegrees(Math.atan(dx/dy));
		if(dy<0)
			rotation+=180F;
		if(dx>0 && dy>=0)
		{
			rotation+=360;
		}	
		if(Float.isNaN(rotation))
		{
			rotation = 0F;
		}
		
		if(selectedDrehKnopf<0)
		{
			if(dis>=10*10 && dis<15*15)
			{
				selectedDrehKnopf = 1;
			}
			else if(dis>=15*15 && dis<20*20)
			{
				selectedDrehKnopf = 2;
			}
			else if(dis>=20*20  && dis<25*25)
			{
				selectedDrehKnopf = 3;
			}
		}
		else
		{
			float ff = 0.01F;
			
			if(selectedDrehKnopf==1)
			{
				float rot = rotY % 360;		
				rot = rotation - rot;						
				if(rot<-270)
				{
					rot = 360 + rot;
				}
				else if(rot > 270)
				{
					rot = -360 + rot;
				}
				
				if(hasControlDown())
				{
					if(rot>ff)
						rot += 360;
					else if(rot<-ff)
						rot -= 360;
				}
				
				rotY += rot;
			}
			else if(selectedDrehKnopf==2)
			{
				float rot = rotX % 360;		
				rot = rotation - rot;
				if(rot<-270)
				{
					rot = 360 + rot;
				}
				else if(rot > 270)
				{
					rot = -360 + rot;
				}
				
				if(hasControlDown())
				{
					if(rot>ff)
						rot += 360;
					else if(rot<-ff)
						rot -= 360;
				}
				
				rotX += rot;
			}
			else if(selectedDrehKnopf==3)
			{
				float rot = rotZ % 360;	
				rot = rotation - rot;					
				if(rot<-270)
				{
					rot = 360 + rot;
				}
				else if(rot > 270)
				{
					rot = -360 + rot;
				}	
				
				if(hasControlDown())
				{
					if(rot>ff)
						rot += 360;
					else if(rot<-ff)
						rot -= 360;
				}
				
				rotZ += rot;
			}
		}
	}
	
	public static class ContainerBoardComputer extends ContainerSyncBase implements IGuiSyncronisedContainer, ISyncable
	{
		public boolean home;
		Player pl;
		TileEntityBoardComputer tile;
		
		private IPlanet[] planet;
		private int seleced;
		
		private BlockPos dimensionMove = null;
		private int minimapWidth;
		private int minimapHeight;

		public ContainerBoardComputer(Inventory inv, TileEntityBoardComputer tile)
		{
			super(tile, tile.getLevel().isClientSide());
			pl = inv.player;
			this.tile = tile;
			dimensionMove = tile.getTelePos();
			planet = new IPlanet[3];
			minimapWidth = MiniMapSize.width;
			minimapHeight = MiniMapSize.height;
			
			if(!tile.getItem(0).isEmpty())
			{
				planet[2] = FPPlanetRegistry.instance.generatePlanetByItem(tile.getItem(0));
			}
			else
			{
				planet[2] = null;
			}
			planet[0] = FPPlanetRegistry.instance.MINECRAFT.get();
			planet[1] = FPPlanetRegistry.instance.getPlanetSafe(tile.getLevel());
			
			
			this.addSlot(new SlotUses(tile, 0, 20, 108, 1));
			
			int x,y;
			for (y = 0; y < 3; ++y)
			{
				for (x = 0; x < 9; ++x)
				{
					this.addSlot(new Slot(inv, x + y * 9 + 9, 8 + x * 18, 140 + y * 18));
				}
			}	
			for(x=0;x<9;x++)
			{
				this.addSlot(new Slot(inv, x, 8 + 18*x,198));
			}
		}

		@Override
		public boolean stillValid(Player playerIn)
		{
			return true;
		}

		@Override
		public void writeToBuffer(FriendlyByteBuf buf)
		{
			buf.writeVarInt(seleced);
			buf.writeBoolean(home);
			home=false;
		}

		@Override
		public void readFromBuffer(FriendlyByteBuf buf)
		{
			seleced = buf.readVarInt();
			home = buf.readBoolean();
			tile.currentUser = pl;
			
			CompoundTag fp = CapabilityPlayerData.getPlayerdata(pl);
			if(System.currentTimeMillis() - fp.getLong("lastJump") < FPConfig.SERVER.spaceTravelCooldown.get())
			{
				//if last jump is less than 1 minute ago.
				pl.sendMessage(new TranslatableComponent("spaceship.jump.colldown"), Util.NIL_UUID);
				return;
			}
			
			if(home)
			{
				home = false;
				tile.home();
			}
			else
			{
				tile.prepareJump(planet[seleced], dimensionMove);
			}
			
		}
		
		@Override
		public ItemStack quickMoveStack(Player par1EntityPlayer, int par2)
		{
			 Slot slot = this.slots.get(par2);
			 if(slot !=null && slot.hasItem())
			 {
				 if(par2==0)
				 {
					 this.moveItemStackTo(slot.getItem(), 1, this.slots.size(), false);
				 }
				 else
				 {
					 this.moveItemStackTo(slot.getItem(), 0, 1, false);
				 }
				 
				 if(slot.getItem().getCount()<=0)
				 {
					 slot.set(ItemStack.EMPTY);
				 }
				 this.broadcastChanges();
			 }
			return ItemStack.EMPTY;
		}

		@Override
		public void broadcastChanges()
		{
			if(tile.getItem(0)!=null)
			{
				planet[2] = FPPlanetRegistry.instance.generatePlanetByItem(tile.getItem(0));
			}
			else
			{
				planet[2] = null;
			}
			
			super.broadcastChanges();
			if(tile.chat!=null && pointer==0)
			{
				FPPacketHandler.sendToClient(this, pl);
				pointer=-1;
			}
		}
		
		int pointer = 0;
		
		@Override
		public void setData(int id, int data)
		{
			super.setData(id, data);
		}

		public void setCoords(int x, int y, int z)
		{
			dimensionMove = new BlockPos(x + minimapWidth / 2, y, z + minimapHeight / 2);//Minimap origin plus half map size. That's a bit dirty and error-prone
			seleced = 1;
		}

		@Override
		public void writeAdditional(DataOutputStream buffer) throws IOException 
		{
			String s = Component.Serializer.toJson(tile.chat);
			buffer.writeUTF(s);
		}

		@Override
		public void readAdditional(DataInputStream buffer) throws IOException 
		{
			String json = buffer.readUTF();
			Component comp = Component.Serializer.fromJsonLenient(json);
			tile.chat = comp;
			buffer=null;
			pointer=0;
			pl.sendMessage(comp, Util.NIL_UUID);
		}
		
	}
	
	//Advanced guis
	
	public static class White extends GuiBoardComputer
	{

		public White(Player pl, TileEntityBoardComputer tile)
		{
			super(pl, tile);
			super.res = new ResourceLocation(Constants.MOD_ID, "textures/gui/boardcomputer_w.png");
			super.pos = new int[][]{{28,9,126,9},{22,33,132,33},{28,57,126,57}};
		}
		
	}
	
	public static class Gray extends GuiBoardComputer
	{

		public Gray(Player pl, TileEntityBoardComputer tile)
		{
			super(pl, tile);
			super.res = new ResourceLocation(Constants.MOD_ID, "textures/gui/boardcomputer_g.png");
			super.pos = new int[][]{{28,9,126,9},{28,33,126,33},{28,57,126,57}};
			super.color = 0xff1b893c;
		}
		
	}
	
	public static class Black extends GuiBoardComputer
	{

		public Black(Player pl, TileEntityBoardComputer tile)
		{
			super(pl, tile);
			super.res = new ResourceLocation(Constants.MOD_ID, "textures/gui/boardcomputer_s.png");
			super.pos = new int[][]{{22,9,132,9},{25,33,129,33},{28,57,126,57}};
			super.color = 0xff8c0b11;
		}
		
	}
}
