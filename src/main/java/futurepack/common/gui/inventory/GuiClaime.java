package futurepack.common.gui.inventory;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.block.misc.TileEntityClaime;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperRendering;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.client.gui.components.AbstractButton;
import net.minecraft.client.gui.components.EditBox;
import net.minecraft.client.gui.narration.NarrationElementOutput;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;

public class GuiClaime extends ActuallyUseableContainerScreen<GuiClaime.ContainerClaime>
{
	TileEntityClaime tile;
	EditBox text;
	
	
	public GuiClaime(Player pl, TileEntityClaime tile)
	{
		super(new ContainerClaime(pl.getInventory(), tile), pl.getInventory(), "gui.claime");
		this.tile=tile;
		imageWidth=248;
	}

	@Override
	public void init() 
	{
		super.init();
		int k = (this.width - this.imageWidth) / 2 -6;
		int l = (this.height - this.imageHeight) / 2;
		
		addRenderableWidget(new AbstractButton(k+35, l+39, 10, 20, new TextComponent("-")) {
			@Override
			public void onPress() 
			{
				int i= hasControlDown()? 100 : hasShiftDown()? 10 : 1;
				tile.x-=i;
				FPPacketHandler.syncWithServer(getMenu());
			}

			@Override
			public void updateNarration(NarrationElementOutput pNarrationElementOutput)
			{
				// TODO Auto-generated method stub
				
			}
		});
		addRenderableWidget(new AbstractButton(k+35, l+61, 10, 20, new TextComponent("-")) {
			@Override
			public void onPress() 
			{
				int i= hasControlDown()? 100 : hasShiftDown()? 10 : 1;
				tile.y-=i;
				FPPacketHandler.syncWithServer(getMenu());
			}

			@Override
			public void updateNarration(NarrationElementOutput pNarrationElementOutput)
			{
				// TODO Auto-generated method stub
				
			}
		});
		addRenderableWidget(new AbstractButton(k+35, l+83, 10, 20, new TextComponent("-")) {
			@Override
			public void onPress() 
			{
				int i= hasControlDown()? 100 : hasShiftDown()? 10 : 1;
				tile.z-=i;
				FPPacketHandler.syncWithServer(getMenu());
			}

			@Override
			public void updateNarration(NarrationElementOutput pNarrationElementOutput)
			{
				// TODO Auto-generated method stub
				
			}
		});
		
		addRenderableWidget(new AbstractButton(k+110, l+39, 10, 20, new TextComponent("+")) {
			@Override
			public void onPress() 
			{
				int i= hasControlDown()? 100 : hasShiftDown()? 10 : 1;
				tile.x+=i;
				FPPacketHandler.syncWithServer(getMenu());
			}

			@Override
			public void updateNarration(NarrationElementOutput pNarrationElementOutput)
			{
				// TODO Auto-generated method stub
				
			}
		});
		addRenderableWidget(new AbstractButton(k+110, l+61, 10, 20, new TextComponent("+")) {
			@Override
			public void onPress() 
			{
				int i= hasControlDown()? 100 : hasShiftDown()? 10 : 1;
				tile.y+=i;
				FPPacketHandler.syncWithServer(getMenu());
			}

			@Override
			public void updateNarration(NarrationElementOutput pNarrationElementOutput)
			{
				// TODO Auto-generated method stub
				
			}
		});
		addRenderableWidget(new AbstractButton(k+110, l+83, 10, 20, new TextComponent("+")) {
			@Override
			public void onPress() 
			{
				int i= hasControlDown()? 100 : hasShiftDown()? 10 : 1;
				tile.z+=i;
				FPPacketHandler.syncWithServer(getMenu());
			}

			@Override
			public void updateNarration(NarrationElementOutput pNarrationElementOutput)
			{
				// TODO Auto-generated method stub
				
			}
		});
		
		
		addRenderableWidget(new AbstractButton(k+35+124, l+39, 10, 20, new TextComponent("-")) {
			@Override
			public void onPress() 
			{
				int i= hasControlDown()? 100 : hasShiftDown()? 10 : 1;
				tile.mx-=i;
				FPPacketHandler.syncWithServer(getMenu());
			}

			@Override
			public void updateNarration(NarrationElementOutput pNarrationElementOutput)
			{
				// TODO Auto-generated method stub
				
			}
		});
		addRenderableWidget(new AbstractButton(k+35+124, l+61, 10, 20, new TextComponent("-")) {
			@Override
			public void onPress() 
			{
				int i= hasControlDown()? 100 : hasShiftDown()? 10 : 1;
				tile.my-=i;
				FPPacketHandler.syncWithServer(getMenu());
			}

			@Override
			public void updateNarration(NarrationElementOutput pNarrationElementOutput)
			{
				// TODO Auto-generated method stub
				
			}
		});
		addRenderableWidget(new AbstractButton(k+35+124, l+83, 10, 20, new TextComponent("-")) {
			@Override
			public void onPress() 
			{
				int i= hasControlDown()? 100 : hasShiftDown()? 10 : 1;
				tile.mz-=i;
				FPPacketHandler.syncWithServer(getMenu());
			}

			@Override
			public void updateNarration(NarrationElementOutput pNarrationElementOutput)
			{
				// TODO Auto-generated method stub
				
			}
		});
		
		addRenderableWidget(new AbstractButton(k+110+124, l+39, 10, 20, new TextComponent("+")) {
			@Override
			public void onPress() 
			{
				int i= hasControlDown()? 100 : hasShiftDown()? 10 : 1;
				tile.mx+=i;
				FPPacketHandler.syncWithServer(getMenu());
			}

			@Override
			public void updateNarration(NarrationElementOutput pNarrationElementOutput)
			{
				// TODO Auto-generated method stub
				
			}
		});
		addRenderableWidget(new AbstractButton(k+110+124, l+61, 10, 20, new TextComponent("+")) {
			@Override
			public void onPress() 
			{
				int i= hasControlDown()? 100 : hasShiftDown()? 10 : 1;
				tile.my+=i;
				FPPacketHandler.syncWithServer(getMenu());
			}

			@Override
			public void updateNarration(NarrationElementOutput pNarrationElementOutput)
			{
				// TODO Auto-generated method stub
				
			}
		});
		addRenderableWidget(new AbstractButton(k+110+124, l+83, 10, 20, new TextComponent("+")) {
			@Override
			public void onPress() 
			{
				int i= hasControlDown()? 100 : hasShiftDown()? 10 : 1;
				tile.mz+=i;
				FPPacketHandler.syncWithServer(getMenu());
			}

			@Override
			public void updateNarration(NarrationElementOutput pNarrationElementOutput)
			{
				// TODO Auto-generated method stub
				
			}
		});
		
		addRenderableWidget(new AbstractButton(k+26, l+110, 95, 20, new TextComponent("Render:"+tile.renderAll)) {
			@Override
			public void onPress() 
			{
				tile.renderAll = !tile.renderAll;
				this.setMessage(new TextComponent("Render:"+tile.renderAll));
				FPPacketHandler.syncWithServer(getMenu());
			}

			@Override
			public void updateNarration(NarrationElementOutput pNarrationElementOutput)
			{
				// TODO Auto-generated method stub
				
			}
		});
		
		
		this.text = new EditBox(this.font, k + 27+124, l + 115, 100, 12, new TranslatableComponent("gui.claime.textbox")) {
			@Override
			public boolean charTyped(char p_charTyped_1_, int p_charTyped_2_) 
			{
				boolean b = super.charTyped(p_charTyped_1_, p_charTyped_2_);
				if(tile.name != this.getValue())
				{
					tile.name = getValue();
					FPPacketHandler.syncWithServer(getMenu());
				}
				return b;
			}
		};	
        this.text.setTextColor(-1);
        this.text.setTextColorUneditable(-1);
        this.text.setBordered(false);
        this.text.setMaxLength(39);
        this.text.setValue(tile.name);
        this.text.setCanLoseFocus(false);
        this.text.changeFocus(true);
        addRenderableWidget(this.text);
        
	}
	
	@Override
	protected void renderLabels(PoseStack matrixStack, int p_146979_1_, int p_146979_2_)
	{
		this.font.draw(matrixStack, I18n.get("container.claime", new Object[0]), 20, 6, 4210752);
		this.font.draw(matrixStack, I18n.get("Use Shift Click for +/-10", new Object[0]), 20, this.imageHeight-font.lineHeight-6, 4210752);
		
		this.font.draw(matrixStack, "Size", 20, 24, 4210752);
		
		this.font.draw(matrixStack, "X", 20, 45, 4210752);
		this.font.draw(matrixStack, "Y", 20, 67, 4210752);
		this.font.draw(matrixStack, "Z", 20, 89, 4210752);
		
		this.font.draw(matrixStack, ""+tile.x, 45, 45,  0xffffffff);
		this.font.draw(matrixStack, ""+tile.y, 45, 67,  0xffffffff);
		this.font.draw(matrixStack, ""+tile.z, 45, 89,  0xffffffff);
		
		this.font.draw(matrixStack, "Middle", 20+124, 24, 4210752);
		
		this.font.draw(matrixStack, "X", 20+124, 45, 4210752);
		this.font.draw(matrixStack, "Y", 20+124, 67, 4210752);
		this.font.draw(matrixStack, "Z", 20+124, 89, 4210752);
		
		this.font.draw(matrixStack, ""+tile.mx, 45+124, 45, 0xffffffff);
		this.font.draw(matrixStack, ""+tile.my, 45+124, 67,  0xffffffff);
		this.font.draw(matrixStack, ""+tile.mz, 45+124, 89,  0xffffffff);
	}
	
	@Override
	protected void renderBg(PoseStack matrixStack, float p_146976_1_, int p_146976_2_, int p_146976_3_)
	{
		RenderSystem.setShaderTexture(0, new ResourceLocation("textures/gui/demo_background.png"));
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);
		
		fill(matrixStack, k+29, l+39, k+114, l+39+20, 0xff000000);
		fill(matrixStack, k+29, l+61, k+114, l+61+20, 0xff000000);
		fill(matrixStack, k+29, l+83, k+114, l+83+20, 0xff000000);
		
		fill(matrixStack, k+29+124, l+39, k+114+124, l+39+20, 0xff000000);
		fill(matrixStack, k+29+124, l+61, k+114+124, l+61+20, 0xff000000);
		fill(matrixStack, k+29+124, l+83, k+114+124, l+83+20, 0xff000000);
		
		fill(matrixStack, k+20+124-2, l+110, k+20+124+100-2, l+110+20, 0xff000000);
	}

	


    @Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
    	this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        GL11.glDisable(GL11.GL_LIGHTING);
        GL11.glDisable(GL11.GL_BLEND);
        this.text.render(matrixStack, mouseX, mouseY, partialTicks);
        
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
    
    @Override
    public boolean keyPressed(int keyCode, int p_keyPressed_2_, int p_keyPressed_3_) 
    {
        if (keyCode == 256) {
           this.minecraft.player.closeContainer();
        }

        //return !this.text.keyPressed(keyCode, p_keyPressed_2_, p_keyPressed_3_) && !this.text.canConsumeInput() ? super.keyPressed(keyCode, p_keyPressed_2_, p_keyPressed_3_) : true;
        return super.keyPressed(keyCode, p_keyPressed_2_, p_keyPressed_3_);
     }
	
	public static class ContainerClaime extends ContainerSyncBase implements IGuiSyncronisedContainer
	{
		TileEntityClaime tile;
		
		
		public ContainerClaime(Inventory pl, TileEntityClaime tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
		}
		
		@Override
		public boolean stillValid(Player pl)
		{
			return true;
		}

		@Override
		public void writeToBuffer(FriendlyByteBuf buf) 
		{
			CompoundTag nbt = new CompoundTag();
			tile.save(nbt);
			IGuiSyncronisedContainer.writeNBT(buf, nbt);
		}

		@Override
		public void readFromBuffer(FriendlyByteBuf buf) 
		{
			CompoundTag nbt = IGuiSyncronisedContainer.readNBT(buf);
			tile.load(nbt);
			tile.BroudcastData();
		}
		
	}
}
