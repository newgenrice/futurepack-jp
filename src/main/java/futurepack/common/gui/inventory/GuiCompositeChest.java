package futurepack.common.gui.inventory;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.gui.SlotUses;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.helper.HelperRendering;
import futurepack.depend.api.interfaces.ITileInventoryProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.Container;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class GuiCompositeChest extends ActuallyUseableContainerScreen<GuiCompositeChest.GenericContainer>
{
	private final int inventoryRows;
	private final int slotPerLine;
	private static final ResourceLocation CHEST_GUI_TEXTURE = new ResourceLocation("textures/gui/container/generic_54.png");
	private static final ResourceLocation DISPENSER_GUI_TEXTURE = new ResourceLocation("textures/gui/container/dispenser.png");

	public GuiCompositeChest(Player pl, ITileInventoryProvider upperInv)
	{
		
		super(upperInv.getInventoryContainer(pl.getInventory()), pl.getInventory(), upperInv.getGUITitle());
		this.inventoryRows = this.getMenu().getSlotLines();
		this.slotPerLine = this.getMenu().getSlotsPerLine();
		
		if(slotPerLine==9)
		{
			imageHeight = 97 + 17 + this.inventoryRows * 18;
		}
		else if(slotPerLine==3)
		{
			imageHeight = 96 + 16 + this.inventoryRows * 18;
		}
	}
	
	@Override
	public void render(PoseStack matrixStack, int mx, int my, float partialTicks) 
	{
		this.renderBackground(matrixStack);
		super.render(matrixStack, mx, my, partialTicks);
		this.renderTooltip(matrixStack, mx, my);
	}

	@Override
	protected void renderLabels(PoseStack matrixStack, int mouseX, int mouseY) 
	{
		this.font.draw(matrixStack, this.title.getString(), 8.0F, 6.0F, 4210752);
		this.font.draw(matrixStack, playerInventoryTitle.getString(), 8.0F, this.imageHeight - 96 + 2, 4210752);
	}
	
	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		if(slotPerLine == 9)
			RenderSystem.setShaderTexture(0, CHEST_GUI_TEXTURE);
		else if(slotPerLine == 3)
			RenderSystem.setShaderTexture(0, DISPENSER_GUI_TEXTURE);
		
		int x = (this.width - this.imageWidth) / 2;
		int y = (this.height - this.imageHeight) / 2;
		
		int invY = 0;
		if(slotPerLine == 9)
		{
			if(inventoryRows >= 6)
			{
				this.blit(matrixStack, x, y, 0, 0, this.imageWidth, 6 * 18 + 17);
				this.blit(matrixStack, x, y + 6*18+17, 0, 17, this.imageWidth, (this.inventoryRows - 6) * 18);
			}
			else
			{
				this.blit(matrixStack, x, y, 0, 0, this.imageWidth, this.inventoryRows * 18 + 17);
			}
			invY = this.inventoryRows * 18 + 17;
			
			this.blit(matrixStack, x, y + invY, 0, 125, this.imageWidth, 97);
		}
		else if(slotPerLine==3)
		{
			if(inventoryRows >= 3)
			{
				this.blit(matrixStack, x, y, 0, 0, this.imageWidth, 6 * 18 + 16);
				this.blit(matrixStack, x, y +6*18+16, 0, 0, this.imageWidth, (this.inventoryRows-6) * 18);
			}
			else
			{
				this.blit(matrixStack, x, y, 0, 0, this.imageWidth, this.inventoryRows * 18 + 16);
			}
			invY = this.inventoryRows * 18 + 16;
			this.blit(matrixStack, x, y + invY, 0, 70, this.imageWidth, 96);
		}
		
	}
	
	public static class GenericContainer extends ActuallyUseableContainer
	{
		private final int slot_count;
		private int lines;
		private final Container base;
		private final int slotsPerLine;
		
		public GenericContainer(Inventory inv, ITileInventoryProvider store)
		{
			base = store.getInventory();
			base.startOpen(inv.player);
			
			slot_count = base.getContainerSize();
			slotsPerLine = (slot_count % 3 == 0 && slot_count/3 <= 3) ? 3 : 9;
			
			lines = slot_count / slotsPerLine;
			if(lines*slotsPerLine < slot_count)
				lines++;
			
			int x,y;
			x = (178 - 18 * slotsPerLine) / 2; 
			y = slotsPerLine==9 ? 17 : 16;
			y+=1;
			
			for (int l = 0; l < lines; ++l)
	        {
	            for (int i1 = 0; i1 < slotsPerLine; ++i1)
	            {
	            	int id = i1 + l * slotsPerLine;
	            	if(id >= slot_count)
	            		break;
	                this.addSlot(new SlotUses(base, id, x + i1 * 18, y + l * 18));
	            }
	        }
			y+= lines * 18;
			if(slotsPerLine==3)
				y+=13;
			else if(slotsPerLine==9)
				y+=14;
			
			HelperContainerSync.addInventorySlots(8, y, inv, this::addSlot);
			
			int slots = super.slots.size();
		}
		
		public int getSlotLines()
		{
			return lines;
		}
		
		public int getSlotsPerLine()
		{
			return slotsPerLine;
		}
		
		
		@Override
		public boolean stillValid(Player playerIn) 
		{
			return true;
		}
		
		@Override
		public void removed(Player playerIn) 
		{
			base.stopOpen(playerIn);
			super.removed(playerIn);
		}
		
		@Override
		public ItemStack quickMoveStack(Player pl, int sl)
		{
			if(!pl.level.isClientSide)
			{
				Slot slot= getSlot(sl);
				if(!slot.hasItem())		
					return ItemStack.EMPTY;
				
				if(slot.container == pl.getInventory())
				{
					this.moveItemStackTo(slot.getItem(), 0, slot_count, false);
				}
				else
				{
					this.moveItemStackTo(slot.getItem(), slot_count, this.slots.size(), false);
				}
			
				if(slot.getItem().getCount()<=0)
				{
					slot.set(ItemStack.EMPTY);
				}
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}

		public Container getBase() {
			return base;
		}
	}
}
