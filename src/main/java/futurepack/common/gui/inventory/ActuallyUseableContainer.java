package futurepack.common.gui.inventory;

import net.minecraft.world.inventory.AbstractContainerMenu;

public abstract class ActuallyUseableContainer extends AbstractContainerMenu 
{

	protected ActuallyUseableContainer() 
	{
		super(null, -1);//will be ovrwritten later
	}
}

