package futurepack.common.gui.inventory;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.common.gui.SlotFakeItem;
import futurepack.common.item.tools.ItemAirBrush;
import futurepack.common.item.tools.ToolItems;
import futurepack.depend.api.helper.HelperRendering;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.Container;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ResultContainer;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class GuiAirBrush extends ActuallyUseableContainerScreen<GuiAirBrush.ContainerAirBrush>
{

	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/airbrush.png");
	
	public GuiAirBrush(Player pl)
	{
		super(new ContainerAirBrush(pl), pl.getInventory(), "gui.airbrush");
	}

	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderBg(PoseStack matrixStack, float var1, int var2, int var3) 
	{
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		RenderSystem.setShaderTexture(0, res);
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);

	}
	
	public static class ContainerAirBrush extends ActuallyUseableContainer
	{
		public Inventory inv;
		public Container airbrush = new ResultContainer();
		private ItemStack it;
		
		public ContainerAirBrush(Player pl)
		{
			inv = pl.getInventory();
			it = pl.getMainHandItem();
			if(it ==null || it.getItem()!=ToolItems.airBrush)
			{
				it = pl.getOffhandItem();
				if(it==null|| it.getItem()!=ToolItems.airBrush)
				{
					pl.closeContainer();
					return;
				}
			}
			if(it.hasTag())
				airbrush.setItem(0, ItemAirBrush.getItem(it.getTag()));
			
			this.addSlot(new Slot(airbrush, 0, 98, 47));
			
			for (int l = 0; l < 3; ++l)
			{
				for (int i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
			for (int l = 0; l < 9; ++l)
			{
				if(inv.selected == l)
				{
					this.addSlot(new SlotFakeItem(inv, l, 8 + l * 18, 142));
				}
				else
				{
					this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
				}
				
			}
		}
		
		@Override
		public ItemStack quickMoveStack(Player par1EntityPlayer, int par2)
		{
			 Slot slot = this.slots.get(par2);
			 if(slot !=null)
			 {
				 Slot target = slots.get(0);
				 if(target!=null && target.mayPlace(slot.getItem()))
				 {
					  if(!target.hasItem())
					  {
						  target.set(slot.getItem());
						  slot.set(ItemStack.EMPTY);
						  return ItemStack.EMPTY;
					  }
				 }
				 if(slot == target && target!=null)
				 {
					 if(target.hasItem() && slot!=null)
					 {
						 if(this.moveItemStackTo(target.getItem(), 1, slots.size()-1, false))
						 {
							 target.set(ItemStack.EMPTY);
						 }
					 }
					 return ItemStack.EMPTY;
				 }
				
				 return ItemStack.EMPTY;
			 }
			return super.quickMoveStack(par1EntityPlayer, par2);
		}
		
		@Override
		public boolean stillValid(Player var1)
		{
			return true;
		}
		
		@Override
		public void removed(Player pl)
		{
			if(it!=null)
			{
				if(!it.hasTag())
					it.setTag(new CompoundTag());
				
				ItemAirBrush.setItem(it.getTag(), airbrush.getItem(0));
				//pl.setHeldItem(EnumHand.MAIN_HAND, it);
			}
			super.removed(pl);
		}
	}
}
