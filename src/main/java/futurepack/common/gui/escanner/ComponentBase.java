package futurepack.common.gui.escanner;

import com.google.gson.JsonObject;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.depend.api.interfaces.IGuiComponent;
import net.minecraft.client.gui.screens.Screen;

/**
 * Register your component here {@link futurepack.common.research.ScannerPageResearch}
 */
public abstract class ComponentBase implements IGuiComponent
{

	public ComponentBase(JsonObject obj)
	{
		
	}
	
	@Override
	public abstract void init(int maxWidth, Screen gui);

	int ad;
	
	@Override
	public void setAdditionHeight(int additionHight)
	{
		ad = additionHight;
	}

	@Override
	public int getAdditionHeight()
	{
		return ad;
	}

	@Override
	public abstract int getWidth();

	@Override
	public abstract int getHeight();

	@Override
	public abstract void render(PoseStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, GuiScannerBase gui);

	@Override
	public abstract void postRendering(PoseStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, boolean hover, GuiScannerBase gui);
	
	@Override
	public void onClicked(int x, int y, int mouseButton, double mouseX, double mouseY, GuiScannerBase gui)
	{
		
	}

}
