package futurepack.common.gui.escanner;

import java.util.Arrays;
import java.util.Collections;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.client.research.LocalPlayerResearchHelper;
import futurepack.common.gui.PartRenderer;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.research.Research;
import futurepack.common.research.ResearchPage;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperRendering;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.protocol.game.ServerboundClientCommandPacket;

public class GuiResearchMainOverview extends GuiResearchMainOverviewBase implements Runnable
{
//	Random r = new Random(42L);
//	TextureAtlasSprite[][] background = new TextureAtlasSprite[32][32];
	
//	ResourceLocation border = new ResourceLocation(Constants.MOD_ID, "textures/gui/FP-ForschungRand.png");
//	ResourceLocation bg = new ResourceLocation(Constants.MOD_ID, "textures/gui/FP-ForschungHintergrund.png");
//	ResourceLocation slot = new ResourceLocation(Constants.MOD_ID, "textures/gui/SlotBg.png");
	CustomPlayerData custom=null;
//	int bgw=980,bgh=540;	
	
	static ResearchPage[] tabs;
	static int selected = -1;
	GuiResearchTreePage page=null;
	
	private Screen[] buffer;
	private boolean moved;
	
	public GuiResearchMainOverview()
	{
		super("gui.research.overview");
//		tabs = new ResearchPage[ResearchPage.size];
//		System.arraycopy(ResearchPage.pages, 0, tabs, 0, tabs.length);
		GuiResearchMainOverview.tabs = Arrays.copyOf(ResearchPage.pages,ResearchPage.size);
	}
	
	@Override
	public void init() 
	{	
		if(page!=null)
		{
			page.init(this.minecraft, width, height);
		}
		super.init();
		this.minecraft.getConnection().send(new ServerboundClientCommandPacket(ServerboundClientCommandPacket.Action.REQUEST_STATS));
		LocalPlayerResearchHelper.requestServerData(this);
	}
	
//	private TextureAtlasSprite getIcon(ItemStack it)
//	{
//		return Minecraft.getInstance().getBlockRendererDispatcher().getBlockModelShapes().getTexture(Block.getBlockFromItem(it.getItem()).getStateFromMeta(it.getItemDamage()));
//	}
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
	{
		
		renderBackground(matrixStack);
		
		if(custom!=null)
		{		
			if(page != null)
				page.render(matrixStack, mouseX, mouseY, partialTicks);
		}
		else
		{
			//this.drawCenteredString(matrixStack, font, "loading data ...", width/2, height/2, 0xff0000);
			HelperRendering.glColor4f(1F, 1F, 1F, 1F);
		}
		
		RenderSystem.disableDepthTest();
		drawTabs(matrixStack, mouseX,mouseY);
		RenderSystem.enableDepthTest();
		
		//Buttons
		super.render(matrixStack, mouseX, mouseY, partialTicks);
	}

//	
	
	@Override
	public boolean mouseDragged(double x, double y, int button,	double p_mouseDragged_6_, double p_mouseDragged_8_)
	{
		if(this.page!=null)
		{
			return this.page.mouseDragged(x, y, button, p_mouseDragged_6_, p_mouseDragged_8_);
		}
		moved = true;
		return super.mouseDragged(x, y, button, p_mouseDragged_6_, p_mouseDragged_8_);
	}
	
	@Override
	public boolean mouseReleased(double mx, double my, int button) 
	{
		if(button == 0)
		{
			int x = (this.width - 256) / 2;
			int y = (this.height - 256) / 2;
			for(int i=0;i<tabs.length;i++)
			{
				boolean right = i>8;
				int pos = right?i-9 : i;			
				int tx = x + (right?256:-28);
				int ty = y + pos*28;
				
				if(HelperComponent.isInBox(mx, my, tx, ty, tx+28, ty+28))
				{
					if(!moved)
					{
						initTabClick(i);
						return true;
					}
				}
			}		
		}
		moved  = false;
		if(this.page!=null)
		{
			return this.page.mouseReleased(mx, my, button);
		}
		
		return super.mouseReleased(mx, my, button);
	}
	
	@Override
	public boolean mouseScrolled(double mx, double my, double dw) 
	{
		if(this.page!=null)
		{
			return this.page.mouseScrolled(mx, my, dw);
		}
		return super.mouseScrolled(mx, my, dw);
	}
	
	private void drawTabs(PoseStack matrixStack, int mx, int my)
	{
		HelperRendering.glColor4f(1F, 1F, 1F, 1F);
		this.setBlitOffset(220);
		int x = (this.width - 256) / 2;
		int y = (this.height - 256) / 2;

		for(int i=0;i<tabs.length;i++)
		{
			if(!tabs[i].isVisible(custom))
				continue;
			
			boolean activated = i == selected;
			boolean right = i>8;
			int pos = right?i-9 : i;			
			int tx = x + (right?256:-28);
			int ty = y + pos*28;
			
			PartRenderer.renderTabBase(matrixStack, tx, ty, right, mx, my, activated, getBlitOffset());
			
			if(tabs[i].getIcon()!=null)
			{					
				tabs[i].getIcon().render(matrixStack, mx, my, tx+6, ty+6, getBlitOffset());
			}
			
			if(hasUnreadResearch(tabs[i]))
			{
				int ix = tx +20;
				int iy = ty +6;
				
				int base= 0xff0050;
				int a = (int) ((Math.sin(  (System.currentTimeMillis()/70+ ix*iy)/3.0)) * 64) +128;
				GuiComponent.fill(matrixStack, ix, iy, ix+3, iy+3, (255)<<24 | 0x000000);
				
				GuiComponent.fill(matrixStack, ix-2, iy+1, ix+5, iy+2, a<<24 | base);
				GuiComponent.fill(matrixStack, ix+1, iy-2, ix+2, iy+5, a<<24 | base);
				
				HelperRendering.glColor4f(1F, 1F, 1F, 1F);
			}
		}		
		for(int i=0;i<tabs.length;i++)
		{
			boolean right = i>8;
			int pos = right?i-9 : i;			
			int tx = x + (right?256:-28);
			int ty = y + pos*28;
			
			if(HelperComponent.isInBox(mx, my, tx, ty, tx+28, ty+28))
			{	
				PartRenderer.drawHoveringTextFixedString(matrixStack, Arrays.asList(tabs[i].getLocalizedName()), mx - 2, my + 10, -1, font);
				//renderTooltip(, mx, my);
				//PartRenderer.renderHoverText(mx + 7, my + 4, getBlitOffset() + 200, tabs[i].getLocalizedName());
			}
		}
		this.setBlitOffset(0);
	}

	private boolean hasUnreadResearch(ResearchPage page)
	{
		for(Research r : page.getEntries())
		{
			if(Collections.binarySearch(LocalPlayerResearchHelper.notReaded, r.getName())>=0)
			{
				return true;
			}
		}
		return false;
	}
	
	@Override
	public void run()
	{
		custom = LocalPlayerResearchHelper.getLocalPlayerData();
		tabs = Arrays.stream(ResearchPage.pages)
				.filter(p -> p!=null && p.isVisible(custom))
				.toArray(ResearchPage[]::new);
		
		if(selected==-1)
			initTabClick(0);
		else if(page!=null)
			page.updatePlayerData(custom);
		else if(page==null)
			initTabClick(selected);
	}
	
	@Override
	public boolean isPauseScreen()
	{
		return false;
	}
	
	@Override
	public boolean charTyped(char typedChar, int keyCode)
	{
		if (keyCode == 1)
        {
            return super.charTyped(typedChar, keyCode);
        }
		else
		{
			if(page!=null)
			{
				return page.charTyped(typedChar, keyCode);
			}
		}
		return false;
	}

	@Override
	public void openResearchText(Research r)
	{
		if(custom.hasResearch(r))
		{
			GuiScannerPageBase page = new GuiScannerPageResearch(r.getName(), this);
			if(buffer!=null)
			{
				buffer[0] = this;
				page.init(buffer);
			}		
			this.minecraft.setScreen(page);
			LocalPlayerResearchHelper.notReaded.remove(r.getName());
		}
	}
	
	private void initTabClick(int id)
	{
		if(id!=selected || page==null)
		{
			if(id >= tabs.length)
				id = 0;
			
			selected = id;
			page = new GuiResearchTreePage(tabs[id], this.custom, this);
			page.init(getMinecraft(), width, height);
//			page.init(); init(minecraft, width, height) already calls that
		}		
	}
	
	@Override
	public void initBuffer(Screen[] buffer)
	{
		this.buffer = buffer;
		this.buffer[0] = this;	
	}
}
