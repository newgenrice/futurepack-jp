package futurepack.common.gui.escanner;

import java.util.Iterator;

import com.google.gson.JsonObject;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.research.Research;
import futurepack.common.research.ResearchLoader;
import futurepack.common.research.ResearchManager;
import futurepack.common.research.ScannerPageResearch;
import it.unimi.dsi.fastutil.ints.Int2ObjectAVLTreeMap;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.resources.language.LanguageInfo;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.util.FormattedCharSequence;

public class ComponentRevision extends ComponentHeading
{
	public static final String LANG_KEY_WRONG_FORMAT = "futurepack.research.revision.wrong_date_format";
	public static final String LANG_KEY_OUTDATED = "futurepack.research.revision.outdated_english_research_is_newer";
	public static final String LANG_KEY_OUT_OF_SYNC = "futurepack.research.revision.en_and_de_out_of_sync";
	
	
	private static Int2ObjectAVLTreeMap<ResearchLangEntry> researchId2Revision;
	

	private static LanguageInfo currentlyLoading = null;
	
	public static void init()
	{
		researchId2Revision = new Int2ObjectAVLTreeMap<>();
	}
	
	private static class ResearchLangEntry
	{
		/**
		 * special values: 
		 * 	 0 = still loading
		 *  -1 = not defined in json
		 */
		int de_revision = 0, en_revision = 0;
	}
	
	/**
	 * Checks the revision date and compares it to en & de (and loads them if not loaded yet) 
	 */
	public static TranslatableComponent getTranslationString(JsonObject component)
	{
		String date = component.get("changed_date").getAsString(); //Format YYYY-MM-DD
		if(date.length()!="YYYY-MM-DD".length())
		{
			return new TranslatableComponent(LANG_KEY_WRONG_FORMAT, date);
		}
		else if(date.charAt(4)!='-' || date.charAt(7)!='-')
		{
			return new TranslatableComponent(LANG_KEY_WRONG_FORMAT, date);
		}
		else
		{
			int revisionDate = Integer.parseInt(date.replace("-", ""));
			
			Research currentResearch = ResearchManager.getResearch(ScannerPageResearch.getCurrentlyLoadingResearch());
			ResearchLangEntry entry = getRevisionFromResearch(currentResearch);
			LanguageInfo info = currentlyLoading != null ? currentlyLoading : Minecraft.getInstance().getLanguageManager().getSelected();
			if("en_us".equalsIgnoreCase(info.getCode()))
			{
				entry.en_revision = revisionDate;
				if(entry.de_revision==0)
				{
					loadDe(currentResearch);
				}
				
				if(entry.en_revision != entry.de_revision)
				{
					return new TranslatableComponent(LANG_KEY_OUT_OF_SYNC, entry.en_revision, entry.de_revision);
				}
			}
			else if("de_de".equalsIgnoreCase(info.getCode()))
			{
				entry.de_revision = revisionDate;
				if(entry.en_revision == 0)
				{
					loadEn(currentResearch);
				}
				if(entry.en_revision != entry.de_revision)
				{
					return new TranslatableComponent(LANG_KEY_OUT_OF_SYNC, entry.en_revision, entry.de_revision);
				}
			}
			else
			{
				
				loadEn(currentResearch);
				
				if(revisionDate < Math.max(entry.en_revision, entry.de_revision))
				{
					return new TranslatableComponent(LANG_KEY_OUTDATED, revisionDate, Math.max(entry.en_revision, entry.de_revision));
				}
			}
			
			return new TranslatableComponent(""); // everything is fine;
		}
	}

	public static ResearchLangEntry getRevisionFromResearch(Research r)
	{
		return researchId2Revision.computeIfAbsent(r.id, id -> new ResearchLangEntry());
	}
	
	private static void loadDe(Research r) 
	{
		ResearchLangEntry entry = getRevisionFromResearch(r);
		if(entry.de_revision == 0)
		{
			entry.de_revision = -1;
			
			
			LanguageInfo old = currentlyLoading;
			ScannerPageResearch.getComponetsFromJSON(ResearchLoader.instance.getLocalisated(r.getDomain(), r.getName(), currentlyLoading=new LanguageInfo("de_de", "Germany", "Deutsch", false)));
			currentlyLoading=old;
		}
	}
	
	private static void loadEn(Research r) 
	{
		ResearchLangEntry entry = getRevisionFromResearch(r);
		if(entry.en_revision == 0)
		{
			entry.en_revision = -1;
		}
		LanguageInfo old = currentlyLoading;
		ScannerPageResearch.getComponetsFromJSON(ResearchLoader.instance.getLocalisated(r.getDomain(), r.getName(), currentlyLoading=new LanguageInfo("en_us", "US", "English", false)));
		currentlyLoading=old;
	}
	
	
	public ComponentRevision(JsonObject component)
	{
		super(getTranslationString(component));
	}
	
	
	@Override
	public void init(int maxWidth, Screen gui)
	{
		width = maxWidth;
		
		font = gui.getMinecraft().font;	
//		StringTextComponent tc = new StringTextComponent(rawText);
		rawText.setStyle(rawText.getStyle().withFont(null));
		
		parts = font.split(rawText, width);
		height = font.lineHeight * parts.size();
	}
	
	@Override
	public void render(PoseStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, GuiScannerBase gui)
	{
		Iterator<FormattedCharSequence> it = parts.iterator();
		for(;it.hasNext();y+= font.lineHeight)
		{
			font.draw(matrixStack, it.next() , x, y, 0x547bb1);
		}
	}
}
