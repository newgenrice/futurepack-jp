package futurepack.common.spaceships;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import futurepack.api.interfaces.IPlanet;
import futurepack.api.interfaces.ISpaceshipUpgrade;
import futurepack.common.FPConfig;
import futurepack.common.FPLog;
import futurepack.common.block.inventory.TileEntityBoardComputer;
import futurepack.common.item.misc.MiscItems;
import futurepack.common.recipes.ISyncedRecipeManager;
import net.minecraft.ResourceLocationException;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class FPPlanetRegistry implements ISyncedRecipeManager<IPlanet>
{
	public static final String NAME = "spaceship_registry";
	
	public static FPPlanetRegistry instance;
	
	public static void init()
	{
		instance = new FPPlanetRegistry();
	}
	
	
//	private Map<String, IPlanet> nameToPlanet;
	private Map<ResourceLocation, IPlanet> DimToPlanet;
	
	private Map<String, ISpaceshipUpgrade> upgrades;
	
	public Supplier<IPlanet> MINECRAFT;
	/**
	 * this fields values are not static and keep changing.
	 */
	private PlanetUNKNOWN UNKNOWN;
	
	public FPPlanetRegistry()
	{
//		nameToPlanet = new HashMap<String, IPlanet>();
		DimToPlanet = new HashMap<ResourceLocation, IPlanet>();
		upgrades = new HashMap<String, ISpaceshipUpgrade>();
	
		//this.MINECRAFT=new PlanetBase(new ResourceLocation(FPConfig.SERVER.overworldDimension.get()), new ResourceLocation(Constants.MOD_ID, "textures/gui/planet_minecraft.png"), "Mincra", new String[0]);
		//registerPlanet(this.MINECRAFT);
		this.MINECRAFT = () -> getPlanetByName(FPConfig.SERVER.overworldDimension.get());
		
		//registerPlanet(new PlanetBase(Dimensions.MENELAUS_ID, new ResourceLocation(Constants.MOD_ID, "textures/gui/planet_menelaus.png"), "Menelaus", new String[0]));
//		registerPlanet(new PlanetBase(Dimensions.TYROS_ID, new ResourceLocation(Constants.MOD_ID, "textures/gui/planet_tyros.png"), "Tyros", new String[]{"drive.FTL"}));
//		registerPlanet(new PlanetBase(Dimensions.ENTROS_ID, new ResourceLocation(Constants.MOD_ID, "textures/gui/planet_entros.png"), "Entros", new String[]{"drive.FTL", "lifesupport"}).setBreathableAtmosphere(false));
		//registerPlanet(new PlanetBase(Dimensions.ENVIA_ID, new ResourceLocation(Constants.MOD_ID, "textures/gui/planet_envia.png"), "Envia", new String[]{"lifesupport"}).setBreathableAtmosphere(false).setOxygenProprties(0.2F, 0.4F));
		UNKNOWN = new PlanetUNKNOWN();
		
		registerUpgrade(new UpgradeFTLDrive());
		registerUpgrade(new UpgradeManeuverThrusters());
		registerUpgrade(new UpgradeLifeSupport());
	}
	
	public void registerPlanet(IPlanet pl)
	{
		if(! DimToPlanet.containsKey(pl.getDimenionId()))
		{
			DimToPlanet.put(pl.getDimenionId(), pl);
//			nameToPlanet.put(pl.getName(), pl);
		}
	}
	
	protected void registerPlanetOptionalOverrite(IPlanet pl) 
	{
		DimToPlanet.put(pl.getDimenionId(), pl);
	}
	
	public void registerUpgrade(ISpaceshipUpgrade spu)
	{
		if(!upgrades.containsKey(spu.getTranslationKey()))
		{
			upgrades.put(spu.getTranslationKey(), spu);
		}
	}
	
	
	public IPlanet getPlanetByName(String name)
	{
		return getPlanetByDimension(new ResourceLocation(name));
	}
	
	public IPlanet getPlanetByDimension(ResourceLocation id)
	{
		return DimToPlanet.get(id);
	}
	
	public IPlanet getPlanetByDimension(ResourceKey<Level> type)
	{
		return getPlanetByDimension(type.location());
	}
	
	
	public IPlanet getPlanetSafe(ResourceLocation registryKey)
	{
		IPlanet pl = getPlanetByDimension(registryKey);
		if(pl==null)
		{
			UNKNOWN.initDim(registryKey);
			pl = UNKNOWN;
		}
		return pl;		
	}
	
	public IPlanet getPlanetSafe(ResourceKey<Level> id)
	{
		if(id==null)
		{
			UNKNOWN.initDim(null);
			return UNKNOWN;
		}
		return getPlanetSafe(id.location());
	}
	
	public IPlanet getPlanetSafe(Level id)
	{
		return getPlanetSafe(id.dimension());
	}
	
	public ISpaceshipUpgrade getSpaceshipUpgradebyName(String name)
	{
		return upgrades.get(name);
	}

	public IPlanet generatePlanetByItem(ItemStack it)
	{
		if(it!=null && it.hasTag())
		{
			CompoundTag nbt = it.getTag();
			try
			{
				return getPlanetByName(nbt.getString("Planet"));
			}
			catch (ResourceLocationException e)
			{
				FPLog.logger.catching(e);
				FPLog.logger.error("Item %s had invalid planet name %s", it, nbt.getString("Planet"));
				it.setTag(null);
				return UNKNOWN;
			}
		}
		return MINECRAFT.get();
	}

	public boolean canJump(TileEntityBoardComputer tile, IPlanet start, IPlanet finish)
	{
		if(start==null || finish==null)
		{
			return false;
		}
		String[] up1 = start.getRequiredShipUpgrades();
		String[] up2 = finish.getRequiredShipUpgrades();
		for(String name : up1)
		{
			ISpaceshipUpgrade upgrade = getSpaceshipUpgradebyName(name);
			if(upgrade==null)
			{
				FPLog.logger.warn("Could not find modul '%s'", name);
				continue;
			}
			if(!tile.isUpgradeinstalled(upgrade))
				return false;
		}
		for(String name : up2)
		{
			ISpaceshipUpgrade upgrade = getSpaceshipUpgradebyName(name);
			if(upgrade==null)
			{
				FPLog.logger.warn("Could not find modul '%s'", name);
				continue;
			}
			if(!tile.isUpgradeinstalled(upgrade))
				return false;
		}
		return true;
	}

	public ItemStack getItemFromPlanet(IPlanet men)
	{
		ItemStack it = new ItemStack(MiscItems.spacecoordinats);
		CompoundTag nbt = new CompoundTag();
		nbt.putString("Planet", men.getDimenionId().toString());
		it.setTag(nbt);
		it.setHoverName(new TextComponent(men.getName()));
		return it;
	}

	@Override
	public Collection<IPlanet> getRecipes() 
	{
		return DimToPlanet.values();
	}

	@Override
	public void addRecipe(IPlanet t) 
	{
		registerPlanetOptionalOverrite(t);
	}

	@Override
	public ResourceLocation getName() 
	{
		return null;
	}
	
}
