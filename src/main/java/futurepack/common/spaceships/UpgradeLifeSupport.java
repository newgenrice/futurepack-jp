package futurepack.common.spaceships;

import java.util.Collection;
import java.util.function.Predicate;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockValidator;
import futurepack.api.interfaces.IChunkAtmosphere;
import futurepack.api.interfaces.IPlanet;
import futurepack.api.interfaces.ISpaceshipSelector;
import futurepack.api.interfaces.ISpaceshipUpgrade;
import futurepack.api.interfaces.tilentity.ITileBoardComputer;
import futurepack.common.block.logistic.LogisticBlocks;
import futurepack.common.block.modification.ModifiableBlocks;
import futurepack.common.block.modification.machines.TileEntityLifeSupportSystem;
import futurepack.common.modification.EnumChipType;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.tags.FluidTags;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.util.LazyOptional;

public class UpgradeLifeSupport implements ISpaceshipUpgrade, IBlockValidator
{
	public static final Predicate<Block> life_support_system = b -> b == ModifiableBlocks.life_support_system_w || b == ModifiableBlocks.life_support_system_g || b == ModifiableBlocks.life_support_system_b;

	
	static
	{
		SafeBlockMover.createCallback.add(UpgradeLifeSupport::onSafeBlockMoverCreated);
	}
	
	
	public static void onSafeBlockMoverCreated(SafeBlockMover sbm)
	{
		IPlanet src = FPPlanetRegistry.instance.getPlanetSafe(sbm.getSourceWorld());
		IPlanet trg = FPPlanetRegistry.instance.getPlanetSafe(sbm.getTargetWorld());
		
		if(!trg.hasBreathableAtmosphere())
		{
			if(src.hasBreathableAtmosphere())
			{
				//infiity air
				sbm.copyCallback.add(UpgradeLifeSupport::addFullAir);
			}
			else
			{
				//checking both capabilities
				sbm.copyCallback.add(UpgradeLifeSupport::moveAllAir);
			}
		}
	}
	
	public static void addFullAir(SafeBlockMover sbm, Vec3i offset)
	{
		BlockPos end = sbm.getEndCoords().offset(offset);
		
		LazyOptional<IChunkAtmosphere> opt = sbm.getChunkCap(sbm.getTargetWorld(), AtmosphereManager.cap_ATMOSPHERE, end);
		opt.ifPresent(athmos -> {
			athmos.setAir(end.getX()&15, end.getY()&255, end.getZ()&15, athmos.getMaxAir());
		});
	}
	
	public static void moveAllAir(SafeBlockMover sbm, Vec3i offset)
	{
		BlockPos start = sbm.getStartCoords().offset(offset);
		BlockPos end = sbm.getEndCoords().offset(offset);
		
		LazyOptional<IChunkAtmosphere> optSrc = sbm.getChunkCap(sbm.getSourceWorld(), AtmosphereManager.cap_ATMOSPHERE, start);
		LazyOptional<IChunkAtmosphere> optTrg = sbm.getChunkCap(sbm.getTargetWorld(), AtmosphereManager.cap_ATMOSPHERE, end);
		
		if(optSrc.isPresent() && optTrg.isPresent())
		{
			IChunkAtmosphere src = optSrc.orElse(null);
			IChunkAtmosphere trg = optTrg.orElse(null);
			
			int added  = trg.addAir(end.getX() &15, end.getY()&255, end.getZ()&15, src.getAirAt(start.getX()&15, start.getY()&255, start.getZ()&15));
			src.removeAir(start.getX()&15, start.getY()&255, start.getZ()&15, added);
		}
	}
	
	
	private Component error = null;
	
	@Override
	public String getTranslationKey()
	{
		return "lifesupport";
	}

	@Override
	public boolean isUpgradeInstalled(ISpaceshipSelector sel)
	{
		error = null;
		Collection<ParentCoords> blocks = sel.getSelector().getValidBlocks(this);
		for(ParentCoords pos : blocks)
		{
			if(sel.getWorld().getBlockState(pos.below()).getBlock() == LogisticBlocks.fluid_intake)
			{
				for(int dx=-1;dx<=1;dx++)
				{
					for(int dz=-1;dz<=1;dz++)
					{
						if(dx!=0 && dz!=0)
						{
							BlockPos p = pos.offset(dx, -1, dz);
							if(!sel.getWorld().getBlockState(p).getFluidState().is(FluidTags.WATER))
							{
								error = new TextComponent("No water at " + p.toString());
								return false;
							}
						}
					}
				}
				
				if(sel.getWorld().isEmptyBlock(pos.above()))
				{
					return true;
				}
				else
				{
					error = new TextComponent("No air at " + pos.above().toString());
				}
			}
			else
			{
				error = new TextComponent("No Fluid Intake at " + pos.below().toString());
			}
		}
		error = new TextComponent("No Life Support System Block found in ship. Did you install the Transport chip?");
		return false;
	}

	@Override
	public boolean isBoardComputerValid(ITileBoardComputer tile)
	{
		return true;
	}

	@Override
	public boolean isValidBlock(Level w, ParentCoords c)
	{
		BlockState state = w.getBlockState(c);
		if(life_support_system.test(state.getBlock()))
		{
			TileEntityLifeSupportSystem lss = (TileEntityLifeSupportSystem) w.getBlockEntity(c);
			return lss.getChipPower(EnumChipType.TRANSPORT) > 0 && lss.getEngine() > 0;
		}
		return false;
	}

	@Override
	public Component getErrorMessage() 
	{
		return error;
	}


}
