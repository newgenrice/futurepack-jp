package futurepack.common.spaceships;


import java.util.ArrayList;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.WeakHashMap;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;

import com.google.common.collect.ImmutableMap;
import com.mojang.datafixers.util.Pair;

import futurepack.depend.api.StableConstants.BlockFlags;
import futurepack.depend.api.helper.HelperEntities;
import it.unimi.dsi.fastutil.longs.Long2ObjectAVLTreeMap;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Vec3i;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.TorchBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.Property;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public class SafeBlockMover
{
	private final ServerLevel source, target;
	
	private BlockPos start, end;
	private int w,h,d;
	private Predicate<BlockPos> pred;
	
	
	/**
	 * Called whenever a new {@code SafeBlockMover} is created, use this to register custom stuff when transfering blocks. This is used to also copy the AIR capability
	 */
	public static final List<Consumer<SafeBlockMover>> createCallback;
	static
	{
		createCallback = Collections.synchronizedList(new ArrayList<>(8));
	}
	
	public final List<BiConsumer<SafeBlockMover, Vec3i>> copyCallback, deleteCallback;
	
	
	public SafeBlockMover(ServerLevel source, ServerLevel target)
	{
		this.source = source;
		this.target = target;
		
		copyCallback = new ArrayList<>(8);
		deleteCallback = new ArrayList<>(8);
		
		createCallback.forEach(c -> c.accept(this));
	}
	
	public void setStartCoords(BlockPos pos, int w, int h, int d, Predicate<BlockPos> pred)
	{
		this.start = pos;
		this.w=w;
		this.h=h;
		this.d=d;
		this.pred=pred;
	}
	
	public void setEndCoords(BlockPos pos)
	{
		this.end = pos;
	}
	
	public boolean performTransport()
	{
		if(isBlockless())
		{
			copie();
			delete();
			return true;
		}
		return false;
	}
	
//	private boolean isNull()
//	{
//		return start!=null && end!=null;
//	}
	
	public boolean isBlockless()
	{
		for(int x=0;x<w;x++)
		{
			for(int y=0;y<h;y++)
			{
				for(int z=0;z<d;z++)
				{
					if(pred.test(start.offset(x,y,z)))
					{
						if(!target.isEmptyBlock(end.offset(x,y,z)))
						{
							return false;
						}
					}
				}
			}
		}
		return true;
	}
	
	public static void copy(Level source, BlockPos ns, Level target, BlockPos ne, BiConsumer<BlockPos, BlockState> prePlacementCallback)
	{
		BlockState state = source.getBlockState(ns);
		Block block = state.getBlock();
						
		BlockEntity t = source.getBlockEntity(ns);
		CompoundTag nbt = null;
		if(t!=null)
		{
			nbt = new CompoundTag();
			t.save(nbt);
		}						
		//Reseting Tile
		if(block instanceof EntityBlock)
			source.setBlockEntity(((EntityBlock)block).newBlockEntity(ns, state));
		else
			source.removeBlockEntity(ns);

		if(prePlacementCallback!=null)					
			prePlacementCallback.accept(ne, state);
							
		target.setBlock(ne, state, BlockFlags.BLOCK_UPDATE);
							
		if(nbt!=null) //yes this has happened 0_o
		{
			nbt.putInt("x", ne.getX());
			nbt.putInt("y", ne.getY());
			nbt.putInt("z", ne.getZ());

			target.setBlockEntity(BlockEntity.loadStatic(ne, state, nbt));
		}	
	}

	public void copie()
	{
		final ArrayList<BlockPos> toUpdate = new ArrayList<>(w*h*d);
		
		for(int x=0;x<w;x++)
		{
			for(int y=0;y<h;y++)
			{
				for(int z=0;z<d;z++)
				{
					BlockPos ns = start.offset(x,y,z);
					if(pred.test(ns))
					{
						BlockPos ne = end.offset(x,y,z);
							
						toUpdate.add(ne);
						
						copy(source, ns, target, ne, this::fixTorches);
						Vec3i offset = new Vec3i(x, y, z);
						copyCallback.forEach(c -> c.accept(this, offset));
					}
				}
			}
		}
		
		toUpdate.forEach(p -> target.blockUpdated(p, Blocks.AIR));
	}
	
	private Map<Block, Boolean> abort = new IdentityHashMap<Block, Boolean>();
	
	private void fixTorches(BlockPos pos, BlockState state)
	{
		Boolean bool = abort.get(state.getBlock());
		if(bool==null)
		{
			bool = !(state.getBlock() instanceof TorchBlock);
			abort.put(state.getBlock(), bool);
		}
		if(bool)	
		{
			return;
		}
			
		ImmutableMap<Property<?>, Comparable<? >> map = state.getValues();
		for(Entry<Property<?>, Comparable<?>> e : map.entrySet())
		{	
			if(e.getKey().getValueClass()== Direction.class)
			{
				if(e.getValue()== Direction.WEST || e.getValue()== Direction.NORTH || e.getValue()== Direction.DOWN)
				{
					BlockPos hanging = pos.relative((Direction) e.getValue(), -1);
					target.setBlockAndUpdate(hanging, Blocks.BEDROCK.defaultBlockState());
				}
				return;
			}
		}
		abort.put(state.getBlock(), true);
	}
	
	public void delete()
	{
		abort.clear();
		
		final ArrayList<Pair<BlockPos, Block>> toUpdate = new ArrayList<>(w*h*d);
		HelperEntities.disableItemSpawn();
		for(int x=0;x<w;x++)
		{
			for(int y=0;y<h;y++)
			{
				for(int z=0;z<d;z++)
				{
					BlockPos ns = start.offset(x,y,z);
					if(pred.test(ns))
					{
						toUpdate.add(new Pair<BlockPos, Block>(ns, source.getBlockState(ns).getBlock()));
						source.setBlock(ns, Blocks.AIR.defaultBlockState(), 2);
//						source.removeBlock(ns, false); <- this will leave fluids
						deleteCallback.forEach(c -> c.accept(this, ns));
					}
				}
			}
		}
		HelperEntities.enableItemSpawn();
		
		toUpdate.forEach( p -> source.blockUpdated(p.getFirst(), p.getSecond()));
	}

	public boolean checkWorldBorders()
	{
		return isValid(this.end) && isValid(end.offset(w,h,d)); 
	}
	
	private boolean isValid(BlockPos pos)
    {
        return !this.isOutsideBuildHeight(pos) && pos.getX() >= -30000000 && pos.getZ() >= -30000000 && pos.getX() < 30000000 && pos.getZ() < 30000000 && target.getWorldBorder().isWithinBounds(pos);
    }

    private boolean isOutsideBuildHeight(BlockPos pos)
    {
        return pos.getY() < 0 || pos.getY() >= 256; //FIXME: in 1.17 use world 
    }
    
//    public MovingShip createMovableShip(Function<ServerWorld, EntityMovingShipBase> constructor)
//    {
//    	BlockState[][][] states = new BlockState[w][h][d];
//    	CompoundNBT[][][] tiles = new CompoundNBT[w][h][d];
//    	
//    	for(int x=0;x<w;x++)
//		{
//			for(int y=0;y<h;y++)
//			{
//				for(int z=0;z<d;z++)
//				{
//					if(pred.test(start.offset(x,y,z)))
//					{
//						BlockPos ns = start.offset(x,y,z);
//						if(pred.test(ns))
//						{
//							states[x][y][z] = source.getBlockState(ns);
//							TileEntity t = source.getBlockEntity(ns);
//							CompoundNBT nbt = null;
//							if(t!=null)
//							{
//								nbt = new CompoundNBT();
//								tiles[x][y][z] = t.save(nbt);
//							}
//							source.setBlockEntity(ns, states[x][y][z].getBlock().newBlockEntity(states[x][y][z], source)); // resetting tile Entity
//						}
//					}
//				}
//			}
//		}
//    	EntityMovingShipBase eship = constructor.apply(source);
//    	MovingShip ship = new MovingShip(states, tiles, eship);
//    	
//    	AxisAlignedBB bb = new AxisAlignedBB(start, start.offset(w,h,d));
//    	List<Entity> passengers = source.getEntitiesOfClass(Entity.class, bb, new Predicate<Entity>()
//    	{
//			@Override
//			public boolean test(Entity input)
//			{
//				return pred.test(input.blockPosition());
//			}
//		});
//    	
//    	List<LivingEntity> living = new ArrayList<LivingEntity>(passengers.size());
//    	List<Entity> notliving = new ArrayList<Entity>(passengers.size());
//    	
//    	for(Entity e : passengers)
//    	{
//    		if(e instanceof LivingEntity)
//    		{
//    			living.add((LivingEntity) e);
//    		}
//    		else
//    		{
//    			notliving.add(e);
//    		}
//    	}
//    	passengers.clear();
//    	passengers = null;
//    	
//    	eship.moveTo(start.offset(w/2, 0, d/2), 0F, 0F);
//    	//eship.moveToBlockPosAndAngles(start, 0F, 0F);
//    	source.addFreshEntity(eship);
//    	
//    	ship.startMoving(this, living, notliving);
//    	
//    	return ship;
//    }

    private WeakHashMap<Level, WeakHashMap<Capability, Long2ObjectAVLTreeMap<LazyOptional>>> deepMap = new WeakHashMap<>();
    
    public <T> LazyOptional<T> getChunkCap(Level w, Capability<T> cap, BlockPos pos)
	{
		WeakHashMap<Capability, Long2ObjectAVLTreeMap<LazyOptional>> cap2chunk = deepMap.computeIfAbsent(w, c -> new WeakHashMap<>());
		Long2ObjectAVLTreeMap<LazyOptional>  chunk2cap = cap2chunk.computeIfAbsent(cap, c -> new Long2ObjectAVLTreeMap<>());
		LazyOptional<T> opt = chunk2cap.computeIfAbsent(new ChunkPos(pos).toLong(), p -> w.getChunk(ChunkPos.getX(p), ChunkPos.getZ(p)).getCapability(cap));
		return opt;
	}
    
	public BlockPos getEndCoords()
	{
		return end;
	}

	public BlockPos getStartCoords() 
	{
		return start;
	}
	
	public ServerLevel getSourceWorld()
	{
		return source;
	}
	
	public ServerLevel getTargetWorld()
	{
		return target;
	}
	
}
