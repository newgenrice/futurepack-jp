package futurepack.common.spaceships.moving;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.function.Predicate;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.ISelector;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.util.Tuple;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

public class SimpleCollision
{
	public static Vec3i normalize(Vec3 direction)
	{
		Vec3 normalised = direction.normalize();

		int x = normalised.x() > 0.5 ? 1 : normalised.x() < -0.5 ? -1 : 0;
		int y = normalised.y() > 0.5 ? 1 : normalised.y() < -0.5 ? -1 : 0;
		int z = normalised.z() > 0.5 ? 1 : normalised.z() < -0.5 ? -1 : 0;

		return new Vec3i(x, y, z);
	}
	
	
	private final ISelector blocks;
	private final Vec3i direction;
	private final Predicate<BlockPos> isFreeSpace;
	
	private AABB box;

	private Set<BlockPos> positionsToCheck;
	private Set<BlockPos> unchangedPositions;
	private Set<BlockPos> oldPositions;
	
	private Boolean canMove=null;
	
	public SimpleCollision(ISelector blocks, Vec3i direction, Predicate<BlockPos> isFreeSpace)
	{
		this.blocks = blocks;
		this.direction = direction;
		this.isFreeSpace = isFreeSpace;
		
		if(Math.abs(direction.getX()) > 1 || Math.abs(direction.getY()) > 1 || Math.abs(direction.getZ()) > 1)
		{
			throw new IllegalArgumentException("Direction is too large");
		}
	}
	
	
	public Set<BlockPos> getPositionsToCheck()
	{
		if(positionsToCheck!=null)
			return positionsToCheck;
		else
		{
			Collection<ParentCoords> allBlocks = blocks.getAllBlocks();
			
			Set<BlockPos> set = new HashSet<BlockPos>(allBlocks.size());
			allBlocks.stream().map(BlockPos::new).forEach(set::add);
			
			//this contains all postions that are new, for example if we mvoe a cube 1 up we only need to check the top blocks if there is space for them
			Set<BlockPos> newPositions = Collections.synchronizedSet(new HashSet<BlockPos>(allBlocks.size()));
			Set<BlockPos> unchangedPositions = Collections.synchronizedSet(new HashSet<BlockPos>(allBlocks.size()));
			allBlocks.parallelStream().map(p -> p.offset(direction)).map(p -> new Tuple<>(p, set.contains(p) ? unchangedPositions : newPositions)).forEach(t -> t.getB().add(t.getA()));
			
			this.positionsToCheck = newPositions;
			this.unchangedPositions = unchangedPositions;
			
			Set<BlockPos> oldPositions = Collections.synchronizedSet(new HashSet<>(set));
			oldPositions.removeAll(unchangedPositions);
			
			this.oldPositions = oldPositions;
			
			return newPositions;
		}
	}
	
	public Set<BlockPos> getUnchangedPositions()
	{
		if(unchangedPositions==null)
		{
			getPositionsToCheck();
		}
		
		return unchangedPositions;
	}
	
	public Set<BlockPos> getOldPositions()
	{
		if(oldPositions==null)
		{
			getPositionsToCheck();
		}
		return oldPositions;
	}

	public AABB getSize()
	{
		if(box!=null)
			return box;
		else
		{
			int x0,y0,z0,x1,y1,z1;
			Iterator<ParentCoords> allBlocks = blocks.getAllBlocks().iterator();
			BlockPos p = allBlocks.next();
			x0=x1=p.getX();
			y0=y1=p.getY();
			z0=z1=p.getZ();
			while(allBlocks.hasNext())
			{
				p = allBlocks.next();
				x0 = Math.min(x0, p.getX());
				y0 = Math.min(y0, p.getY());
				z0 = Math.min(z0, p.getZ());
				x1 = Math.max(x1, p.getX());
				y1 = Math.max(y1, p.getY());
				z1 = Math.max(z1, p.getZ());
			}
			x0 = Math.min(x0, x0 + direction.getX());
			y0 = Math.min(y0, y0 + direction.getY());
			z0 = Math.min(z0, z0 + direction.getZ());
			x1 = Math.max(x1, x1 + direction.getX());
			y1 = Math.max(y1, y1 + direction.getY());
			z1 = Math.max(z1, z1 + direction.getZ());
			
			box = new AABB(x0, y0, z0, x1, y1, z1);
			return box;
		}
	}	
	
	public boolean isFree(BlockPos pos)
	{
		return isFreeSpace.test(pos);
	}

	public boolean canMove()
	{
		if(canMove==null)
		{
			for(BlockPos pos : getPositionsToCheck())
			{
				if(!isFree(pos))
				{
					canMove = false;
					return false;
				}
			}
			canMove = true;
			return true;
		}
		return canMove;
	}
	
	public ISelector getSelector()
	{
		return blocks;
	}
	
	public Vec3i getDirection()
	{
		return direction;
	}
}
