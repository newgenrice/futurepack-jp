package futurepack.common.spaceships.moving;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import javax.annotation.Nullable;

import futurepack.common.FPBlockSelector;
import futurepack.common.block.logistic.LogisticBlocks;
import futurepack.common.block.logistic.frames.TileEntityMovingBlocks;
import futurepack.depend.api.MiniWorld;
import futurepack.depend.api.StableConstants.BlockFlags;
import futurepack.world.dimensions.TreeUtils;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerTickList;
import net.minecraft.world.level.TickNextTickData;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.PushReaction;
import net.minecraft.world.phys.AABB;

public class MovingBlocktUtil 
{
	public static TileEntityMovingBlocks beginMoveBlocks(FPBlockSelector selected, Vec3i direction, Consumer<SimpleCollision> callback)
	{
		if(!selected.getWorld().isClientSide())
		{
			SimpleCollision col = new SimpleCollision(selected, direction, pos -> selected.getWorld().isEmptyBlock(pos) || selected.getWorld().getBlockState(pos).getPistonPushReaction() == PushReaction.DESTROY);
			
			if(col.canMove())
			{
				MiniWorld mw = createFromBlockSelector(selected);
				//original blocks are still there
				
				if(callback!=null)
					callback.accept(col);
				
				ArrayList<TickNextTickData<Block>> pendingTicks = copyTickList((ServerLevel) selected.getWorld(), col, direction);
				
				replaceBlocksFast(selected.getWorld(), col);
				//on old positons is air
				//an all positions where block will be are barrier blocks
				
				BlockPos randomNewPos = col.getPositionsToCheck().stream().findAny().get();
				
				selected.getWorld().setBlock(randomNewPos, LogisticBlocks.moving_blocks.defaultBlockState(), BlockFlags.DEFAULT | 128);
				TileEntityMovingBlocks mb = (TileEntityMovingBlocks) selected.getWorld().getBlockEntity(randomNewPos);
	//			mw.start = randomNewPos;
				
				mb.setMiniWorld(mw);
				mb.setDirection(direction);
				mb.setPendingTicks(pendingTicks);
				
				return mb;
			}
		}
		return null;
	}
	
	public static void endMoveBlocks(Level w, MiniWorld mw, Vec3i direction, @Nullable List<TickNextTickData<Block>> pendingTicks)
	{
		if(!w.isClientSide())
		{
			place(mw, w, mw.start.offset(direction));
			if(pendingTicks!=null)
			{
				ServerTickList<Block> tickList = ((ServerTickList<Block>)w.getBlockTicks());
				pendingTicks.forEach(tickList::addTickData);
			}
		}
	}
	
	public static MiniWorld createFromBlockSelector(FPBlockSelector selected)
	{
		return TreeUtils.copyFromWorld((WorldGenLevel)selected.getWorld(), selected.getAllBlocks());
	}
	
	public static void replaceBlocksFast(Level w, SimpleCollision collision)
	{
		int flag = BlockFlags.DEFAULT | BlockFlags.IS_MOVING | 128 | BlockFlags.NO_NEIGHBOR_DROPS; //the 128 prevents light updates - yes this will look ugly probaply, but its fast
		//HelperItems.disableItemSpawn();
		
		
		Consumer<BlockPos> remTileEntity = p -> w.removeBlockEntity(p);
		
		collision.getOldPositions().forEach(remTileEntity);
		collision.getUnchangedPositions().forEach(remTileEntity);
		collision.getPositionsToCheck().forEach(remTileEntity);
		
		collision.getOldPositions().forEach(pos -> {
			FluidState fluidstate = w.getFluidState(pos);
			w.setBlock(pos, fluidstate.createLegacyBlock(), BlockFlags.BLOCK_UPDATE); //no neighbour updates
	    });
		collision.getUnchangedPositions().forEach(pos -> w.setBlock(pos, Blocks.BARRIER.defaultBlockState(), flag));
		collision.getPositionsToCheck().forEach(pos -> w.setBlock(pos, Blocks.BARRIER.defaultBlockState(), flag));
		
		//HelperItems.enableItemSpawn();
	}
	
	
	
	public static void place(MiniWorld mw, Level w, BlockPos pos)
	{
		BlockPos.MutableBlockPos mut = new BlockPos.MutableBlockPos();
		BlockState void_air = Blocks.VOID_AIR.defaultBlockState();
		for(int x=0;x<mw.width;x++)
		{
			mut.setX(x + pos.getX());
			
			for(int y=0;y<mw.height;y++)
			{
				mut.setY(y + pos.getY());
				
				for(int z=0;z<mw.depth;z++)
				{
					mut.setZ(z + pos.getZ());
					
					if(mw.states[x][y][z]!=void_air)
					{
						w.setBlock(mut, mw.states[x][y][z], BlockFlags.DEFAULT_AND_RERENDER);
						if(mw.tiles[x][y][z]!=null)
						{
							w.setBlockEntity(BlockEntity.loadStatic(mut.immutable(), mw.states[x][y][z], mw.tiles[x][y][z].save(new CompoundTag()))); //create a copy at the new position
						}
					}
				}
			}
		}
	}
	
	public static ArrayList<TickNextTickData<Block>> copyTickList(ServerLevel w, SimpleCollision collision, Vec3i direction)
	{
		ServerTickList<Block> list = w.getBlockTicks();
		AABB box =  collision.getSize();
		BoundingBox bb = new BoundingBox((int)box.minX, (int)box.minY, (int)box.minZ, (int)box.maxX+1, (int)box.maxY+1, (int)box.maxZ+1);//+1 becuase id does a block.x < box.maxX check and nto a a <= check 
		
		final boolean removePendingTicks = true;
		final boolean ignoreAlreadyTicked = true;
		List<TickNextTickData<Block>> pendingBlocks = list.fetchTicksInArea(bb, removePendingTicks, ignoreAlreadyTicked);
		ArrayList<TickNextTickData<Block>> movedBlocks = new ArrayList<>(pendingBlocks.size());
		
		Iterator<TickNextTickData<Block>> iter = pendingBlocks.iterator();
		Set<BlockPos> movedBlocksSet = new HashSet<>(collision.getUnchangedPositions());
		movedBlocksSet.addAll(collision.getOldPositions());
		
		while(iter.hasNext())
		{
			TickNextTickData<Block> entry = iter.next();
			if(movedBlocksSet.contains(entry.pos))
			{
				movedBlocks.add(moveTicks(entry, direction));
				iter.remove();
			}
		}
		movedBlocksSet = null;
		iter = null;
		
		pendingBlocks.forEach(list::addTickData);
		
		
		movedBlocks.trimToSize();
		return movedBlocks;
	}
	
	public static <T> TickNextTickData<T> moveTicks(TickNextTickData<T> entry, Vec3i direction)
	{
		return new TickNextTickData<T>(entry.pos.offset(direction), entry.getType(), entry.triggerTick, entry.priority);
	}
}
