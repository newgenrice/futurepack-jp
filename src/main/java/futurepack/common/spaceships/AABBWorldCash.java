//package futurepack.common.spaceships;
//
//import java.lang.ref.WeakReference;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Map.Entry;
//
//import javax.annotation.Nullable;
//
//import com.google.common.collect.Lists;
//
//import net.minecraft.entity.Entity;
//import net.minecraft.util.math.AxisAlignedBB;
//import net.minecraft.util.math.BlockPos;
//import net.minecraft.util.math.MathHelper;
//import net.minecraft.util.math.Vector3i;
//import net.minecraft.world.World;
//
//public class AABBWorldCash<T extends Entity>
//{
//	public final WeakReference<T> moving_ship;
//	
//	public final HashMap<Vector3i, AABBChunkCash> bbMap;
//	
//	private final double maxChunkDistanceSq;
//	
//	public AABBWorldCash(T e, double maxChunkDistanceSq)
//	{
//		moving_ship = new WeakReference<T>(e);
//		bbMap = new HashMap<Vector3i, AABBChunkCash>();
//		this.maxChunkDistanceSq = Math.max(4D, maxChunkDistanceSq);
//	}
//	
//	public void checkDistance()
//	{
//		T entity = moving_ship.get();
//		if(entity != null && entity.isAlive())
//		{
//			Vector3i pos = toChunkCoords(entity.getPosition());
//			Iterator<Entry<Vector3i, AABBChunkCash>> iter = bbMap.entrySet().iterator();
//			while(iter.hasNext())
//			{
//				Entry<Vector3i, AABBChunkCash> e = iter.next();
//				Vector3i vec = e.getKey();
//				double dis = pos.distanceSq(vec);
//				if(dis > maxChunkDistanceSq)
//				{
//					iter.remove();
//				}
//			}
//		}
//		else
//		{
//			bbMap.clear();
//		}
//		
//	}
//	
//	public List<AxisAlignedBB> getCollisionBoxes(@Nullable Entity entityIn, AxisAlignedBB aabb)
//	{
//		List<AxisAlignedBB> list = Lists.<AxisAlignedBB>newArrayList();
//		this.getCollisionBoxes(this.moving_ship.get().world, entityIn, aabb, list);
//		return list;
//	}
//	
///*	private void getCollisionBoxes(World w, @Nullable Entity entityIn, AxisAlignedBB aabb, boolean p_191504_3_, @Nullable List<AxisAlignedBB> outList)
//	{
//		w.getProfiler().startSection("getCollisionBoxes");
//		if(outList==null)
//			return;
//		
//		int x1 = MathHelper.floor(aabb.minX) - 1;
//		int x2 = MathHelper.ceil(aabb.maxX) + 1;
//		int y1 = MathHelper.floor(aabb.minY) - 1;
//		int y2 = MathHelper.ceil(aabb.maxY) + 1;
//		int z1 = MathHelper.floor(aabb.minZ) - 1;
//		int z2 = MathHelper.ceil(aabb.maxZ) + 1;
//		WorldBorder worldborder = w.getWorldBorder();
//		boolean entityOutsideFlag = entityIn != null && entityIn.isOutsideBorder();
//		boolean worldEntityInsideFlag = entityIn != null && w.isInsideWorldBorder(entityIn);
//		IBlockState iblockstate = Blocks.STONE.getDefaultState();
//		BlockPos.PooledMutable pospool = BlockPos.PooledMutable.retain();
//		
//		// if (p_191504_3_ && !net.minecraftforge.event.ForgeEventFactory.gatherCollisionBoxes(this, entityIn, aabb, outList)) return true;
//		try
//		{
//			for (int x = x1; x < x2; ++x)
//			{
//				for (int z = z1; z < z2; ++z)
//				{
//					boolean border_x = x == x1 || x == x2 - 1;
//					boolean border_z = z == z1 || z == z2 - 1;
//
//					if ((!border_x || !border_z) && w.isBlockLoaded(pospool.setPos(x, 64, z)))
//					{
//						for (int y = y1; y < y2; ++y)
//						{
//							if (!border_x && !border_z || y != y2 - 1)
//							{
//								if (p_191504_3_)
//								{
//									if (x < -30000000 || x >= 30000000 || z < -30000000 || z >= 30000000)
//									{
//										w.getProfiler().endSection();
//										return;
//									}
//								}
//								else if (entityIn != null && entityOutsideFlag == worldEntityInsideFlag)
//								{
//									entityIn.setOutsideBorder(!worldEntityInsideFlag);
//								}
//								
//								if (!p_191504_3_ && !worldborder.contains(pospool.setPos(x, y, z)) && worldEntityInsideFlag)
//								{
//									iblockstate.addCollisionBoxToList(w, pospool, aabb, outList, entityIn, false);
//								}	
//								else
//								{
//									int cx = x >> 4;
//									int cy = y >> 4;
//									int cz = z >> 4;
//									
//									w.getProfiler().startSection("getChunk");
//									AABBChunkCash cash = getOrCreateChunk(pospool.setPos(cx, cy, cz));
//									w.getProfiler().endStartSection("addBoxes");
//									cash.addCollisionBoxToList(w, x & 15, y & 15, z & 15, aabb, entityIn, outList);
//									w.getProfiler().endSection();
//								}
//							}
//						}
//					}
//				}
//			}
//		}
//		finally
//		{
//			pospool.release();
//		}
//		w.getProfiler().endSection();
////        return !outList.isEmpty();	
//    }*/
//	
//	
//	private void getCollisionBoxes(World w, @Nullable Entity entityIn, AxisAlignedBB aabb, @Nullable List<AxisAlignedBB> outList)
//	{
//		if(outList==null)
//			return;
//		
//		int x1 = (MathHelper.floor(aabb.minX) - 1) >> 4;
//		int x2 = (MathHelper.ceil(aabb.maxX) + 1) >> 4;
//		int y1 = (MathHelper.floor(aabb.minY) - 1) >> 4;
//		int y2 = (MathHelper.ceil(aabb.maxY) + 1) >> 4;
//		int z1 = (MathHelper.floor(aabb.minZ) - 1) >> 4;
//		int z2 = (MathHelper.ceil(aabb.maxZ) + 1) >> 4;
////		WorldBorder worldborder = w.getWorldBorder();
//		boolean entityOutsideFlag = entityIn != null && entityIn.isOutsideBorder();
//		boolean worldEntityInsideFlag = entityIn != null && w.isInsideWorldBorder(entityIn);w.getWorldBorder().is
////		IBlockState iblockstate = Blocks.STONE.getDefaultState();
//		BlockPos.PooledMutable pospool = BlockPos.PooledMutable.retain();
//		
//		// if (p_191504_3_ && !net.minecraftforge.event.ForgeEventFactory.gatherCollisionBoxes(this, entityIn, aabb, outList)) return true;
//		try
//		{
//			for (int x = x1; x <= x2; ++x)
//			{
//				for (int z = z1; z <= z2; ++z)
//				{
//					for (int y = y1; y <= y2; ++y)
//					{
//						if (entityIn != null && entityOutsideFlag == worldEntityInsideFlag)
//						{
//							entityIn.setOutsideBorder(!worldEntityInsideFlag);
//						}
//								
//						AABBChunkCash cash = getOrCreateChunk(pospool.setPos(x, y, z));
//						cash.addCollisionBoxes(w, aabb, entityIn, outList);
//					}
//				}
//			}
//		}
//		finally
//		{
//			pospool.close();
//		}
//    }
//
//	
//	public AABBChunkCash getOrCreateChunk(Vector3i pos)
//	{
//		AABBChunkCash cash = bbMap.getOrDefault(pos, null);
//		if(cash == null)
//		{
//			cash = new AABBChunkCash(pos);
//			T entity = moving_ship.get();
//			if(entity!=null && entity.world!=null)
//			{
//				cash.init(entity.world);
//				bbMap.put(cash.getPos(), cash);
//				return cash;
//			}
//		}
//		else
//			return cash;
//		
//		return null;
//	}
//	
//	
//	private Vector3i toChunkCoords(BlockPos pos)
//	{
//		return new Vector3i(pos.getX() >> 4, pos.getY() >> 4, pos.getZ() >> 4);
//	}
//}
