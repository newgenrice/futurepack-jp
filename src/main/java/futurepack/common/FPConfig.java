package futurepack.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.function.Predicate;

import futurepack.api.Constants;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.ForgeConfigSpec.BooleanValue;
import net.minecraftforge.common.ForgeConfigSpec.ConfigValue;
import net.minecraftforge.common.ForgeConfigSpec.DoubleValue;
import net.minecraftforge.common.ForgeConfigSpec.IntValue;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModContainer;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.config.ModConfig.Type;
import net.minecraftforge.fml.event.config.ModConfigEvent;

public class FPConfig 
{
	public static final String STEAM_NAME = "steam";
	
	public static final Predicate<Object> IS_STRING_OR_STRING_LIST = new Predicate<Object>()
	{

		@Override
		public boolean test(Object t)
		{
			if( t instanceof String)
				return true;
			else if (t instanceof List)
			{
				for(Object o : (List)t)
				{
					if(!(o instanceof String))
						return false;
				}
				return true;
			}
			else
			{
				return false;
			}
		}
		
	};
	
	/**
	 * EASTER: 
	 * APRIL_FOOLS: colored research background
	 * CHRISTMAS: 
	 * NEW_YEAR:
	 * HALOWEEN: pumpkin gehuf
	 */
	public static final BooleanSupplier EASTER, APRIL_FOOLS, CHRISTMAS, NEW_YEAR, HALOWEEN;
	public static final BooleanSupplier IS_RESEARCH_DEBUG = () -> Boolean.getBoolean("futurepack.common.research.check");
	
	static
	{
		Calendar c = Calendar.getInstance();
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);
		EASTER = () -> (month == Calendar.MARCH && day >= 29) || (month == Calendar.APRIL && day <= 15); //29.03 bis 15.04
		APRIL_FOOLS = () -> month == Calendar.APRIL && day == 1; //01.04
		CHRISTMAS = () ->  month == Calendar.DECEMBER && (day >= 21 && day <= 27); //21.12 bis 27.12
		NEW_YEAR = () -> (month == Calendar.DECEMBER && day >= 31) || (month == Calendar.JANUARY && day <= 1); // 31.12 bis 01.01
		HALOWEEN = () -> (month == Calendar.OCTOBER && day >= 15) || (month == Calendar.NOVEMBER && day <= 3); //15.10 bis 03.11
	}
	
	private static final ForgeConfigSpec.Builder BUILDER  = new ForgeConfigSpec.Builder();
	
	public static final Client CLIENT = new Client(BUILDER);
	public static final Research RESEARCH = new Research(BUILDER);
	public static final Common COMMON = new Common(BUILDER);
	public static final WorldGenOres WORLDGEN_ORES = new WorldGenOres(BUILDER);
	public static final WorldGenCaves WORLDGEN_CAVES = new WorldGenCaves(BUILDER);
	public static final WorldGen WORLDGEN = new WorldGen(BUILDER);
	public static final Server SERVER = new Server(BUILDER);
	
	private static final ForgeConfigSpec CONFIG_SPEC = BUILDER.build();
	
	
	public static void registerConfig(ModContainer mod)
	{
		if(!Constants.MOD_ID.equals(mod.getModId()))
			throw new IllegalArgumentException("The provied ModContainer has not the not the ID " + Constants.MOD_ID);

		mod.addConfig(new ModConfig(Type.COMMON, CONFIG_SPEC, mod));
	}
	
	@SubscribeEvent
    public static void onLoad(final ModConfigEvent.Loading configEvent) 
	{
        FPLog.logger.debug("Loaded FP config file {}", configEvent.getConfig().getFileName());
    }

    @SubscribeEvent
    public static void onFileChange(final ModConfigEvent.Reloading configEvent) 
    {
    	FPLog.logger.fatal("FP config just got changed on the file system!");
    }
	
	public static class Client
	{
		
		public final BooleanValue futurepackStartMenu;
		public final BooleanValue futurepackSkipBackupScreen;
		public final BooleanValue futurepackCreateBackupOnSkip;
		public final BooleanValue futurepackShowCustomLoadingScreen;
		public final BooleanValue doEntityEaterSync;
		public final BooleanValue renderPipeItems;
		public final BooleanValue enableBlueprintsInCreative;
		public final BooleanValue allowUnicodeFont;
		
		public final ConfigValue<List<? extends String>> creativeQuickHologram;
		
		public final DoubleValue volume_genereator;
		public final DoubleValue volume_emagnet;
		public final DoubleValue volume_partpress;
		public final DoubleValue volume_spacedoor;
		public final DoubleValue volume_optibench;
		public final DoubleValue volume_deepcoreminer;
		
		Client(ForgeConfigSpec.Builder builder) 
		{
			builder.push("client");
			
			futurepackStartMenu = builder
					.comment("Show custom loading Screen")
					.define("futurepack_start_menu", false);
			
			futurepackSkipBackupScreen = builder
					.comment("Should the backup info screen on world load be hidden?")
					.define("futurepack_hide_backup_screen", true);
			
			futurepackShowCustomLoadingScreen = builder
					.comment("Should the backup screen be replaced by a custom futurepack screen?")
					.define("futurepack_replace_backup_screen_with_custom", true);
			
			futurepackCreateBackupOnSkip = builder
					.comment("Should your world be backuped on world load?")
					.define("futurepack_backup_on_load", true);
			
			doEntityEaterSync = builder
					.comment("Synchronize and show: Lasers from turrets")
					.define("do_entity_eater_sync", true);
			
			renderPipeItems = builder
					.comment("Synchronize and show: Itemflow in Pipes")
					.define("render_pipe_items", true);
			
			enableBlueprintsInCreative = builder
					.comment("If true, will add all Research-Blueprints to the Futurepack Tab.")
					.define("enable_blueprints_in_creative", false);

			allowUnicodeFont = builder
					.comment("If true the EScanner text will be smaler and more text can be rendered, using the default font otherwise.")
					.define("allow_uncide_font", true);
					
			creativeQuickHologram = builder
					.comment("This is for Mantes and BTM")
					.defineList("quick_holograms", Arrays.asList("{res:\"minecraft:grass\",snowy:false}"), IS_STRING_OR_STRING_LIST);
			
			
			volume_genereator = builder
					.comment("This is the volume of the T0 generator")
					.defineInRange("volume_generator", 1.0F, 0F, 2F);
			
			volume_emagnet = builder
					.comment("This is the volume of the electro magnet")
					.defineInRange("volume_emagnet", 1.0F, 0F, 2F);
			
			volume_partpress = builder
					.comment("This is the volume of the partpress")
					.defineInRange("volume_partpress", 1.0F, 0F, 2F);
			
			volume_spacedoor = builder
					.comment("This is the volume of the airlock")
					.defineInRange("volume_airlock", 1.0F, 0F, 2F);
			
			volume_optibench = builder
					.comment("This is the volume of the optibench")
					.defineInRange("volume_optibench", 1.0F, 0F, 2F);
			
			volume_deepcoreminer = builder
					.comment("This is the volume of the deepcore miner")
					.defineInRange("volume_deepcoreminer", 1.0F, 0F, 2F);
			
			builder.pop();
		}
	}
	
	public static class Research
	{
		public final BooleanValue disable_researching;
		
		public final DoubleValue time_factor, neon_factor, support_factor, xp_factor;
		
		public Research(ForgeConfigSpec.Builder builder)
		{
			builder.push("research");
			
			disable_researching = builder
					.comment("This will disable the research feature so everybody will have access to all researches from the very start.")
					.worldRestart()
					.define("disable_researching", false);
			
			time_factor = builder
					.comment("Factor to manipulate requirements of all Researches (Time Bar). This entry should have the same value on server and client!")
					.worldRestart()
					.defineInRange("time_factor", 1.0F,  0.001f, 100.0f);
				
			neon_factor = builder
					.comment("Factor to manipulate requirements of all Researches (Neon-Energy Bar). This entry should have the same value on server and client!")
					.worldRestart()
					.defineInRange("neon_factor", 1.0F,  0.000001f, 100.0f);
			
			support_factor = builder
					.comment("Factor to manipulate requirements of all Researches (Support Bar). This entry should have the same value on server and client!")
					.worldRestart()
					.defineInRange("support_factor", 1.0F,  0.001f, 100.0f);
			
			xp_factor = builder
					.comment("Factor to manipulate requirements of all Researches (Experience Bar). This entry should have the same value on server and client!")
					.worldRestart()
					.defineInRange("xp_factor", 1.0F,  0.001f, 100.0f);
			
			builder.pop();
		}
	}
	
	public static class Common
	{
		public final BooleanValue forceSmeltRecipes;
		
		public final IntValue thrusterBlockMoveCount;
		public final IntValue erse_food_value;
		
		public final DoubleValue erse_food_saturation;
		
		public Common(ForgeConfigSpec.Builder builder)
		{
			builder.push("common");
			
			forceSmeltRecipes = builder
					.comment("Should smelt recipes allways be added for Futurepack materials. Only use this if Dust to Ingot recipes are missing!")
					.worldRestart()
					.define("force_smelt_recipes", false);
			
			thrusterBlockMoveCount = builder
					.comment("Number of blocks supported by one thruster")
					.defineInRange("thruster_block_move_count", 101, 0, 100000);
			erse_food_value = builder
					.comment("The Amount of food when eating an Erse")
					.worldRestart()
					.defineInRange("erse_food_value", 3, 1, 20);
			
			erse_food_saturation = builder
					.comment("The saturation added after eating an Erse")
					.worldRestart()
					.defineInRange("erse_food_saturation", 0.3F, 0.1F, 10.0F);
			
			builder.pop();
		}
	}
	
	public static class WorldGenOres
	{
		public final IntValue copperOre, tinOre, zincOre, bauxiteOre, magnetiteOre, coalOreM, quartzOreM, copperOreM;
		
		//Biome blacklists
		public final ConfigValue<List<? extends String>> bl_copper, bl_tin, bl_zinc, bl_bauxite, bl_magnetite;
		
		public WorldGenOres(ForgeConfigSpec.Builder builder)
		{
			builder.push("world_generation");
			builder.push("ores");
			
			copperOre = 	ores(builder, "copper", 10, "Copper");
			tinOre = 		ores(builder, "tin", 10, "Tin");
			zincOre =	 	ores(builder, "zinz", 10, "Zinc");
			bauxiteOre = 	ores(builder, "bauxite", 7, "Bauxite");
			magnetiteOre = 	ores(builder, "magnetite", 7, "Magnetite");
			coalOreM = 		ores(builder, "coal_m", 10, "Menelaus Coal");
			quartzOreM = 	ores(builder, "quartz_m", 8, "Menelaus Quartz");
			copperOreM = 	ores(builder, "copper_m", 6, "Menelaus Copper");
			
			bl_copper =		blacklist(builder, "copper");
			bl_tin =		blacklist(builder, "tin");
			bl_zinc =		blacklist(builder, "zinc");
			bl_bauxite =	blacklist(builder, "bauxite");
			bl_magnetite =	blacklist(builder, "magnetite");
			
			builder.pop();
			builder.pop();
		}
		
		private static IntValue ores(ForgeConfigSpec.Builder builder, String id, int defaultV, String name)
		{
			return builder
				.comment("Quantity of " + name + " Ore")
				.worldRestart()
				.defineInRange(id + "_ore", defaultV, 0, 25);
		}
		
		private static ConfigValue<List<? extends String>> blacklist(ForgeConfigSpec.Builder builder, String ore)
		{
			return builder
				.comment("A blacklist to stop " + ore +" ore from spawning in this biome")
				.worldRestart()
				.defineList("bl_"+ ore +"_ore", new ArrayList<String>(1), IS_STRING_OR_STRING_LIST);
		}
	}
	
	public static class WorldGenCaves
	{
		public final IntValue hole_neon;
		public final IntValue hole_alutin;
		public final IntValue hole_retium;
		public final IntValue hole_glowtite;
		public final IntValue hole_bioterium;
		
		public final ConfigValue<List<? extends String>> wl_neon_caves;
		public final ConfigValue<List<? extends String>> wl_alutin_caves;
		public final ConfigValue<List<? extends String>> wl_glowtite_caves;
		public final ConfigValue<List<? extends String>> wl_retium_caves;
		public final ConfigValue<List<? extends String>> wl_bioterium_caves;

		public WorldGenCaves(ForgeConfigSpec.Builder builder)
		{
			builder.push("world_generation");
			builder.push("caves");
			
			hole_neon = cave(builder, "neon", "Neon");
			hole_alutin = cave(builder, "alutin", "Alutin");
			hole_retium = cave(builder, "retium", "Retium");
			hole_glowtite = cave(builder, "glowtite", "Glowtite");
			hole_bioterium = cave(builder, "bioterium", "Bioterium");
			
			
			wl_neon_caves =			whitelist(builder, "neon",		"minecraft:(?!nether|end|the_end)");
			wl_alutin_caves =		whitelist(builder, "alutin",	"minecraft:(?!nether|end|the_end)");
			wl_glowtite_caves =		whitelist(builder, "glowtite",	"futurepack:menelaus[\\w]*");
			wl_retium_caves =		whitelist(builder, "retium",	"futurepack:menelaus[\\w]*");
			wl_bioterium_caves =	whitelist(builder, "bioterium",	"futurepack:tyros[\\w]*");
			
			builder.pop();
			builder.pop();
		}
		
		private static IntValue cave(ForgeConfigSpec.Builder builder, String id, String name)
		{
			return builder
				.comment("Chance of " + name +" Holes in Percent (per chunk)")
				.worldRestart()
				.defineInRange("hole_" + id, 5, 0, 100);
		}
		
		private static ConfigValue<List<? extends String>> whitelist(ForgeConfigSpec.Builder builder, String name, String...defaultV)
		{
			return builder
				.comment("A biome whitelist for enabling " + name + " crystal caves in other dimensions. (regular expression)")
				.worldRestart()
				//.define("wl_" + name + "_cave", defaultV);
				.defineList("wl_" + name + "_cave", Arrays.asList(defaultV), IS_STRING_OR_STRING_LIST);
		}
	}
	
	public static class WorldGen
	{
		public final IntValue bedrockRift;
		public final IntValue bedrockRiftHeight;
		
		public final DoubleValue hugedungeon_spawnrate;
		public final IntValue hugedungeon_mindis;
		
		public final DoubleValue tecdungeon_spawnrate;
		public final IntValue tecdungeon_mindis;
		
		public final DoubleValue erseCount;
		
		public final ConfigValue<List<? extends String>> wl_huge_dungeons;
		public final ConfigValue<List<? extends String>> bl_tec_dungeons;
		
		public WorldGen(ForgeConfigSpec.Builder builder)
		{
			builder.push("world_generation");
			
			bedrockRift = builder
					.comment("Quantity of Bedrock Rifts")
					.worldRestart()
					.defineInRange("bedrock_rift", 1, 0, 25);
			bedrockRiftHeight = builder
					.comment("Heigth for Bedrockrift: Should be equal to flat bedrock heigth (if used)")
					.worldRestart()
					.defineInRange("bedrock_rift_height", 1, 0, 255);
			
			hugedungeon_spawnrate = builder
					.comment("Chance of a huge Tecdungeon to spawn in a chunk")
					.worldRestart()
					.defineInRange("huge_dungeon_spawnrate", 0.0012, 0.0, 0.1);
			hugedungeon_mindis = builder
					.comment("Distance in Chunks ( so *16 for block coords) around the spawn were no huge dungeons will spawn.")
					.worldRestart()
					.defineInRange("huge_dungeon_mindis", 0, 0, Integer.MAX_VALUE);
			
			tecdungeon_spawnrate = builder
					.comment("Chance of a normal Tecdungeon to spawn in a chunk")
					.worldRestart()
					.defineInRange("tec_dungeon_spawnrate", 0.00667F, 0.0, 0.2);
			tecdungeon_mindis = builder
					.comment("Distance in Chunks ( so *16 for block coords) around the spawn were no tec dungeons will spawn.")
					.worldRestart()
					.defineInRange("tec_dungeon_mindis", 0, 0, Integer.MAX_VALUE);
			
			erseCount = builder
					.comment("The amount erse generation  loops per chunk. If greater then one it will run mutliple times, smaler then one will result in percentage per chunk. Keep in mind erse will only spawn if there is the tall grass ( not the grass block, not the 2-block tall grass)")
					.worldRestart()
					.defineInRange("erse_count", 2.0, 0.0, 100);
			
			wl_huge_dungeons = builder
					.comment("A whitelist for enabling huge dungeon spawn in other dimensions.")
					.worldRestart()
					.defineList("wl_huge_dungeons", Arrays.asList("minecraft:overworld"), IS_STRING_OR_STRING_LIST);
			bl_tec_dungeons = builder
					.comment("A blacklist to stop tec dungeon spawn in biomes.")
					.worldRestart()
					.define("bl_tec_dungeons", Arrays.asList("minecraft:nether_wastes", "minecraft:crimson_forest", "minecraft:warped_forest", "minecraft:soul_sand_valey", "minecraft:basalt_deltas",
							"minecraft:the_end", "minecraft:small_end_islands", "minecraft:end_midlands", "minecraft:end_highlands", "minecraft:end_barrens",
							"minecraft:the_void"), IS_STRING_OR_STRING_LIST);
			
			builder.pop();
		}
	}
	
	public static class Server
	{
		public final IntValue spaceTravelCooldown;
		
		public final BooleanValue isSpawnNoteEnabled;
		public final BooleanValue disableMachineLock;
		public final BooleanValue glowmelow_drop;
		public final BooleanValue disableResearchCheck;
		public final BooleanValue disableClaimeChunkloading;
		public final BooleanValue disableMinerChunkloading;
		public final BooleanValue enforceExtraThreadForNetwork;
		public final BooleanValue disableSpawnerChest;
		
		public final ConfigValue<String> overworldDimension;
		
		public final ConfigValue<List<? extends String>> bl_recycler_repair;
		
		public Server(ForgeConfigSpec.Builder builder)
		{
			builder.push("server");
			
			spaceTravelCooldown = builder
					.comment("A Player must wait this time (in ms) until he can jump with a spaceship again. This prevents griefers from attacking servers.")
					.defineInRange("spacetravel_cooldown", 60000, 1, Integer.MAX_VALUE);
			
			isSpawnNoteEnabled = builder
					.comment("If false the player will not receive a note a the start on a world")
					.worldRestart()
					.define("spawn_note_enabled", true);
			disableMachineLock = builder
					.comment("This disables the 'You dont know how to use this machine' message, and you can open the gui.")
					.worldRestart()
					.define("disable_machine_lock", false);
			disableResearchCheck = builder
					.comment("This disables the research checking ins die the researcher. If enabled you can research a blueprint, even if this would not be possible.")
					.worldRestart()
					.define("disable_research_check", false);
			glowmelow_drop = builder
					.comment("Disable this, to prevent glowmelows to fall down after they are fully grown.")
					.worldRestart()
					.define("glowmelow_drop", true);
			disableClaimeChunkloading = builder
					.comment("If set to true, the claime will no longer load the chunk it is placed in.")
					.worldRestart()
					.define("disable_claime_chunkloading", false);
			disableMinerChunkloading = builder
					.comment("If set to true, the Miner will no longer load itself")
					.worldRestart()
					.define("disbale_miner_chunkloading", false);
			enforceExtraThreadForNetwork = builder
					.comment("This will force the ingame network (network cables) too use an extra thread, thus improving performance. This will most likely crash SpongeForge, so disable it when this happens (Will not corrupt your worlds).")
					.worldRestart()
					.define("enforce_extra_thread_for_network", true);
			
			disableSpawnerChest = builder
					.comment("If set to true you can no longer pickup spawner with the spawner chest. Crafting and researching tt is still possible.")
					.worldRestart()
					.define("disable_spawnerchest", false);			
			
			overworldDimension = builder
					.comment("Change this if you dont not want to use the vanilla dimension as start but rather a differen one.")
					.worldRestart()
					.define("overworld_dimension", "overworld");
			
			bl_recycler_repair = builder
					.comment("A blacklist for disabling the repair of items with the recycler tool.")
					.worldRestart()
					.defineList("bl_recycler_repair", Arrays.asList("modid1:itemA", "modid2:itemB"), IS_STRING_OR_STRING_LIST);
			
			builder.pop();
		}
	}
}
