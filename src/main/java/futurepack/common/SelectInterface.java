package futurepack.common;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockValidator;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;

public class SelectInterface implements IBlockValidator
{
	Class face;
	
	public SelectInterface(Class c)
	{
		face = c;
	}
	
	@Override
	public boolean isValidBlock(Level w, ParentCoords pos)
	{
		BlockEntity t = w.getBlockEntity(pos);
		if(t!=null)
		{
			return face.isAssignableFrom(t.getClass());
		}
		return false;
	}	
}
