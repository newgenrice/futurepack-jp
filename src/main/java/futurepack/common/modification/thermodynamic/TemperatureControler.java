package futurepack.common.modification.thermodynamic;


import javax.annotation.Nonnull;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.block.modification.TileEntityModificationBase;
import futurepack.common.item.ComputerItems;
import futurepack.common.item.ItemChip;
import futurepack.common.item.ItemCore;
import futurepack.common.item.ItemRam;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ItemLike;

public class TemperatureControler
{
	private final TileEntityModificationBase container;
	
	private EnumSlotHeat[] core;
	private EnumSlotHeat[] ram;
	private EnumSlotHeat[] chipset;
	
	public TemperatureControler(TileEntityModificationBase base, int cores, int rams, int chips)
	{
		container = base;
		core = new EnumSlotHeat[cores];
		ram = new EnumSlotHeat[rams];
		chipset = new EnumSlotHeat[chips];
	}
	
	public float getReducedPower(float f)
	{
		return 0;
	}

//	public float getHotChip(ItemStack it)
//	{
//		return applyHeat(ItemChip.getChipPower(it), TemperatureManager.getTemp(it));
//	}
//	
//	public float getHotRam(ItemStack it)
//	{
//		return applyHeat(ItemRam.getRamSpeed(it), TemperatureManager.getTemp(it));
//	}
//	
	public float applyHeat(float base, float max)
	{
		if(base==0)
			return 0;
		return base * line(max, container.getHeat());
	}
//	public int getHotCore(ItemStack it)
//	{
//		int base = ItemCore.getCorePower(it);
//		if(base==0)
//			return 0;
//		float max = TemperatureManager.getTemp(it);
//		return (int) (base * line(max, getHeat()));
//	}
	
	
	private static float line(float max, float current)
	{
		float m = 1 - (current / max);
		return m<0.01F?0.01F:m;
	}
	

	public void applyHeatDamage(ObjectContainer<ItemStack>[] core, ObjectContainer<ItemStack>[] ram, ObjectContainer<ItemStack>[] chipset)
	{
		float temp = container.getHeat();
			
		for(int i=0;i<core.length;i++)
		{
			this.core[i] = update(core[i], temp);
		}		
		for(int i=0;i<ram.length;i++)
		{
			this.ram[i] = update(ram[i], temp);
		}
		for(int i=0;i<chipset.length;i++)
		{
			this.chipset[i] = update(chipset[i], temp);
		}
	}
	
	private EnumSlotHeat update(ObjectContainer<ItemStack> it, float heat)
	{
		if(it.object==null || it.object.isEmpty())
			return EnumSlotHeat.UNUSED;
		else
		{
			if(heat < 0.8F * TemperatureManager.getGlobalMinHeat())
				return EnumSlotHeat.OK;
			
			float max = TemperatureManager.getTemp(it.object);
			float l = line(max, heat);
			if(l<=0.2)
			{
				if(l<=0.02F)
				{
					it.object = kill(it.object);
				}
				else if(container.getLevel().random.nextInt((int) (5000*l))==0)
				{
					damage(it.object);
				}
			}
			return l<=0.2 ? EnumSlotHeat.HOT : l<=0.5 ? EnumSlotHeat.WARM : EnumSlotHeat.OK;
		}
	}
	
	private void damage(ItemStack it)
	{
		if(!it.hasTag())
		{
			it.setTag(new CompoundTag());
		}
		
		if(it.getItem() instanceof ItemCore)
		{			
			CompoundTag nbt = it.getTag();
			int core = nbt.getInt("core");	
			if(core>0)
				nbt.putInt("core", core-1);
			nbt.putBoolean("toasted", true);
		}
		else if(it.getItem() instanceof ItemRam)
		{
			CompoundTag nbt = it.getTag();
			int ram = nbt.getInt("ram");
			if(ram>0)
				nbt.putInt("ram", ram-1);
			nbt.putBoolean("toasted", true);
		}
		else if(it.getItem() instanceof ItemChip)
		{
			CompoundTag nbt = it.getTag();
			float f = nbt.getFloat("power");
			if(f>0)
				nbt.putFloat("power", f-0.1F);
			nbt.putBoolean("toasted", true);
		}
		container.getInventory().resetCash();
	}
	
	//@SuppressWarnings("deprecation")
	private ItemStack kill(@Nonnull ItemStack it)
	{
		CompoundTag old = new CompoundTag();
		it.save(old);
		old.putBoolean("toasted", true);
		
		ItemLike toasted = null;
		if(it.getItem() instanceof ItemCore)
		{			
			toasted = ComputerItems.toasted_core;
		}
		else if(it.getItem() instanceof ItemRam)
		{
			toasted = ComputerItems.toasted_ram;
		}
		else if(it.getItem() instanceof ItemChip)
		{
			toasted = ComputerItems.toasted_chip;
		}
		ItemStack broken = new ItemStack(toasted, it.getCount());
		broken.setTag(old);
		container.getInventory().resetCash();
		return broken;
	}
	
	public static enum EnumSlotHeat
	{
		OK,
		WARM(203,2),
		HOT(221,2),
		UNUSED(185, 2);
		
		final boolean render;
		final int u,v;
		
		EnumSlotHeat(boolean render, int x, int y)
		{
			this.render = render;
			this.u=x;
			this.v=y;
		}
		
		EnumSlotHeat()
		{
			this(false,-1,-1);
		}
		
		EnumSlotHeat(int x, int y)
		{
			this(true,x,y);
		}
		
		public void render(PoseStack matrixStack, int x, int y)
		{
			if(render)
				GuiComponent.blit(matrixStack, x, y, u, v, 18, 18, 256, 256);
		}
	}

	public void drawCore(PoseStack matrixStack, int x, int y, int id)
	{
		core[id].render(matrixStack, x, y);
	}
	
	public void drawRam(PoseStack matrixStack, int x, int y, int id)
	{
		ram[id].render(matrixStack, x, y);
	}
	
	public void drawChip(PoseStack matrixStack, int x, int y, int id)
	{
		chipset[id].render(matrixStack, x, y);
	}

	
	public static class ObjectContainer<T>
	{
		public T object;
		
		public ObjectContainer() {}
		
		public ObjectContainer(T t)
		{
			this.object = t;
		
		}
	}
}
