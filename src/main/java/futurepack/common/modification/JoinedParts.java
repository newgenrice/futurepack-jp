package futurepack.common.modification;

import java.util.Arrays;
import java.util.EnumMap;


public class JoinedParts implements IModificationPart
{
	private final IModificationPart[] parts;

	private IModificationPart[] chips;
	private Float ramSpeed;
	private Integer core_needed, core_provided;
	private final EnumMap<EnumChipType, Float> map = new EnumMap<EnumChipType, Float>(EnumChipType.class);
	
	public JoinedParts(IModificationPart[] parts)
	{
		super();
		this.parts = parts;
		reseatCache();
	}

	@Override
	public boolean isChip() 
	{
		if(chips == null)
		{
			this.chips = Arrays.stream(parts).filter(IModificationPart::isChip).toArray(IModificationPart[]::new);
		}
		return chips.length > 0;
	}

	@Override
	public boolean isRam() 
	{
		if(ramSpeed == null)
		{
			ramSpeed = (float) Arrays.stream(parts).filter(IModificationPart::isRam).mapToDouble(IModificationPart::getRamSpeed).sum();
		}
		return ramSpeed > 0;
	}

	@Override
	public boolean isCore() 
	{
		if(core_provided == null)
		{
			core_provided = Arrays.stream(parts).mapToInt(p -> p.getCorePower(EnumCorePowerType.PROVIDED)).sum();
		}
		return core_provided > 0;
	}

	@Override
	public int getCorePower(EnumCorePowerType type)
	{
		if(core_needed == null)
			core_needed = Arrays.stream(parts).mapToInt(p -> p.getCorePower(EnumCorePowerType.NEEDED)).sum();
		if(core_provided == null)
			core_provided = Arrays.stream(parts).mapToInt(p -> p.getCorePower(EnumCorePowerType.PROVIDED)).sum();
		
		switch (type)
		{
		case NEEDED:
			return core_needed;
		case PROVIDED:
			return core_provided;
		case BOTH:
			return core_provided - core_needed;
		default:
			return 0;
		}
	}
	
	@Override
	public float getRamSpeed() 
	{
		if(ramSpeed == null)
		{
			ramSpeed = (float) Arrays.stream(parts).filter(IModificationPart::isRam).mapToDouble(IModificationPart::getRamSpeed).sum();
		}
		return ramSpeed;
	}
	
	@Override
	public float getChipPower(EnumChipType type)
	{
		if(chips == null)
		{
			this.chips = Arrays.stream(parts).filter(IModificationPart::isChip).toArray(IModificationPart[]::new);
		}
		
		return map.computeIfAbsent(type, t -> {
			return (float) Arrays.stream(chips).mapToDouble(c -> c.getChipPower(t)).sum();
		});
	}
	
	public void reseatCache()
	{
		chips = null;
		ramSpeed = null;
		core_needed = null;
		core_provided = null;
		map.clear();
	}

	@Override
	public float getMaximumTemperature()
	{
		throw new UnsupportedOperationException("getMaximumTemperature");
	}
	
	
}
