package futurepack.common.modification;

public interface IModificationPart
{
	/**
	 * is this a chip, if not {@code IModificationPart#getChipPower(EnumChipType)} will always return 0.
	 */
	boolean isChip();
	
	/**
	 * is this a ram modul, if not {@code IModificationPart#getRamSpeed()} will always return 0.
	 */
	boolean isRam();
	
	/**
	 * is this a core, if not {@code IModificationPart#getChipPower(EnumChipType)} will always return 0 for {@code EnumCorePowerType#PROVIDED}
	 * @return
	 */
	boolean isCore();
	
	/**
	 * @param type see {@code EnumCorePowerType} for more information
	 */
	int getCorePower(EnumCorePowerType type);
	
	float getRamSpeed();
	
	float getChipPower(EnumChipType type);
	
	/**
	 * The maximum temperature before this part gets destroyedS
	 * @return Temperature as float
	 */
	float getMaximumTemperature();
	
	public enum EnumCorePowerType
	{
		/**
		 * Only needed core power should get returned, so for example for ram: 3, for chips: 1. Cores would return 0
		 */
		NEEDED,
		/**
		 * Only return provided core power. For example chips & rams would return 0, and cores e.g. 3.
		 */
		PROVIDED,
		/**
		 * Retun the joined core power. needed is negative and provided positive. So rams & chips will return negative values.
		 */
		BOTH,
		
		
	}
}
