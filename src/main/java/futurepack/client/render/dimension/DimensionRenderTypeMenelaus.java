package futurepack.client.render.dimension;

import futurepack.client.render.RenderSkyMenelaus;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.DimensionSpecialEffects;
import net.minecraft.world.phys.Vec3;

public class DimensionRenderTypeMenelaus extends DimensionSpecialEffects 
{

	private final static float cloud_height = 180F;
	
	private final static boolean isNether = false;
	private final static boolean isEnd = false;
	
	public DimensionRenderTypeMenelaus()
	{
		super(cloud_height, true, SkyType.NORMAL, isEnd, isNether);
		setSkyRenderHandler(new RenderSkyMenelaus());
	}

	/**
	 * Calculate sunset color
	 */
	@Override
	public Vec3 getBrightnessDependentFogColor(Vec3 sky_color_body, float celestialAngle_clamped) 
	{
		//From 1.14
		//@Override
    	//public Vec3d getSkyColor(BlockPos cameraEntity, float partialTicks)
    	//{
	  	//Vec3d base = world.getSkyColorBody(cameraEntity, partialTicks);
	  	//float f1 = MathHelper.clamp((float) (base.x + 0.05F), 0.0F, 1.0F);
	  	//float f2 = MathHelper.clamp((float) (base.y + 0.025F), 0.0F, 1.0F);
	  	//return new Vec3d(f1, f2, base.z);


		return sky_color_body.multiply((double)(celestialAngle_clamped * 0.94F + 0.16F), (double)(celestialAngle_clamped * 0.94F + 0.08F), (double)(celestialAngle_clamped * 0.91F + 0.09F)); //from overworld
	}

	/**
	 * show fog at
	 */
	@Override
	public boolean isFoggyAt(int p_230493_1_, int p_230493_2_) 
	{
		if(Minecraft.getInstance().level.isRaining())
		{
			return true;
		}
		return false;
	}

}
