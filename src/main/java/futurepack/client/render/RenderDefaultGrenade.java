package futurepack.client.render;

import com.mojang.math.Vector4f;

import futurepack.common.entity.throwable.EntityGrenadeBase;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.resources.ResourceLocation;

public class RenderDefaultGrenade extends RenderThrowable3DBase<EntityGrenadeBase>
{
	private final ResourceLocation tex;
	
	public RenderDefaultGrenade(EntityRendererProvider.Context renderManager, ResourceLocation res)
	{
		super(renderManager);
		this.tex = res;
	}
	
	@Override
	public ResourceLocation getTextureLocation(EntityGrenadeBase entity)
	{
		return tex;// 
	}

	private final Vector4f side = new Vector4f(3F/32F, 0F, 13F/32F, 1F);
	
	@Override
	public Vector4f getUVForSides(EntityGrenadeBase entity) 
	{
		return side;
	}

	private final Vector4f front = new Vector4f(0.0625F * 1.5F +0.5F,
												0.0625F * 6.5F +0.5F,
												0.0625F * 1.5F ,
												0.0625F * 6.5F );
	
	@Override
	public Vector4f getUVForFront(EntityGrenadeBase entity) 
	{
		return front;
	}

	@Override
	public float getWidth(EntityGrenadeBase entity) 
	{
		return 5f/32F;
	}

	@Override
	public float getDepth(EntityGrenadeBase entity) 
	{
		return 0.25f;
	}
	
	
}
