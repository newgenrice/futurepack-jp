package futurepack.client.render.hologram;

import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.ClipBlockStateContext;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.ColorResolver;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.LightLayer;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeManager;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.border.WorldBorder;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.chunk.ChunkStatus;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.levelgen.Heightmap.Types;
import net.minecraft.world.level.lighting.LevelLightEngine;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class DelegatedWorldReader implements LevelReader
{
	public int getSectionsCount()
	{
		return delegate.getSectionsCount();
	}

	public int getMinSection()
	{
		return delegate.getMinSection();
	}

	public int getMaxSection()
	{
		return delegate.getMaxSection();
	}

	public boolean isOutsideBuildHeight(BlockPos pPos)
	{
		return delegate.isOutsideBuildHeight(pPos);
	}

	public boolean isOutsideBuildHeight(int pY)
	{
		return delegate.isOutsideBuildHeight(pY);
	}

	public <T extends BlockEntity> Optional<T> getBlockEntity(BlockPos pPos, BlockEntityType<T> pBlockEntityType)
	{
		return delegate.getBlockEntity(pPos, pBlockEntityType);
	}

	public int getSectionIndex(int pY)
	{
		return delegate.getSectionIndex(pY);
	}

	public int getSectionIndexFromSectionY(int pSectionIndex)
	{
		return delegate.getSectionIndexFromSectionY(pSectionIndex);
	}

	public int getSectionYFromSectionIndex(int pSectionIndex)
	{
		return delegate.getSectionYFromSectionIndex(pSectionIndex);
	}

	public BlockHitResult isBlockInLine(ClipBlockStateContext pContext)
	{
		return delegate.isBlockInLine(pContext);
	}

	public Biome getPrimaryBiome(ChunkPos pChunkPos)
	{
		return delegate.getPrimaryBiome(pChunkPos);
	}

	public int getMinBuildHeight()
	{
		return delegate.getMinBuildHeight();
	}

	public int getHeight()
	{
		return delegate.getHeight();
	}

	public boolean hasBlockCollision(Entity pEntity, AABB pCollisionBox, BiPredicate<BlockState, BlockPos> pFilter)
	{
		return delegate.hasBlockCollision(pEntity, pCollisionBox, pFilter);
	}

	public Optional<Vec3> findFreePosition(Entity pEntity, VoxelShape pShape, Vec3 pPos, double pX, double pY, double pZ)
	{
		return delegate.findFreePosition(pEntity, pShape, pPos, pX, pY, pZ);
	}

	public boolean hasChunkAt(int pX, int pZ)
	{
		return delegate.hasChunkAt(pX, pZ);
	}

	public boolean hasChunksAt(int pFromX, int pFromZ, int pToX, int pToZ)
	{
		return delegate.hasChunksAt(pFromX, pFromZ, pToX, pToZ);
	}

	private final LevelReader delegate;

	public DelegatedWorldReader(LevelReader delegate) 
	{
		super();
		this.delegate = delegate;
	}

	@Override
	public Biome getBiome(BlockPos pos) {
		return delegate.getBiome(pos);
	}
	@Override
	public int getBrightness(LightLayer type, BlockPos pos) {
		return delegate.getBrightness(type, pos);
	}
	@Override
	public BlockEntity getBlockEntity(BlockPos pos) {
		return delegate.getBlockEntity(pos);
	}
	@Override
	public BlockState getBlockState(BlockPos pos) {
		return delegate.getBlockState(pos);
	}
	@Override
	public FluidState getFluidState(BlockPos pos) {
		return delegate.getFluidState(pos);
	}
	@Override
	public int getLightEmission(BlockPos pos) {
		return delegate.getLightEmission(pos);
	}
	@Override
	public int getMaxLightLevel() {
		return delegate.getMaxLightLevel();
	}
	@Override
	public int getMaxBuildHeight() {
		return delegate.getMaxBuildHeight();
	}
	@Override
	public BlockHitResult clip(ClipContext context) {
		return delegate.clip(context);
	}
	@Override
	public boolean isEmptyBlock(BlockPos pos) {
		return delegate.isEmptyBlock(pos);
	}
	@Override
	public boolean canSeeSkyFromBelowWater(BlockPos pos) {
		return delegate.canSeeSkyFromBelowWater(pos);
	}
	@Override
	public int getRawBrightness(BlockPos pos, int amount) {
		return delegate.getRawBrightness(pos, amount);
	}
	@Override
	public ChunkAccess getChunk(int x, int z, ChunkStatus requiredStatus, boolean nonnull) {
		return delegate.getChunk(x, z, requiredStatus, nonnull);
	}
	@Override
	public boolean hasChunk(int chunkX, int chunkZ) {
		return delegate.hasChunk(chunkX, chunkZ);
	}
	@Override
	public BlockHitResult clipWithInteractionOverride(Vec3 p_217296_1_, Vec3 p_217296_2_, BlockPos p_217296_3_,
			VoxelShape p_217296_4_, BlockState p_217296_5_) {
		return delegate.clipWithInteractionOverride(p_217296_1_, p_217296_2_, p_217296_3_, p_217296_4_, p_217296_5_);
	}
	@Override
	public BlockPos getHeightmapPos(Types heightmapType, BlockPos pos) {
		return delegate.getHeightmapPos(heightmapType, pos);
	}
	@Override
	public int getHeight(Types heightmapType, int x, int z) {
		return delegate.getHeight(heightmapType, x, z);
	}
	@Override
	public float getBrightness(BlockPos pos) {
		return delegate.getBrightness(pos);
	}
	@Override
	public int getSkyDarken() {
		return delegate.getSkyDarken();
	}
	@Override
	public WorldBorder getWorldBorder() {
		return delegate.getWorldBorder();
	}
	@Override
	public boolean isUnobstructed(Entity entityIn, VoxelShape shape) {
		return delegate.isUnobstructed(entityIn, shape);
	}
	@Override
	public int getDirectSignal(BlockPos pos, Direction direction) {
		return delegate.getDirectSignal(pos, direction);
	}
	@Override
	public boolean isClientSide() {
		return delegate.isClientSide();
	}
	@Override
	public int getSeaLevel() {
		return delegate.getSeaLevel();
	}
	@Override
	public ChunkAccess getChunk(BlockPos pos) {
		return delegate.getChunk(pos);
	}
	@Override
	public ChunkAccess getChunk(int chunkX, int chunkZ) {
		return delegate.getChunk(chunkX, chunkZ);
	}
	@Override
	public ChunkAccess getChunk(int chunkX, int chunkZ, ChunkStatus requiredStatus) {
		return delegate.getChunk(chunkX, chunkZ, requiredStatus);
	}
	public boolean canSeeSky(BlockPos blockPosIn) {
		return delegate.canSeeSky(blockPosIn);
	}

	public boolean isUnobstructed(BlockState p_226663_1_, BlockPos p_226663_2_, CollisionContext p_226663_3_) {
		return delegate.isUnobstructed(p_226663_1_, p_226663_2_, p_226663_3_);
	}

	public Stream<BlockState> getBlockStates(AABB p_234853_1_) {
		return delegate.getBlockStates(p_234853_1_);
	}

	public Stream<BlockState> getBlockStatesIfLoaded(AABB p_234939_1_) {
		return delegate.getBlockStatesIfLoaded(p_234939_1_);
	}

	public boolean isUnobstructed(Entity p_226668_1_) {
		return delegate.isUnobstructed(p_226668_1_);
	}

	public boolean noCollision(AABB p_226664_1_) {
		return delegate.noCollision(p_226664_1_);
	}

	public int getBlockTint(BlockPos blockPosIn, ColorResolver colorResolverIn) {
		return delegate.getBlockTint(blockPosIn, colorResolverIn);
	}

	public boolean noCollision(Entity p_226669_1_) {
		return delegate.noCollision(p_226669_1_);
	}

	public Biome getNoiseBiome(int x, int y, int z) {
		return delegate.getNoiseBiome(x, y, z);
	}

	public boolean noCollision(Entity p_226665_1_, AABB p_226665_2_) {
		return delegate.noCollision(p_226665_1_, p_226665_2_);
	}

	public boolean noCollision(Entity p_234865_1_, AABB p_234865_2_,
			Predicate<Entity> p_234865_3_) {
		return delegate.noCollision(p_234865_1_, p_234865_2_, p_234865_3_);
	}

	public Stream<VoxelShape> getCollisions(Entity p_234867_1_, AABB p_234867_2_,
			Predicate<Entity> p_234867_3_) {
		return delegate.getCollisions(p_234867_1_, p_234867_2_, p_234867_3_);
	}

	public Stream<VoxelShape> getBlockCollisions(Entity p_241457_1_, AABB p_241457_2_,
			BiPredicate<BlockState, BlockPos> p_241457_3_) {
		return delegate.getBlockCollisions(p_241457_1_, p_241457_2_, p_241457_3_);
	}

	public double getBlockFloorHeight(VoxelShape p_242402_1_, Supplier<VoxelShape> p_242402_2_) {
		return delegate.getBlockFloorHeight(p_242402_1_, p_242402_2_);
	}

	public double getBlockFloorHeight(BlockPos p_242403_1_) {
		return delegate.getBlockFloorHeight(p_242403_1_);
	}

	public BlockGetter getChunkForCollisions(int chunkX, int chunkZ) {
		return delegate.getChunkForCollisions(chunkX, chunkZ);
	}

	@Override
	public Stream<VoxelShape> getBlockCollisions(Entity entityIn, AABB aabb) {
		return delegate.getBlockCollisions(entityIn, aabb);
	}
	@Override
	public boolean isWaterAt(BlockPos pos) {
		return delegate.isWaterAt(pos);
	}
	@Override
	public boolean containsAnyLiquid(AABB bb) {
		return delegate.containsAnyLiquid(bb);
	}
	@Override
	public int getMaxLocalRawBrightness(BlockPos pos) {
		return delegate.getMaxLocalRawBrightness(pos);
	}
	@Override
	public int getMaxLocalRawBrightness(BlockPos pos, int amount) {
		return delegate.getMaxLocalRawBrightness(pos, amount);
	}
	@Override
	public boolean hasChunkAt(BlockPos pos) {
		return delegate.hasChunkAt(pos);
	}
	@Override
	public boolean isAreaLoaded(BlockPos center, int range) {
		return delegate.isAreaLoaded(center, range);
	}
	@Override
	public boolean hasChunksAt(BlockPos from, BlockPos to) {
		return delegate.hasChunksAt(from, to);
	}
	@Override
	public boolean hasChunksAt(int p_217344_1_, int p_217344_2_, int p_217344_3_, int p_217344_4_,
			int p_217344_5_, int p_217344_6_) {
		return delegate.hasChunksAt(p_217344_1_, p_217344_2_, p_217344_3_, p_217344_4_, p_217344_5_, p_217344_6_);
	}

    @Override
    public LevelLightEngine getLightEngine() 
    {
        return delegate.getLightEngine();
    }

    @Override
    public BiomeManager getBiomeManager() {
        return delegate.getBiomeManager();
    }

    @Override
    public Biome getUncachedNoiseBiome(int x, int y, int z) {
        return delegate.getUncachedNoiseBiome(x, y, z);
    }

	@Override
	public float getShade(Direction p_230487_1_, boolean p_230487_2_) 
	{
		return delegate.getShade(p_230487_1_, p_230487_2_);
	}

	@Override
	public Stream<VoxelShape> getEntityCollisions(Entity p_230318_1_, AABB p_230318_2_, Predicate<Entity> p_230318_3_) 
	{
		return delegate.getEntityCollisions(p_230318_1_, p_230318_2_, p_230318_3_);
	}

	@Override
	public DimensionType dimensionType() 
	{
		return delegate.dimensionType();
	}
	
	
}
