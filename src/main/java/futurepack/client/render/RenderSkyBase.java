package futurepack.client.render;

import java.lang.reflect.Field;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.blaze3d.vertex.BufferUploader;
import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.blaze3d.vertex.VertexBuffer;
import com.mojang.blaze3d.vertex.VertexFormat;
import com.mojang.math.Matrix4f;
import com.mojang.math.Vector3f;

import futurepack.common.DirtyHacks;
import net.minecraft.client.Camera;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.renderer.FogRenderer;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.client.renderer.LevelRenderer;
import net.minecraft.client.renderer.ShaderInstance;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.client.ISkyRenderHandler;

//@ TODO: OnlyIn(Dist.CLIENT)
public class RenderSkyBase implements ISkyRenderHandler
{
//	private int pass;
//	private boolean vboEnabled;
//	private net.minecraft.client.renderer.vertex.VertexBuffer skyVBO, sky2VBO, starVBO;
//	private int glSkyList, glSkyList2, starGLCallList;
	
	private com.mojang.blaze3d.vertex.VertexBuffer skyVBO, starVBO, sky2VBO;
	private final VertexFormat skyVertexFormat = DefaultVertexFormat.POSITION;
	
	protected ResourceLocation locationMoonPhasesPng = new ResourceLocation("textures/environment/moon_phases.png");
	protected ResourceLocation locationSunPng = new ResourceLocation("textures/environment/sun.png");

	public RenderSkyBase()
	{
//		Class man = WorldRenderer.class;
		LevelRenderer glob = Minecraft.getInstance().levelRenderer;
//		Field[] ff =  man.getDeclaredFields();
//		ff[15].setAccessible(true);
//		ff[16].setAccessible(true);
//		ff[17].setAccessible(true);
//		ff[19].setAccessible(true);
//		ff[20].setAccessible(true);
//		ff[21].setAccessible(true);
		
		
		
		try
		{
			Field f = DirtyHacks.findField(LevelRenderer.class, "skyVBO", "skyBuffer", VertexBuffer.class);
			f.setAccessible(true);
			skyVBO = (VertexBuffer) f.get(glob);
			
			f = DirtyHacks.findField(LevelRenderer.class, "starVBO", "starBuffer", VertexBuffer.class);
			f.setAccessible(true);
			starVBO = (VertexBuffer) f.get(glob); 
			
			f = DirtyHacks.findField(LevelRenderer.class, "sky2VBO", "darkBuffer", VertexBuffer.class);
			f.setAccessible(true);
			sky2VBO = (VertexBuffer) f.get(glob);
			
//			starGLCallList = ff[15].getInt(glob);
//			glSkyList = ff[16].getInt(glob);
//			glSkyList2 = ff[17].getInt(glob);
//			
//			starVBO =  (net.minecraft.client.renderer.vertex.VertexBuffer) ff[19].get(glob);
//			skyVBO = (net.minecraft.client.renderer.vertex.VertexBuffer) ff[20].get(glob);
//			sky2VBO =  (net.minecraft.client.renderer.vertex.VertexBuffer) ff[21].get(glob);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void render(int ticks, float partialTicks, PoseStack matrixStack, ClientLevel world, Minecraft mc)
	{
		Camera activerenderinfo = mc.gameRenderer.getMainCamera();
		PoseStack matrixStackIn = new PoseStack();
		float renderDistance = mc.gameRenderer.getRenderDistance();
		
		Vec3 vec3 = activerenderinfo.getPosition();
		double d0 = vec3.x();
		double d1 = vec3.y();
		double d2 = vec3.z();
		
		boolean flag1 = world.effects().isFoggyAt(Mth.floor(d0), Mth.floor(d1)) || mc.gui.getBossOverlay().shouldCreateWorldFog();
		
		net.minecraftforge.client.event.EntityViewRenderEvent.CameraSetup cameraSetup = net.minecraftforge.client.ForgeHooksClient.onCameraSetup(mc.gameRenderer, activerenderinfo, partialTicks);
		activerenderinfo.setAnglesInternal(cameraSetup.getYaw(), cameraSetup.getPitch());
		matrixStackIn.mulPose(Vector3f.ZP.rotationDegrees(cameraSetup.getRoll()));

	    matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(activerenderinfo.getXRot()));
	    matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(activerenderinfo.getYRot() + 180.0F));
	    
		renderSky(matrixStackIn, partialTicks, mc, world, () -> {
	         FogRenderer.setupFog(activerenderinfo, FogRenderer.FogMode.FOG_SKY, renderDistance, flag1);
	      });
	}

	protected void renderAfterStars(float partialTicks, ClientLevel world, Minecraft mc, BufferBuilder bufferbuilder, PoseStack matrixStackIn)
	{
		
	}

	protected void renderBeforeStars(float partialTicks, ClientLevel world, Minecraft mc, BufferBuilder bufferbuilder, PoseStack matrixStackIn)
	{
		
	}
	
	public float getMoonSize()
	{
		return 20F;
	}
	
	public float getSunSize()
	{
		return 30F;
	}
	
	protected void renderMoon(float size, int phase, BufferBuilder bufferbuilder, Matrix4f matrix4f1)
	{
		RenderSystem.setShader(GameRenderer::getPositionTexShader);
		RenderSystem.setShaderTexture(0, locationMoonPhasesPng);
        int l = phase % 4;
        int i1 = phase / 4 % 2;
        float f13 = (float)(l + 0) / 4.0F;
        float f14 = (float)(i1 + 0) / 2.0F;
        float f15 = (float)(l + 1) / 4.0F;
        float f16 = (float)(i1 + 1) / 2.0F;
        bufferbuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX);
        bufferbuilder.vertex(matrix4f1, -size, -100.0F, size).uv(f15, f16).endVertex();
        bufferbuilder.vertex(matrix4f1, size, -100.0F, size).uv(f13, f16).endVertex();
        bufferbuilder.vertex(matrix4f1, size, -100.0F, -size).uv(f13, f14).endVertex();
        bufferbuilder.vertex(matrix4f1, -size, -100.0F, -size).uv(f15, f14).endVertex();
        bufferbuilder.end();
        BufferUploader.end(bufferbuilder);
	}
	
	protected void renderSun(float size, BufferBuilder bufferbuilder, Matrix4f matrix4f1)
	{
		RenderSystem.setShader(GameRenderer::getPositionTexShader);
		RenderSystem.setShaderTexture(0, locationSunPng);
        bufferbuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX);
        bufferbuilder.vertex(matrix4f1, -size, 100.0F, -size).uv(0.0F, 0.0F).endVertex();
        bufferbuilder.vertex(matrix4f1, size, 100.0F, -size).uv(1.0F, 0.0F).endVertex();
        bufferbuilder.vertex(matrix4f1, size, 100.0F, size).uv(1.0F, 1.0F).endVertex();
        bufferbuilder.vertex(matrix4f1, -size, 100.0F, size).uv(0.0F, 1.0F).endVertex();
        bufferbuilder.end();
        BufferUploader.end(bufferbuilder);
	}
	
	public void renderSky(PoseStack matrixStackIn, float partialTicks, Minecraft minecraft, ClientLevel level, Runnable p_181413_) 
	{
		Matrix4f p_181411_ = matrixStackIn.last().pose();

	         RenderSystem.disableTexture();
	         Vec3 vec3 = level.getSkyColor(minecraft.gameRenderer.getMainCamera().getPosition(), partialTicks);
	         float f = (float)vec3.x;
	         float f1 = (float)vec3.y;
	         float f2 = (float)vec3.z;
	         FogRenderer.levelFogColor();
	         BufferBuilder bufferbuilder = Tesselator.getInstance().getBuilder();
	         RenderSystem.depthMask(false);
	         RenderSystem.setShaderColor(f, f1, f2, 1.0F);
	         ShaderInstance shaderinstance = RenderSystem.getShader();
	         this.skyVBO.drawWithShader(matrixStackIn.last().pose(), p_181411_, shaderinstance);
	         RenderSystem.enableBlend();
	         RenderSystem.defaultBlendFunc();
	         float[] afloat = level.effects().getSunriseColor(level.getTimeOfDay(partialTicks), partialTicks);
	         if (afloat != null) {
	            RenderSystem.setShader(GameRenderer::getPositionColorShader);
	            RenderSystem.disableTexture();
	            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
	            matrixStackIn.pushPose();
	            matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(90.0F));
	            float f3 = Mth.sin(level.getSunAngle(partialTicks)) < 0.0F ? 180.0F : 0.0F;
	            matrixStackIn.mulPose(Vector3f.ZP.rotationDegrees(f3));
	            matrixStackIn.mulPose(Vector3f.ZP.rotationDegrees(90.0F));
	            float f4 = afloat[0];
	            float f5 = afloat[1];
	            float f6 = afloat[2];
	            Matrix4f matrix4f = matrixStackIn.last().pose();
	            bufferbuilder.begin(VertexFormat.Mode.TRIANGLE_FAN, DefaultVertexFormat.POSITION_COLOR);
	            bufferbuilder.vertex(matrix4f, 0.0F, 100.0F, 0.0F).color(f4, f5, f6, afloat[3]).endVertex();
	            int i = 16;

	            for(int j = 0; j <= 16; ++j) {
	               float f7 = (float)j * ((float)Math.PI * 2F) / 16.0F;
	               float f8 = Mth.sin(f7);
	               float f9 = Mth.cos(f7);
	               bufferbuilder.vertex(matrix4f, f8 * 120.0F, f9 * 120.0F, -f9 * 40.0F * afloat[3]).color(afloat[0], afloat[1], afloat[2], 0.0F).endVertex();
	            }

	            bufferbuilder.end();
	            BufferUploader.end(bufferbuilder);
	            matrixStackIn.popPose();
	         }

	         RenderSystem.enableTexture();
	         RenderSystem.blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
	         matrixStackIn.pushPose();
	         float f11 = 1.0F - level.getRainLevel(partialTicks);
	         RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, f11);
	         matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(-90.0F));
	         matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(level.getTimeOfDay(partialTicks) * 360.0F));
	         Matrix4f matrix4f1 = matrixStackIn.last().pose();
	         
	         renderSun(getSunSize(), bufferbuilder, matrix4f1);//added this
	         renderMoon(getMoonSize(), level.getMoonPhase(), bufferbuilder, matrix4f1);//added this
	         
	         renderBeforeStars(partialTicks, level, minecraft, bufferbuilder, matrixStackIn);//added this!
	         
	         RenderSystem.disableTexture();
	         float f10 = level.getStarBrightness(partialTicks) * f11;
	         if (f10 > 0.0F) {
	            RenderSystem.setShaderColor(f10, f10, f10, f10);
	            FogRenderer.setupNoFog();
	            this.starVBO.drawWithShader(matrixStackIn.last().pose(), p_181411_, GameRenderer.getPositionShader());
	            p_181413_.run();
	         }

	         RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
	         
	         renderAfterStars(partialTicks, level, minecraft, bufferbuilder, matrixStackIn);//added this
	         
	         RenderSystem.disableBlend();
	         matrixStackIn.popPose();
	         RenderSystem.disableTexture();
	         RenderSystem.setShaderColor(0.0F, 0.0F, 0.0F, 1.0F);
	         double d0 = minecraft.player.getEyePosition(partialTicks).y - level.getLevelData().getHorizonHeight(level);
	         if (d0 < 0.0D) {
	            matrixStackIn.pushPose();
	            matrixStackIn.translate(0.0D, 12.0D, 0.0D);
	            this.sky2VBO.drawWithShader(matrixStackIn.last().pose(), p_181411_, shaderinstance);
	            matrixStackIn.popPose();
	         }

	         if (level.effects().hasGround()) {
	            RenderSystem.setShaderColor(f * 0.2F + 0.04F, f1 * 0.2F + 0.04F, f2 * 0.6F + 0.1F, 1.0F);
	         } else {
	            RenderSystem.setShaderColor(f, f1, f2, 1.0F);
	         }

	         RenderSystem.enableTexture();
	         RenderSystem.depthMask(true);
	      
	}

	protected float getStarBrightness(float partialTicks, ClientLevel world, float skyBrightness) 
	{
		return world.getStarBrightness(partialTicks) * skyBrightness;
	}
}
