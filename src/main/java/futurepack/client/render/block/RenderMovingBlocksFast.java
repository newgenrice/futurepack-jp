package futurepack.client.render.block;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.block.logistic.frames.TileEntityMovingBlocks;
import futurepack.depend.api.MiniWorld;
import futurepack.depend.api.helper.HelperRenderBlocks;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;

public class RenderMovingBlocksFast implements BlockEntityRenderer<TileEntityMovingBlocks>
{

	public RenderMovingBlocksFast(BlockEntityRendererProvider.Context rendererDispatcherIn) 
	{
//		super(rendererDispatcherIn);
	}
	
	@Override
	public boolean shouldRenderOffScreen(TileEntityMovingBlocks te)
	{
		return true;
	}

	@Override
	public void render(TileEntityMovingBlocks te, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int combinedLightIn, int combinedOverlayIn) 
	{
		GlStateManager._enableCull();
		
		MiniWorld world = te.getMiniWorld();
		if(world!=null)
		{
			float progress =  1F-( ( te.ticks-partialTicks ) /te.maxticks);
			
			matrixStackIn.pushPose();
			
			
			matrixStackIn.translate(progress * te.getDirection().getX(), progress* te.getDirection().getY(), progress* te.getDirection().getZ());
			
			HelperRenderBlocks.renderFast(world, partialTicks, matrixStackIn, bufferIn);
			
			
			
			
			matrixStackIn.popPose();
			//HelperRenderBlocks.renderFastBase(world, buf); // <-- geht
		}
	}

}
