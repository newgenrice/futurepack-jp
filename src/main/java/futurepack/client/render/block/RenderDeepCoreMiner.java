package futurepack.client.render.block;

import java.util.Random;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Matrix3f;
import com.mojang.math.Matrix4f;
import com.mojang.math.Vector3f;

import futurepack.api.Constants;
import futurepack.common.block.multiblock.BlockDeepCoreMiner;
import futurepack.common.block.multiblock.BlockDeepCoreMiner.EnumDeepCoreMiner;
import futurepack.common.block.multiblock.DeepCoreLogic;
import futurepack.common.block.multiblock.MultiblockBlocks;
import futurepack.common.block.multiblock.TileEntityDeepCoreMinerMain;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.phys.Vec3;

public class RenderDeepCoreMiner implements BlockEntityRenderer<TileEntityDeepCoreMinerMain>
{
	ModelDeepCoreMiner model;
	public static final ResourceLocation tex = new ResourceLocation(Constants.MOD_ID, "textures/model/kernbohrer.png");
	
	
	public RenderDeepCoreMiner(BlockEntityRendererProvider.Context rendererDispatcherIn) 
	{
		model = new ModelDeepCoreMiner(rendererDispatcherIn.bakeLayer(ModelDeepCoreMiner.LAYER_LOCATION));
	}

	@Override
	public void render(TileEntityDeepCoreMinerMain te, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int combinedLightIn, int combinedOverlayIn) 
	{
		
		if(te.getBlockState().getBlock() == MultiblockBlocks.deepcore_miner)
		{
			if(te.getBlockState().getValue(BlockDeepCoreMiner.variants) == EnumDeepCoreMiner.Main)
			{
				DeepCoreLogic logic = te.getLogic();
				if(logic!=null)
				{
					matrixStackIn.pushPose();
					
					RenderSystem.setShaderColor(1, 1, 1, 1);
					matrixStackIn.translate(0.5, 0, 0.5);
					matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(180F));
					matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(logic.getFacing().toYRot()));
					matrixStackIn.translate(0, 0, -1);
					
		
					float laser = 0F;
					if(te.clientWorking)
					{	
						laser = (float) Math.sin(System.currentTimeMillis() / 1000D) + 1F;
					}
					laser *= 0.5 * 0.85;		
					model.setLaserPosition(laser);			
								
					int color = 0x000000; //0xRRGGBB
					if(logic.getLense()!=null)
					{
						color = logic.getLense().getColor(logic.getLenseStack(), logic);
						model.Linse.visible = true;
						model.Linse_glow.visible = true;
						model.Fokusmechanik.visible = true;
					}
					else
					{
						model.Linse.visible = false;
						model.Linse_glow.visible = false;
						model.Fokusmechanik.visible = false;
					}
					float r = ((color>>16) & 0xFF) / 255F;
					float g = ((color>>8) & 0xFF) / 255F;
					float b = (color & 0xFF) / 255F;

					VertexConsumer builder = bufferIn.getBuffer(model.renderType(tex));
					model.renderToBuffer(matrixStackIn, builder, combinedLightIn, combinedOverlayIn, r, g, b, 1F);
					
					if(te.clientWorking && logic.getLense()!=null)
					{
						matrixStackIn.mulPose(Vector3f.YN.rotationDegrees(logic.getFacing().toYRot()));
						
						float x = (float) (te.getBlockPos().getX() - Minecraft.getInstance().player.getX());
						float z = (float) (te.getBlockPos().getZ() - Minecraft.getInstance().player.getZ());

						renderLaser(laser, x+te.getFacing().getStepX()+0.5, z+te.getFacing().getStepZ()+0.5, builder, matrixStackIn, 0xF000F0, combinedOverlayIn, r,g,b);
						
						Vec3 vec = Vec3.atLowerCornerOf(te.getBlockPos().relative(te.getFacing())).add(0.5, 0.2, 0.5);
						ParticleOptions type = logic.getLense().randomParticle(logic.getLenseStack(), logic);
						if(type!=null)
						{
							Random rr = te.getLevel().random;
							float mx = (rr.nextFloat() - 0.5F) * 0.2F;
							float mz = (rr.nextFloat() - 0.5F) * 0.2F;
							float px = (rr.nextFloat() - 0.5F) * 0.4F;
							float pz = (rr.nextFloat() - 0.5F) * 0.4F;
							Minecraft.getInstance().level.addParticle(type, false, vec.x+px, vec.y, vec.z+pz, mx, 0.2, mz);
						}
					}

					matrixStackIn.popPose();
				}
			}
		}
	}
	
	private void renderLaser(float laserOffset, double px, double pz, VertexConsumer buf, PoseStack stackIn, int combinedLightIn, int combinedOverlayIn, float r, float g, float b)
	{
		double offset = Math.sin(System.currentTimeMillis() / 1000D) + 1D;// 0-2
		int pos = (int) (offset /2D * 6D);
		float u0 = pos*16F / 256F;
		float v0 = (180F + 16F*((System.currentTimeMillis()/2) % 1000)/1000)/256F;
		float u1 = u0 + 16F/256F;
		float v1 = v0+ +16F/256F;
		
		final float length = 0.5F + 1F;
		final float x = 0F;
		final float y = -1.25F + laserOffset;
		final float z = 0F;
		final float w = 0.125F;
		
		if(pz>0)
			px = -px;
		double angle = Math.asin(px / Math.sqrt(px*px+pz*pz));
		if(pz<0)
			angle += Math.PI;
		double d = angle - Math.PI;
		float wx = (float) (w * Math.cos(d));
		float wz = (float) (- w * Math.sin(d));
        
        Matrix4f matrix = stackIn.last().pose();
        Matrix3f normals = stackIn.last().normal();
        
		
        //.add(POSITION_3F).add(COLOR_4UB).add(TEX_2F).add(TEX_2S).add(TEX_2SB).add(NORMAL_3B).add(PADDING_1B).build());
        //pos color uv overlay lightmap normal
        
//		BufferBuilder buf = Tessellator.getInstance().getBuffer();
//		buf.begin(VertexFormat.Mode.QUADS, DefaultVertexFormats.POSITION_TEX);
		buf.vertex(matrix, x-wx, y+length, z-wz).color(r, g, b, 1F).uv(u0, v1).overlayCoords(combinedOverlayIn).uv2(combinedLightIn).normal(normals, 0, 0, 1).endVertex();
		buf.vertex(matrix, x+wx, y+length, z+wz).color(r, g, b, 1F).uv(u1, v1).overlayCoords(combinedOverlayIn).uv2(combinedLightIn).normal(normals, 0, 0, 1).endVertex();
		buf.vertex(matrix, x+wx, y, z+wz)       .color(r, g, b, 1F).uv(u1, v0).overlayCoords(combinedOverlayIn).uv2(combinedLightIn).normal(normals, 0, 0, 1).endVertex();
		buf.vertex(matrix, x-wx, y, z-wz)       .color(r, g, b, 1F).uv(u0, v0).overlayCoords(combinedOverlayIn).uv2(combinedLightIn).normal(normals, 0, 0, 1).endVertex();
		
//		Tessellator.getInstance().draw();
	}
}
