package futurepack.client.render.block;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.blaze3d.vertex.VertexFormat;
import com.mojang.math.Matrix4f;

import futurepack.client.render.FuturepackRenderTypes;
import futurepack.common.block.misc.TileEntityClaime;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;

public class RenderClaime implements BlockEntityRenderer<TileEntityClaime>
{
	
	public RenderClaime(BlockEntityRendererProvider.Context rendererDispatcherIn) 
	{
//		super(rendererDispatcherIn);
	}

	@Override
	public void render(TileEntityClaime tile, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int combinedLight, int combinedOverlayIn) 
	{
		tile.getLevel().getProfiler().push("renderClaime");
		matrixStackIn.pushPose();
		
//		GlStateManager.disableTexture();
//		//GL11.glDisable(GL11.GL_ALPHA_TEST);
//		GlStateManager.disableLighting();
		//GL11.glDisable(GL11.GL_CULL_FACE);
		//GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE);
		
		VertexConsumer builder = bufferIn.getBuffer(FuturepackRenderTypes.CLAIME);
		
		RenderSystem.setShaderColor(1F, 1F, 1F, 1F);
		
		float x=tile.mx, y=tile.my+1, z=tile.mz;
		
		float d = 0.03125F;
		
		combinedLight |= 0xF000F0;
		float x_0 = x-tile.x;
		float z_0 = z-tile.z;
		float y_0 = y-tile.y;
		float x_1 = x+tile.x;
		float z_1 = z+tile.z;
		drawWireframeCubes(builder, matrixStackIn, x_0, y_0, z_0, x_1, y, z_1, d, 1F, 1F, 0F, 1F, combinedLight);
		
		float x1,y1,z1,x2,y2,z2;
		x1=x2=x;
		y1=y2=y-1;
		z1=z2=z;
		
		x1+=0.5-0.0625;
		y1+=0.5-0.0625;
		z1+=0.5-0.0625;
		x2+=0.5+0.0625;
		y2+=0.5+0.0625;
		z2+=0.5+0.0625;
		
		drawQube(builder, x1, y1, z1, x2, y2, z2, 1F, 0F, 0F, 1F, matrixStackIn, combinedLight);
		
//		if(tile.renderAll = false)
//		{
//			GlStateManager._disableTexture();
//			GlStateManager._disableDepthTest();
//			
//			Tesselator tes = Tesselator.getInstance();
//			builder = tes.getBuilder();
//			((BufferBuilder)builder).begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_COLOR_LIGHTMAP);
//			float col = (float) (0.5+Math.sin(System.currentTimeMillis()%1000/500F*Math.PI)*0.5);
//			d = 0.03125F *0.5F;
//						
//			drawWireframeCubes(builder, matrixStackIn, x_0, y_0, z_0, x_1, y, z_1, d, col,col, 0F, 1F, combinedLight);
//			
//			tes.end();
//			
//			GlStateManager._enableTexture();
//			GlStateManager._enableDepthTest();
//		}

		matrixStackIn.popPose();
			
		tile.getLevel().getProfiler().pop();
	}
	
	public static void drawWireframeCubes(VertexConsumer builder, PoseStack matrixStackIn, float x_0, float y_0, float z_0, float x_1, float y_1, float z_1, float d, float red, float green, float blue, float alpha, int combinedLight)
	{
		drawQube(builder, x_0-d+1, y_0, z_0-d+1, x_0+d+1, y_1, z_0+d+1,		red, green, blue, alpha, matrixStackIn, combinedLight);
		drawQube(builder, x_0-d+1, y_0, z_1-d,   x_0+d+1, y_1, z_1+d,		red, green, blue, alpha, matrixStackIn, combinedLight);
		drawQube(builder, x_1-d,   y_0, z_0-d+1, x_1+d,   y_1, z_0+d+1,		red, green, blue, alpha, matrixStackIn, combinedLight);
		drawQube(builder, x_1-d,   y_0, z_1-d,   x_1+d,   y_1, z_1+d,		red, green, blue, alpha, matrixStackIn, combinedLight);
		
		drawQube(builder, x_0-d+1, y_1-d,        z_0+1, x_0+d+1, y_1+d,        z_1,	red, green, blue, alpha, matrixStackIn, combinedLight);
		drawQube(builder, x_1-d,   y_1-d,        z_0+1, x_1+d,   y_1+d,        z_1, red, green, blue, alpha, matrixStackIn, combinedLight);
		drawQube(builder, x_0-d+1, y_0-d, z_0+1, x_0+d+1, y_0+d, z_1, red, green, blue, alpha, matrixStackIn, combinedLight);
		drawQube(builder, x_1-d,   y_0-d, z_0+1, x_1+d,   y_0+d, z_1, red, green, blue, alpha, matrixStackIn, combinedLight);
		
		drawQube(builder, x_0+1, y_1-d,        z_0-d+1, x_1, y_1+d,        z_0+d+1,	red, green, blue, alpha, matrixStackIn, combinedLight);
		drawQube(builder, x_0+1, y_1-d,        z_1-d,   x_1, y_1+d,        z_1+d,	red, green, blue, alpha, matrixStackIn, combinedLight);
		drawQube(builder, x_0+1, y_0-d, z_0-d+1, x_1, y_0+d, z_0+d+1,	red, green, blue, alpha, matrixStackIn, combinedLight);
		drawQube(builder, x_0+1, y_0-d, z_1-d,   x_1, y_0+d, z_1+d,	red, green, blue, alpha, matrixStackIn, combinedLight);
	}
	
	public static void drawQube(VertexConsumer builder, float x1, float y1, float z1, float x2, float y2, float z2, float red, float green, float blue, float alpha, PoseStack matrixStack, int lightMap)
	{
		Matrix4f matrix = matrixStack.last().pose();
		
		builder.vertex(matrix, x1, y2, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x2, y2, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x2, y1, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x1, y1, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		
		builder.vertex(matrix, x2, y2, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x1, y2, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x1, y1, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x2, y1, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		
		builder.vertex(matrix, x1, y2, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x1, y2, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x1, y1, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x1, y1, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		
		builder.vertex(matrix, x2, y2, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x2, y2, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x2, y1, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x2, y1, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		
		builder.vertex(matrix, x1, y2, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x2, y2, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x2, y2, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x1, y2, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		
		builder.vertex(matrix, x2, y1, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x1, y1, z2).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x1, y1, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
		builder.vertex(matrix, x2, y1, z1).color(red, green, blue, alpha).uv2(lightMap).endVertex();
	}


}
