package futurepack.client.render.block;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Matrix4f;
import com.mojang.math.Vector3f;

import futurepack.common.block.modification.BlockEntityLaserBase;
import futurepack.common.block.modification.TileEntityLaserBase;
import futurepack.common.block.modification.TileEntityRocketLauncher;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.phys.Vec3;

public class RenderLaserBase implements BlockEntityRenderer<TileEntityLaserBase>
{

	private ModelLaserBase eater;
	private ModelLaserBase rocket;

	public RenderLaserBase(BlockEntityRendererProvider.Context rendererDispatcherIn) 
	{
//		super(rendererDispatcherIn);
		eater = new ModelEater(rendererDispatcherIn.bakeLayer(ModelEater.LAYER_LOCATION));
		
		rocket = new ModelRocketLauncher(rendererDispatcherIn.bakeLayer(ModelRocketLauncher.LAYER_LOCATION));
	}

	
	@Override
	public void render(TileEntityLaserBase tile, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int combinedLightIn, int combinedOverlayIn) 
	{
		tile.getLevel().getProfiler().push("renderLaserBase");
		
		ModelLaserBase eater = this.eater;
		if(tile.getClass()==TileEntityRocketLauncher.class)
			eater = rocket;
		
		matrixStackIn.pushPose();
		Vec3 src = tile.blockPos;
		matrixStackIn.translate(0.5, 0.375, 0.5);

		
		float rotX = 0;
		float rotY = 0;
		if(src!=null)
		{
			Vec3 base = tile.getVecPos();
			float dx = (float) -(src.x - base.x);
			float dy = (float) -(src.y - base.y);
			float dz = (float) (src.z - base.z);
			float dis = (float) Math.sqrt(dx*dx + dz*dz);
			
			rotY = (float) Math.atan(dx / dz);
			rotX = (float) Math.atan(dy / dis);
			
			if(dz<0)
				rotY-= Math.PI;		
		}
		
		if(tile.pass == 0)
		{	
			matrixStackIn.pushPose();

			if(tile.getBlockState().getValue(BlockEntityLaserBase.ROTATION_VERTICAL) == Direction.DOWN)
			{
				matrixStackIn.translate(0, 0.25, 0);
				eater.rotateXUnit((float) (rotX+Math.PI));
				eater.rotateYUnit(-rotY);
			}
			else
			{
				matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(180F));
				eater.rotateXUnit(rotX);
				eater.rotateYUnit(rotY);
			}
			VertexConsumer buf = bufferIn.getBuffer(eater.renderType(tile.getTexture()));
			eater.renderToBuffer(matrixStackIn, buf, combinedLightIn, combinedOverlayIn, 1F, 1F, 1F, 1F);

			matrixStackIn.popPose();
		}
//		if(true || tile.pass == 1)//we dont have render passes anymore
		{		
			if(tile.getLaser()!=null)
			{
				src = tile.entityPos;
				if(src!=null && tile.lastMessageTime > System.currentTimeMillis() - 333)
				{
					Vec3 base = tile.getVecPos();
					float dx = (float) -(src.x - base.x);
					float dy = (float) -(src.y - base.y);
					float dz = (float) (src.z - base.z);
					double dis = Math.sqrt(dx*dx+dy*dy+dz*dz);
		
					if(tile.getBlockState().getValue(BlockEntityLaserBase.ROTATION_VERTICAL) == Direction.DOWN)
					{
						matrixStackIn.translate(0, -1, 0);
					}
						
					renderBeam((float) dis, rotX, rotY, tile.getLaserColor(), tile.getLaser(), matrixStackIn, bufferIn, combinedOverlayIn);
				}
			}		
		}

		matrixStackIn.popPose();
		tile.getLevel().getProfiler().pop();
	}
	
	
	private void renderBeam(float dis, float rotX, float rotY, int col3i, ResourceLocation laser, PoseStack matrixStack, MultiBufferSource bufferIn, int overlay)
	{
		matrixStack.pushPose();
//		RenderSystem.setShaderTexture(0, laser);
//		
//		RenderSystem.enableBlend();
//		GlStateManager._blendFuncSeparate(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA, GL11.GL_ONE, GL11.GL_ZERO);
//		RenderSystem.enableDepthTest();
//        GL11.glDepthMask(true);
//		GL11.glDisable(GL11.GL_LIGHTING);
//		GL11.glEnable(GL11.GL_ALPHA_TEST);
		
		matrixStack.translate(0, 0.625+0.0625, 0);
		matrixStack.mulPose(Vector3f.YP.rotation(-rotY));
		matrixStack.mulPose(Vector3f.XP.rotation(rotX));
		
		float r = ((col3i>>16) & 0xFF) /255F;
		float g = ((col3i>>8) & 0xFF) /255F;
		float b = ((col3i>>0) & 0xFF) /255F;

		float width = 0.125F;
		float texStart = (float) (dis / (2*width) - (System.currentTimeMillis()%1000)*0.005);
		float texEnd = (float) (- (System.currentTimeMillis()%1000)*0.005);
		
		VertexConsumer consumer = bufferIn.getBuffer(RenderType.entityTranslucent(laser));

		Matrix4f mat = matrixStack.last().pose();
		
		addVertexWithUV(-width, 0F, 0.5F, 0F, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
		addVertexWithUV(+width, 0F, 0.5F, 1F, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
		addVertexWithUV(+width, 0F, dis, 1F, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);
		addVertexWithUV(-width, 0F, dis, 0F, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);
		
		addVertexWithUV(+width, 0, 0.5F, 0, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
		addVertexWithUV(-width, 0, 0.5F, 1, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
		addVertexWithUV(-width, 0, dis, 1, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);
		addVertexWithUV(+width, 0, dis, 0, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);

		addVertexWithUV(0, +width, 0.5F, 0, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
		addVertexWithUV(0, -width, 0.5F, 1, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
		addVertexWithUV(0, -width, dis, 1, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);
		addVertexWithUV(0, +width, dis, 0, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);

		addVertexWithUV(0, -width, 0.5F, 0, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
		addVertexWithUV(0, +width, 0.5F, 1, texStart, mat, consumer, r, g, b, 0xF000F0, overlay);
		addVertexWithUV(0, +width, dis, 1, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);
		addVertexWithUV(0, -width, dis, 0, texEnd, mat, consumer, r, g, b, 0xF000F0, overlay);
		
//		matrixStack.mulPose(Vector3f.XP.rotationDegrees((float) -Math.toDegrees(rotX)));
//		matrixStack.mulPose(Vector3f.YP.rotationDegrees((float) Math.toDegrees(rotY)));
//		matrixStack.translate(0, -0.625, 0);
//		GL11.glDepthMask(true);
//		RenderSystem.disableBlend();
//		GL11.glEnable(GL11.GL_LIGHTING);
		
//		RenderSystem.setShaderColor(1, 1, 1, 1);//Tessellator.getInstance().getVertexBuffer().addVertexWithUV(p_178985_1_, p_178985_3_, p_178985_5_, p_178985_7_, p_178985_9_);
		matrixStack.popPose();
	}
	
	private static void addVertexWithUV(float x, float y, float z, float u, float v, Matrix4f mat, VertexConsumer consumer, float r, float g, float b, int lightMap, int overlayMap)
	{
		//("Position", ELEMENT_POSITION).put("Color", ELEMENT_COLOR).put("UV0", ELEMENT_UV0).put("UV1", ELEMENT_UV1).put("UV2", ELEMENT_UV2).put("Normal", ELEMENT_NORMAL).put("Padding", ELEMENT_PADDING).build());
		consumer.vertex(mat, x, y, z).color(r, g, b, 1).uv(u, v).overlayCoords(overlayMap).uv2(lightMap).normal(0, 1, 0).endVertex();
	}
}
