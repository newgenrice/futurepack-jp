package futurepack.client.render.entity;

import futurepack.api.Constants;
import futurepack.common.entity.living.EntityEvilRobot;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.HumanoidMobRenderer;
import net.minecraft.client.renderer.entity.layers.HumanoidArmorLayer;
import net.minecraft.client.renderer.entity.layers.ItemInHandLayer;
import net.minecraft.resources.ResourceLocation;

public class RenderEvilRobot extends HumanoidMobRenderer<EntityEvilRobot, ModelEvilRobot>
{
	private static final ResourceLocation TEX = new ResourceLocation(Constants.MOD_ID, "textures/entity/bot.png");

	public RenderEvilRobot(EntityRendererProvider.Context renderManagerIn)
	{
		super(renderManagerIn, new ModelEvilRobot(renderManagerIn.bakeLayer(ModelEvilRobot.LAYER_LOCATION)), 0.5F);
		this.addLayer(new ItemInHandLayer<EntityEvilRobot, ModelEvilRobot>(this));
        this.addLayer(new HumanoidArmorLayer<EntityEvilRobot, ModelEvilRobot, ModelEvilRobot>(this, 
        		new ModelEvilRobot(renderManagerIn.bakeLayer(ModelEvilRobot.LAYER_LOCATION_ARMOR)),
        		new ModelEvilRobot(renderManagerIn.bakeLayer(ModelEvilRobot.LAYER_LOCATION_ARMOR))));//new ModelEvilRobot(0.5F, true), new ModelEvilRobot(1.0F, true)));		
		this.addLayer(new LayerEvilRobot(this, new ModelEvilRobot(renderManagerIn.bakeLayer(ModelEvilRobot.LAYER_LOCATION_ARMOR))));
	}
	
	
//	@Override
//	public void transformHeldFull3DItemLayer()
//    {
//	       GlStateManager.translatef(0.09375F, 0.1875F, 0.0F);
//    }
	
	
	
	@Override
	public ResourceLocation getTextureLocation(EntityEvilRobot entity)
	{
		return TEX;
	}

}
