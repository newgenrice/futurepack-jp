package futurepack.client.render.entity;

import com.google.common.collect.ImmutableList;

import futurepack.api.Constants;
import futurepack.common.entity.EntityForstmaster;
import futurepack.common.entity.EntityForstmaster.EnumState;
import net.minecraft.client.model.ListModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.resources.ResourceLocation;

/**
 * Forstmaster - Mantes
 * Created using Tabula 5.1.0
 */
public class ModelForstmaster extends ListModel<EntityForstmaster>
{
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(Constants.MOD_ID, "forstmaster"), "main");
	private final ModelPart drone;
	
	private final ModelPart sawblade_1;
	private final ModelPart sawblade_2;

	public ModelForstmaster(ModelPart root) 
	{
		this.drone = root.getChild("drone");
		sawblade_1 = this.drone.getChild("saw_arms").getChild("sawblade_1");
		sawblade_2 = this.drone.getChild("saw_arms").getChild("sawblade_2");
	}

	public static LayerDefinition createBodyLayer() 
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition drone = partdefinition.addOrReplaceChild("drone", CubeListBuilder.create().texOffs(22, 10).addBox(4.0F, -5.0F, -5.0F, 1.0F, 2.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(22, 10).addBox(-5.0F, -5.0F, -5.0F, 1.0F, 2.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(41, 0).addBox(-4.0F, -5.0F, -5.0F, 8.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(52, 4).addBox(-1.0F, -5.0F, 4.0F, 2.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-4.0F, -8.0F, -4.0F, 8.0F, 6.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 2.0F));

		PartDefinition thruster = drone.addOrReplaceChild("thruster", CubeListBuilder.create().texOffs(51, 11).addBox(2.0F, -1.0F, 0.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(51, 11).addBox(-4.0F, -1.0F, 0.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(41, 3).addBox(1.0F, -1.5F, 0.0F, 1.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(41, 3).addBox(4.0F, -1.5F, 0.0F, 1.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(41, 3).addBox(-2.0F, -1.5F, 0.0F, 1.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(41, 3).addBox(-5.0F, -1.5F, 0.0F, 1.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -4.0F, 3.0F));

		PartDefinition saw_arms = drone.addOrReplaceChild("saw_arms", CubeListBuilder.create(), PartPose.offset(0.0F, -4.0F, -4.0F));

		PartDefinition saw_chest_2_r1 = saw_arms.addOrReplaceChild("saw_chest_2_r1", CubeListBuilder.create().texOffs(0, 28).addBox(-5.0F, -1.0F, -3.0F, 5.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, 2.0F, -2.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition saw_chest_1_r1 = saw_arms.addOrReplaceChild("saw_chest_1_r1", CubeListBuilder.create().texOffs(0, 28).addBox(0.0F, -1.0F, -3.0F, 5.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, 2.0F, -2.0F, 0.0F, 0.7854F, 0.0F));

		PartDefinition saw_arm_2_r1 = saw_arms.addOrReplaceChild("saw_arm_2_r1", CubeListBuilder.create().texOffs(29, 21).addBox(0.0F, 0.0F, -1.0F, 1.0F, 6.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(29, 21).addBox(-7.0F, 0.0F, -1.0F, 1.0F, 6.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(3.0F, 0.0F, 0.0F, -1.0828F, 0.0F, 0.0F));

		PartDefinition sawblade_1 = saw_arms.addOrReplaceChild("sawblade_1", CubeListBuilder.create().texOffs(47, 17).addBox(-1.5F, -0.6F, -1.5F, 3.0F, 1.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(-3.5F, 3.0F, -7.0F));

		PartDefinition Sageblatt2_r1 = sawblade_1.addOrReplaceChild("Sageblatt2_r1", CubeListBuilder.create().texOffs(47, 17).addBox(-1.5F, -1.0F, -1.5F, 3.0F, 1.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition sawblade_2 = saw_arms.addOrReplaceChild("sawblade_2", CubeListBuilder.create().texOffs(47, 17).addBox(-1.5F, -0.6F, -1.5F, 3.0F, 1.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(3.5F, 3.0F, -7.0F));

		PartDefinition Sageblatt4_r1 = sawblade_2.addOrReplaceChild("Sageblatt4_r1", CubeListBuilder.create().texOffs(47, 17).addBox(-1.5F, -1.0F, -1.5F, 3.0F, 1.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition head = drone.addOrReplaceChild("head", CubeListBuilder.create().texOffs(25, 0).addBox(-2.0F, -1.0F, -4.0F, 4.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -5.0F, -4.0F));

		PartDefinition trapdoor_1 = drone.addOrReplaceChild("trapdoor_1", CubeListBuilder.create().texOffs(0, 17).addBox(-3.0F, -0.5F, -3.0F, 3.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offset(3.0F, -2.0F, 0.0F));

		PartDefinition trapdoor_2 = drone.addOrReplaceChild("trapdoor_2", CubeListBuilder.create().texOffs(0, 17).addBox(0.0F, -0.5F, 0.0F, 3.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offset(-3.0F, -2.0F, -3.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}
/*	
    public ModelPart Ramen1;
    public ModelPart Ramen2;
    public ModelPart Ramen3;
    public ModelPart Ramen6;
    public ModelPart Sagearm2;
    public ModelPart Sagearm1;
    public ModelPart Triebwerk2;
    public ModelPart Triebwerk1;
    public ModelPart Ramen52;
    public ModelPart Ramen5;
    public ModelPart Ramen4;
    public ModelPart Ramen42;
    public ModelPart Sagekasten134;
    public ModelPart Sagekasten212;
    public ModelPart Kopf;
    public ModelPart Sezlingsbox;
    public ModelPart Luke1;
    public ModelPart Luke2;
    public ModelPart Sageblatt1;
    public ModelPart Sageblatt2;
    public ModelPart Sageblatt3;
    public ModelPart Sageblatt4;

	private ImmutableList<ModelPart> parts;
	
    public ModelForstmaster()
    {
        this.texWidth = 128;
        this.texHeight = 128;
        this.Sagekasten134 = new ModelPart(this, 0, 28);
        this.Sagekasten134.setPos(-2.0F, 8.0F, -6.0F);
        this.Sagekasten134.addBox(-5.0F, -1.0F, -3.0F, 5, 3, 3, 0.0F);
        this.setRotateAngle(Sagekasten134, 0.0F, -0.7853981852531433F, 0.0F);
        this.Ramen2 = new ModelPart(this, 22, 10);
        this.Ramen2.setPos(0.0F, 0.0F, 0.0F);
        this.Ramen2.addBox(4.0F, 5.0F, -5.0F, 1, 2, 8, 0.0F);
        this.setRotateAngle(Ramen2, 5.890622339648657E-16F, 3.4103603158333183E-16F, -4.650491460089887E-16F);
        this.Ramen4 = new ModelPart(this, 41, 3);
        this.Ramen4.setPos(0.0F, 0.0F, 0.0F);
        this.Ramen4.addBox(1.0F, 4.5F, 3.0F, 1, 3, 3, 0.0F);
        this.Sageblatt3 = new ModelPart(this, 47, 17);
        this.Sageblatt3.setPos(-3.5F, 9.0F, -11.0F);
        this.Sageblatt3.addBox(-1.5F, -0.6000000238418579F, -1.5F, 3, 1, 3, 0.0F);
        this.Sagearm2 = new ModelPart(this, 29, 21);
        this.Sagearm2.setPos(3.0F, 6.0F, -4.0F);
        this.Sagearm2.addBox(0.0F, 0.0F, -1.0F, 1, 6, 2, 0.0F);
        this.setRotateAngle(Sagearm2, -1.082827091217041F, 2.480262023815341E-16F, 0.0F);
        this.Ramen5 = new ModelPart(this, 41, 3);
        this.Ramen5.setPos(0.0F, 0.0F, 0.0F);
        this.Ramen5.addBox(-2.0F, 4.5F, 3.0F, 1, 3, 3, 0.0F);
        this.Luke1 = new ModelPart(this, 0, 17);
        this.Luke1.setPos(-3.0F, 7.5F, 0.0F);
        this.Luke1.addBox(0.0F, 0.0F, -3.0F, 3, 1, 6, 0.0F);
        this.Ramen6 = new ModelPart(this, 52, 4);
        this.Ramen6.setPos(0.0F, 0.0F, 0.0F);
        this.Ramen6.addBox(-1.0F, 5.0F, 4.0F, 2, 2, 1, 0.0F);
        this.Ramen42 = new ModelPart(this, 41, 3);
        this.Ramen42.setPos(0.0F, 0.0F, 0.0F);
        this.Ramen42.addBox(4.0F, 4.5F, 3.0F, 1, 3, 3, 0.0F);
        this.Sageblatt2 = new ModelPart(this, 47, 17);
        this.Sageblatt2.setPos(3.5F, 8.0F, -11.0F);
        this.Sageblatt2.addBox(-1.5F, 0.0F, -1.5F, 3, 1, 3, 0.0F);
        this.setRotateAngle(Sageblatt2, 0.0F, 0.7853981852531433F, 0.0F);
        this.Ramen52 = new ModelPart(this, 41, 3);
        this.Ramen52.setPos(0.0F, 0.0F, 0.0F);
        this.Ramen52.addBox(-5.0F, 4.5F, 3.0F, 1, 3, 3, 0.0F);
        this.Sagekasten212 = new ModelPart(this, 0, 28);
        this.Sagekasten212.setPos(2.0F, 8.0F, -6.0F);
        this.Sagekasten212.addBox(0.0F, -1.0F, -3.0F, 5, 3, 3, 0.0F);
        this.setRotateAngle(Sagekasten212, 0.0F, 0.7853981852531433F, 0.0F);
        this.Sagearm1 = new ModelPart(this, 29, 21);
        this.Sagearm1.setPos(-3.0F, 6.0F, -4.0F);
        this.Sagearm1.addBox(-1.0F, 0.0F, -1.0F, 1, 6, 2, 0.0F);
        this.setRotateAngle(Sagearm1, -1.082827091217041F, 2.480262023815341E-16F, 0.0F);
        this.Kopf = new ModelPart(this, 25, 0);
        this.Kopf.setPos(0.0F, 0.0F, 0.0F);
        this.Kopf.addBox(-2.0F, 4.0F, -8.0F, 4, 4, 4, 0.0F);
        this.Ramen1 = new ModelPart(this, 22, 10);
        this.Ramen1.setPos(0.0F, 0.0F, 0.0F);
        this.Ramen1.addBox(-5.0F, 5.0F, -5.0F, 1, 2, 8, 0.0F);
        this.Luke2 = new ModelPart(this, 0, 17);
        this.Luke2.setPos(3.0F, 7.5F, -3.0F);
        this.Luke2.addBox(-3.0F, 0.0F, 0.0F, 3, 1, 6, 0.0F);
        this.Triebwerk2 = new ModelPart(this, 51, 11);
        this.Triebwerk2.setPos(-3.0F, 6.0F, 4.0F);
        this.Triebwerk2.addBox(-1.0F, -1.0F, -1.0F, 2, 2, 2, 0.0F);
        this.setRotateAngle(Triebwerk2, 0.7853981852531433F, 4.960524047630682E-16F, -3.72039316807191E-16F);
        this.Triebwerk1 = new ModelPart(this, 51, 11);
        this.Triebwerk1.setPos(3.0F, 6.0F, 4.0F);
        this.Triebwerk1.addBox(-1.0F, -1.0F, -1.0F, 2, 2, 2, 0.0F);
        this.setRotateAngle(Triebwerk1, 0.7853981852531433F, 4.960524047630682E-16F, -3.72039316807191E-16F);
        this.Sezlingsbox = new ModelPart(this, 0, 0);
        this.Sezlingsbox.setPos(0.0F, 2.0F, 0.0F);
        this.Sezlingsbox.addBox(-4.0F, 0.0F, -4.0F, 8, 6, 8, 0.0F);
        this.Ramen3 = new ModelPart(this, 41, 0);
        this.Ramen3.setPos(0.0F, 0.0F, 0.0F);
        this.Ramen3.addBox(-4.0F, 5.0F, -5.0F, 8, 2, 1, 0.0F);
        this.Sageblatt4 = new ModelPart(this, 47, 17);
        this.Sageblatt4.setPos(-3.5F, 8.0F, -11.0F);
        this.Sageblatt4.addBox(-1.5F, 0.0F, -1.5F, 3, 1, 3, 0.0F);
        this.setRotateAngle(Sageblatt4, 0.0F, 0.7853981852531433F, 0.0F);
        this.Sageblatt1 = new ModelPart(this, 47, 17);
        this.Sageblatt1.setPos(3.5F, 9.0F, -11.0F);
        this.Sageblatt1.addBox(-1.5F, -0.6000000238418579F, -1.5F, 3, 1, 3, 0.0F);
        
        Builder<ModelPart> builder = ImmutableList.builder();
        builder.add(Ramen1);
        builder.add(Ramen2);
        builder.add(Ramen3);
        builder.add(Ramen6);
        builder.add(Sagearm2);
        builder.add(Sagearm1);
        builder.add(Triebwerk2);
        builder.add(Triebwerk1);
        builder.add(Ramen52);
        builder.add(Ramen5);
        builder.add(Ramen4);
        builder.add(Ramen42);
        builder.add(Sagekasten134);
        builder.add(Sagekasten212);
        builder.add(Kopf);
        builder.add(Sezlingsbox);
        builder.add(Luke1);
        builder.add(Luke2);
        builder.add(Sageblatt1);
        builder.add(Sageblatt2);
        builder.add(Sageblatt3);
        builder.add(Sageblatt4);
        this.parts = builder.build();
    }

    public void setRotateAngle(ModelPart modelRenderer, float x, float y, float z)
    {
        modelRenderer.xRot = x;
        modelRenderer.yRot = y;
        modelRenderer.zRot = z;
    }*/


	@Override
	public Iterable<ModelPart> parts() 
	{
		return ImmutableList.of(this.drone);
	}


	@Override
	public void setupAnim(EntityForstmaster entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) 
	{		
		if(entityIn.getState()==EnumState.FELLING)//Spinn
		{
			float rot = (float) (Math.PI * (System.currentTimeMillis()%1000)/150);
			sawblade_1.yRot=rot;
			sawblade_2.yRot=rot;
		}
	}
}
