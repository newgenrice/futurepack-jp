package futurepack.client.render.entity;

import java.util.Collections;

import com.google.common.collect.ImmutableList;

import futurepack.api.Constants;
import futurepack.common.entity.living.EntityCrawler;
import net.minecraft.client.model.AgeableListModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;

public class ModelCrawler extends AgeableListModel<EntityCrawler>
{
	// This layer location should be baked with EntityRendererProvider.Context in
	// the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(Constants.MOD_ID, "crawler"), "main");
	private final ModelPart root;

	
	// fields
	ModelPart Head;
//	ModelPart Body;
//	ModelPart RearEnd;
	ModelPart Leg8;
	ModelPart Leg6;
	ModelPart Leg4;
	ModelPart Leg2;
	ModelPart Leg7;
	ModelPart Leg5;
	ModelPart Leg3;
	ModelPart Leg1;
	ModelPart Fang1;
	ModelPart Fang2;
//	ModelPart Shape1;

	
	public ModelCrawler(ModelPart root) 
	{
		this.root = root.getChild("root");
		
		Head = this.root.getChild("Head");
		Leg1 = this.root.getChild("Leg1");
		Leg2 = this.root.getChild("Leg2");
		Leg3 = this.root.getChild("Leg3");
		Leg4 = this.root.getChild("Leg4");
		Leg5 = this.root.getChild("Leg5");
		Leg6 = this.root.getChild("Leg6");
		Leg7 = this.root.getChild("Leg7");
		Leg8 = this.root.getChild("Leg8");
		Fang1 = this.root.getChild("Fang1");
		Fang2 = this.root.getChild("Fang2");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition root = partdefinition.addOrReplaceChild("root", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition Head = root.addOrReplaceChild("Head", CubeListBuilder.create().texOffs(32, 4).addBox(-4.0F, -4.0F, -8.0F, 8.0F, 8.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -4.0F, -3.0F));

		PartDefinition Body = root.addOrReplaceChild("Body", CubeListBuilder.create().texOffs(0, 0).addBox(-3.0F, -3.0F, -3.0F, 6.0F, 6.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -4.0F, 0.0F));

		PartDefinition RearEnd = root.addOrReplaceChild("RearEnd", CubeListBuilder.create().texOffs(0, 12).addBox(-5.0F, -4.0F, -6.0F, 10.0F, 8.0F, 12.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -4.0F, 9.0F));

		PartDefinition Leg8 = root.addOrReplaceChild("Leg8", CubeListBuilder.create().texOffs(18, 0).addBox(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-4.0F, -4.0F, -1.0F, 0.0F, -0.576F, 0.192F));

		PartDefinition Leg6 = root.addOrReplaceChild("Leg6", CubeListBuilder.create().texOffs(18, 0).addBox(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-4.0F, -4.0F, 0.0F, 0.0F, -0.2793F, 0.192F));

		PartDefinition Leg4 = root.addOrReplaceChild("Leg4", CubeListBuilder.create().texOffs(18, 0).addBox(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-4.0F, -4.0F, 1.0F, 0.0F, 0.2793F, 0.192F));

		PartDefinition Leg2 = root.addOrReplaceChild("Leg2", CubeListBuilder.create().texOffs(18, 0).addBox(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-4.0F, -4.0F, 2.0F, 0.0F, 0.576F, 0.192F));

		PartDefinition Leg7 = root.addOrReplaceChild("Leg7", CubeListBuilder.create().texOffs(18, 0).addBox(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -4.0F, -1.0F, 0.0F, 0.576F, -0.192F));

		PartDefinition Leg5 = root.addOrReplaceChild("Leg5", CubeListBuilder.create().texOffs(18, 0).addBox(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -4.0F, 0.0F, 0.0F, 0.2793F, -0.192F));

		PartDefinition Leg3 = root.addOrReplaceChild("Leg3", CubeListBuilder.create().texOffs(18, 0).addBox(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -4.0F, 1.0F, 0.0F, -0.2793F, -0.192F));

		PartDefinition Leg1 = root.addOrReplaceChild("Leg1", CubeListBuilder.create().texOffs(18, 0).addBox(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -4.0F, 2.0F, 0.0F, -0.576F, -0.192F));

		PartDefinition Fang1 = root.addOrReplaceChild("Fang1", CubeListBuilder.create().texOffs(18, 0).addBox(-16.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -6.0F, -2.0F, 0.0F, 1.5708F, -0.6981F));

		PartDefinition Fang2 = root.addOrReplaceChild("Fang2", CubeListBuilder.create().texOffs(18, 0).addBox(-16.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -6.0F, -2.0F, 0.0F, 1.5708F, -0.6981F));

		PartDefinition Shape1 = root.addOrReplaceChild("Shape1", CubeListBuilder.create().texOffs(0, 32).addBox(-6.0F, 0.0F, 0.0F, 12.0F, 6.0F, 11.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -10.0F, 3.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

/*	ImmutableList<ModelPart> body;

	public ModelCrawler()
	{
		texWidth = 128;
		texHeight = 128;

		Head = new ModelPart(this, 32, 4);
		Head.addBox(-4F, -4F, -8F, 8, 8, 8);
		Head.setPos(0F, 20F, -3F);
		Head.setTexSize(128, 128);
		Head.mirror = true;
		setRotation(Head, 0F, 0F, 0F);
		Body = new ModelPart(this, 0, 0);
		Body.addBox(-3F, -3F, -3F, 6, 6, 6);
		Body.setPos(0F, 20F, 0F);
		Body.setTexSize(128, 128);
		Body.mirror = true;
		setRotation(Body, 0F, 0F, 0F);
		RearEnd = new ModelPart(this, 0, 12);
		RearEnd.addBox(-5F, -4F, -6F, 10, 8, 12);
		RearEnd.setPos(0F, 20F, 9F);
		RearEnd.setTexSize(128, 128);
		RearEnd.mirror = true;
		setRotation(RearEnd, 0F, 0F, 0F);
		Leg8 = new ModelPart(this, 18, 0);
		Leg8.addBox(-1F, -1F, -1F, 16, 2, 2);
		Leg8.setPos(4F, 20F, -1F);
		Leg8.setTexSize(128, 128);
		Leg8.mirror = true;
		setRotation(Leg8, 0F, 0.5759587F, 0.1919862F);
		Leg6 = new ModelPart(this, 18, 0);
		Leg6.addBox(-1F, -1F, -1F, 16, 2, 2);
		Leg6.setPos(4F, 20F, 0F);
		Leg6.setTexSize(128, 128);
		Leg6.mirror = true;
		setRotation(Leg6, 0F, 0.2792527F, 0.1919862F);
		Leg4 = new ModelPart(this, 18, 0);
		Leg4.addBox(-1F, -1F, -1F, 16, 2, 2);
		Leg4.setPos(4F, 20F, 1F);
		Leg4.setTexSize(128, 128);
		Leg4.mirror = true;
		setRotation(Leg4, 0F, -0.2792527F, 0.1919862F);
		Leg2 = new ModelPart(this, 18, 0);
		Leg2.addBox(-1F, -1F, -1F, 16, 2, 2);
		Leg2.setPos(4F, 20F, 2F);
		Leg2.setTexSize(128, 128);
		Leg2.mirror = true;
		setRotation(Leg2, 0F, -0.5759587F, 0.1919862F);
		Leg7 = new ModelPart(this, 18, 0);
		Leg7.addBox(-15F, -1F, -1F, 16, 2, 2);
		Leg7.setPos(-4F, 20F, -1F);
		Leg7.setTexSize(128, 128);
		Leg7.mirror = true;
		setRotation(Leg7, 0F, -0.5759587F, -0.1919862F);
		Leg5 = new ModelPart(this, 18, 0);
		Leg5.addBox(-15F, -1F, -1F, 16, 2, 2);
		Leg5.setPos(-4F, 20F, 0F);
		Leg5.setTexSize(128, 128);
		Leg5.mirror = true;
		setRotation(Leg5, 0F, -0.2792527F, -0.1919862F);
		Leg3 = new ModelPart(this, 18, 0);
		Leg3.addBox(-15F, -1F, -1F, 16, 2, 2);
		Leg3.setPos(-4F, 20F, 1F);
		Leg3.setTexSize(128, 128);
		Leg3.mirror = true;
		setRotation(Leg3, 0F, 0.2792527F, -0.1919862F);
		Leg1 = new ModelPart(this, 18, 0);
		Leg1.addBox(-15F, -1F, -1F, 16, 2, 2);
		Leg1.setPos(-4F, 20F, 2F);
		Leg1.setTexSize(128, 128);
		Leg1.mirror = true;
		setRotation(Leg1, 0F, 0.5759587F, -0.1919862F);
		Fang1 = new ModelPart(this, 18, 0);
		Fang1.addBox(0F, -1F, -1F, 16, 2, 2);
		Fang1.setPos(2F, 18F, -2F);
		Fang1.setTexSize(128, 128);
		Fang1.mirror = true;
		setRotation(Fang1, 1.570796F, -0.7981317F, -1.570796F);
		Fang2 = new ModelPart(this, 18, 0);
		Fang2.addBox(0F, -1F, -1F, 16, 2, 2);
		Fang2.setPos(-2F, 18F, -2F);
		Fang2.setTexSize(128, 128);
		Fang2.mirror = true;
		setRotation(Fang2, 1.570796F, -0.7981317F, -1.570796F);
		Shape1 = new ModelPart(this, 0, 32);
		Shape1.addBox(-6F, 0F, 0F, 12, 6, 11);
		Shape1.setPos(0F, 14F, 2.9F);
		Shape1.setTexSize(128, 128);
		Shape1.mirror = true;
		setRotation(Shape1, 0F, 0F, 0F);

		Builder<ModelPart> builder = ImmutableList.builder();
		builder.add(Body);
		builder.add(RearEnd);
		builder.add(Leg1);
		builder.add(Leg2);
		builder.add(Leg3);
		builder.add(Leg4);
		builder.add(Leg5);
		builder.add(Leg6);
		builder.add(Leg7);
		builder.add(Leg8);
		builder.add(Fang1);
		builder.add(Fang2);
		builder.add(Shape1);
		builder.add(Head);
		this.body = builder.build();
	}*/
	/*
	 * FIXME: Check if this is still nessessary
	 * 
	 * @Override public void render(EntityCrawler entity, float f, float f1, float
	 * f2, float f3, float f4, float f5) { if(entity instanceof MobEntity) {
	 * if(((MobEntity) entity).isChild()) { f5 *= 0.25; } }
	 * 
	 * super.render(entity, f, f1, f2, f3, f4, f5); setRotationAngles(entity, f, f1,
	 * f2, f3, f4, f5); Head.render(f5); Body.render(f5); RearEnd.render(f5);
	 * Leg8.render(f5); Leg6.render(f5); Leg4.render(f5); Leg2.render(f5);
	 * Leg7.render(f5); Leg5.render(f5); Leg3.render(f5); Leg1.render(f5);
	 * Fang1.render(f5); Fang2.render(f5); Shape1.render(f5); }
	 */

	private void setRotation(ModelPart model, float x, float y, float z)
	{
		model.xRot = x;
		model.yRot = y;
		model.zRot = z;
	}

	@Override
	public void setupAnim(EntityCrawler e, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		this.Head.yRot = netHeadYaw * 0.017453292F;
		this.Head.xRot = headPitch * 0.017453292F;
		float f = ((float) Math.PI / 4F);
		float f_legsUp = 0.15F;
		this.Leg1.zRot = f - f_legsUp;
		this.Leg2.zRot = -f+ f_legsUp;
		this.Leg3.zRot = f * 0.74F- f_legsUp;
		this.Leg4.zRot = -f * 0.74F+ f_legsUp;
		this.Leg5.zRot = f * 0.74F- f_legsUp;
		this.Leg6.zRot = -f * 0.74F+ f_legsUp;
		this.Leg7.zRot = f- f_legsUp;
		this.Leg8.zRot = -f+ f_legsUp;
		float f1 = -0.0F;
		float f2 = -0.3926991F;
		this.Leg1.yRot = f2 * 2.0F + f1;
		this.Leg2.yRot = -f2 * 2.0F - f1;
		this.Leg3.yRot = f2 * 1.0F + f1;
		this.Leg4.yRot = -f2 * 1.0F - f1;
		this.Leg5.yRot = -f2 * 1.0F + f1;
		this.Leg6.yRot = f2 * 1.0F - f1;
		this.Leg7.yRot = -f2 * 2.0F + f1;
		this.Leg8.yRot = f2 * 2.0F - f1;
		float f3 = -(Mth.cos(limbSwing * 0.6662F * 2.0F + 0.0F) * 0.4F) * limbSwingAmount;
		float f4 = -(Mth.cos(limbSwing * 0.6662F * 2.0F + (float) Math.PI) * 0.4F) * limbSwingAmount;
		float f5 = -(Mth.cos(limbSwing * 0.6662F * 2.0F + ((float) Math.PI / 2F)) * 0.4F) * limbSwingAmount;
		float f6 = -(Mth.cos(limbSwing * 0.6662F * 2.0F + ((float) Math.PI * 3F / 2F)) * 0.4F) * limbSwingAmount;
		float f7 = Math.abs(Mth.sin(limbSwing * 0.6662F + 0.0F) * 0.4F) * limbSwingAmount;
		float f8 = Math.abs(Mth.sin(limbSwing * 0.6662F + (float) Math.PI) * 0.4F) * limbSwingAmount;
		float f9 = Math.abs(Mth.sin(limbSwing * 0.6662F + ((float) Math.PI / 2F)) * 0.4F) * limbSwingAmount;
		float f10 = Math.abs(Mth.sin(limbSwing * 0.6662F + ((float) Math.PI * 3F / 2F)) * 0.4F) * limbSwingAmount;
		this.Leg1.yRot += f3;
		this.Leg2.yRot += -f3;
		this.Leg3.yRot += f4;
		this.Leg4.yRot += -f4;
		this.Leg5.yRot += f5;
		this.Leg6.yRot += -f5;
		this.Leg7.yRot += f6;
		this.Leg8.yRot += -f6;
		this.Leg1.zRot -= f7;
		this.Leg2.zRot -= -f7;
		this.Leg3.zRot -= f8;
		this.Leg4.zRot -= -f8;
		this.Leg5.zRot -= f9;
		this.Leg6.zRot -= -f9;
		this.Leg7.zRot -= f10;
		this.Leg8.zRot -= -f10;

		if (e.isHiding())
		{
			setRotation(Fang1, 1.570796F, +0.2981317F, (float) Math.PI * +0.33F);
			setRotation(Fang2, 1.570796F, +0.2981317F, (float) Math.PI * +0.66F);
		} else
		{
			setRotation(Fang1, 1.570796F, +0.7981317F, +1.570796F);
			setRotation(Fang2, 1.570796F, +0.7981317F, +1.570796F);
		}

	}

	@Override
	protected Iterable<ModelPart> headParts()
	{
		return Collections.EMPTY_LIST;
	}

	@Override
	protected Iterable<ModelPart> bodyParts()
	{
		return ImmutableList.of(root);
	}
}
