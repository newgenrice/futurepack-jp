package futurepack.client.render.entity;

import com.google.common.collect.ImmutableList;

import futurepack.api.Constants;
import futurepack.common.entity.throwable.EntityRocket;
import net.minecraft.client.model.ListModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.resources.ResourceLocation;

public class ModelRocket extends ListModel<EntityRocket>
{
	// This layer location should be baked with EntityRendererProvider.Context in
	// the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(Constants.MOD_ID, "rocket"), "main");
	private final ModelPart root;

	public ModelRocket(ModelPart root)
	{
		this.root = root.getChild("root");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition root = partdefinition.addOrReplaceChild("root", CubeListBuilder.create(), PartPose.offset(0.0F, 21.0F, 0.0F));

		PartDefinition Finne1 = root.addOrReplaceChild("Finne1", CubeListBuilder.create().texOffs(0, 1).addBox(-2.5F, -0.5F, 7.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, -5.0F));

		PartDefinition Reketenkorpus = root.addOrReplaceChild("Reketenkorpus", CubeListBuilder.create().texOffs(0, 0).addBox(-1.5F, -1.5F, 0.0F, 3.0F, 3.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, -5.0F));

		PartDefinition Shape1 = root.addOrReplaceChild("Shape1", CubeListBuilder.create().texOffs(17, 1).addBox(-0.5F, -0.5F, -2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, -5.0F));

		PartDefinition Spitze2 = root.addOrReplaceChild("Spitze2", CubeListBuilder.create().texOffs(0, 5).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, -5.0F));

		PartDefinition Finne4 = root.addOrReplaceChild("Finne4", CubeListBuilder.create().texOffs(0, 1).addBox(-0.5F, 1.5F, 7.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, -5.0F));

		PartDefinition Finne2 = root.addOrReplaceChild("Finne2", CubeListBuilder.create().texOffs(0, 1).addBox(-2.5F, -0.5F, 7.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -4.0F, -5.0F, 0.0F, 0.0F, -1.5708F));

		PartDefinition Finne3 = root.addOrReplaceChild("Finne3", CubeListBuilder.create().texOffs(0, 1).addBox(-2.5F, -0.5F, 7.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, -5.0F, 0.0F, 0.0F, -3.1416F));

		return LayerDefinition.create(meshdefinition, 32, 32);
	}

	@Override
	public void setupAnim(EntityRocket e, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		// FIXME super.setRotationAngles(e, limbSwing, limbSwingAmount, ageInTicks,
		// netHeadYaw, headPitch);
	}

	@Override
	public Iterable<ModelPart> parts()
	{
		return ImmutableList.of(root);
	}
}
