package futurepack.client.render.entity;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import futurepack.api.Constants;
import futurepack.common.entity.living.EntityHeuler;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.resources.ResourceLocation;

public class ModelHeuler extends EntityModel<EntityHeuler>
{
	// This layer location should be baked with EntityRendererProvider.Context in
	// the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(Constants.MOD_ID, "heuler"), "main");
	private final ModelPart heuler;

	public ModelHeuler(ModelPart root) 
	{
		this.heuler = root.getChild("heuler");
		
		Fluegel1 = this.heuler.getChild("wing_1");
		Fluegel11 = this.heuler.getChild("wing_1_1");
		Fluegel2 = this.heuler.getChild("wing_2");
		Fluegel21 = this.heuler.getChild("wing_2_1");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition heuler = partdefinition.addOrReplaceChild("heuler", CubeListBuilder.create().texOffs(0, 0).addBox(-1.0F, -3.0F, 0.0F, 2.0F, 2.0F, 3.0F, new CubeDeformation(0.0F)).texOffs(20, 0)
				.addBox(-2.0F, -2.5F, 0.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(20, 0).addBox(1.0F, -2.5F, 0.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition head = heuler.addOrReplaceChild("head", CubeListBuilder.create(), PartPose.offset(0.0F, -2.0F, 0.0F));

		PartDefinition eye_2_r1 = head.addOrReplaceChild("eye_2_r1",
				CubeListBuilder.create().texOffs(10, 6).addBox(0.2F, -0.5F, -2.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(10, 6).addBox(-1.2F, -0.5F, -2.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.3491F, 0.0F, 0.0F));

		PartDefinition head_r1 = head.addOrReplaceChild("head_r1", CubeListBuilder.create().texOffs(0, 6).addBox(-1.0F, -1.0F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.3718F, 0.0F, 0.0F));

		PartDefinition wing_1 = heuler.addOrReplaceChild("wing_1", CubeListBuilder.create().texOffs(18, 6).addBox(-5.0F, 0.0F, 0.0F, 5.0F, 0.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.5F, -2.0F, 0.0F, 0.0F, 0.3491F, 0.3491F));

		PartDefinition wing_1_1 = heuler.addOrReplaceChild("wing_1_1", CubeListBuilder.create().texOffs(18, 6).addBox(-5.0F, 0.0F, 0.0F, 5.0F, 0.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.5F, -2.0F, 0.0F, 0.0F, 0.3491F, -0.3491F));

		PartDefinition wing_2 = heuler.addOrReplaceChild("wing_2", CubeListBuilder.create().texOffs(35, 6).addBox(0.0F, 0.0F, 0.0F, 5.0F, 0.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.5F, -2.0F, 0.0F, 0.0F, -0.3491F, -0.3491F));

		PartDefinition wing_2_1 = heuler.addOrReplaceChild("wing_2_1", CubeListBuilder.create().texOffs(35, 6).addBox(0.0F, 0.0F, 0.0F, 5.0F, 0.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.5F, -2.0F, 0.0F, 0.0F, -0.3491F, 0.3491F));

		PartDefinition back = heuler.addOrReplaceChild("back", CubeListBuilder.create(), PartPose.offset(0.0F, -2.0F, 3.0F));

		PartDefinition back_r1 = back.addOrReplaceChild("back_r1", CubeListBuilder.create().texOffs(28, 0).addBox(-1.0F, -0.5F, 0.0F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.4461F, 0.0F, 0.0F));

		PartDefinition sting = back.addOrReplaceChild("sting", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 2.0F));

		PartDefinition sting_r1 = sting.addOrReplaceChild("sting_r1", CubeListBuilder.create().texOffs(37, 0).addBox(-0.5F, 0.5F, 0.0F, 1.0F, 1.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.8179F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}


	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha)
	{
		heuler.render(poseStack, buffer, packedLight, packedOverlay);
	}

//	public ModelPart Kopf;
//	public ModelPart Auge1;
//	public ModelPart Auge2;
//	public ModelPart Hinterteil1;
//	public ModelPart Seite1;
//	public ModelPart Seite2;
	public ModelPart Fluegel1;
	public ModelPart Fluegel11;
	public ModelPart Fluegel2;
	public ModelPart Fluegel21;
//	public ModelPart Hinterteil3;
//	public ModelPart Hinterteil2;
//
//	private ImmutableList<ModelPart> parts;
//
//	public ModelHeuler()
//	{
//		super(RenderType::entityTranslucentCull);
//
//		this.texWidth = 128;
//		this.texHeight = 128;
//		this.Kopf = new ModelPart(this, 0, 6);
//		this.Kopf.setPos(0.0F, 0.0F, 0.0F);
//		this.Kopf.addBox(-1.0F, -1.0F, -2.0F, 2, 2, 2, 0.0F);
//		this.setRotateAngle(Kopf, 0.37178611755371094F, -0.0F, 0.0F);
//		this.Auge2 = new ModelPart(this, 10, 6);
//		this.Auge2.setPos(0.0F, 0.0F, 0.0F);
//		this.Auge2.addBox(-1.2999999523162842F, -0.5F, -2.5F, 1, 1, 1, 0.0F);
//		this.setRotateAngle(Auge2, 0.34906584024429316F, -0.0F, 0.0F);
//		this.Hinterteil1 = new ModelPart(this, 0, 0);
//		this.Hinterteil1.setPos(0.0F, 0.0F, 0.0F);
//		this.Hinterteil1.addBox(-1.0F, -1.0F, 0.0F, 2, 2, 3, 0.0F);
//		this.Hinterteil3 = new ModelPart(this, 37, 0);
//		this.Hinterteil3.setPos(0.0F, 0.0F, 5.0F);
//		this.Hinterteil3.addBox(-0.5F, 0.5F, 0.0F, 1, 1, 3, 0.0F);
//		this.setRotateAngle(Hinterteil3, -0.8179294466972353F, -0.0F, 0.0F);
//		this.Auge1 = new ModelPart(this, 10, 6);
//		this.Auge1.setPos(0.0F, 0.0F, 0.0F);
//		this.Auge1.addBox(0.30000001192092896F, -0.5F, -2.5F, 1, 1, 1, 0.0F);
//		this.setRotateAngle(Auge1, 0.34906584024429316F, -0.0F, 0.0F);
//		this.Fluegel11 = new ModelPart(this, 18, 6);
//		this.Fluegel11.setPos(1.5F, 0.0F, 0.0F);
//		this.Fluegel11.addBox(0.0F, 0.0F, 0.0F, 5, 0, 3, 0.0F);
//		this.setRotateAngle(Fluegel11, 0.12384802760623698F, -0.3272010074684565F, -0.3695356217171503F);
//		this.Hinterteil2 = new ModelPart(this, 28, 0);
//		this.Hinterteil2.setPos(0.0F, 0.0F, 3.0F);
//		this.Hinterteil2.addBox(-1.0F, -0.5F, 0.0F, 2, 1, 2, 0.0F);
//		this.setRotateAngle(Hinterteil2, -0.4461433291435241F, -0.0F, 0.0F);
//		this.Seite1 = new ModelPart(this, 20, 0);
//		this.Seite1.setPos(0.0F, 0.0F, 0.0F);
//		this.Seite1.addBox(1.0F, -0.5F, 0.0F, 1, 1, 2, 0.0F);
//		this.Fluegel21 = new ModelPart(this, 35, 6);
//		this.Fluegel21.setPos(-1.5F, 0.0F, 0.0F);
//		this.Fluegel21.addBox(-5.0F, 0.0F, 0.0F, 5, 0, 3, 0.0F);
//		this.setRotateAngle(Fluegel21, 0.12384802760623698F, 0.3272010074684565F, 0.3695356217171503F);
//		this.Fluegel2 = new ModelPart(this, 35, 6);
//		this.Fluegel2.setPos(-1.5F, 0.0F, 0.0F);
//		this.Fluegel2.addBox(-5.0F, 0.0F, 0.0F, 5, 0, 3, 0.0F);
//		this.setRotateAngle(Fluegel2, -0.12384802760623698F, 0.3272010074684565F, -0.3695356217171503F);
//		this.Seite2 = new ModelPart(this, 20, 0);
//		this.Seite2.setPos(0.0F, 0.0F, 0.0F);
//		this.Seite2.addBox(-2.0F, -0.5F, 0.0F, 1, 1, 2, 0.0F);
//		this.Fluegel1 = new ModelPart(this, 18, 6);
//		this.Fluegel1.setPos(1.5F, 0.0F, 0.0F);
//		this.Fluegel1.addBox(0.0F, 0.0F, 0.0F, 5, 0, 3, 0.0F);
//		this.setRotateAngle(Fluegel1, -0.12384802760623698F, -0.3272010074684565F, 0.3695356217171503F);
//
//		Builder<ModelPart> builder = ImmutableList.builder();
//		builder.add(Kopf);
//		builder.add(Auge1);
//		builder.add(Auge2);
//		builder.add(Hinterteil1);
//		builder.add(Seite1);
//		builder.add(Seite2);
//		builder.add(Fluegel1);
//		builder.add(Fluegel11);
//		builder.add(Fluegel2);
//		builder.add(Fluegel21);
//		builder.add(Hinterteil3);
//		builder.add(Hinterteil2);
//		this.parts = builder.build();
//	}

	@Override
	public void setupAnim(EntityHeuler entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		// super.setRotationAngles(entityIn, limbSwing, limbSwingAmount, ageInTicks,
		// netHeadYaw, headPitch);

		double f = ageInTicks % Math.PI;

		Fluegel11.zRot = (float) Math.sin(f);
		Fluegel2.zRot = (float) Math.cos(f);
		Fluegel21.zRot = (float) -Math.sin(f);
		Fluegel1.zRot = (float) -Math.cos(f);
	}
}
