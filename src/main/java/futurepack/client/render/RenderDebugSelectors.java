package futurepack.client.render;

import java.awt.Color;
import java.util.ConcurrentModificationException;
import java.util.Map;

import org.apache.commons.lang3.concurrent.ConcurrentException;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.blaze3d.vertex.VertexFormat;

import futurepack.api.interfaces.IBlockSelector;
import futurepack.client.render.block.RenderClaime;
import futurepack.common.FPSelectorHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.client.renderer.debug.DebugRenderer;
import net.minecraft.core.Vec3i;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

public class RenderDebugSelectors
{

	public RenderDebugSelectors()
	{
		
	}
	
	public static void render(float partialTicks, PoseStack matrixStack)
	{
		if(Minecraft.getInstance().options.renderDebug)
		{
			Map<IBlockSelector, FPSelectorHelper> map = FPSelectorHelper.getHelperForWorld(Minecraft.getInstance().level);
			int i=0;
			
			
			Player pl = Minecraft.getInstance().player;
			ClientLevel cl = Minecraft.getInstance().level;
			
			float apartialTicks = 1F -partialTicks;
			double pX,pY,pZ;
			pX = (pl.getX() * partialTicks + apartialTicks * pl.xo);
			pY = (pl.getY() * partialTicks + apartialTicks * pl.yo) + pl.getEyeHeight();
			pZ = (pl.getZ() * partialTicks + apartialTicks * pl.zo);
			
			matrixStack.pushPose();
			matrixStack.translate(-pX, -pY, -pZ);
			
			FuturepackRenderTypes.CLAIME.setupRenderState();
			Tesselator tes = Tesselator.getInstance();
			BufferBuilder builder = tes.getBuilder();
			RenderSystem.setShader(GameRenderer::getPositionColorLightmapShader);
			builder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_COLOR_LIGHTMAP);
			
			try
			{
				Vec3 vec3 = Minecraft.getInstance().gameRenderer.getMainCamera().getPosition();
				double d0 = vec3.x();
				double d1 = vec3.y();
				double d2 = vec3.z();
					
				for(var entries : map.entrySet())
				{
					IBlockSelector sel = entries.getKey();
					FPSelectorHelper helper = entries.getValue();
					i++;
					
					Color c = Color.getHSBColor(i*0.02F, 0.9F, 0.9F);
					
					BoundingBox box = helper.getArea();
					if(box.getLength().distSqr(Vec3i.ZERO) == 0)
						continue;
					RenderClaime.drawWireframeCubes(builder, matrixStack, box.minX(), box.minY(), box.minZ(), box.maxX(), box.maxY(), box.maxZ(), 0.0625F, c.getRed()/255F, c.getGreen()/255F, c.getBlue()/255F, 1F, 0xF000F0 | 15728880);
					
					//DebugRenderer.renderFilledBox(AABB.of(helper.getArea()).move(-d0, -d1, -d2), c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha());
				}
			}
			catch(ConcurrentModificationException e)
			{
				//yeah, ignoring tthat
			}
			
			tes.end();
			
			matrixStack.popPose();
			FuturepackRenderTypes.CLAIME.clearRenderState();
		}
	}

}
