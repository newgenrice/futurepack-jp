package futurepack.world.gen.feature;

import java.util.Random;

import futurepack.common.dim.structures.StructureBase;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;

public class AbstractDungeonFeature extends Feature<DungeonFeatureConfig>
{
	public AbstractDungeonFeature() 
	{
		super(DungeonFeatureConfig.CODEC);
	}
	@Override
	public boolean place(FeaturePlaceContext<DungeonFeatureConfig> c) 
	{
		BlockPos pos = c.origin();
		Random rand = c.random();
		WorldGenLevel w = c.level();
		
		pos = new BlockPos(pos.getX() & 0xFFFFFFF0, pos.getY(), pos.getZ() & 0xFFFFFFF0); //chunk  start
		
		int i = rand.nextInt(c.config().structures.length);
		StructureBase b = c.config().structures[i].base;
		
		b.generateBase(w, pos);
		if(w.getLevel()!=null && w.getLevel() instanceof ServerLevel)
		{
			CompoundTag nbt = new CompoundTag();
			nbt.putBoolean("worlgen", true);
			b.addChestContentBase(w, pos, rand, nbt, (w.getLevel().getServer().getLootTables()));
		}
		return true;
	}

}
