package futurepack.world.gen;

import java.util.Random;
import java.util.stream.Stream;

import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.WorldgenRandom;
import net.minecraft.world.level.levelgen.placement.DecorationContext;
import net.minecraft.world.level.levelgen.placement.FeatureDecorator;

public class GroupPlacementAtSurface extends FeatureDecorator<GroupPlacementConfig>
{

	public GroupPlacementAtSurface(Codec<GroupPlacementConfig> configFactoryIn) 
	{
		super(configFactoryIn);
	}

	@Override
	public Stream<BlockPos> getPositions(DecorationContext helper, Random random, GroupPlacementConfig placementConfig, BlockPos pos) 
	{
		int x = pos.getX() / placementConfig.areaSize;
		int z = pos.getZ() / placementConfig.areaSize;
        Random r = WorldgenRandom.seedSlimeChunk(x, z, helper.level.getSeed(), 324657867L);
        
        if(r.nextFloat() < placementConfig.areaProbability)
        {
        	if(random.nextFloat() < placementConfig.placeProbability)
        	{
	        	int k = random.nextInt(16);
	            int l = random.nextInt(16);
	            return Stream.of(helper.level.getHeightmapPos(Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, pos.offset(k, 0, l)));
        	}
        }
        
		return Stream.empty();
	}

}
