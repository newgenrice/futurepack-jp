package futurepack.world.gen.carver;

import java.util.BitSet;
import java.util.function.Function;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.levelgen.Aquifer;
import net.minecraft.world.level.levelgen.carver.CanyonCarverConfiguration;
import net.minecraft.world.level.levelgen.carver.CarvingContext;
import net.minecraft.world.level.levelgen.carver.WorldCarver;

public class LargeCanyonWorldCaver extends ExtendedCanyonWorldCaver
{
	protected int LAVA_LEVEL = 22;
	protected float scaleYRange = 1.2F;
	protected float scaleplacementYBound = 3.5F;
	protected float scaleplacementXZBound = 1.75F;
	protected boolean closeTop = true;

	protected boolean carveEllipsoid(CarvingContext pContext, CanyonCarverConfiguration pConfig, ChunkAccess pChunk, Function<BlockPos, Biome> pBiomeAccessor, long pSeed, Aquifer pAquifer, double pX, double pY, double pZ, double placementXZBound, double placementYBound, BitSet pCarvingMask, WorldCarver.CarveSkipChecker pSkipChecker) 
	{
		pY *= scaleYRange;
		placementYBound *= scaleplacementYBound;
		placementXZBound *= scaleplacementXZBound;
		return super.carveEllipsoid(pContext, pConfig, pChunk, pBiomeAccessor, pSeed, pAquifer, pX, pY, pZ, placementXZBound, placementYBound, pCarvingMask, pSkipChecker);
	}
		
}
