package futurepack.world.gen.carver;

import java.util.BitSet;
import java.util.Random;
import java.util.function.Function;

import futurepack.world.gen.carver.ExtendedCaveWorldCaver.CarveConfig;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.levelgen.Aquifer;
import net.minecraft.world.level.levelgen.carver.CanyonCarverConfiguration;
import net.minecraft.world.level.levelgen.carver.CanyonWorldCarver;
import net.minecraft.world.level.levelgen.carver.CarvingContext;

public class ExtendedCanyonWorldCaver extends CanyonWorldCarver 
{
	
	public ExtendedCanyonWorldCaver() 
	{
		super(CanyonCarverConfiguration.CODEC);
	}

	public CarveConfig getConfig()
	{
		return ExtendedCaveWorldCaver.DEFAULT;
	}
	
	@Override
	public boolean carve(CarvingContext pContext, CanyonCarverConfiguration pConfig, ChunkAccess pChunk, Function<BlockPos, Biome> pBiomeAccessor, Random pRandom, Aquifer pAquifer, ChunkPos pChunkPos, BitSet pCarvingMask)
	{
		return super.carve(pContext, pConfig, pChunk, pBiomeAccessor, pRandom, pAquifer, pChunkPos, pCarvingMask);
	}
}
