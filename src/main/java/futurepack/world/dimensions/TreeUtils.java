package futurepack.world.dimensions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.base.Predicate;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.common.FPBlockSelector;
import futurepack.common.FPSelectorHelper;
import futurepack.common.block.misc.MiscBlocks;
import futurepack.common.block.misc.TileEntityFallingTree;
import futurepack.depend.api.MiniWorld;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Direction.Axis;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.Tag.Named;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LightLayer;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

public class TreeUtils
{
	public static final Named<Block> LOG_WOOD = BlockTags.LOGS;
	public static final Named<Block> TREE_LEAVES = BlockTags.LEAVES;
	public static final Named<Block> HUGE_MUSHROOM = BlockTags.bind("forge:huge_mushrooms");
	
	

	public static class LogSelector implements IBlockSelector
	{
		Predicate<BlockState> isLog;
		boolean sameY;
		
		public LogSelector(boolean sameY)
		{
			isLog = getLogPredicate();
			this.sameY = sameY;
		}
		
		@Override
		public boolean isValidBlock(Level w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent)
		{
			if(sameY)
			{
				if(pos.getY()!=parent.getY())
					return false;
			}
			BlockState state = w.getBlockState(pos);
			
				
			return isLog.apply(state);
		}

		@Override
		public boolean canContinue(Level w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent)
		{
			return true;
		}
		
	}
	
	public static class LeavesSelector implements IBlockSelector
	{
		private HashMap<BlockPos, Boolean> leaves;
		private Predicate<BlockState> isLeave;
		
		public LeavesSelector(HashMap<BlockPos, Boolean> leaves)
		{
			this.leaves = leaves;
			isLeave = TreeUtils.getLeavesPredicate();
			
		}

		@Override
		public boolean isValidBlock(Level w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent)
		{
			Boolean inte = leaves.get(pos);
			if(inte==null)
				return false;			
			if(!inte)
				return true;
			
			if(!diagonal)
			{
				BlockState state = w.getBlockState(pos);
				return isLeave.apply(state);
			}
			return false;
		}
		
		@Override
		public boolean canContinue(Level w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent)
		{
			return true;
		}
	}
	
	public static class MushroomSelector implements IBlockSelector
	{
		Predicate<BlockState> isShroom;
		
		public MushroomSelector()
		{
			isShroom = getMushroomPredicate();
		}
		
		@Override
		public boolean isValidBlock(Level w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent)
		{
			BlockState state = w.getBlockState(pos);
			return isShroom.apply(state);
		}

		@Override
		public boolean canContinue(Level w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent)
		{
			return true;
		}
	}
	
	/**
	 * Thius selects and creats a falling tree
	 * 
	 * @param w
	 * @param begin
	 * @param face
	 * @return
	 */
	public static TileEntityFallingTree selectTree(Level w, BlockPos begin, Direction face)
	{
		if(face== Direction.UP || face== Direction.DOWN)
			face = Direction.NORTH;
			
		long time = System.nanoTime();
		
		BlockState state = w.getBlockState(begin);
		boolean mushroomFlag = getMushroomPredicate().apply(state);
		IBlockSelector log;
		if(mushroomFlag)
		{
			log = new MushroomSelector();
		}
		else
		{
			log = new LogSelector(false);
		}
		
		FPBlockSelector sel = new FPBlockSelector(w, log);
		sel.selectBlocks(begin);
		Collection<ParentCoords> col = sel.getAllBlocks();	
		
		if(!mushroomFlag)
		{
			HashMap<BlockPos, Boolean> leaves = new HashMap<BlockPos, Boolean>(); 
			int s = 5;
			for(ParentCoords pc : col)
			{
				for(int x=-s;x<=s;x++)
				{
					for(int y=-s;y<=s;y++)
					{
						for(int z=-s;z<=s;z++)
						{
							if(Math.abs(x) + Math.abs(y) + Math.abs(z) <= s)
							{
								BlockPos pos = pc.offset(x,y,z);
								
								if(x==0 && y==0 && z==0)
								{
									leaves.put(pos, false);
									continue;
								}				
								if(leaves.get(pos)!=null)
									continue;
								leaves.put(pos, true);	
							}
						}
					}
				}
			}
			sel = new FPBlockSelector(w, new LeavesSelector(leaves));
			sel.selectBlocks(begin);
			col = sel.getAllBlocks();	
		}
		
		long time2 = System.nanoTime();
		
		MiniWorld world = copyFromWorld((WorldGenLevel) w, col);
		world.face = face;
		world.rotationpoint = Vec3.atLowerCornerOf(begin).subtract(Vec3.atLowerCornerOf(world.start)).add(0.5, 0.5, 0.5);
		
		Direction fall = face.getCounterClockWise();
		
		final ArrayList<ItemStack>[][] drops = new ArrayList[face.getAxis()==Axis.Z?world.depth : world.width][world.height];	
		BlockState air = Blocks.AIR.defaultBlockState();
		
		for(BlockPos pos : col)
		{
			final int i1=face.getAxis()==Axis.Z ? pos.getZ()-world.start.getZ() : pos.getX()-world.start.getX();
			final int i2 = pos.getY()-world.start.getY();
			
			if(drops[i1][i2]==null)
				drops[i1][i2]=new ArrayList<ItemStack>();
			
			state = w.getBlockState(pos);
			Block.dropResources(state, w, pos);
			w.setBlock(pos, air, 2);
			
			List<ItemEntity> items = w.getEntitiesOfClass(ItemEntity.class, new AABB(pos), ItemEntity::isAlive);
			for(ItemEntity input : items)
			{
				if(!input.getItem().isEmpty())
					drops[i1][i2].add(input.getItem());
				input.discard();
			}
			
		}
		updateAllBlockAround(w, col);
		
		//System.out.println("Raplcing to air tock: " + (System.currentTimeMillis()-time2));
		
		
		
		state = MiscBlocks.falling_tree.defaultBlockState();
		w.setBlockAndUpdate(begin, state);
		TileEntityFallingTree tree = (TileEntityFallingTree) w.getBlockEntity(begin);
		tree.setMiniWorld(world);
		tree.drops = drops;
		tree.fall = fall;
		tree.setChanged();
		
		time = time2 - time;
		time2 = System.nanoTime() - time2;
		if(time+time2 > 40e6)
		{
			System.out.println("TreeUtils.selectTree() " + ((time+time2) / 1.0e6D)  + " ms (Select:" + (time/1.0e6D) + "ms; Remove Blocks:" + (time2/1.0e6D) + "ms)");
		}
		
		
		return tree;
	}
	
	private static void updateAllBlockAround(Level w, Collection<ParentCoords> col)
	{
		if(!w.isClientSide)
		{
			ServerLevel server = (ServerLevel) w;
			Thread t= new Thread(new Runnable()
			{	
				@Override
				public void run()
				{
					try
					{
						Thread.sleep(200);
						for(BlockPos pos : col)
						{							
							server.getServer().submitAsync(() -> server.updateNeighborsAt(pos, Blocks.AIR));
							Thread.sleep(20);
						}
					} 
					catch (InterruptedException e) {e.printStackTrace();}
				}
			});
			t.setDaemon(true);
			t.setName("FallingTreeBlockNotifier");
			t.start();
		}
	}
	
 	public static<T extends BlockPos> MiniWorld copyFromWorld(WorldGenLevel original, Collection<T> cord)
	{
		int minX,minY,minZ;
		int maxX,maxY,maxZ;
			
		minX=minY=minZ=Integer.MAX_VALUE;
		maxX=maxY=maxZ=Integer.MIN_VALUE;
		
		for(BlockPos pos : cord)
		{
			if(pos.getX()<minX)
				minX=pos.getX();
			if(pos.getX()>maxX)
				maxX=pos.getX();
			if(pos.getY()<minY)
				minY=pos.getY();
			if(pos.getY()>maxY)
				maxY=pos.getY();
			if(pos.getZ()<minZ)
				minZ=pos.getZ();
			if(pos.getZ()>maxZ)
				maxZ=pos.getZ();
		}
		
		MiniWorld mini = new MiniWorld(new BlockPos(minX,minY,minZ), new BlockPos(maxX-minX+1,maxY-minY+1,maxZ-minZ+1), original.getSeed());
		mini.setWorld((Level) original);
		
		
		Set<BlockPos> lightPos = new HashSet<>(cord.size() * 2);
		lightPos.addAll(cord);
		
		for(BlockPos pos : cord)
		{
			mini.setObject(mini.states, pos, original.getBlockState(pos));
			mini.setObject(mini.tiles, pos, original.getBlockEntity(pos));
			mini.setObject(mini.bioms, pos, original.getBiome(pos));
			
			Integer[] red = new Integer[6];
			
			for(int i=0;i<red.length;i++)
			{
				Direction d = Direction.from3DDataValue(i);
				red[i] = original.getDirectSignal(pos, d);
				
				
				lightPos.add(pos.relative(d));
			}
			mini.setObject(mini.redstone, pos, red);
		}
		
		for(BlockPos pos : BlockPos.betweenClosed(minX-1, minY-1, minZ-1, maxX+1, maxY+1, maxZ+1))
		{
			mini.setObjectSafe(mini.skylight, pos,  original.getBrightness(LightLayer.SKY, pos));
			mini.setObjectSafe(mini.blocklight, pos, original.getBrightness(LightLayer.BLOCK, pos));
		}
		
		final BlockState state_air = Blocks.VOID_AIR.defaultBlockState();
		(BlockPos.betweenClosedStream(new BlockPos(minX,minY,minZ), new BlockPos(maxX+1,maxY+1,maxZ+1))).forEach(pos -> 
		{
			if(mini.getBlockState(pos)==null)
			{
				mini.setObject(mini.states, pos, state_air);
				mini.setObject(mini.bioms, pos, original.getBiome(pos));
				
				Integer[] red = new Integer[6];
				for(int i=0;i<red.length;i++)
				{
					red[i] = original.getDirectSignal(pos, Direction.from3DDataValue(i));
				}
				mini.setObject(mini.redstone, pos, red);
				
//				mini.setObject(mini.skylight, pos,  original.getBrightness(LightType.SKY, pos));
//				mini.setObject(mini.blocklight, pos, original.getBrightness(LightType.BLOCK, pos));
			}			
		});
		return mini;
	}

	public static Predicate<BlockState> getLogPredicate()
	{
		return state -> state.is(LOG_WOOD);
	}
	
	public static Predicate<BlockState> getLeavesPredicate()
	{
		return state -> state.is(TREE_LEAVES);
	}
	
	public static Predicate<BlockState> getMushroomPredicate()
	{
		return state -> state.is(HUGE_MUSHROOM);
	}
	
	/**
	 * This function is used by the EntityForstmaster to destroy all wood blocks befor the tree falls
	 * 
	 * @param w
	 * @param start
	 * @return
	 */
	public static BlockPos getFarWoodInHeight(Level w, final BlockPos start)
	{
		LogSelector log = new LogSelector(true);//TODO: do test for checking if this works
		FPBlockSelector sel = FPSelectorHelper.getSelector(w, start, log); //no TIleEntity so can be offthread
		sel.selectBlocks(start);
		Collection<ParentCoords> col = sel.getAllBlocks();	
		ArrayList<ParentCoords> list = new ArrayList<ParentCoords>(col);
		list.remove(start);
		if(list.isEmpty())
		{
			return start;
		}
		list.sort(new Comparator<ParentCoords>()
		{
			@Override
			public int compare(ParentCoords o1, ParentCoords o2)
			{
				return Mth.floor(start.distSqr(o2) - start.distSqr(o1));
			}
			
		});
		
		return list.get(0);
	}
}
