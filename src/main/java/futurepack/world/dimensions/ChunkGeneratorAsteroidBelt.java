package futurepack.world.dimensions;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.function.Consumer;

import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.WorldGenRegion;
import net.minecraft.world.level.LevelHeightAccessor;
import net.minecraft.world.level.NoiseColumn;
import net.minecraft.world.level.StructureFeatureManager;
import net.minecraft.world.level.biome.BiomeSource;
import net.minecraft.world.level.biome.FixedBiomeSource;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.Heightmap.Types;

public class ChunkGeneratorAsteroidBelt extends ChunkGenerator 
{
	public static final Codec<ChunkGeneratorAsteroidBelt> CODEC = AsteroidBeltSettings.CODEC.fieldOf("settings").xmap(ChunkGeneratorAsteroidBelt::new, ChunkGeneratorAsteroidBelt::settings).codec();

	private AsteroidBeltSettings settings;
	
	public ChunkGeneratorAsteroidBelt(BiomeSource p_i231887_1_, BiomeSource p_i231887_2_, AsteroidBeltSettings setting, long seed) 
	{
		super(p_i231887_1_, p_i231887_2_, setting.getSettings(), seed);
		this.settings = setting;
	}
	
	public ChunkGeneratorAsteroidBelt(AsteroidBeltSettings setting) 
	{
		this(new FixedBiomeSource(setting.getBiomeSupplier()), new FixedBiomeSource(setting.getBiomeSupplier()), setting, 0L);
	}

	@Override
	protected Codec<ChunkGeneratorAsteroidBelt> codec() 
	{
		return CODEC;
	}

	public AsteroidBeltSettings settings() 
	{
	      return this.settings;
	   }
	
	@Override
	public ChunkGenerator withSeed(long seed) 
	{
		return new ChunkGeneratorAsteroidBelt(biomeSource, runtimeBiomeSource, settings.withSeed(seed), seed);
	}

	@Override
	public void buildSurfaceAndBedrock(WorldGenRegion p_225551_1_, ChunkAccess chunk) 
	{
//		BlockState bedrock = Blocks.BEDROCK.getDefaultState();
//		BlockPos.Mutable mut = new BlockPos.Mutable();
//		for(int x=0;x<16;x++)
//		{
//			for(int z=0;z<16;z++)
//			{
//				mut.setPos(x, 0, z);
//				chunk.setBlockState(mut, bedrock, false);
//			}
//		}
	}

	//set blocks in chunk
	@Override
	public CompletableFuture<ChunkAccess> fillFromNoise(Executor pExecutor, StructureFeatureManager pStructureFeatureManager, ChunkAccess chunk) 
	{
		BlockPos.MutableBlockPos mut = new BlockPos.MutableBlockPos();
		Heightmap heightmap = chunk.getOrCreateHeightmapUnprimed(Heightmap.Types.OCEAN_FLOOR_WG);
		Heightmap heightmap1 = chunk.getOrCreateHeightmapUnprimed(Heightmap.Types.WORLD_SURFACE_WG);
	    BlockState stone = Blocks.STONE.defaultBlockState();
		
		for(int x=0;x<16;x++)
		{
			for(int z=0;z<16;z++)
			{
				
				generatePillarAt(chunk.getPos().x*16 +x, chunk.getPos().z*16 +z, mut, p -> {
					chunk.setBlockState(p, stone, false);
					heightmap .update(p.getX() & 15, p.getY(), p.getZ() & 15, stone);
					heightmap1.update(p.getX() & 15, p.getY(), p.getZ() & 15, stone);
				});
			}
		}
		
		return CompletableFuture.completedFuture(chunk);
	}

	private void generatePillarAt(int x, int z, BlockPos.MutableBlockPos mut, Consumer<BlockPos.MutableBlockPos> callback)
	{
		settings.getAsteroidLayers().forEach(l -> l.generatePillarAt(x, z, mut, callback));
	}
	
	
	
	@Override
	public int getBaseHeight(int x, int z, Types heightmapType, LevelHeightAccessor w) 
	{
		BlockState[] states = new BlockState[w.getHeight()];
		generatePillarAt(x, z, new BlockPos.MutableBlockPos(), p -> {
			states[p.getY()] = Blocks.STONE.defaultBlockState();
		});
		BlockState air = Blocks.AIR.defaultBlockState();
		for(int i=states.length;i>=0;i--)
		{
			if(heightmapType.isOpaque().test(states[i]==null?air:states[i]))
			{
				return i;
			}
		}
		
		return 0;
	}

	//basicly a horizontal block list ?
	@Override
	public NoiseColumn getBaseColumn(int x, int z, LevelHeightAccessor w)
	{
		BlockState[] states = new BlockState[w.getHeight()];
		generatePillarAt(x, z, new BlockPos.MutableBlockPos(), p -> {
			states[p.getY()] = Blocks.STONE.defaultBlockState();
		});
		BlockState air = Blocks.AIR.defaultBlockState();
		for(int i=0;i<states.length;i++)
		{
			if(states[i]==null)
			{
				states[i] = air;
			}
		}
		return new NoiseColumn(w.getMinBuildHeight(), states);
	}

}
