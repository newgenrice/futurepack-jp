//package futurepack.world.dimensions;
//
//import net.minecraft.nbt.CompoundNBT;
//import net.minecraft.util.math.BlockPos;
//import net.minecraft.world.Difficulty;
//import net.minecraft.world.DimensionType;
//import net.minecraft.world.World;
//import net.minecraft.world.WorldType;
//import net.minecraft.world.storage.DerivedWorldInfo;
//import net.minecraft.world.storage.WorldInfo;
//
//public class CustomWorldInfo extends DerivedWorldInfo
//{
////	private static Field field;
////	
////	static
////	{
////		Class cls = World.class;
////		for(Field ff : cls.getDeclaredFields())
////		{
////			if(ff.getType() == WorldInfo.class)
////			{
////				field = ff;
////				field.setAccessible(true);
////				break;
////			}
////		}
////	}
//	
//	public static void overwriteWorld(World w, DimensionType id)
//	{
//		if(true)
//			return; //for now no custom world info
//		
//		if(!w.isRemote)
//		{
////			ServerWorld server = (ServerWorld)w;
//			if(w!=null)
//			{
//				WorldInfo newInfo = new CustomWorldInfo(w.getWorldInfo(), id);
////				try 
////				{
////					field.set(w, newInfo);
//				
//				
//					w.worldInfo = newInfo;
//				
//				//TODO: check if custom time for tyros & menelaus works
//				
//				
//				
//				
////				}
////				catch (IllegalArgumentException e) {
////					e.printStackTrace();
////				} catch (IllegalAccessException e) {
////					e.printStackTrace();
////				}
//			}
//		}
//	}
//	
//	private WorldInfo info;
//	private final DimensionType id;
//	
//	public CustomWorldInfo(WorldInfo worldInfoIn, DimensionType id)
//	{
//		super(worldInfoIn);
//		info = worldInfoIn;
//		if(id==null)
//			throw new NullPointerException("DimensionType is null");
//		this.id = id;
//	}
//	
//	@Override
//	public void setDayTime(long time)
//	{
//		CompoundNBT nbt = getNBT();
//		nbt.putLong("time", time);
//	}
//	
//	@Override
//	public void setGameTime(long time)
//	{
//		CompoundNBT nbt = getNBT();
//		nbt.putLong("totalTime", time);
//	}
//	 
//	@Override
//	public long getGameTime()
//	{
//		CompoundNBT nbt = getNBT();
//		if(nbt.contains("totalTime"))
//		{
//			return nbt.getLong("totalTime");
//		}
//		else
//		{
//			long time = super.getGameTime();
//			nbt.putLong("totalTime", time);
//			return time;
//		}
//	}
//
//	@Override
//	public long getDayTime()
//	{
//		CompoundNBT nbt = getNBT();
//		if(nbt.contains("time"))
//		{
//			return nbt.getLong("time");
//		}
//		else
//		{
//			long time = super.getDayTime();
//			nbt.putLong("time", time);
//			return time;
//		}
//	}
//	
//	private CompoundNBT getNBT()
//	{
////		return info.getDimensionData(id); TODO. when the forge bug is fixed (worldinfo is read befor dimension types are registered, this will get fixed)
//		CompoundNBT nbt = info.getDimensionData(DimensionType.OVERWORLD);
//		CompoundNBT custom 
//		;
//		if(nbt.contains(DimensionType.getKey(id).toString()))
//		{
//			custom = nbt.getCompound(DimensionType.getKey(id).toString());
//		}
//		else
//		{
//			custom = new CompoundNBT();
//			nbt.put(DimensionType.getKey(id).toString(), custom);
//		}
//		
//		info.setDimensionData(DimensionType.OVERWORLD, nbt);
//		return custom;
//		
////		return new NBTTagCompound();
//	}
//	
//
//	@Override
//	public void setSpawn(BlockPos spawnPoint) 
//	{
//		getNBT().putInt("spawnX", spawnPoint.getX());
//		getNBT().putInt("spawnY", spawnPoint.getX());
//		getNBT().putInt("spawnZ", spawnPoint.getX());
//	}
//
//	@Override
//	public int getSpawnX() 
//	{
//		if(getNBT().contains("spawnX"))
//			return getNBT().getInt("spawnX");
//		
//		return super.getSpawnX();
//	}
//	
//	@Override
//	public int getSpawnY() 
//	{
//		if(getNBT().contains("spawnY"))
//			return getNBT().getInt("spawnY");
//		
//		return super.getSpawnY();
//	}
//	
//	@Override
//	public int getSpawnZ()
//	{
//		if(getNBT().contains("spawnZ"))
//			return getNBT().getInt("spawnZ");
//		
//		return super.getSpawnZ();
//	}
//	
//	@Override
//	public void setWorldName(String worldName) 
//	{
//		info.setWorldName(worldName);
//	}
//
//	@Override
//	public void setSaveVersion(int version)
//	{
//		info.setSaveVersion(version);
//	}
//
//	@Override
//	public void setThundering(boolean thunderingIn)
//	{
//		info.setThundering(thunderingIn);
//	}
//
//	@Override
//	public void setThunderTime(int time) 
//	{
//		info.setThunderTime(time);
//	}
//
//	@Override
//	public void setRaining(boolean isRaining) 
//	{
//		info.setRaining(isRaining);
//	}
//
//	@Override
//	public void setRainTime(int time) 
//	{
//		info.setRainTime(time);
//	}
//	   
//	@Override
//	public void setGenerator(WorldType type) 
//	{
//		info.setGenerator(type);
//	}
//	   
//	@Override
//	public void setAllowCommands(boolean allow) 
//	{
//		info.setAllowCommands(allow);
//	}
//	   
//	@Override
//	public void setInitialized(boolean initializedIn) 
//	{
//		info.setInitialized(initializedIn);
//	}
//	   
//	@Override
//	public void setDifficulty(Difficulty newDifficulty) 
//	{
//		info.setDifficulty(newDifficulty);
//	}
//
//	@Override
//	public void setDifficultyLocked(boolean locked) 
//	{
//		info.setDifficultyLocked(locked);
//	}
//
//}
