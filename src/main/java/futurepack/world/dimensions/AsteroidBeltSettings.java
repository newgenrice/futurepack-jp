package futurepack.world.dimensions;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import com.google.common.collect.Lists;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import futurepack.common.FPLog;
import net.minecraft.core.Registry;
import net.minecraft.resources.RegistryLookupCodec;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.levelgen.StructureSettings;

public class AsteroidBeltSettings
{
	public static final Codec<AsteroidBeltSettings> CODEC = RecordCodecBuilder.<AsteroidBeltSettings>create((p_236938_0_) -> 
	{
		return p_236938_0_.group(RegistryLookupCodec.create(Registry.BIOME_REGISTRY).forGetter((p_242874_0_) -> 
		{
			return p_242874_0_.biome_registry;
		}),
				StructureSettings.CODEC.fieldOf("structures").forGetter(AsteroidBeltSettings::getSettings),
				AsteroidLayer.CODEC.listOf().fieldOf("asteroids").forGetter(AsteroidBeltSettings::getAsteroidLayers),
				Biome.CODEC.optionalFieldOf("biome").orElseGet(Optional::empty).forGetter((set) -> {
					return Optional.of(set.biomeToUse);
				}))
				.apply(p_236938_0_, AsteroidBeltSettings::new);
	   }).stable();
	
	private final Registry<Biome> biome_registry;
	private final StructureSettings settings;
	private final List<AsteroidLayer> asteroidLayers = Lists.newArrayList();
	private Supplier<Biome> biomeToUse;
	
	   
	public AsteroidBeltSettings(Registry<Biome> p_i242012_1_, StructureSettings p_i242012_2_, List<AsteroidLayer> p_i242012_3_, Optional<Supplier<Biome>> p_i242012_6_) 
	{
		this(p_i242012_2_, p_i242012_1_);


		this.asteroidLayers.addAll(p_i242012_3_);
//		this.updateLayers();
		if (!p_i242012_6_.isPresent()) {
			FPLog.logger.error("Unknown biome, defaulting to plains");
			this.biomeToUse = () -> {
				return p_i242012_1_.getOrThrow(Biomes.PLAINS);
			};
		} else {
			this.biomeToUse = p_i242012_6_.get();
		}

	}	
	
	public AsteroidBeltSettings withSeed(long seed)
	{
		ArrayList<AsteroidLayer> layer = new ArrayList<>(asteroidLayers.size());
		for(AsteroidLayer old : asteroidLayers)
		{
			layer.add(old.withSeed(seed));
		}
		return new AsteroidBeltSettings(biome_registry, settings, layer, Optional.of(biomeToUse));
	}
	
	public AsteroidBeltSettings(StructureSettings p_i242011_1_, Registry<Biome> p_i242011_2_)
	{
		this.biome_registry = p_i242011_2_;
		this.settings = p_i242011_1_;
		this.biomeToUse = () -> {
			return p_i242011_2_.getOrThrow(Biomes.PLAINS);
		};
	}
	
	public StructureSettings getSettings() 
	{
		return this.settings;
	}
	
	public List<AsteroidLayer> getAsteroidLayers() 
	{
		return this.asteroidLayers;
	}
	
	public Supplier<Biome> getBiomeSupplier()
	{
		return biomeToUse;
	}
}
