package futurepack.api.interfaces;

import futurepack.api.PacketBase;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;

/**
 * This is needed for everything that sends a {@link futurepack.api.PacketBase}
 * See {@link futurepack.common.network.NetworkManager}
 */
public interface INetworkUser
{
	/**
	 * The World of the sender.
	 */
	public Level getSenderWorld();
	
	/**
	 * the Position of the sender.
	 */
	public BlockPos getSenderPosition();
	
	/**
	 * This is called after the Packet is sent.
	 * @param pkt the Packet after everyone has received it.
	 */
	public void postPacketSend(PacketBase pkt);
}
