package futurepack.api.interfaces;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.Callable;

import javax.annotation.Nonnull;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.BlockHitResult;

/**
 * This interface is used to add custom information to the gui of the Escanner, after scanning something.
 * It is registered in {@link futurepack.common.research.ScanningManager} (API method will follow)
 * Register in {@link futurepack.api.FPApiMain#registerScanPart(futurepack.api.EnumScanPosition, IScanPart)}
 */
public interface IScanPart
{
	/**
	 * Called if a Block is scanned. Return null if nothing should added or a chat component
	 * 
	 * @param w the World
	 * @param pos the Postiion
	 * @param inGUI if true its in the EScanner, false in the normal chat. (use it for different colors -> better readable)
	 * @param res 
	 * @return the ChatComponent
	 */
	@Deprecated
	public Component doBlock(Level w, BlockPos pos, boolean inGUI, BlockHitResult res);
	
	/**
	 * Called if an Entity is scanned. Return null if nothing should added or a chat component
	 * 
	 * @param w the World
	 * @param e the Entity
	 * @param inGUI if true its in the EScanner, false in the normal chat.(use it for different colors -> better readable)
	 * @return the ChatComponent
	 */
	@Deprecated
	public Component doEntity(Level w, LivingEntity e, boolean inGUI);

	/**
	 * Called if a Block is scanned. Return null if nothing should added or a chat component
	 * 
	 * @param w the World
	 * @param pos the Postiion
	 * @param inGUI if true its in the EScanner, false in the normal chat. (use it for different colors -> better readable)
	 * @param res 
	 * @return the ChatComponents
	 */
	@Nonnull
	public default Callable<Collection<Component>> doBlockMulti(Level w, BlockPos pos, boolean inGUI, BlockHitResult res)
	{
		return () -> Collections.singletonList(doBlock(w, pos, inGUI, res));
	}
	
	/**
	 * Called if an Entity is scanned. Return null if nothing should added or a chat component
	 * 
	 * @param w the World
	 * @param e the Entity
	 * @param inGUI if true its in the EScanner, false in the normal chat.(use it for different colors -> better readable)
	 * @return the ChatComponents
	 */
	@Nonnull
	public default Callable<Collection<Component>> doEntityMulti(Level w, LivingEntity e, boolean inGUI)
	{
		return () -> Collections.singletonList(doEntity(w, e, inGUI));
	}
	
}
