package futurepack.generating;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ItemDef extends RegistryDef
{
	Model inventory;
	
	public ItemDef(String block_class, String args, String registry_name) 
	{
		super("Item", block_class, args, registry_name);
	}

	public static ItemDef fromJson(String s)
	{
		return Blocks.gson.fromJson(s, ItemDef.class);
	}
	
	public static ItemDef jsonToItemDef(JsonElement json, Type typeOfT, JsonDeserializationContext context)
	{
		JsonObject obj = json.getAsJsonObject();
		
		ItemDef def = new ItemDef(obj.get("class").getAsString(), obj.get("args").getAsString(), obj.get("registry").getAsString());
		def.inventory = new Model(obj.get("model"), context);
//		def.inventory.registry_name = def.registry_name;
		
		return def;
	}
	
	public static void writeItemDefs(ClassBuilder build, ItemDef[] defs)
	{
		RegistryDef.writeRegistryDefs(build, defs, "Item");
	}
	
	public static void writeItemModels(ItemDef[] defs, File resOutput) throws IOException
	{
		
		
		for(ItemDef def : defs)
		{
			if(def.inventory != null)
			{
				String[] a =  def.registry_name.split(":", 2);
				String modId = a[0];
				String path = a[1];
				
				File modelsDir = new File(resOutput, "assets" + File.separator + modId + File.separator +  "models" +  File.separator + "item");
				if(!modelsDir.exists())
					modelsDir.mkdirs();
				
				File out2 = new File(modelsDir,  path+".json");
				System.out.println(out2);
				FileWriter writer2 = new FileWriter(out2);
				Blocks.gson_out.toJson(def.inventory.asJson(), Blocks.gson_out.newJsonWriter(writer2));
				writer2.close();
			}
		}
	}
}
