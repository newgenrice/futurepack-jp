package futurepack.generating;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

public class BlockBuilder extends BuilderBase<BlockDef>
{	
	public String ITEM = "net.minecraft.world.item.Item";
	public String BLOCK_ITEM = "net.minecraft.world.item.BlockItem";
	public String MATERIAL = "net.minecraft.world.level.material.Material";
	public String BLOCK = "net.minecraft.world.level.block.Block";
	
	private BlockBuilder(File srcOut, File resOut)
	{
		super(srcOut, resOut);
		imports.add(MATERIAL);
		imports.add(BLOCK);
	}
	
	public static BlockBuilder create(File srcOut, File resOut)
	{
		return new BlockBuilder(srcOut, resOut);
	}
	
	public void addBlock(BlockDef def)
	{
		super.add(def);
	}
	
	public void addBlock(String className, String registry_name, String texture, String args)
	{
		BlockDef def = new BlockDef(className, args, registry_name);
		def.model = new ModelDefinition();
		def.model.registry_name = def.registry_name;
		HashMap<String, String> tex = new HashMap<>();
		tex.put("all", texture);
		def.model.addModel(def.registry_name+"_model", "block/cube_all", tex);
		
		this.defs.add(def);
	}	
	
	@Override
	public void buildExtraBody(ClassBuilder build, String packageName, String className, String group)
	{
		BlockDef[] def = defs.toArray(new BlockDef[defs.size()]);
		BlockDef.writeBlockDefs(build, def);
		
		build.imports.add(ITEM);
		build.imports.add(BLOCK_ITEM);
		build.methods.add(w -> {
			String s = "	public static void registerItems(RegistryEvent.Register<Item> event)\r\n" + 
						"	{\r\n" + 
						"		IForgeRegistry<Item> r = event.getRegistry();\r\n" + 
						"		\r\n";
			try
			{
				w.write(s);
				for(BlockDef d : defs)
					w.append(String.format("\t\tr.register(new BlockItem(%s, (new Item.Properties()).tab(%s)).setRegistryName(\"%s\"));\r\n", d.getVariableName(), group, d.registry_name));
				w.write("	}\r\n\r\n");
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		});
		
		Arrays.stream(def).map(d -> d.model).forEach(m -> 
		{
			try {
				m.createModels(resOut);
			}
			catch (IOException e1) {
				e1.printStackTrace();
			}
		});
		Arrays.stream(def).forEach(m -> 
		{
			try {
				m.createLoottables(resOut);
			}
			catch (IOException e1) {
				e1.printStackTrace();
			}
		});
	}
}
