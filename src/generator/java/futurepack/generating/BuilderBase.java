package futurepack.generating;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class BuilderBase<T> 
{
	protected final File srcOut, resOut;

	
	protected ArrayList<T> defs = new ArrayList<T>();
	protected Set<String> imports;
	protected ArrayList<AbstractVarDefinition> vars = new ArrayList<AbstractVarDefinition>();
	
	protected BuilderBase(File srcOut, File resOut)
	{
		super();
		this.srcOut = srcOut;
		this.resOut = resOut;
		imports = new HashSet<String>();
	}


	public void add(T def)
	{
		this.defs.add(def);
	}
	
	public void addImport(String s)
	{
		this.imports.add(s);
	}
	
	public void addVar(String s)
	{
		vars.add(AbstractVarDefinition.create(s));
	}
	
	public List<T> getDefs()
	{
		return this.defs;
	}
	
	
	public abstract void buildExtraBody(ClassBuilder build, String packageName, String className, String group);
	
	public void build(String packageName, String className, String group)
	{
		ClassBuilder build = new ClassBuilder(packageName, className);
		build.imports.addAll(this.imports);
		
		build.methods.add(w -> {
			vars.stream().forEach(t -> 
			{
				try
				{
					w.write(t.toString());
					w.flush();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			});
		});
		
		buildExtraBody(build, packageName, className, group);
		
		
		FileWriter writer;
		try
		{
			File out = build.getFile(srcOut);
			File parent = out.getParentFile();
			if(!parent.exists())
				parent.mkdirs();
			
			writer = new FileWriter(out);
			build.build(writer);
			writer.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
