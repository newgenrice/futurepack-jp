echo Starting setup $build_id
JAVA_HOME="C:\\Program Files\\Java\\jdk-16.0.2\\"""
export vmessage="Build Started $build_id"
curl -s --data "content=$vmessage" $discord
./gradlew printVersion
export lastCommit="$(git log -1 --format="%at" -- )"
export lastEdit1="$(git log -1 --format="%at" -- build.gradle)"
export lastEdit2="$(git log -1 --format="%at" -- gradle.properties)"
export lastEdit3="$(git log -1 --format="%at" -- src/main/resources/META-INF/accesstransformer.cfg)"
export timediff1="$(expr $lastCommit - $lastEdit1)"
export timediff2="$(expr $lastCommit - $lastEdit2)"
export timediff3="$(expr $lastCommit - $lastEdit3)"
#edit in the last 30 minutes
if (( $timediff1 < 1800 )) || (( $timediff2 < 1800 )) || (( $timediff3 < 1800 )); then
	./setup.sh
fi
export vmessage="[$build_id] Starting Server"
curl -s --data "content=$vmessage" $discord
./gradlew runServer &
#server test
PID=$!
sleep 300
kill $PID
export vmessage="[$build_id] Build jar"
curl -s --data "content=$vmessage" $discord
# build
mkdir -p build
cd build
rm -rf libs/*
cd ..
./gradlew build signJar verify publish
sleep 10
cd build/libs
rm Futurepack*source.jar
rm Futurepack*api.jar
fpfile=$(dir)
curl -s -F "pw=$PW" -F "action=Build" -F "type=Latest" -F "id=$build_id" -F "jar=@${fpfile}" http://new.redsnake-games.de/Futurepack/BuildAPI.php
export vmessage="[$build_id] Done"
curl -s --data "content=$vmessage" $discord
