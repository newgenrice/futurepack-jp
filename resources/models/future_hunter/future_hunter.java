// Made with Blockbench 4.0.0-beta.3
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


public class future_hunter<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "future_hunter"), "main");
	private final ModelPart drone;

	public future_hunter(ModelPart root) {
		this.drone = root.getChild("drone");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition drone = partdefinition.addOrReplaceChild("drone", CubeListBuilder.create().texOffs(0, 0).addBox(-6.0F, -7.0F, -6.0F, 12.0F, 12.0F, 12.0F, new CubeDeformation(0.0F))
		.texOffs(73, 0).addBox(-1.0F, -8.0F, -3.0F, 8.0F, 10.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 11.0F, 0.0F));

		PartDefinition thruster = drone.addOrReplaceChild("thruster", CubeListBuilder.create().texOffs(0, 25).addBox(-4.0F, -3.0F, 4.5F, 8.0F, 6.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(0, 25).addBox(-4.0F, -3.0F, -10.5F, 8.0F, 6.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(4.0F, -6.0F, 1.5F));

		PartDefinition neck = drone.addOrReplaceChild("neck", CubeListBuilder.create(), PartPose.offsetAndRotation(-5.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F));

		PartDefinition neck_r1 = neck.addOrReplaceChild("neck_r1", CubeListBuilder.create().texOffs(48, 0).addBox(-6.0F, -2.0F, -2.0F, 8.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.7558F));

		PartDefinition head = neck.addOrReplaceChild("head", CubeListBuilder.create(), PartPose.offset(-3.5F, 4.0F, 0.0F));

		PartDefinition head_r1 = head.addOrReplaceChild("head_r1", CubeListBuilder.create().texOffs(48, 8).addBox(-2.5F, -2.5F, -2.5F, 5.0F, 5.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -3.098F));

		PartDefinition arm_2 = drone.addOrReplaceChild("arm_2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 4.0F, -4.0F, 0.0F, 0.0F, 0.0F));

		PartDefinition arm_2_r1 = arm_2.addOrReplaceChild("arm_2_r1", CubeListBuilder.create().texOffs(0, 34).addBox(-2.0F, -2.0F, -6.0F, 4.0F, 4.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.7854F, 0.0F, 0.0F));

		PartDefinition laser = arm_2.addOrReplaceChild("laser", CubeListBuilder.create().texOffs(20, 34).addBox(-4.0F, -1.0F, -2.0F, 7.0F, 4.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(0, 44).addBox(-2.5F, -1.5F, -2.5F, 5.0F, 5.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(42, 34).addBox(-10.0F, -0.5F, -1.5F, 7.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 4.0F, -5.0F));

		PartDefinition arm_1 = drone.addOrReplaceChild("arm_1", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 4.0F, 4.0F, 0.0F, 0.0F, 0.0F));

		PartDefinition arm_1_r1 = arm_1.addOrReplaceChild("arm_1_r1", CubeListBuilder.create().texOffs(0, 34).addBox(-2.0F, -2.0F, -6.0F, 4.0F, 4.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 2.3562F, 0.0F, 0.0F));

		PartDefinition rocket_launcher = arm_1.addOrReplaceChild("rocket_launcher", CubeListBuilder.create().texOffs(20, 44).addBox(-4.5F, -2.5F, -2.5F, 9.0F, 7.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 4.0F, 5.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		drone.render(poseStack, buffer, packedLight, packedOverlay);
	}
}