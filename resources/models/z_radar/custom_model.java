// Made with Blockbench 4.0.5
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


public class custom_model<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "custom_model"), "main");
	private final ModelPart Radar;

	public custom_model(ModelPart root) {
		this.Radar = root.getChild("Radar");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition Radar = partdefinition.addOrReplaceChild("Radar", CubeListBuilder.create().texOffs(0, 0).addBox(-8.0F, -6.0F, -8.0F, 16.0F, 6.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(49, 0).addBox(-4.0F, -7.0F, -4.0F, 8.0F, 1.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition Spiegel = Radar.addOrReplaceChild("Spiegel", CubeListBuilder.create().texOffs(64, 15).addBox(-2.5F, -2.0F, -2.5F, 5.0F, 2.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(0, 23).addBox(-1.5F, -3.0F, -4.0F, 3.0F, 2.0F, 10.0F, new CubeDeformation(0.0F))
		.texOffs(17, 24).addBox(-4.0F, -8.0F, 5.5F, 8.0F, 6.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -7.0F, 0.0F));

		PartDefinition boneWest = Spiegel.addOrReplaceChild("boneWest", CubeListBuilder.create(), PartPose.offset(4.0F, -5.0F, 6.0F));

		PartDefinition SpiegelWest_r1 = boneWest.addOrReplaceChild("SpiegelWest_r1", CubeListBuilder.create().texOffs(36, 25).addBox(-2.0F, -7.5F, 6.5F, 6.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-4.0F, 5.0F, -6.0F, 0.0F, 0.7854F, 0.0F));

		PartDefinition boneOst = Spiegel.addOrReplaceChild("boneOst", CubeListBuilder.create(), PartPose.offset(-4.0F, -5.0F, 6.0F));

		PartDefinition SpiegelOst_r1 = boneOst.addOrReplaceChild("SpiegelOst_r1", CubeListBuilder.create().texOffs(51, 25).addBox(-4.0F, -7.5F, 6.5F, 6.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, 5.0F, -6.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition boneParabolEmpf = Spiegel.addOrReplaceChild("boneParabolEmpf", CubeListBuilder.create(), PartPose.offset(0.0F, -2.0F, -4.0F));

		PartDefinition EmpfArm_r1 = boneParabolEmpf.addOrReplaceChild("EmpfArm_r1", CubeListBuilder.create().texOffs(0, 7).addBox(-1.0F, -0.6F, -3.5F, 2.0F, 2.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -0.4F, 0.5F, -0.6109F, 0.0F, 0.0F));

		PartDefinition boneLMB = boneParabolEmpf.addOrReplaceChild("boneLMB", CubeListBuilder.create().texOffs(0, 0).addBox(-1.5F, -2.0F, -1.0F, 3.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(0, 25).addBox(-1.0F, -1.6F, 2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -2.4F, -2.5F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		Radar.render(poseStack, buffer, packedLight, packedOverlay);
	}
}